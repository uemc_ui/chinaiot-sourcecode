#!/bin/bash


filename="/home/pi/dl-1.8-prod/deeplaser-1.8.5/data"
if test -e $filename; then

echo "DL 1.8.5 already installed"

#sleep 45	#to ensure the files are copied as part of updations from qt app

else

echo "DL 1.8.5 not installed... Installing now"

cd /home/pi/Desktop/EdgeFirmware/deeplaser-1.8.5
./install.sh /home/pi/dl-1.8-prod

sleep 50

sudo cp -r /home/pi/dl-1.8-prod/deeplaser-1.8.4/data /home/pi/dl-1.8-prod/deeplaser-1.8.5/

sleep 4

cp -r /home/pi/dl-1.8-prod/deeplaser-1.8.4/conf/com.thermofisher.deeplaser.registration.RegistrationService.config.js /home/pi/dl-1.8-prod/deeplaser-1.8.5/conf/

sleep 2

cp -r /home/pi/dl-1.8-prod/deeplaser-1.8.4/conf/ /home/pi/dl-1.8-prod/deeplaser-1.8.4/conf/com.thermofisher.deeplaser.certificate.CertificateManagerService.config.js /home/pi/dl-1.8-prod/deeplaser-1.8.5/conf/

sleep 2

fi

sudo /bin/rm -rf /home/pi/Desktop/QR/*.PNG

sleep 1



sudo chmod 777 -R /home/pi/Desktop/modbox-webserver
sudo rm -rf /home/pi/Desktop/modbox-webserver

sudo i2cset  -f -y 1 0x51 0x00 0x0

sudo tar -zxvf /home/pi/Desktop/EdgeFirmware/updatefiles.tar.gz -C /

if [ $? -eq 0 ] ; then
	echo "File copied successfully"
	sleep 15
else
	echo "File copy failed !!! Attempt 2"
	sleep 5
	sudo chmod 777 /home/pi/Desktop/EdgeFirmware/updatefiles.tar.gz
	sudo tar -zxvf /home/pi/Desktop/EdgeFirmware/updatefiles.tar.gz -C /

	if [ $? -eq 0 ] ; then
		echo "File copied successfully in attempt 2"
		sleep 15

	else
		echo "File copy failed !!! Attemp 3"
		sleep 5
		sudo chmod 777 /home/pi/Desktop/EdgeFirmware/updatefiles.tar.gz	
		sudo tar -zxvf /home/pi/Desktop/EdgeFirmware/updatefiles.tar.gz -C /
		if [ $? -eq 0 ] ; then
			echo "File copied successfully in attempt 3"
			sleep 15
		else
			echo "File copy failed !!! Attempt 4"
			sleep 5	
			sudo chmod 777 /home/pi/Desktop/EdgeFirmware/updatefiles.tar.gz	
			sudo tar -zxvf /home/pi/Desktop/EdgeFirmware/updatefiles.tar.gz -C /
			if [ $? -eq 0 ] ; then
				echo "File copied successfully in attempt 4"
				sleep 15
			else
				echo "File copy failed !!! Attempt 5"
				sleep 5
				sudo chmod 777 /home/pi/Desktop/EdgeFirmware/updatefiles.tar.gz	
				sudo tar -zxvf /home/pi/Desktop/EdgeFirmware/updatefiles.tar.gz -C /
				if [ $? -eq 0 ] ; then
					echo "File copied successfully in attempt 5"
					sleep 15
				else
					echo "File copied failed after all attempts"
					sleep 5
				fi
			fi					
		fi
	fi
fi

sudo ln /lib/systemd/system/watchdog.service /etc/systemd/system/multi-user.target.wants/watchdog.service

sudo cp /home/pi/Desktop/EdgeFirmware/config.txt /boot/
sleep 1

sudo cp /home/pi/Desktop/EdgeFirmware-old/metadata/* /home/pi/Desktop/EdgeFirmware/metadata/
sleep 5


rsync -av /home/pi/Desktop/EdgeFirmware-old/miscfiles/* /home/pi/Desktop/EdgeFirmware/miscfiles/ --exclude=release-version.txt --exclude=fileupdatedone.txt --exclude=cleanmodbox.txt


sleep 25

sudo reboot -f
