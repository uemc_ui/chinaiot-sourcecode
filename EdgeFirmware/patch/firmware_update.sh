#!/bin/sh
/usr/bin/fbi -T 1 -noverbose -a /home/pi/fwupd-images/fw-upd-progress.gif &
killall rtdapp
killall webserver
/home/pi/Desktop/EdgeFirmware/patch/flashled.sh &
#gpicview  /home/pi/fwupd-images-prog/ --slideshow
rm -rf /home/pi/Desktop/EdgeFirmware-old
rm -rf /home/pi/Desktop/modbox-webserver-old
rm -rf /home/pi/latestfirmware
mkdir /home/pi/latestfirmware/
yes | unzip -d /home/pi/latestfirmware/ /home/pi/LatestFirmware.zip
if [ $? -eq 0 ] ; then
	echo "1.Downloaded LatestFirmware.zip extracted successfully"
	sleep 45
	yes | tar -zxvf /home/pi/latestfirmware/EdgeFirmware.tar.gz -C /home/pi/latestfirmware/
	if [ $? -eq 0 ] ; then
		echo "2.EdgeFirmware tar file extracted successfully"
		sleep 45
		#rsync -av /home/pi/Desktop/EdgeFirmware /home/pi/latestfirmware/current_version/
		#if [ $? -eq 0 ] ; then
		#	echo "3.Copied Existing firmware Successfully into current_version"
			mv /home/pi/Desktop/EdgeFirmware /home/pi/Desktop/EdgeFirmware-old
			if [ $? -eq 0 ] ; then
				echo "4.Backing of existing EdgeFirmware is successful"
				sleep 30
				mv /home/pi/Desktop/modbox-webserver /home/pi/Desktop/modbox-webserver-old
				if [ $? -eq 0 ] ; then
					echo "5.Backing of existing modbox-webserver is successful"
					sleep 30
					rsync -av /home/pi/latestfirmware/EdgeFirmware /home/pi/Desktop/
					if [ $? -eq 0 ] ; then
						echo "6.Latest Firmware Copied Successfully"
						sleep 45
						rsync -av  /home/pi/Desktop/EdgeFirmware-old/metadata/* /home/pi/Desktop/EdgeFirmware/metadata/
						if [ $? -eq 0 ] ; then
							echo "7.Existing metadata retained successfully"
							sleep 30

							rsync -av /home/pi/Desktop/EdgeFirmware-old/miscfiles/* /home/pi/Desktop/EdgeFirmware/miscfiles/ --exclude=release-version.txt --exclude=fileupdatedone.txt --exclude=cleanmodbox.txt
							if [ $? -eq 0 ] ; then
							echo "7.1.Existing miscdata retained successfully"
							sleep 30
								echo 0 > /home/pi/firmwareupdatestatus.txt       #0-update success 1-update failed
							
								chmod 777 -R /home/pi/Desktop/EdgeFirmware
								rsync -av  /home/pi/Desktop/EdgeFirmware/metadata/* /home/pi/Desktop/EdgeFirmware/modbox-webserver/chinaIoT-English/
								rsync -av  /home/pi/Desktop/EdgeFirmware/metadata/* /home/pi/Desktop/EdgeFirmware/modbox-webserver/chinaIoT-Chinese/
								killall -9 fbi
 								/usr/bin/fbi -T 1 -noverbose -a /home/pi/fwupd-images/fw-upd-success.png &
								sleep 5
								killall -9 fbi
								killall rtdapp
								killall webserver
								bash /home/pi/Desktop/EdgeFirmware/patch/modbox_clean.sh 
								#sleep 2
								#sudo reboot -f
							else
								echo "7.1.b.Existing miscdata data failed to retain"
								echo 1 > /home/pi/firmwareupdatestatus.txt       #0-update success 1-update failed
								/usr/bin/fbi -T 1 -noverbose -a /home/pi/fwupd-images/fw-upd-fail.png
								sleep 10
								killall -9 fbi
								sudo reboot -f
							fi
						else
							echo "7b.Existing metadata data failed to retain"
							echo 1 > /home/pi/firmwareupdatestatus.txt       #0-update success 1-update failed
							/usr/bin/fbi -T 1 -noverbose -a /home/pi/fwupd-images/fw-upd-fail.png
							sleep 10
							killall -9 fbi
							sudo reboot -f
						fi
					else
						echo "6b.Latest Firmware Copied Failed... Retaining existing version"
						echo 1 > /home/pi/firmwareupdatestatus.txt	#0-update success 1-update failed
						/usr/bin/fbi -T 1 -noverbose -a /home/pi/fwupd-images/fw-upd-fail.png
						sleep 10
						killall -9 fbi
						sudo reboot -f
					fi
				else
					echo "5b.Backing of existing modbox-webserver failed"
					echo 1 > /home/pi/firmwareupdatestatus.txt       #0-update success 1-update failed
					/usr/bin/fbi -T 1 -noverbose -a /home/pi/fwupd-images/fw-upd-fail.png
					sleep 10
					killall -9 fbi
					sudo reboot -f


				fi
			else
				echo "4b.Backing of existing EdgeFirmware failed"
				echo 1 > /home/pi/firmwareupdatestatus.txt       #0-update success 1-update failed
				/usr/bin/fbi -T 1 -noverbose -a /home/pi/fwupd-images/fw-upd-fail.png
				sleep 10
				killall -9 fbi
				sudo reboot -f

			fi
		#else
		#	echo "3b.Copying of existing firmware failed"
		#	echo 1 > /home/pi/firmwareupdatestatus.txt       #0-update success 1-update failed
		#fi

	else
		echo "2b.EdgeFirmware tar file extraction failed"
		/usr/bin/fbi -T 1 -noverbose -a /home/pi/fwupd-images/fw-upd-fail.png
		sleep 10
		killall -9 fbi
		sudo reboot -f
	fi
else
	echo "1b.LatestFirmware.zip extracted failed"
	/usr/bin/fbi -T 1 -noverbose -a /home/pi/fwupd-images/fw-upd-fail.png
	sleep 10
	killall -9 fbi
	sudo reboot -f
fi 
