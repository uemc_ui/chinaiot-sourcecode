#!/bin/sh
/usr/bin/fbi -T 1 -noverbose -a /home/pi/fwupd-images/fw-upd-fail.png

edgfwvar=0;
modvar=0;

#check if EdgeFirmware-old directory exists
if [ -d /home/pi/Desktop/EdgeFirmware-old ] ; then
	mv /home/pi/Desktop/EdgeFirmware-old /home/pi/Desktop/EdgeFirmware
	if [ $? -eq 0 ] ; then
		echo "Restoring EdgeFirmware is successful"
		sleep 10
		chmod 777 -R /home/pi/Desktop/EdgeFirmware/*
		edgfwvar=1;
	else
		echo "Restoring EdgeFirmware failed!!!"
	fi
else #EdgeFirmware-old directory does not exists
edgfwvar=1;
echo "EdgeFirmware-old folder does not exit"
fi

#check if modbox-webserver-old directory exists
if [ -d /home/pi/Desktop/modbox-webserver-old ] ; then
	mv /home/pi/Desktop/modbox-webserver-old /home/pi/Desktop/modbox-webserver
	if [ $? -eq 0 ] ; then
		echo "Restoring modbox-webserver is successful"
		sleep 10
		chmod 777 -R /home/pi/Desktop/modbox-webserver
		modvar=1;
	else
		echo "Restoring modbox-webserver failed!!!"
	fi
else
modvar=1;
echo "modbox-webserver-old folder does not exit"
fi

if [ "$edgfwvar" -eq "1" -a "$modvar" -eq "1" ];then
echo 0 > /home/pi/firmwareupdatestatus.txt
else
echo "Unable to retain existing firmware";
fi

