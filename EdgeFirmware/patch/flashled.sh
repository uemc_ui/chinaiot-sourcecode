#!/bin/bash

echo 26 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio26/direction

while true
do        
     echo 1 > /sys/class/gpio/gpio26/value    #YELLOW LED ON
     sleep 0.05
     echo 0 > /sys/class/gpio/gpio26/value    #YELLOW LED OFF
     sleep 0.05
done
