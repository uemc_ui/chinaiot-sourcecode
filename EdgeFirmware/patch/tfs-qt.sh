#!/bin/bash

fwfilename="/home/pi/firmwareupdatestatus.txt"	#0-Start DL As usual; 1-Fw update failed.. restore existing version; 2-New Update available.. initiate

if test -e $fwfilename; then
	while read -r line 
	do
	    fwstatus="$line"
	    echo "Name read from file - $fwstatus"
	    break;
	done < "$fwfilename"

else	#file not found
echo "firmwareupdatestatus.txt does not exist"
fwstatus=4;
fi

if [ $fwstatus -eq 1 ]; then
	echo "Firmware Update Failed... Restore the existing version"
elif [ $fwstatus -eq 2 ]; then
	echo "New Firmare Update is available. Initiate firmware update now"
else
	if [ $fwstatus -eq 0 ]; then
	echo "Firmware Updated Successfully. No need to initiate update now"
	elif [ $fwstatus -eq 4 ]; then
	echo "FileNotFound"
	fi
	export QT_QPA_PLATFORM=eglfs
	export QT_QPA_GENERIC_PLUGINS=tslib:/dev/input/touchscreen
	export QT_QPA_EVDEV_TOUCHSCREEN_PARAMETERS=/dev/input/touchscreen
	export TSLIB_TSEVENTTYPE=INPUT    
	export TSLIB_CALIBFILE=/etc/pointercal
	export TSLIB_CONFFILE=/etc/ts.conf 
	export QT_QPA_EGLFS_DISABLE_INPUT=1

	fctfile="/home/pi/Desktop/EdgeFirmware/fct/FCTCompleted.txt"

	if test -e $fctfile; then
	#if [ -e $fctfile ]
	#then
	echo "The $fctfile file exists... FCT Completed... Starting with main app"
	cp /home/pi/Desktop/EdgeFirmware/mainapp/Modbox /home/pi/Desktop/EdgeFirmware

	else

	echo "The $fctfile file does not exists... Starting with FCT app"
	cp /home/pi/Desktop/EdgeFirmware/fct/Modbox /home/pi/Desktop/EdgeFirmware
	fi


/home/pi/Desktop/EdgeFirmware/Modbox

fi

