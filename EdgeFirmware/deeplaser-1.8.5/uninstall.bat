@echo off
REM @author Sachin Burange
CALL setuppath.bat

ECHO "[INFO] *** Uninstalling  DEEPLASER Framework... ***"
call :ColorText 0C "[INFO] DEEPLASER Framework will get removed from device."
set INPUT=
set /P INPUT=Are you sure? [Y/N]: %=%
echo %INPUT%
If "%INPUT%"=="y" goto usersaysyes 
If "%INPUT%"=="n" goto usersaysno
If "%INPUT%"=="Y" goto usersaysyes
If "%INPUT%"=="N" goto usersaysno



REM #User don't want to uninstall.
:usersaysno
call :ColorText 0b "Thank you for using DEEPLASER Framework.
pause
exit 0
:end

REM #User want to uninstall.
:usersaysyes
REM Remove it from $PATH
call :ColorText 0a "[INFO]  Removing DEEPLASAER sepcific enviornment variable's from Enviornment "
setlocal EnableDelayedExpansion
set path
set $line=%path%
set $line=%$line: =#%
set $line=%$line:;= %
set $line=%$line:)=^^)%

for %%a in (%$line%) do echo %%a | find /i "deeplaser" || set $newpath=!$newpath!;%%a
set $newpath=!$newpath:#= !
set $newpath=!$newpath:^^=!
set path=!$newpath:~1!


REM #Unsetting  env variable and deleting the deeplaser directory
set DEEPLASER_HOME=
SET DEEPLASER_VERSION=1.6.0
SET DL_HOME=%USERPROFILE%\deeplaser-%DEEPLASER_VERSION%
echo %DL_HOME%
IF EXIST %USERPROFILE%\deeplaser-%DEEPLASER_VERSION% (
call :ColorText 0C "[INFO] DELETING DEEPLASER HOME DIRECTORY "
ECHO %DL_HOME%
rmdir /s /q  %DL_HOME%
)

call :ColorText 0b "[INFO] Deeplaser has successfully been uninstalled."


SETLOCAL DisableDelayedExpansion
for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do rem"') do (
  set "DEL=%%a"
)

goto :eof

:ColorText
echo off
<nul set /p .=. > "%~2"
findstr /v /a:%1 /R "^$" "%~2" nul
echo(%DEL%%DEL%%DEL%
del "%~2" > nul 2>&1
goto :eof
