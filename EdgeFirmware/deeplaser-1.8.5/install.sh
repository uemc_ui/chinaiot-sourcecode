#@author Sachin Burange
echo "[INFO] *** Installing  DEEPLASER Framework... ***"

cd "$(dirname "$0")"

INSTALL_DIR=$1

if [ $# -lt 1 ]; then
  echo "[INFO] Please specify deep laser installation directory. For example sh install.sh /vendor."
  echo "[INFO] Exiting now"
  echo "[INFO] *** Goodbye ***"
  exit 
fi


if [[ "$1" = /* ]]; then
	echo "[INFO] Deep Laser will be installed under directory $INSTALL_DIR."
else
	echo "[INFO] Please specify absolute path of the directory."
	exit
fi

#For removing trailing slashes.
INSTALL_DIR=$(echo $1 | sed 's:/*$::')

if [ -d "${INSTALL_DIR}" ] ; then
    echo "[INFO] $INSTALL_DIR is a valid directory"
else
	echo "[INFO] Please specify valid directory for deeplaser installation."
	echo "[INFO] Exiting now"
  	echo "[INFO] *** Goodbye ***"
  	exit 
fi

#Unsetting earlier env variable
echo "[INFO] *** Removing DEEPLASAER sepcific enviornment variable's from shell environment. ***"
unset DEEPLASER_HOME

# Setup Deep Laser Home Directory.
WORKING_DIR=`pwd`
DEEPLASER_VERSION=`python -c 'import json; print json.load(open("conf/version.js","r"))["version"]'`

if [ ! -d $INSTALL_DIR/deeplaser-$DEEPLASER_VERSION ] 
    echo "[INFO] *** Found deeplaser home directory. Deleting it. ***"
   rm -rf $INSTALL_DIR/deeplaser-$DEEPLASER_VERSION
then
    echo "[INFO] Did not find deeplaser home directory. Creating home directory now."
    mkdir $INSTALL_DIR/deeplaser-$DEEPLASER_VERSION
fi
LOCAL_PYTHON_HOME=$PYTHON_HOME
if [ -z $LOCAL_PYTHON_HOME ]
then
    echo "[INFO] Did not find python home directory."
    LOCAL_PYTHON_HOME=$(python -c "import site; import sys; sites = site.getsitepackages(); site = sites[0] if not None else None; sys.exit(site);" 2>&1)
    echo "[INFO] Local python home set to: $LOCAL_PYTHON_HOME"
fi

export DEEPLASER_HOME=$INSTALL_DIR/deeplaser-$DEEPLASER_VERSION
export PATH="$DEEPLASER_HOME/bin":$PATH

echo "[INFO] DEEPLASER_HOME=$DEEPLASER_HOME"
echo "[INFO] Done"

if [ ! -d $DEEPLASER_HOME/repo ]
then
    echo "[INFO] Did not find deeplaser repo directory. Creating repo directory now."
    mkdir $DEEPLASER_HOME/repo
fi

# Copying deep laser into repository.
echo "[INFO] Copying deeplaser core modules to $DEEPLASER_HOME/repo"
cp -a repo/. $DEEPLASER_HOME/repo/


echo "[INFO] Exporting libraries to PYTHONPATH."
export PYTHONPATH=$(python -c "import sys; print(':'.join(x for x in sys.path if x))"):$DEEPLASER_HOME/lib:$DEEPLASER_HOME/repo
export PATH="$PYTHONPATH":$PATH


echo "[INFO] Storing third party libraries in $DEEPLASER_HOME/lib"
cp -r lib/ $DEEPLASER_HOME/lib/

if [ ! -d $DEEPLASER_HOME/logs ]
then
    echo "[INFO] Did not find deeplaser logs directory. Creating logs directory now."
    mkdir $DEEPLASER_HOME/logs
fi

echo "[INFO] Copying deeplaser configuration to $DEEPLASER_HOME/conf"
cp -r conf/  $DEEPLASER_HOME/conf/

echo "[INFO] Copying deeplaser bin to $DEEPLASER_HOME/bin"
cp -r bin/  $DEEPLASER_HOME/bin/

cp ./start.sh $DEEPLASER_HOME/
cp ./uninstall.sh $DEEPLASER_HOME/
cp ./switch_env.sh $DEEPLASER_HOME/


echo "[INFO] DEEPLASER_HOME=$DEEPLASER_HOME"
if test -e "$DEEPLASER_HOME/setuppath.sh"; then
    echo "[INFO] there is already a setuppath.sh file in your deeplaser home folder."
    if grep "DEEPLASER_HOME" $DEEPLASER_HOME/setuppath.sh &> /dev/null; then
        echo "[INFO] DEEPLASER_HOME environment variable already declared! We will update it"
        echo "`sed  /DEEPLASER_HOME/d  $DEEPLASER_HOME/setuppath.sh`" > $DEEPLASER_HOME/setuppath.sh
    else
        echo "[INFO] DEEPLASER_HOME not declared! We will add it"
    fi
    if grep "DEEPLASER_PATH" $DEEPLASER_HOME/setuppath.sh &> /dev/null; then
    echo "[INFO] DEEPLASER_PATH already set! But we will update it"
    echo "`sed  /DEEPLASER_PATH/d  $DEEPLASER_HOME/setuppath.sh`" > $DEEPLASER_HOME/setuppath.sh
    else
    echo "[INFO] DEEPLASER_PATH not set! We will set it"
    fi

else
    echo "[INFO] setuppath.sh doest not exist on your deeplaser home folder. We will create it"

fi
echo "export DEEPLASER_HOME=\"$DEEPLASER_HOME\" #DEEPLASER_HOME" >> $DEEPLASER_HOME/setuppath.sh


#echo "[INFO] PYTHONPATH=$PYTHONPATH"
if test -e "$DEEPLASER_HOME/setuppath.sh"; then
    echo "[INFO] there is already a setuppath.sh file in your deeplaser home folder."
    if grep "PYTHONPATH" $DEEPLASER_HOME/setuppath.sh &> /dev/null; then
        echo "[INFO] PYTHONPATH environment variable already declared! We will update it"
        echo "`sed  /PYTHONPATH/d  $DEEPLASER_HOME/setuppath.sh`" > $DEEPLASER_HOME/setuppath.sh
    else
        echo "[INFO] PYTHONPATH not declared! We will add it"
    fi
else
    echo "[INFO] setuppath.sh doest not exist on your deeplaser home folder. We will create it"

fi
echo "export PYTHONPATH=\"$PYTHONPATH\" #PYTHONPATH" >> $DEEPLASER_HOME/setuppath.sh
echo "[INFO] Done"

echo "[INFO] PYTHONPATH=$PYTHONPATH"
echo "[INFO] Deep Laser framework has been deployed."
echo "[INFO] *** Goodbye ***"
