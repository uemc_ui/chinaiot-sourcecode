{
    "name": "boot-update.js",
    "version": "1.8.5",
    "configurations": [ 
        {
            "name": "boot-module.js",
            "config": {
                "bundles" : [
                    {
                        "name" : "deeplaser.scheduleddata.handler"
                    },
                    {
                        "name" : "deeplaser.scheduleddatashell.scheduleddata_shell_command"
                    },
                    {
                        "name" : "deeplaser.mqtt.handler"
                    },
                    {
                        "name" : "deeplaser.datacollector.handler"
                    },
                    {
                        "name" : "deeplaser.updateshell.update_shell_command"
                    }
                ], 
                "components" : [
                {
                    "factory" : "ipopo-remote-shell-factory",
                    "name" : "pelix-remote-shell",
                    "properties" : {
                        "pelix.shell.port" : 8889
                    }
                },
                {
                    "factory" : "deeplaser-update-service-factory",
                    "name" : "deeplaser-update-service",
                    "properties" : {
                        "deeplaser.configuration.url" : "http://localhost:5000"
                    }
                },
                {
                    "factory" : "deeplaser-registration-service-factory",
                    "name" : "deeplaser-registration-service",
                    "properties" : {
                        "deeplaser.registration.url": "https://ft0an41ctl.execute-api.us-east-1.amazonaws.com/test/api/v1/device/initialize"
                    }
                }]
            }
        }
    ]
}