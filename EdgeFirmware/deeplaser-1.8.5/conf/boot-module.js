{
  "bundles": [
    {
      "name": "deeplaser.certificate.crypto.standard"
    },
    {
      "name": "deeplaser.certificate.manager"
    },
    {
      "name": "deeplaser.registration.handler"
    },
    {
      "name": "deeplaser.scheduleddata.handler"
    },
    {
      "name": "deeplaser.dataformatter.handler"
    },
    {
      "name": "deeplaser.scheduleddatashell.scheduleddata_shell_command"
    },
    {
      "name": "deeplaser.mqtt.handler"
    },
    {
      "name": "deeplaser.heartbeat.mqtt_heartbeat"
    },
    {
      "name": "deeplaser.heartbeat.http_heartbeat"
    },
    {
      "name": "deeplaser.scheduleddata.handler"
    },
    {
      "name": "deeplaser.dispatch.dispatcher"
    },
    {
      "name": "deeplaser.dataprocessor.factory"
    },
    {
      "name": "deeplaser.dataprocessor.iotrequest_data_processor"
    },
    {
      "name": "deeplaser.dataprocessor.device_data_processor"
    },
    {
      "name": "deeplaser.datacollector.handler"
    },
    {
      "name": "deeplaser.updateshell.update_shell_command"
    },
    {
      "name": "pelix.http.basic"
    },
    {
      "name": "deeplaser.webservice.webservice"
    },
    {
      "name": "deeplaser.webservice.asyncWShandler"
    },
    {
      "name": "deeplaser.webservice.webservice_remote_commands"
    },
    {
      "name": "deeplaser.webservice.asyncWShandler_remote_commands"
    }
  ],
  "components": [
     {
      "name": "web_service",
      "factory": "webservice_components_factory",
      "properties": {
        "deeplaser.webservice.rest_request": "rest_request"
      }
    },
     {
      "name": "command_web_service",
      "factory": "command_webservice_components_factory",
      "properties": {
        "deeplaser.webservice.device_response": "device_response"
      }
    },
    {
      "factory": "pelix.http.service.basic.factory",
      "name": "http-server",
      "properties": {
        "pelix.http.address": "localhost",
        "pelix.http.port": 9000,
        "pelix.http.request_queue_size" : 10
      }
    },
    {
      "factory": "deeplaser-update-service-factory",
      "name": "deeplaser-update-service",
      "properties": {
        "deeplaser.configuration.url": "http://localhost:5000"
      }
    },
    {
      "factory": "python-mqtt-heartbeat-service",
      "name": "deeplaser-mqtt-heartbeat-service",
      "properties": {
        "deeplaser.client.ping_rate": "1",
        "deeplaser.client.max_retries": "5",
        "deeplaser.client.port": "8883"
      }
    },
    {
      "factory": "python-rest-handler-service",
      "name": "rest-handler-service",
      "properties": {
        "deeplaser.webservice.instrument_content_type": "application/json",
        "deeplaser.webservice.instrument_end_point": "http://127.0.0.1:8000/v1/iotresponse",
        "deeplaser.webservice.request_type": "IoTRequest",
        "deeplaser.webservice.rest_request": "rest_request",
        "deeplaser.webservice.rest_response": "rest_response"
      }
    },
    {
      "factory": "python-device-rest-handler-service",
      "name": "device-rest-handler-service",
      "properties": {
        "deeplaser.webservice.instrument_content_type": "text/plain",
        "deeplaser.webservice.instrument_end_point": "http://127.0.0.1:8000/v1/devicerequest",
        "deeplaser.webservice.request_type": "DeviceRequest",
        "deeplaser.webservice.device_request": "device_request",
        "deeplaser.webservice.device_response": "device_response"
      }
    },
    {
      "factory": "python-http-heartbeat-service",
      "name": "deeplaser-http-heartbeat-service",
      "properties": {
        "deeplaser.client.ping_rate": "1",
        "deeplaser.client.max_retries": "5",
        "deeplaser.http.url": "www.google.com"
      }
    },
    {
      "factory": "deeplaser-registration-service-factory",
      "name": "deeplaser-registration-service",
      "properties": {
        "deeplaser.multiregion.endpoint": "https://mrgr.instrumentconnect.com/mrgr/svc/api/v1/gateways/deviceconnect",
        "deeplaser.multiple_gatway_entry" : true
      }
    }
  ]
}
