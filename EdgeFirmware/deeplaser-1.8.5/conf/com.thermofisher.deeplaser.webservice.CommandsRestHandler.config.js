{
    "device_request": "device_request",
    "device_response": "device_response",
    "instrument_content_type": "application/json",
    "instrument_end_point": "http://127.0.0.1:5000/v1/devicerequest",
    "request_type": "DeviceResponse",
    "service.pid": "com.thermofisher.deeplaser.webservice.CommandsRestHandler"
}
