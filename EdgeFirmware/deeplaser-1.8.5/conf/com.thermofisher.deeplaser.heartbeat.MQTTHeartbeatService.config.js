{
    "max_retries": 5,
    "ping_rate": 10,
    "service.pid": "com.thermofisher.deeplaser.heartbeat.MQTTHeartbeatService",
    "timeout": 10,
    "network_latency" : 5
}
