{
    "instrument_content_type": "application/json",
    "instrument_end_point": "http://127.0.0.1:8888/v1/iotresponse",
    "request_type": "IoTRequest",
    "rest_request": "rest_request",
    "rest_response": "rest_response",
    "service.pid": "com.thermofisher.deeplaser.webservice.RestHandler"
}
