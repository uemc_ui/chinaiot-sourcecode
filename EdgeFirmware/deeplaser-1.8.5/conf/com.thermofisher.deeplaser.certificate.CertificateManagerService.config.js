{
    "certificates": [
        {
            "group": "device",
            "id": "root_pem",
            "location": "/home/pi/certification-prod.deeplaser/rootca.pem"
        },
        {
            "group": "device",
            "id": "device_certificate",
            "location": "/home/pi/certification-prod.deeplaser/certificate.pem.cer"
        }
    ],
    "device_certificate_id": "14b5f1b62e741fb5cc1d38ebd990f73d30e68c58c465b12927cb1c9f2ec91a5e",
    "device_shared_token": "MODBOX||Model-2016-01||123223",
    "service.pid": "com.thermofisher.deeplaser.certificate.CertificateManagerService"
}
