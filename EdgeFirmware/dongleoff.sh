#!/bin/bash

#echo 0 > /sys/class/gpio/gpio27/value
sudo killall -9 dongleon


StaticIPAddress="192.168.50.1"

my_ip=$(ifconfig wlan0 | perl -nle '/t addr:(\S+)/&&print$1')

if [ "$my_ip" == "$StaticIPAddress" ] || [ -z "$my_ip" ]; then

     	echo "No IP Assigned or in static Mode.., need to run dongleoff script"

	sudo /etc/init.d/hostapd stop && 
	sudo /etc/init.d/udhcpd stop &&
	sudo /etc/init.d/dnsmasq stop &&
	sudo /etc/init.d/dhcpcd stop &&

	#sudo ifconfig wlan0 down &&
	sudo ifdown wlan0 &&
	sudo ip addr flush dev wlan0 &&
	sudo rm -rf /etc/network/interfaces
	sudo cp /home/pi/Desktop/EdgeFirmware/dongleoff/interfaces.sta /etc/network/interfaces

	sudo /etc/init.d/dhcpcd start &&
	sudo /etc/init.d/dnsmasq start &&
	sudo service dhcpcd status &&
	sudo service dnsmasq status &&
	#sudo ifconfig wlan0 down &&
	#sudo ifconfig wlan0 up 
	sudo ifdown wlan0 &&
	sudo ifup wlan0

	i=1
	for i  in 1 2 3 4 5 6
	do	
		sleep 30
		my_ip=$(ifconfig wlan0 | perl -nle '/t addr:(\S+)/&&print$1')
		if $my_ip
		then
			echo "reset "
			sudo ifdown wlan0 
			sudo ifup wlan0 
		else
			echo "IP: $my_ip"
			break
		fi

	done

  
else
    	echo "IP Assigned., no need to run script"
fi



