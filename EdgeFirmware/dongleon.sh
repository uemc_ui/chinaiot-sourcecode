#!/bin/bash

#echo 1 > /sys/class/gpio/gpio27/value

sudo killall -9 dongleoff

sudo ifconfig wlan0 down &&

my_ip=$(ifconfig wlan1 | perl -nle '/t addr:(\S+)/&&print$1')

if $my_ip
then
        echo "No IP Assigned for wlan1"

	sudo /etc/init.d/hostapd stop &&
	sudo /etc/init.d/udhcpd stop &&
	sudo /etc/init.d/dnsmasq stop &&
	sudo /etc/init.d/dhcpcd stop &&

	sudo ifdown wlan0 &&
	sudo ip addr flush dev wlan0 &&
	sudo rm -rf /etc/network/interfaces
	sudo cp /home/pi/Desktop/EdgeFirmware/dongleon/interfaces.sta /etc/network/interfaces

	sudo /etc/init.d/dhcpcd start &&
	sudo /etc/init.d/dnsmasq start &&
	sudo service dhcpcd status &&
	sudo service dnsmasq status &&
	sudo ifconfig wlan0 down &&
	sudo ifdown wlan1 &&
	sudo ifup wlan1

	i=1
	for i  in 1 2 3 4 5 6
	do	
		sleep 30
		my_ip=$(ifconfig wlan1 | perl -nle '/t addr:(\S+)/&&print$1')
		if $my_ip
		then
			echo "reset "
			sudo ifdown wlan1 
			sudo ifup wlan1 
		else
			echo "IP: $my_ip"
			break
		fi

	done


else
        echo  "IP Already assigned for wlan1"

fi


