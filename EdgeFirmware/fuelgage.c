#define BQ27441_ADDR 0x55       // taken from datasheet - page 13
#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include <math.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <time.h>
#include <sys/time.h>
#include <wiringPi.h>
#include <math.h>
#include <sys/stat.h>
#include <sys/socket.h>

#define new_desmsb  0x13    //27
#define new_deslsb  0x88    //10
#define new_tvmsb   0x0B
#define new_tvlsb   0xB8

//Disable all printf's
//#define DEBUG


time_t t;

int soc = 0;

typedef unsigned char byte;

int deviceDescriptor;

char batPercentage[6];
extern int sock;
int sockRetValue;
extern int BatterystatusFlag;

typedef struct Webserver_Struct
{
    char parametersBuffer[40];
    int index;
} Qt_WebServer_Sock;


Qt_WebServer_Sock Client_Webserver2;

int writeToSocket2(int index, char* data);

/* This function initializes the I2C device*/
void init_i2c(char *DeviceName)
{   
        printf("Initialising i2c device \n");
        deviceDescriptor=open(DeviceName, O_RDWR);

        if (deviceDescriptor == -1) {
                printf("Error opening device '%s' \n",DeviceName);
                exit(-1);
        }
}

/* This function sends data to the I2C device*/
void I2cSendData(byte addr,byte *data,int len)
{
        if(ioctl(deviceDescriptor,I2C_SLAVE, addr))
                printf("I2cSendData_device : IOCTL Problem \n");

        write(deviceDescriptor,data,len);
}

/* This function reads data from the I2C device*/
void I2cReadData(byte addr,byte *data,int len)
{
        if(ioctl(deviceDescriptor,I2C_SLAVE, addr))
                printf("I2cReadData_device : IOCTL Problem \n");

        read(deviceDescriptor,data,len);
}

/* Convert the number to hexadecimal representation */
void to_hex_16(char *output, unsigned n)
{
        static const char hex_digits[] = "0123456789abcdef";
        output[0] = hex_digits[(n >> 12) & 0xF];
        output[1] = hex_digits[(n >> 8) & 0xF];
        output[2] = hex_digits[(n >> 4) & 0xF];
        output[3] = hex_digits[(n & 0xF)];
        output[4] = '\0';
}
/* Computes the checksum by adding the values in the register and then subtracting from 255 */
static int checksum(byte *check_data)
{
        int sum = 0;
        int ii = 0;
        
        for(ii = 0; ii < 32; ii++) 
            sum += check_data[ii+62];
            
        sum &= 0xFF;
        
        return 0xFF - sum;
}

/* getliner() reads one line from standard input and copies it to line array 
 * (but no more than max chars)
 * It does not place the terminating \n line array.
 * Returns line length, or 0 for empty line, or EOF for end-of-file.
 */
int getliner(char line[], int max)
{
        int nch = 0;
        int c;
        max = max - 1;          /* Leave room for '\0' */

        while ((c = getchar()) != EOF) {
                if (c == '\n')
                        break;

                if (nch < max) {
                        line[nch] = c;
                        nch = nch + 1;
                }
        }

        if (c == EOF && nch == 0)
                return EOF;

        line[nch] = '\0';
        return nch;
}

void check_batterystatus(void * arg)
{

	int fd,ret,voltage;
	unsigned int temp1,new_csum,result,cmp;
	unsigned char check_sum[64],data_check,OLD_Csum,OLD_DesCapMSB,OLD_DesCapLSB,Temp_Checksum,ReadCheck,OLD_TvCapMSB;
	unsigned char OLD_TvLSB;
	float temp = 0.0;
	int remaining_batt_cap = 0;
	int full_charge_cap = 0;
	int current=0;
	struct tm *tm1 = localtime(&t);
	FILE * file2 = 0;
	FILE * file1 = 0;
	char s[64];
	
	

	fd = wiringPiI2CSetup(BQ27441_ADDR);	
	if(fd < 0)
		printf("File description open filed\n");
	else
		//printf("File description open sucess\n");	
	

#if 0
	ret = wiringPiI2CWriteReg16(fd,0x00,0x0000);
	if(ret < 0)
		printf("writing failed\n");
	result = wiringPiI2CReadReg16(fd,0x00);
	printf("Control status = %x\n",result);

	sleep(1);

	ret = wiringPiI2CWriteReg16(fd,0x00,0x0001);
	if(ret < 0)
		printf("writing failed\n");
	result = wiringPiI2CReadReg16(fd,0x00);
	printf("Device type = %x\n",result);

	sleep(1);

	ret= wiringPiI2CWriteReg16(fd,0x00,0x0002);
	if(ret < 0)
		printf("writing failed\n"); 
	result = wiringPiI2CReadReg16(fd,0x00);
	printf("Fw_Vesrion = %x\n",result);
	
	sleep(1);

#endif


	//Reset the Device
	ret = wiringPiI2CWriteReg16(fd,0x00,0x0042);           
	if(ret < 0)
		printf("writing failed\n");

	
	//if the flag is not set agian start from unsealed
	loop:						  

	//Unsealed the module
	ret = wiringPiI2CWriteReg16(fd,0x00,0x8000);             
	if(ret < 0)
		printf("writing failed\n");

	ret = wiringPiI2CWriteReg16(fd,0x00,0x8000);
	if(ret < 0)
		printf("writing failed\n");
	
	sleep(1);

	//set configuration update
	ret = wiringPiI2CWriteReg16(fd,0x00,0x0013);             
	if(ret < 0)
		printf("writing failed\n"); 

	sleep(1);

	//conform configuration mode update			
	result = wiringPiI2CReadReg16(fd,0x06);
	//printf("Flag_Register = %x\n",result);

	sleep(3);

	if (CHECK_BIT(result, 4)){

		#ifdef DEBUG
		printf("The gauge is ready to be configured \n");
		#endif
		
		//Enable block data memory control
		ret = wiringPiI2CWriteReg8(fd,0x61,0x00);       
		if(ret < 0)
		printf("writing failed_0x61\n");

		sleep(1);
	
		//Access the state subclass
		ret = wiringPiI2CWriteReg8(fd,0x3E,0x52);       
		if(ret < 0)
			printf("writing failed_0x3E\n");  

		sleep(1);	
	
		//write block offset location
		ret = wiringPiI2CWriteReg8(fd,0x3F,0x00); 	
		if(ret < 0)
		printf("writing failed_0x3F\n"); 
		

	}
	else {
		 printf("The gauge configured failed \n");
		 goto loop;
	}
	

	sleep(1);

	//If check sum fails agian start from old checksum
	near_oldcheck:    

	//Read the block check sum
	OLD_Csum = wiringPiI2CReadReg16(fd,0x60);			
	//printf("OLD_Csum = %x\n",OLD_Csum);

	sleep(1);

	Temp_Checksum = 0xFF - OLD_Csum;

	//printf("Temp_Checksum = %x\n",Temp_Checksum);

	OLD_DesCapMSB = wiringPiI2CReadReg8(fd,0x4A);		
	//printf("OLD_DesCapMSB = %x\n",OLD_DesCapMSB);

	sleep(1);


	OLD_DesCapLSB = wiringPiI2CReadReg8(fd,0x4C);
	//printf("OLD_DesCapLSB = %x\n",OLD_DesCapLSB);
	
	sleep(1);

	OLD_TvCapMSB = wiringPiI2CReadReg8(fd,0x50);
	//printf("OLD_TvCapLSB = %x\n",OLD_DesCapLSB);

	sleep(1);

	OLD_TvLSB = wiringPiI2CReadReg8(fd,0x5B);			
	//printf("OLD_TvMSB = %x\n",OLD_DesCapMSB);
		
	sleep(1);

	Temp_Checksum = Temp_Checksum-OLD_DesCapMSB-OLD_DesCapLSB-OLD_TvCapMSB-OLD_TvLSB;
	//printf("Temp_Checksum_Tv = %x\n",Temp_Checksum);	
	


	ret = wiringPiI2CWriteReg8(fd,0x4A, new_desmsb); 	
	if(ret < 0)
	printf("writing failed_0x4A\n");

	sleep(1);


	ret = wiringPiI2CWriteReg8(fd,0x4C,new_deslsb); 	
	if(ret < 0)
	printf("writing failed_0x4C\n"); 

	sleep(1);

	
	ret = wiringPiI2CWriteReg8(fd,0x50,new_tvmsb); 	
	if(ret < 0)
	printf("writing failed_0x50n");

	sleep(1);
 

	ret = wiringPiI2CWriteReg8(fd,0x5B,new_tvlsb); 	
	if(ret < 0)
	printf("writing failed_0x5B\n"); 	

	sleep(1);

	Temp_Checksum = (Temp_Checksum + new_desmsb + new_deslsb + new_tvmsb + new_tvlsb);
	new_csum = 0xFF - Temp_Checksum;
	//printf("new_csum = %x\n",new_csum);	

	

	ret = wiringPiI2CWriteReg16(fd,0x60,new_csum); 			
	if(ret < 0)
		printf("writing failed 0x60\n");

	sleep(1);

	ret = wiringPiI2CWriteReg16(fd,0x3E,0x52); 			
	if(ret < 0)
		printf("writing failed_0x3E\n");
	sleep(1);


	ret = wiringPiI2CWriteReg16(fd,0x3F,0x00); 			
	if(ret < 0)
		printf("writing failed_0x3E\n");

	sleep(1);

	//Read the block check sum
	ReadCheck = wiringPiI2CReadReg16(fd,0x60);			       
	//printf("New_Checksum_After config = %x\n",ReadCheck);


	//Check New checksum with old checksum
	if (ReadCheck == new_csum ){
		#ifdef DEBUG
		printf("Check sum valid after config sucess\n");
		#endif
	}
	else {
		printf("Check sum valid after config fail\n");
		printf("Going to configuration\n");
		goto near_oldcheck;
	}


	//Exit configuration update
	ret = wiringPiI2CWriteReg16(fd,0x00,0x0042); 			
	if(ret < 0)
		printf("writing failed_exit_configupdate\n");


	//Read the block check sum
	result = wiringPiI2CReadReg16(fd,0x06);			       
	//printf("Flag_reg_end = %x\n",result);
	

	//Sealed mode
	ret = wiringPiI2CWriteReg16(fd,0x00,0x0020); 			
	if(ret < 0)
		printf("writing failed 0x00\n");


	while(1)
	{
	
		sleep(3);
	
		soc = wiringPiI2CReadReg16(fd,0x1C);
		#ifdef DEBUG
		printf("State of Charge % = %d\n",soc);
		#endif
	
		file1 =	fopen("/home/pi/Desktop/EdgeFirmware/miscfiles/percentagebat.txt","w+");
		if(file1 == NULL){
		 	printf("percentagebat.txt : file open failed\n"); 
		}
		else
		{
			fprintf(file1,"%d",soc);
			fclose(file1);		
		}
		
		
		if(BatterystatusFlag==1 || soc<25)		//sending battery percentage only when battery is active
		{
			Client_Webserver2.index=17;						//17. Battery Percentage
			
			memset(batPercentage,'\0',sizeof(batPercentage));
			sprintf(batPercentage,"%d",soc);
			
			/*strcpy(Client_Webserver2.parametersBuffer,batPercentage);				

			//sockRetValue = write(sock,(Qt_WebServer_Sock*)&Client_Webserver2,sizeof(Client_Webserver2));
			sockRetValue = send(sock,(Qt_WebServer_Sock*)&Client_Webserver2,sizeof(Client_Webserver2),MSG_DONTWAIT);
			if(sockRetValue < 0){
					error("ERROR writing to socket in adcdata read");
			}   
			sleep(1);*/
			
			writeToSocket2(17,batPercentage);

		}
	}	
       
}


int writeToSocket2(int index, char* data)
{
	Client_Webserver2.index=index;						
	strcpy(Client_Webserver2.parametersBuffer,data);				

	
	sockRetValue = send(sock,(Qt_WebServer_Sock*)&Client_Webserver2,sizeof(Client_Webserver2),MSG_DONTWAIT);
	
	//printf("<-------->Send Socket Return Value for index %d: is %d\n", index,sockRetVal);
	
	if(sockRetValue < 0)
	{
		printf("ERROR writing to socket in webserver socket\n");
	}

	
	sleep(1);
	
	//fflush(sock);
	
	return sockRetValue;

}



