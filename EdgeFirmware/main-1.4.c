#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
//#include "postcloud.h"
#include "i2c_BQ24295.h"
#include <wiringSerial.h>
#include <sched.h>
#include <pthread.h>
#include <linux/spi/spidev.h>
#include <math.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <math.h>
#include <netinet/tcp.h>

/*
    C ECHO client example using sockets
*/
#include<sys/socket.h>  //socket
#include<arpa/inet.h>   //inet_addr
int sock;
int sockRetVal;
int sockfd;
struct sockaddr_in server;
char server_reply[2000];


//Disable all printf's
#define DEBUG

#define NORMAL 		1
#define LOGGING 	2
#define ALARM	 	3
#define BATTERY 	4
#define ALARMANDBATT	5
#define ALARMANDLOG	6
#define BATTLOG		7
#define ALARMBATTLOG	8


//New hardware
#define CALIB_RESISTOR_LOW_VALUE         (560.0F)
#define CALIB_RESISTOR_MID_VALUE         (750.0f)
#define CALIB_RESISTOR_HIGH_VALUE        (1000.0f)


float B0_Value;
float B1_Value;
float B2_Value;

#define device "/dev/spidev1.0"

//ADC Registers configuration
uint8_t GPIOCFG_TX[3]    = {0X4C,0X00,0X03},GPIOCFG_RX[3];
uint8_t GPIODIR_TX[3]    = {0X4D,0X00,0X00},GPIODIR_RX[3];
uint8_t GPIODATLOW_TX[3] = {0X4E,0X00,0X00},GPIODATLOW_RX[3];
uint8_t GPIODATHIG_TX[3] = {0X4E,0X00,0X03},GPIODATHIG_RX[3];


//Transmit
uint8_t setmux0_tx[3]  = {0x40,0x00,0x01},setmux0_rx[3]; 
uint8_t setmux1_tx[3]  = {0x42,0x00,0x30},setmux1_rx[3];   //Internal reference selected
uint8_t setsys0_tx[3]  = {0x43,0x00,0x02},setsys0_rx[3]; 
uint8_t setidac0_tx[3] = {0x4A,0x00,0x08},setidac0_rx[3];  //0x08
uint8_t setidac1_tx[3] = {0x4B,0x00,0xFF},setidac1_rx[3];  //0xFF
uint8_t setdatac_tx[1] = {0x16},setdatac_rx[1];
uint8_t readdata_tx[4] = {0x12,0xFF,0xFF,0xFF},readdata_rx[4];
uint8_t sync_tx[1]     = {0x04},sync_rx[4];
uint8_t resetcmd_tx[1] = {0x06},resetcmd_rx[4];


#define  A_COEFF                    ((double)(0.0039083))
#define  B_COEFF                    ((double)(-0.0000005775))
#define  C_COEFF                    ((double)(-0.000000000004183))
#define  DENOMINATOR   ((double)((A_COEFF) + (2*B_COEFF*t1) - (300 * C_COEFF * t1 * t1) + (4 * C_COEFF * t1 * t1 * t1)))
#define  SUCC_APPROX_CNT              3

#define  VALUE_MAX 30
#define  PT100_RES_AT_ZERO_DEG       100.0F
#define  PT1000_RES_AT_ZERO_DEG      1000.0F


uint8_t rddata[18] = {0x20,0x14,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF},adcread[18];
uint8_t recvdata[3];

typedef struct Webserver_Struct
{
    char parametersBuffer[40];
    int index;
} Qt_WebServer_Sock;


Qt_WebServer_Sock Client_Webserver;

typedef enum
{

  PT1000 = 0,
  PT100,

  MAX_NUM_OF_SENS_TYPES

}Sensor_Types;


float AdcVal_V,AdcVal_I; //,avg;
float RTDResistance,Voltage,temp;
static uint32_t mode;
static uint8_t bits = 8;
static uint32_t speed = 500000;
static uint16_t delay;


char cabinetTemp[20];
char setPointTemp[20];
char alarmsStrings[20][100];
int totalAlarmsCount;
char modeOfOper[25];
char totalAlarmString[1000];

extern int alarmModeSetFlag;
extern int batteryModeSetFlag;
extern int loggingModeSetFlag;
extern int alarmBatModeSetFlag;
extern int alarmLogModeSetFlag;
extern int alarmBatLogModeSetFlag;
extern int batteryLoggingModeSetFlag;

int alarmWithBatteryFlag;
int alarmWithLoggingFlag;
int loggingWithBatteryFlag;
int alarmBatteryLogFlag;

extern int currentMode;
char timeinUTC[50];
static DRV_IFACE_t bq2x;
static DRVD_BQ24295_t bq2x_data;
char external_temp[sizeof(float)];
char tempBuff[10];
float actual_temp;
extern int guiUpdateFile(void);
extern int readycount;
extern int CloudPost_4min(int count);
extern int postToCloud(int counter);
int alarmActiveFlag = 0;
int timeinsecond;
int sdtimeinsecond;
int battrymodebuttonpress = 0;
int LedstatusFlg=0;
float setHighTemp;
float setLowTemp;
float setpoint;
int fd_spi=0;
#define CUSTOMERTD 1
extern void * check_batterystatus(void * arg);	//need to add logic to update into qt gui

float B0_Value,B1_Value,B2_Value;
float Low_Value,Mid_Value,High_Value,Sensor_Resistance;
float Sensor_Res,B0_Val,B1_Val,B2_Val,finalsensor_Res;
float RTD_B0_VALUE,RTD_B1_VALUE,RTD_B2_VALUE;
float RTD_Rx0_VALUE,RTD_Rx1_VALUE,RTD_Rx2_VALUE;

/////////////////SET GPIO //////////////////////////
#define PIN   5 /* P1-18 */
#define POUT  4  /* P1-07 */
#define IN    0
#define OUT   1
#define LOW   0
#define HIGH  1


int keypress = 0;
int startAlarm = 0;
pthread_t thread, thread1, thread2;
int NETWORKstatusFlag = 0;
int BatterystatusFlag = 0;
char RTDBuffer[20];

int writeToSocket(int index, char* data);

static int transfer(int fd, uint8_t const *tx, uint8_t const *rx, size_t len)
{
	int ret = -1;
	int out_fd;	
	struct spi_ioc_transfer tr = {
		.tx_buf = (unsigned long)tx,
		.rx_buf = (unsigned long)rx,
		.len = len,
		.delay_usecs = delay,
		.speed_hz = speed,
		.bits_per_word = bits,
	};


	if (mode & SPI_TX_QUAD)
		tr.tx_nbits = 4;
	else if (mode & SPI_TX_DUAL)
		tr.tx_nbits = 2;
	if (mode & SPI_RX_QUAD)
		tr.rx_nbits = 4;
	else if (mode & SPI_RX_DUAL)
		tr.rx_nbits = 2;
	if (!(mode & SPI_LOOP)) {
		if (mode & (SPI_TX_QUAD | SPI_TX_DUAL))
			tr.rx_buf = 0;
		else if (mode & (SPI_RX_QUAD | SPI_RX_DUAL))
			tr.tx_buf = 0;
	}


	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
	if (ret < 1)
		printf("can't send spi message");

	return ret;
}


void spi_init(void)
{

    int ret=0;
    #ifdef DEBUG
    printf("spi init enter\n");
    #endif
    
	//open the spi device
	fd_spi = open(device, O_RDWR);
	if (fd_spi < 0)
		printf("spi device open failed\n");

	//spi mode	 
	ret = ioctl(fd_spi, SPI_IOC_WR_MODE32, &mode);
	if (ret == -1)
		printf("can't set spi mode\n");

	ret = ioctl(fd_spi, SPI_IOC_RD_MODE32, &mode);
	if (ret == -1)
		printf("can't get spi mode");
	
	//bits per word
	ret = ioctl(fd_spi, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
		printf("can't set bits per word");

	ret = ioctl(fd_spi, SPI_IOC_RD_BITS_PER_WORD, &bits);
	if (ret == -1)
		printf("can't get bits per word");
	
	ret = ioctl(fd_spi, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		printf("can't set max speed hz");

	ret = ioctl(fd_spi, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		printf("can't get max speed hz");

	#ifdef DEBUG
	printf("spi mode: 0x%x\n", mode);
	printf("bits per word: %d\n", bits);
	printf("max speed: %d Hz (%d KHz)\n", speed, speed/1000);
	printf("spi init exit\n");
	#endif

}

static int GPIORead(int pin)
{
	#define VALUE_MAX 30
	char path[VALUE_MAX];
	char value_str[3];
	int fd;
 
	snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
	fd = open(path, O_RDONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open gpio value for reading!\n");
		return(-1);
	}
 
	if (-1 == read(fd, value_str, 3)) {
		fprintf(stderr, "Failed to read value!\n");
		return(-1);
	}
 
	close(fd);
 
	return(atoi(value_str));
}


void adc_reset()
{
	system("sudo echo \"1\" > /sys/class/gpio/gpio14/value");
	sleep(1);
	system("sudo echo \"0\" > /sys/class/gpio/gpio14/value");
	sleep(1);
	system("sudo echo \"1\" > /sys/class/gpio/gpio14/value");
	sleep(1);

}

double Calc_temp_below_zero_deg( float calc_Res, float zero_degree_res)
{
  
  uint8_t Index = 0;
    
  float t1= 0.0F;
  
  float tn = 0.0F;
  
  if(zero_degree_res != 0)
  {
    t1 =  ((calc_Res/zero_degree_res) - 1 )/(A_COEFF + (4* B_COEFF));

    for(Index = 0; Index < SUCC_APPROX_CNT  ; Index++)
    {
      tn = t1 - ((1 + (double)(A_COEFF * t1) + (double)(B_COEFF * t1 * t1) + \
                    (double)(( C_COEFF * t1 * t1 *t1 ) * (t1 - 100)) - (calc_Res/zero_degree_res))/(DENOMINATOR));
      
      
      t1 = tn;
    }
      
  } 
    return tn;
  
  
}

float calculate_temperature(float       *Final_Res_Value, 
                                 Sensor_Types Sensor_Type)
{
	
  float temperature;	  
  float Temp = 0.0F;
  
  float zero_degree_res = 1.0F , Calc_Res = 0.0F;
  
  
  if(Final_Res_Value != 0)  // Sanjay NULL_PTR to zero
  {
     Calc_Res = *Final_Res_Value;
    
     switch(Sensor_Type)
     {  
       case PT1000 :
         
            zero_degree_res  = PT1000_RES_AT_ZERO_DEG;
       
       break;
       
       case PT100 :
            
            zero_degree_res  =  PT100_RES_AT_ZERO_DEG;
       break;
       default :
       break;
     }  
     
    if(Calc_Res > zero_degree_res)
    {   
      Temp = (double)(sqrt( (double)((double)(A_COEFF * A_COEFF) - (double) (4 * B_COEFF * (1 - (Calc_Res/zero_degree_res))))) - A_COEFF);
      Temp = Temp/(2 * B_COEFF) ;
    } 
    else if(Calc_Res < zero_degree_res)
    {
      Temp = Calc_temp_below_zero_deg(Calc_Res,zero_degree_res);
    }
    else
    {
      Temp = 0.0F;
    }
 
  }
      
  return Temp;

}

void adc_start(void)
{
     system("sudo echo \"1\" > /sys/class/gpio/gpio18/value");  //Adc_start
     sleep(1);
     system("sudo echo \"0\" > /sys/class/gpio/gpio18/value");  //Adc_start
     sleep(1);  
     system("sudo echo \"1\" > /sys/class/gpio/gpio18/value");  //Adc_start
}

void adc_defaultconfig()
{

	double temp;
	int val1=0,val2=0,i=0;
	int ret,ret1,ret2,ret3,ret4,ret5,ret6;
	int Adcval;
	float tmp,voltage,temperature;


	system("sudo echo \"0\" > /sys/class/gpio/gpio16/value");   //Chip slection low
	sleep(.5);
	system("sudo echo \"1\" > /sys/class/gpio/gpio18/value");   //adc start high
	
	ret = transfer(fd_spi, setmux0_tx, setmux0_rx, sizeof(setmux0_tx));
	if(ret < 0)
		printf("comunciation failed\n");

	ret = transfer(fd_spi, setmux1_tx, setmux1_rx, sizeof(setmux1_tx));
	if(ret < 0)
		printf("comunciation failed\n");
			
	ret = transfer(fd_spi,  setsys0_tx, setsys0_rx, sizeof( setsys0_tx));
	if(ret < 0)
		printf("comunciation failed\n");

	ret = transfer(fd_spi,  setidac0_tx, setidac0_rx, sizeof(setidac0_tx));
	if(ret < 0)
		printf("comunciation failed\n");

    ret = transfer(fd_spi,  setidac1_tx, setidac1_rx, sizeof(setidac1_tx));
	if(ret < 0)
		printf("comunciation failed\n");

	ret = transfer(fd_spi, setdatac_tx, setdatac_rx, sizeof(setdatac_tx));
	if(ret < 0)
		printf("comunciation failed\n");

	ret = transfer(fd_spi, rddata, recvdata, sizeof(rddata));
	if(ret < 0)
		printf("comunciation failed\n");

	system("sudo echo \"1\" > /sys/class/gpio/gpio16/value");   //Chip slection high

}

float adc_voltage_read()
{

     int i=0,j=0,ret=0,cnt=0;
     float AvgVoltage=0,SecondAvgVoltage=0,AvgVoltage390=0,resistance=0,current390=0,temp;

     system("sudo echo \"0\" > /sys/class/gpio/gpio16/value");   //Chip slection low
     sleep(.1);		
     system("sudo echo \"1\" > /sys/class/gpio/gpio18/value");   //adc start high
      
     setmux0_tx[2] = 0x02;  
     ret= transfer(fd_spi, setmux0_tx, setmux0_rx, sizeof(setmux0_tx));
     if(ret < 0)
		printf("spi comunciation failed in adc_voltage_read function_0x02\n");
		
      setsys0_tx[2] = 0x22;	
      ret=transfer(fd_spi,  setsys0_tx, setsys0_rx, sizeof( setsys0_tx));
      if(ret < 0)
		printf("spi comunciation failed in adc_voltage_read function_0x22\n");

      ret=transfer(fd_spi, rddata, adcread, sizeof(rddata));
      if(ret < 0)
		printf("spi comunciation failed in adc_voltage_read function\n");	
	
      system("sudo echo \"1\" > /sys/class/gpio/gpio16/value"); //set cs high
      system("sudo echo \"0\" > /sys/class/gpio/gpio18/value");   //adc start high	
    
      adc_start();       

      system("sudo echo \"0\" > /sys/class/gpio/gpio16/value"); //set cs low
      sleep(.1);     
      transfer(fd_spi, readdata_tx, readdata_rx, sizeof(readdata_tx));
      system("sudo echo \"1\" > /sys/class/gpio/gpio16/value"); //set cs low  
       
      AdcVal_V=0;
      AdcVal_V = (float)(readdata_rx[1] << 16 | readdata_rx[2] << 8 | readdata_rx[3]);
      #ifdef DEBUG
      printf(" AdcVal_V = %f\n", AdcVal_V);
      #endif

      return AdcVal_V;

}

float adc_current_read()
{
	
   int i=0,j=0,ret=0,cnt=0;
   float AvgVoltage=0,SecondAvgVoltage=0,AvgVoltage390=0,resistance=0,current390=0,temp;
 
 
   system("sudo echo \"0\" > /sys/class/gpio/gpio16/value");   //Chip slection low
   sleep(.1);	
   system("sudo echo \"1\" > /sys/class/gpio/gpio18/value");   //adc start high
 		
   setmux0_tx[2] = 0x13;
   ret=transfer(fd_spi, setmux0_tx, setmux0_rx, sizeof(setmux0_tx));
   if(ret < 0)
		printf("spi comunciation failed in adc_current_read function_0x13\n");

    setsys0_tx[2] = 0x42;		
    transfer(fd_spi,  setsys0_tx, setsys0_rx, sizeof( setsys0_tx));
	if(ret < 0)
		printf("spi comunciation failed in adc_current_read function_0x42\n");
       
    transfer(fd_spi, rddata, adcread, sizeof(rddata));      		
    
    system("sudo echo \"1\" > /sys/class/gpio/gpio16/value");   //set cs high
    system("sudo echo \"0\" > /sys/class/gpio/gpio18/value");   //adc start high	
    
    adc_start();

    system("sudo echo \"0\" > /sys/class/gpio/gpio16/value"); //set cs low
    sleep(.1); 
    transfer(fd_spi, readdata_tx, readdata_rx, sizeof(readdata_tx));
    system("sudo echo \"1\" > /sys/class/gpio/gpio16/value"); //set cs low  
                  	
    AdcVal_I=0;
    AdcVal_I = readdata_rx[1] << 16 | readdata_rx[2] << 8 | readdata_rx[3];
    #ifdef DEBUG
    printf("AdcVal_I = %f\n",AdcVal_I);
    #endif

	return AdcVal_I;

}


float read_calibration_560()
{
	float calib1_v,calib1_c,Resistance_calib1;
	int i;
		
    system("sudo echo \"0\" > /sys/class/gpio/gpio15/value");  //SNR_CAL1
    system("sudo echo \"1\" > /sys/class/gpio/gpio6/value");  //SNR_CAL2
    system("sudo echo \"1\" > /sys/class/gpio/gpio23/value"); //SNR_CAL3 
	
	calib1_v = adc_voltage_read();
	calib1_c = adc_current_read();
	
	Resistance_calib1 = (390.0F * (16.0F/4.0F)*(calib1_v/calib1_c));
	#ifdef DEBUG
	printf("Resistance_calib1_560 = %f\n",Resistance_calib1);
	#endif
		
	return Resistance_calib1;
		
}

float read_calibration_750()
{
	float calib2_v,calib2_c,Resistance_calib2;
	int i;
		
	system("sudo echo \"1\" > /sys/class/gpio/gpio15/value"); //SNR_CAL1
        system("sudo echo \"0\" > /sys/class/gpio/gpio6/value");  //SNR_CAL2
        system("sudo echo \"1\" > /sys/class/gpio/gpio23/value"); //SNR_CAL3 
    	
	calib2_v = adc_voltage_read();
	calib2_c = adc_current_read();
	Resistance_calib2 = (390.0F * (16.0F/4.0F)*(calib2_v/calib2_c));
	#ifdef DEBUG
	printf("Resistance_calib2_750 = %f\n",Resistance_calib2);
	#endif
		
	return Resistance_calib2;
			
}

float read_calibration_1000()
{
	float calib3_v,calib3_c,Resistance_calib3;
	int i;
		
	system("sudo echo \"1\" > /sys/class/gpio/gpio15/value"); //SNR_CAL1
        system("sudo echo \"1\" > /sys/class/gpio/gpio6/value");  //SNR_CAL2
        system("sudo echo \"1\" > /sys/class/gpio/gpio23/value"); //SNR_CAL3 
	
	calib3_v = adc_voltage_read();
	calib3_c = adc_current_read();
	Resistance_calib3 = (390.0F * (16.0F/4.0F)*(calib3_v/calib3_c));
	#ifdef DEBUG
	printf("Resistance_calib3_1000 = %f\n",Resistance_calib3);
	#endif
		
	return Resistance_calib3;
		
}

float read_rtd_resistance()
{

	
    float rtd_v,rtd_c,Resistance_rtd;
    int i;
		
    system("sudo echo \"0\" > /sys/class/gpio/gpio15/value"); //SNR_CAL1
    system("sudo echo \"0\" > /sys/class/gpio/gpio6/value");  //SNR_CAL2
    system("sudo echo \"1\" > /sys/class/gpio/gpio23/value"); //SNR_CAL3 
	
    rtd_v = adc_voltage_read();
    rtd_c = adc_current_read();
    Resistance_rtd = (390.0F * (16.0F/4.0F)*(rtd_v/rtd_c));
    #ifdef DEBUG
    printf("Resistance_rtd = %f\n",Resistance_rtd);
    #endif
		
	return Resistance_rtd;
		
}


int adcdata_read()
{
	
   int timecount=0,n=0,i=0,j=0,k=0,suden_cntr=0;	
   float Def_Low_Value=0,Def_Mid_Value=0,Def_High_Value=0;
   int  res_arr[10];
   float Final_Rtd_resistance[10];
    
   Def_Low_Value  = CALIB_RESISTOR_LOW_VALUE ;   
   Def_Mid_Value  = CALIB_RESISTOR_MID_VALUE ;   
   Def_High_Value = CALIB_RESISTOR_HIGH_VALUE ;
    
   adc_defaultconfig();
   
   Low_Value =  read_calibration_560();

   Mid_Value =  read_calibration_750();
    
   High_Value = read_calibration_1000();
   
   RTD_Rx0_VALUE  =  Low_Value;   //CALIB_RESISTOR_LOW_VALUE ;   
   RTD_Rx1_VALUE  =  Mid_Value;   //CALIB_RESISTOR_MID_VALUE ;   
   RTD_Rx2_VALUE  =  High_Value;  //CALIB_RESISTOR_HIGH_VALUE ;  
   RTD_B0_VALUE   =  CALIB_RESISTOR_LOW_VALUE ;   
   RTD_B1_VALUE   =  1.0F ;                      
   RTD_B2_VALUE    = 0.0F ; 

   #ifdef DEBUG    
   printf("adcdata_readthread before while\n");
   #endif
    
     restart_adc:

     while(1)
     { 
		printf("adc_read while loop\n");
		for(i=0;i<5;i++)
		{
			Sensor_Resistance = read_rtd_resistance();
			B0_Val = RTD_B0_VALUE;
			B1_Val = (Sensor_Resistance - RTD_Rx0_VALUE*RTD_B1_VALUE);
			B2_Val = (Sensor_Resistance - RTD_Rx0_VALUE)*(Sensor_Resistance - RTD_Rx1_VALUE)*RTD_B2_VALUE; 
			Sensor_Res = B0_Val + B1_Val + B2_Val;
			Final_Rtd_resistance [i] = Sensor_Res;             
			#ifdef DEBUG
			printf("Final_Rtd_resistance = %f\n",Final_Rtd_resistance [i]);
			#endif
			if((Final_Rtd_resistance[i] > 1400)||(Final_Rtd_resistance[i] < 180)) 
			{
				printf("Temperature out off range\n");
				sprintf(external_temp,"NA");
				#ifdef DEBUG
                        	printf("external_temp = %s\n",external_temp);

                        	#endif                        	

                        	/*
							Client_Webserver.index=14;						//14. c. RTD Out of Range
							strcpy(Client_Webserver.parametersBuffer,external_temp);				
				
							//n = write(sock,(Qt_WebServer_Sock*)&Client_Webserver,sizeof(Client_Webserver));
							n = send(sock,(Qt_WebServer_Sock*)&Client_Webserver,sizeof(Client_Webserver),MSG_DONTWAIT);
                        	if(n < 0){
                                	error("ERROR writing to socket in adcdata read");
                        	}   
                        	*/
                        	
                        	writeToSocket(14,external_temp);			//15. c. RTD Value
                                adc_reset();
                                adc_defaultconfig();
                                sleep(.5);
                                goto restart_adc;
                                break;
		        }
                }
	       for(k=0;k<4;k++)
               {
			if(fabsf(Final_Rtd_resistance[k] - Final_Rtd_resistance[k+1]) > 50)
			{
			
				printf("Resistance sudden change\n");
				Final_Rtd_resistance[k+1] = Final_Rtd_resistance[k];				
				suden_cntr++;
				#ifdef DEBUG
				printf("suden_cntr = %d\n",suden_cntr);
				#endif
				if(suden_cntr > 4)
				{
					adc_reset();
					adc_defaultconfig();
					sleep(.5);
					goto restart_adc;
					break;
				}
			} 
			else
			{       
				finalsensor_Res = Final_Rtd_resistance[k];
				actual_temp = calculate_temperature(&finalsensor_Res, PT1000);
				sprintf(setPointTemp, "%f",setpoint);
				snprintf(external_temp,2*sizeof(float),"%f",actual_temp);
				#ifdef DEBUG
				printf("external_temp = %s\n",external_temp);
				#endif
				//sleep(1);
				
				/*
				memset(tempBuff,'\0',sizeof(tempBuff));
				strcpy(tempBuff,"f.");
				strcat(tempBuff,external_temp);
                        	
				//n = write(sockfd,external_temp,strlen(external_temp));
				n = write(sockfd,tempBuff,strlen(tempBuff));
				*/
				
				if(k==3)
				{
					/*Client_Webserver.index=15;						//15. c. RTD Value
					strcpy(Client_Webserver.parametersBuffer,external_temp);				
	
					//n = write(sock,(Qt_WebServer_Sock*)&Client_Webserver,sizeof(Client_Webserver));
					n = send(sock,(Qt_WebServer_Sock*)&Client_Webserver,sizeof(Client_Webserver),MSG_DONTWAIT);
					sleep(1);*/
					
					writeToSocket(15,external_temp);			//15. c. RTD Value
				}
				
				if(n < 0)
				{
					error("ERROR writing to socket in adcdata read");
				}
                 	}

                }
                suden_cntr = 0;

	}
	#ifdef DEBUG
   	printf("adc_read thread exit\n");	
	#endif    
			
}

int writeToSocket(int index, char* data)
{
	Client_Webserver.index=index;						
	strcpy(Client_Webserver.parametersBuffer,data);				

	//sockRetVal = write(sock,(Qt_WebServer_Sock*)&Client_Webserver,sizeof(Client_Webserver));
	
	//sockRetVal = send(sock,(Qt_WebServer_Sock*)&Client_Webserver,sizeof(Client_Webserver),0);
	
	//sockRetVal = send(sock,(Qt_WebServer_Sock*)&Client_Webserver,sizeof(Client_Webserver),MSG_DONTWAIT|MSG_OOB);
	
	sockRetVal = send(sock,(Qt_WebServer_Sock*)&Client_Webserver,sizeof(Client_Webserver),MSG_DONTWAIT);
	
	//printf("<-------->Send Socket Return Value for index %d: is %d\n", index,sockRetVal);
	
	if(sockRetVal < 0)
	{
		printf("ERROR writing to socket in webserver socket\n");
	}

	
	sleep(1);
	
	//fflush(sock);
	
	return sockRetVal;

}


void * deeplaser(void * arg)
{

	printf("Created Thread for pushing data to cloud\n");
	while(1)
	{
		sleep(59);
		#if 0
		if(currentMode == NORMAL || currentMode == ALARM)
		{
			#ifdef DEBUG
			printf("\n\nSending data to Deeplaser from the thread\n\n");
			#endif
			if((strncasecmp(external_temp,"146",3)!=0) && (strncasecmp(external_temp,"242",3)!=0) && (strncasecmp(external_temp,"-1",2)!=0) && (strncasecmp(external_temp,"-2",2)!=0))
			{
				pushtoDl(external_temp);
			}
		}
		else if(currentMode == BATTERY)
		{
			pushtoDl(external_temp);
			printf("BATTERY MODE: Writing Data to SD card...\n");
			printf("$$$$$$$$$$$$$$$$$$$$$ Current Mode before writeToSD: %d",currentMode);
			writetoSDcard(external_temp,currentMode);	
		}
		else if(currentMode == LOGGING)
		{
			pushtoDl(external_temp);
			printf("LOGGING MODE: Writing Data to SD card...\n");
			writetoSDcard(external_temp,currentMode);	

		}
		#endif	//0
	 readWifiSignal();
	}
	
	//readWifiSignal();

}



void * appstatus_check(void * arg)
{

	FILE *fp=0;
	
	while(1)
	{
		sleep(3);
		fp = fopen("/home/pi/Desktop/EdgeFirmware/miscfiles/appstatus_check.txt","w+");
		if(fp==NULL){
                	printf("Failed to create appstatus_check.txt file\n");         
		}      
		else
		{
			fprintf(fp,"%s\n","application_alive");
			fclose(fp);
		}
	}	
		
}

static int GPIOExport(int pin)
{
#define BUFFER_MAX 3
	char buffer[BUFFER_MAX];
	ssize_t bytes_written;
	int fd;
 
	fd = open("/sys/class/gpio/export", O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open export for writing!\n");
		return(-1);
	}
	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
	write(fd, buffer, bytes_written);
	close(fd);
	return(0);
}
 
static int GPIOUnexport(int pin)
{
	char buffer[BUFFER_MAX];
	ssize_t bytes_written;
	int fd;
 
	fd = open("/sys/class/gpio/unexport", O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open unexport for writing!\n");
		return(-1);
	}
 
	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
	write(fd, buffer, bytes_written);
	close(fd);
	return(0);
}
 
static int GPIODirection(int pin, int dir)
{
	static const char s_directions_str[]  = "in\0out";
 	#define DIRECTION_MAX 35
	char path[DIRECTION_MAX];
	int fd;
 
	snprintf(path, DIRECTION_MAX, "/sys/class/gpio/gpio%d/direction", pin);
	fd = open(path, O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open gpio direction for writing!\n");
		return(-1);
	}
 
	if (-1 == write(fd, &s_directions_str[IN == dir ? 0 : 3], IN == dir ? 2 : 3)) {
		fprintf(stderr, "Failed to set direction!\n");
		return(-1);
	}
 
	close(fd);
	return(0);
}
 

static int GPIOWrite(int pin, int value)
{
	static const char s_values_str[] = "01";
 
	char path[VALUE_MAX];
	int fd;
 
	snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
	fd = open(path, O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open gpio value for writing!\n");
		return(-1);
	}
 
	if (1 != write(fd, &s_values_str[LOW == value ? 0 : 1], 1)) {
		fprintf(stderr, "Failed to write value!\n");
		return(-1);
	}
 
	close(fd);
	return(0);
}


void LED_init(void)
{

#ifdef DEBUG
	 printf("Enter LED init function \n");
#endif

        //Export GPIO Pins
	system("echo \"27\" > /sys/class/gpio/export");  // Output --> (red)    -->27
	system("echo \"22\" > /sys/class/gpio/export");  // Output --> (green)  -->22
	system("echo \"26\" > /sys/class/gpio/export");  // Output --> (yellow) -->26
	system("echo \"12\" > /sys/class/gpio/export");  // Output --> (Buzzer) -->12
	system("echo \"5\" > /sys/class/gpio/export");   // input  --> (SW_IN)  -->5 

	//Set GPIO direction
	system("echo \"out\" > /sys/class/gpio/gpio27/direction");   // Output --> (red)    -->27
	system("echo \"out\" > /sys/class/gpio/gpio22/direction");   // Output --> (green)  -->22
	system("echo \"out\" > /sys/class/gpio/gpio26/direction");   // Output --> (yellow) -->26
	system("echo \"out\" > /sys/class/gpio/gpio12/direction");   // Output --> (Buzzer) -->12
	system("echo \"in\" > /sys/class/gpio/gpio5/direction");     // input  --> 19(SW_IN)  --5

        //GREEN led ON in boot time
	system("echo \"1\" > /sys/class/gpio/gpio22/value"); //Geen led turn on boot time for normal mode


	//ADC pins configuration
	#ifdef DEBUG
	printf("gpio init\n");
	#endif
	system("sudo echo \"14\" > /sys/class/gpio/export");             // Adc_Reset pin
	system("sudo echo \"out\" > /sys/class/gpio/gpio14/direction");  
	
	#if 0
	system("sudo echo \"15\" > /sys/class/gpio/export");             // Adc_DRDY pin
	system("sudo echo \"out\" > /sys/class/gpio/gpio15/direction");  
	system("sudo echo \"0\" > /sys/class/gpio/gpio15/value");
	#endif

	system("sudo echo \"16\" > /sys/class/gpio/export");             // Adc chip slection pin
	system("sudo echo \"out\" > /sys/class/gpio/gpio16/direction");  
	//system("sudo echo \"0\" > /sys/class/gpio/gpio16/value");	

	system("sudo echo \"18\" > /sys/class/gpio/export");             // Adc_Start pin
	system("sudo echo \"out\" > /sys/class/gpio/gpio18/direction"); 
	system("sudo echo \"1\" > /sys/class/gpio/gpio18/value"); 

	system("sudo echo \"6\" > /sys/class/gpio/export");             // SNR_CAL1
	system("sudo echo \"out\" > /sys/class/gpio/gpio6/direction"); 
	
	system("sudo echo \"15\" > /sys/class/gpio/export");             // SNR_CAL2
	system("sudo echo \"out\" > /sys/class/gpio/gpio15/direction"); 
	
	system("sudo echo \"23\" > /sys/class/gpio/export");             // SNR_CAL3
	system("sudo echo \"out\" > /sys/class/gpio/gpio23/direction"); 
	
#ifdef DEBUG
	printf("Exit export GPIO \n");
#endif

}


void readsetpointconfig(void)
{
FILE * time_fp;
FILE * lowTemp_fp;
FILE * highTemp_fp;
char * line = NULL;
int len;
int result; 
int first, second;
		time_fp = fopen("/home/pi/Desktop/EdgeFirmware/setpoint.txt","r");
		if(time_fp != NULL)
		{
			result = getline(&line,&len,time_fp);
			//printf("freadsetpointconfig %s \n",line);
			setpoint = atof(line);
			//printf("after converting to float %f \n",setpoint);
			fclose(time_fp);
						
		}
		else
		{
			printf("Set point config file not found configring default as +27\n");
			setpoint = 27;
		}
		
		result =0;
		len = 0;
		
		lowTemp_fp = fopen("/home/pi/Desktop/EdgeFirmware/lowtemp.txt","r");
		if(lowTemp_fp != NULL)
		{
			result = getline(&line,&len,lowTemp_fp);
			printf("freadlowTempconfig %s \n",line);
			setLowTemp = atof(line);
			//printf("Low Temp after converting to float %f \n",setLowTemp);
			fclose(lowTemp_fp);
						
		}
		else
		{
			printf("Low Temp config file not found configring default as +20\n");
			setLowTemp = 20;
		}
		
		result =0;
		len = 0;
		
		highTemp_fp = fopen("/home/pi/Desktop/EdgeFirmware/hightemp.txt","r");
		if(highTemp_fp != NULL)
		{
			result = getline(&line,&len,highTemp_fp);
			setHighTemp = atof(line);
			fclose(highTemp_fp);
						
		}
		else
		{
			printf("High Temp config file not found configring default as +30\n");
			setHighTemp = 30;
		}
}


void readUTCTimenow(void)
{
FILE * time_fp;
char * line = NULL;
int len;
int result; 
int first, second;
		time_fp = fopen("/home/pi/Desktop/EdgeFirmware/miscfiles/utctimenow.txt","r");
		if(time_fp != NULL)
		{
			result = getline(&line,&len,time_fp);
			strcpy(timeinUTC,line);
			printf("timeinUTC %s \n",timeinUTC);
			fclose(time_fp);
						
		}
		else
		{
			strcpy(timeinUTC,"2017-01-20T06:43:16Z");
		}
}


void readtimeconfig(void)
{
FILE * time_fp;
int first=0, second=0, result=0;
		time_fp = fopen("/home/pi/Desktop/EdgeFirmware/miscfiles/configtime.txt","r");
		if(time_fp != NULL)
		{
			first = fgetc(time_fp);
			second = fgetc(time_fp);
			timeinsecond = (first-48)*10+(second-48);
                        #ifdef DEBUG
			printf("time in second is %d\n",timeinsecond);
			#endif
			fclose(time_fp);
						
		}
		else
		{
			printf("Time config file not found configring default as 5 min\n");
			timeinsecond = 5;
		}
}


void configbattery(void)
{

system("i2cset -y 1 0x6b 0x00 0x44"); //charge with 1mA,changes done (09May2017)

system("i2cset -y 1 0x6b 0x01 0x3b");

system("i2cset -y 1 0x6b 0x02 0x70");

system("i2cset -y 1 0x6b 0x03 0x11");

system("i2cset -y 1 0x6b 0x04 0xb2");

system("i2cset -y 1 0x6b 0x05 0x0e");

system("i2cset -y 1 0x6b 0x06 0x93");

system("i2cset -y 1 0x6b 0x07 0x4b");


}

////////////////////////////////////////////////////////

int main()
{

int result;
int timecount = 0;
int sdtimecount = 0;
int i = 0;
int j=0;
int rtdfd = -1;
float actual_temp;
pthread_attr_t tattr;
int newprio = 20;
struct sched_param param;
FILE *fp, *fp1;


   system("sudo date -s \"$(curl -s --head www.fudan.edu.cn | grep ^Date: | sed 's/Date: //g') -0500\"");
   system("sudo hwclock -s --utc");
   system("sudo hwclock -r --utc");
   system("sudo hwclock -w --utc");
   system("sudo hwclock -r --utc");

    if (signal(SIGPIPE, SIG_IGN) == SIG_ERR) {
    	perror("Could not ignore the SIGPIPE signal");
    	exit(EXIT_FAILURE);
    }

    //Create socket
    sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock == -1)
    {
        printf("Could not create socket");
    }
    #ifdef DEBUG
    puts("Socket created");
    #endif

    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_family = AF_INET;
    server.sin_port = htons( 1234 );
    sockfd = sock;
    
    int flag = 1;
    
	result = setsockopt(sock,            /* socket affected */
					IPPROTO_TCP,     /* set option at TCP level */
					TCP_NODELAY,     /* name of option */
					(char *) &flag,  /* the cast is historical cruft */
					sizeof(int));    /* length of option value */

    //Connect to remote server
    if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
    {
        
	perror("connect failed. Error");
    while(1)
	{
		sleep(1);
		if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
    		{
     			perror("connect failed. RETRY after a second");
		}
		else
		{
			break;
		}
	}


    }

        #ifdef DEBUG
	puts("Connected\n");
	printf("Socket Des: %d\n",sock);	
        #endif	
	/*Configure the battery*/
	configbattery();

        /*Check the battery status flag */
	bq2x.read.ptr = &bq2x_data;
	bq2x.read.size = sizeof(bq2x_data);
	create_BQ24295(bq2x);

	/*Initilize the spi module*/
	spi_init();

	/*export the Led's */
	LED_init();

	/*Reset the adc*/
	adc_reset();

        /*Export the gpio pin*/
	GPIOExport(PIN);

	/*Read the configured time*/
	readtimeconfig(); // This was done 

        /*To check the GPIO direction*/
	GPIODirection(PIN,IN);


        /**************CREATING THREADs TO ALL THE MODULES ************************/

	pthread_create(&thread,NULL,&check_batterystatus, NULL); //Check the battery percentage status
	
	//pthread_create(&thread1,NULL,&adcdata_read, NULL);  //read the temperature

	pthread_create(&thread1,NULL,&appstatus_check, NULL);  //application check keep alive or not

	/*********** CREATE HIGH PRIORITY THREAD TO PUSH DATA TO DEEPLASER***********/
	pthread_attr_init(&tattr);
	pthread_attr_getschedparam(&tattr,&param);
	param.sched_priority = newprio;
	pthread_attr_setschedparam(&tattr,&param);
	pthread_create(&thread2,&tattr,&deeplaser, NULL); 
	/****************************************************************************/

system("sudo python /home/pi/Desktop/EdgeFirmware/tzupdate.py");

        //system("sudo date -s \"$(curl -s --head www.fudan.edu.cn | grep ^Date: | sed 's/Date: //g') -0500\"");

	while(1)
	{
		adcdata_read();
		sleep(2);
	}
	
	close(sock);
	
	return 0;

}


  
