#include <stdio.h>
#include <unistd.h>
#include <string.h>
void main (void)
{
	//reading serial number i.e. unique device id from file
	char serialNo[50];
	char startDLCmd[100];
	
	memset(serialNo,'\0',sizeof(serialNo));
	memset(startDLCmd,'\0',sizeof(startDLCmd));

	FILE * serialNoFilePtr;
	
	
	serialNoFilePtr = fopen("/home/deviceSerial.txt","r");
	if(serialNoFilePtr)
	{
		fgets(serialNo,sizeof(serialNo), serialNoFilePtr);
		fclose(serialNoFilePtr);
	}

	if(strchr(serialNo,'\n')!=NULL)
	{
		serialNo[strlen(serialNo)-1]='\0';
	}
	else
	{
		serialNo[strlen(serialNo)]='\0';
	}	
	
	
	sprintf(startDLCmd,"./start.sh --device-type MODBOX --device-model TEST --device-serial %s --verbose --debug --filesize=10000000",serialNo);
	printf("Final Deeplaser Start Command: %s\n",startDLCmd);
	
	chdir("/home/pi/dl-1.8-prod/deeplaser-1.8.5");
	
	//system("./start.sh --device-type MODBOX --device-model TEST --device-serial TFS-CoE --verbose --debug");
	system(startDLCmd);
	while(1)
	{
		sleep(10);
		
	}
	

}

