#!/bin/bash

export QT_QPA_PLATFORM=eglfs
export QT_QPA_GENERIC_PLUGINS=tslib:/dev/input/touchscreen
export QT_QPA_EVDEV_TOUCHSCREEN_PARAMETERS=/dev/input/touchscreen
export TSLIB_TSEVENTTYPE=INPUT    
export TSLIB_CALIBFILE=/etc/pointercal
export TSLIB_CONFFILE=/etc/ts.conf 
export QT_QPA_EGLFS_DISABLE_INPUT=1

fctfile="/home/pi/Desktop/EdgeFirmware/fct/FCTCompleted.txt"

if test -e $fctfile; then
#if [ -e $fctfile ]
#then
echo "The $fctfile file exists... FCT Completed... Starting with main app"
cp /home/pi/Desktop/EdgeFirmware/mainapp/Modbox /home/pi/Desktop/EdgeFirmware

else

echo "The $fctfile file does not exists... Starting with FCT app"
cp /home/pi/Desktop/EdgeFirmware/fct/Modbox /home/pi/Desktop/EdgeFirmware
fi


/home/pi/Desktop/EdgeFirmware/Modbox
#/home/pi/Desktop/EdgeFirmware/app2.sh

