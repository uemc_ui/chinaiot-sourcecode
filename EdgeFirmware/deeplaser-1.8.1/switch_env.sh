#!/bin/bash

PROD_URL='https://mrgr.instrumentconnect.com/mrgr/svc/api/v1/gateways/deviceconnect'
STAGE_URL='https://stage.mrgr.instrumentconnect.com/mrgr/svc/api/v1/gateways/deviceconnect'
TEST_URL='https://test.mrgr.instrumentconnect.com/mrgr/svc/api/v1/gateways/deviceconnect'

show_help_and_exit () {

CODE=$@
cat << EOF

usage: ./switch_env.sh --env=[TEST|STAGE|PROD] --data-dir=DATA_DIR
          --deeplaser-home=DEEPLASER_HOME --device-type=DEVICE_TYPE

  _____  ______ ______ _____  _                _____ ______ _____  
 |  __ \|  ____|  ____|  __ \| |        /\    / ____|  ____|  __ \ 
 | |  | | |__  | |__  | |__) | |       /  \  | (___ | |__  | |__) |
 | |  | |  __| |  __| |  ___/| |      / /\ \  \___ \|  __| |  _  /
 | |__| | |____| |____| |    | |____ / ____ \ ____) | |____| | \ \ 
 |_____/|______|______|_|    |______/_/    \_\_____/|______|_|  \_\ 
		

Changes the Thermo Fisher Cloud environment to the specified value.

    --env=               Required. Points deeplaser to the specified environment
                                   and clears the data directory to force device
				   registration in the new environment. 
				   
				   Supported environents are TEST, STAGE and 
				   PROD.

				   Registration is sticky so deeplaser will
				   remain configured to the last environment it
				   was registered with until registered to a 
				   different one.

    --data-dir=          Required. The location of the data directory.

    --deeplaser-home=    Required. The location of the DEEPLASER_HOME directory.
    
    --device-type=       Required. The device type which will be used.

EOF
exit $CODE
}

die () {
  echo >&2 "$@"
  show_help_and_exit 1
}

replace_endpoints () {
  ENVIRONMENT=$@
  echo "[INFO] Setting environment to: $ENVIRONMENT"
  case $ENVIRONMENT in
    PROD)
      URL=$PROD_URL
    ;;
    STAGE)
      URL=$STAGE_URL
    ;;
    TEST)
      URL=$TEST_URL
    ;;
  esac
  for file in "${ENV_CONFIG_FILES[@]}"
  do
    if [[ $file =~ boot-module.js ]]; then
      pref_name='deeplaser.multiregion.endpoint'
      echo "[INFO] Setting $pref_name: ${URL} in $file"
    elif [[ $file =~ IoTRequestDataProcessorService ]]; then
      pref_name='region_url'
      echo "[INFO] Setting $pref_name: ${URL} in $file"
    else echo "[ERROR] Unexpected config file: $file"
    fi
    if [ `uname` == 'Darwin' ]; then
      echo "[INFO] Identified Darwin host. Using BSD syntax..."
      inplace_arg="''"
    else
      echo "[INFO] Trying GNU syntax..."
      inplace_arg=''
    fi
    sed -i $inplace_arg -e 's~\("'"$pref_name"'"\).*$~\1: "'"${URL}"'",~' "${file}"
    
    if [[ $file =~ CertificateManagerService ]]; then
      pref_name='device_certificate_id'
      if [ $ENVIRONMENT == "PROD" ]; then
          sed -i $inplace_arg -e 's~\("'"$pref_name"'"\).*$~\1: "'"68fd5539bc4cee28f7ab89a64440adc918d8784a4787db7b7543cb8997465675"'",~' "${file}"
      else
          sed -i $inplace_arg -e 's~\("'"$pref_name"'"\).*$~\1: "'"5af2ac22a00dd1f17df6233c8f1528a86f4b1726d7012d3a6bce783c2fe51a9b"'",~' "${file}"
      fi
    fi  
    
    pref_name='device_shared_token'
    sed -i $inplace_arg -e 's~\("'"$pref_name"'"\).*$~\1: "'"$DEVICE_TYPE||Model-2016-01||"'",~' "${file}" 
    
    if [ $? -ne 0 ]; then
      echo "[ERROR] sed replace failed on file: $file" 
    fi
  done
}

if [ ! $1 ]; then
  show_help_and_exit
fi

for i in "$@"
do
  echo "$i"
  case "$i" in
    --env=*)
      CLOUD_ENV="${i#*=}"
      shift
      [ $CLOUD_ENV == 'PROD' ] || [ $CLOUD_ENV == 'STAGE' ] || [ $CLOUD_ENV == 'TEST' ] \
        || echo "[ERROR] Invalid cloud environment. Must be one of TEST, STAGE, or PROD."
      ;;
    -h | --help)
      show_help_and_exit 0
      ;;
    --data-dir=*)
      DATA_DIR="${i#*=}"
      ;;
    --deeplaser-home=*)
      DEEPLASER_HOME="${i#*=}"
      ;;
    --device-type=*)
      DEVICE_TYPE="${i#*=}"
      ;;
  esac
done

ENV_CONFIG_FILES=("$DEEPLASER_HOME/conf/boot-module.js"
                  "$DEEPLASER_HOME/conf/com.thermofisher.deeplaser.data_processor.IoTRequestDataProcessorService.config.js"
                  "$DEEPLASER_HOME/conf/com.thermofisher.deeplaser.certificate.CertificateManagerService.config.js")

echo "[INFO] Deeplaser Home is set to $DEEPLASER_HOME"

if test -z $DEEPLASER_HOME;
then
  echo "[ERROR] DEEPLASER_HOME is must be provided."
  show_help_and_exit 1
fi

if test -z "$DATA_DIR"; then
  echo "[ERROR] The data dir location must be provided."
  show_help_and_exit 1
fi

if test -z "$CLOUD_ENV"; then
  echo "[ERROR] The Thermo Fisher Cloud environment must be provided."
  show_help_and_exit 1
else
  replace_endpoints $CLOUD_ENV
  
  if [ -d $DATA_DIR ]; then
    echo "[INFO] Removing data directory: $DATA_DIR"
    rm -r $DATA_DIR
  else
    echo "[INFO] No existing data directory at $DATA_DIR"
  fi
fi

echo "[INFO] Success! Now pointed to $CLOUD_ENV."
