ó
g®Xc           @   s±   d  Z  d d l Z d d l Z d d l m Z m Z d Z d j d   e D  Z d	 Z	 e j
 d d k  ru e Z n e Z d
 Z d e f d     YZ d e f d     YZ d S(   s  
Definition of classes used by the Pelix shell service and its consumers

:author: Thomas Calmant
:copyright: Copyright 2015, isandlaTech
:license: Apache License 2.0
:version: 0.6.3
:status: Alpha

..

    Copyright 2015 isandlaTech

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
iÿÿÿÿN(   t   to_bytest   to_stri    i   i   t   .c         c   s   |  ] } t  |  Vq d  S(   N(   t   str(   t   .0t   x(    (    s   ./repo/pelix/shell/beans.pys	   <genexpr>(   s    s   restructuredtext ent   ?t   ShellSessionc           B   sS   e  Z d  Z d d  Z e d    Z e d    Z d   Z d   Z	 d   Z
 RS(   si   
    Represents a shell session. This is the kind of object given as parameter
    to shell commands
    c         C   s}   | |  _  t | t  s! i  } n  | j   |  _ d |  j t <| j |  _ | j |  _ | j	 |  _	 | j
 |  _
 | j |  _ d S(   s¡   
        Sets up the shell session

        :param io_handler:  The I/O handler associated to the session
        :param initial_vars: Initial variables
        N(   t   _io_handlert
   isinstancet   dictt   copyt   _ShellSession__variablest   Nonet   RESULT_VAR_NAMEt
   write_linet   write_line_no_feedt   writet   flusht   prompt(   t   selft
   io_handlert   initial_vars(    (    s   ./repo/pelix/shell/beans.pyt   __init__B   s    		c         C   s   |  j  j   S(   s1   
        A copy of the session variables
        (   R   R   (   R   (    (    s   ./repo/pelix/shell/beans.pyt	   variables\   s    c         C   s   |  j  t S(   s0   
        Returns the content of $result
        (   R   R   (   R   (    (    s   ./repo/pelix/shell/beans.pyt   last_resultc   s    c         C   s   |  j  | S(   s­   
        Returns the value of a variable

        :param name: Name of the variable
        :return: The value of the variable
        :raise KeyError: Unknown name
        (   R   (   R   t   name(    (    s   ./repo/pelix/shell/beans.pyt   getj   s    c         C   s   | |  j  | <d S(   s|   
        Sets/overrides the value of a variable

        :param name: Variable name
        :param value: New value
        N(   R   (   R   R   t   value(    (    s   ./repo/pelix/shell/beans.pyt   sett   s    c         C   s   |  j  | =d S(   s   
        Unsets the variable with the given name

        :param name: Variable name
        :raise KeyError: Unknown name
        N(   R   (   R   R   (    (    s   ./repo/pelix/shell/beans.pyt   unset}   s    N(   t   __name__t
   __module__t   __doc__R   R   t   propertyR   R   R   R   R   (    (    (    s   ./repo/pelix/shell/beans.pyR   =   s   	
		t	   IOHandlerc           B   sJ   e  Z d  Z d d  Z d d  Z d   Z d   Z d   Z d   Z	 RS(	   s   
    Handles I/O operations between the command handler and the client
    It automatically converts the given data to bytes in Python 3.
    s   UTF-8c         C   sø   | |  _  | |  _ | |  _ y |  j j p0 |  j |  _ Wn t k
 rV |  j |  _ n Xt j   |  _ |  j j |  _ |  j j	 |  _	 t
 j d d k rÍ d t | d d  k r¾ |  j |  _	 qÍ |  j |  _	 n  | t
 j k rè t |  _ n |  j |  _ d S(   s   
        Sets up the printer

        :param in_stream: Input stream
        :param out_stream: Output stream
        :param encoding: Output encoding
        i    i   t   bt   modet    N(   t   inputt   outputt   encodingt   out_encodingt   AttributeErrort	   threadingt   RLockt   _IOHandler__lockR   R   t   syst   version_infot   getattrt   _write_bytest
   _write_strt   stdint
   safe_inputR   t   _prompt(   R   t	   in_streamt
   out_streamR)   (    (    s   ./repo/pelix/shell/beans.pyR      s"    			c         C   s6   | r# |  j  |  |  j j   n  t |  j j    S(   s   
        Reads a line written by the user

        :param prompt: An optional prompt message
        :return: The read line, after a conversion to str
        (   R   R(   R   R   R'   t   readline(   R   R   (    (    s   ./repo/pelix/shell/beans.pyR6   µ   s    c         C   s0   |  j  ! |  j j t | |  j   Wd QXd S(   s   
        Converts the given data then writes it

        :param data: Data to be written
        :return: The result of ``self.output.write()``
        N(   R.   R(   R   R    R)   (   R   t   data(    (    s   ./repo/pelix/shell/beans.pyR2   Ä   s    
c      	   C   sH   |  j  9 |  j j t | |  j  j   j |  j d d  Wd QXd S(   s   
        Converts the given data then writes it

        :param data: Data to be written
        :return: The result of ``self.output.write()``
        t   errorst   replaceN(   R.   R(   R   R   R)   t   encodet   decodeR*   (   R   R:   (    (    s   ./repo/pelix/shell/beans.pyR3   Î   s    
!c      	   O   s­   | d k r |  j d  n | s( | r= | j | |   } n  |  j W |  j |  y$ | d d k rw |  j d  n  Wn t k
 r |  j d  n XWd QX|  j   d S(   s9   
        Formats and writes a line to the output
        s   
iÿÿÿÿN(   R   R   t   formatR.   t
   IndexErrorR   (   R   t   linet   argst   kwargs(    (    s   ./repo/pelix/shell/beans.pyR   Ù   s    
c         O   sn   | d k r d } n> | s! | r6 | j | |   } n  | d d k rS | d  } n  |  j |  |  j   d S(   s9   
        Formats and writes a line to the output
        R&   iÿÿÿÿs   
N(   R   R?   R   R   (   R   RA   RB   RC   (    (    s   ./repo/pelix/shell/beans.pyR   ò   s    	N(
   R   R    R!   R   R   R6   R2   R3   R   R   (    (    (    s   ./repo/pelix/shell/beans.pyR#      s   '	
		(   i    i   i   (   R!   R/   R,   t   pelix.utilitiesR    R   t   __version_info__t   joint   __version__t   __docformat__R0   t	   raw_inputR5   R'   R   t   objectR   R#   (    (    (    s   ./repo/pelix/shell/beans.pyt   <module>   s   	L