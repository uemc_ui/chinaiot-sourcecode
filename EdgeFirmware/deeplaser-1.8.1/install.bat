@echo off
REM @author Sachin Burange
ECHO "[INFO] *** Installing  DEEPLASER Framework... ***"
call :ColorText 0C "[INFO] Previous Installation of DEEEPLASER will get removed."
set INPUT=
set /P INPUT=Are you sure? [Y/N]: %=%
echo %INPUT%
If "%INPUT%"=="y" goto usersaysyes
If "%INPUT%"=="n" goto usersaysno
If "%INPUT%"=="Y" goto usersaysyes
If "%INPUT%"=="N" goto usersaysno



:usersaysno
call :ColorText 0b "Thank you for using DEEPLASER Framework.
pause
exit 0
:end


:usersaysyes
REM Remove it from $PATH
call :ColorText 0a "[INFO]  Removing DEEPLASAER sepcific enviornment variable's from Enviornment "


REM #Unsetting earlier env variable
set DEEPLASER_HOME=

REM # Setup Deep Laser Home Directory.set
SET WORKING_DIR=%cd%

call :ColorText 0a "[INFO]  Locating Deep Laser version... "

python -c "import json; print json.load(open('conf/version.js','r'))['version']" > VERSION.txt
SET /p DEEPLASER_VERSION=<VERSION.txt

SET DL_HOME=%USERPROFILE%\deeplaser-%DEEPLASER_VERSION%
IF EXIST %DL_HOME% (
call :ColorText 0C "[INFO] DELETING DEEPLASER HOME DIRECTORY "
ECHO  %DL_HOME%
rmdir /s /q   %DL_HOME%
)

SET DEEPLASER_HOME=%USERPROFILE%\deeplaser-%DEEPLASER_VERSION%

set PATH=%PATH%;%DEEPLASER_HOME%

call :ColorText 0a  "[INFO] DEEPLASER_HOME is set to "
ECHO %DEEPLASER_HOME%


if NOT EXIST  %DEEPLASER_HOME%\repo (
call :ColorText 0a "[INFO] Did not find repo directory. Creating repo directory now."
mkdir %DEEPLASER_HOME%\repo
)
ECHO "[INFO] Copying modules to  %DEEPLASER_HOME%/repo"
xcopy repo %DEEPLASER_HOME%\repo /S /E

if NOT EXIST  %DEEPLASER_HOME%\lib (
call :ColorText 0a "[INFO] Did not find lib directory. Creating lib directory now."
mkdir %DEEPLASER_HOME%\lib
)
ECHO "[INFO] Storing third party libraries in $DEEPLASER_HOME/lib"
xcopy lib %DEEPLASER_HOME%\lib /S /E


if NOT EXIST  %DEEPLASER_HOME%\logs (
call :ColorText 0a "[INFO] Did not find deeplaser logs directory. Creating logs directory now."
mkdir %DEEPLASER_HOME%\logs
)


if NOT EXIST  %DEEPLASER_HOME%\conf (
call :ColorText 0a "[INFO] Did not find deeplaser conf directory. Creating conf directory now."
mkdir %DEEPLASER_HOME%\conf
)
ECHO "[INFO] Copying deeplaser configuraiton in $DEEPLASER_HOME/conf"
xcopy conf %DEEPLASER_HOME%\conf /S /E

if NOT EXIST  %DEEPLASER_HOME%\bin (
call :ColorText 0a "[INFO] Did not find deeplaser bin directory. Creating bin directory now."
mkdir %DEEPLASER_HOME%\bin
)

ECHO "[INFO] Copying deeplaser bin in $DEEPLASER_HOME/bin"
xcopy bin %DEEPLASER_HOME%\bin /S /E
copy start.sh  %DEEPLASER_HOME%
copy start.bat  %DEEPLASER_HOME%

call :ColorText 0C "[INFO] Pleae Create an DEEPLASER_HOME  and Set to
ECHO %DEEPLASER_HOME%
call :ColorText 0C "[INFO] Please Create a PythonPath variable add add following directories to it."
ECHO %DEEPLASER_HOME%\lib
ECHO %DEEPLASER_HOME%\repo

python.exe -c "import sys;  print(';'.join(x for x in sys.path if(x))) " >log
set /p python_path= <log
echo %python_path%

del /F setuppath.bat

ECHO SET DEEPLASER_HOME=%DEEPLASER_HOME%>>"setuppath.bat"
ECHO SET PYTHONPATH=%python_path%;%DEEPLASER_HOME%\lib;%DEEPLASER_HOME%\repo>>"setuppath.bat"

copy setuppath.bat  %DEEPLASER_HOME%
copy uninstall.bat  %DEEPLASER_HOME%


call :ColorText 0b "[INFO] Deep Laser framework has been deployed."
call :ColorText 0a "[INFO]  GOODBYE "

:end

REM #FOR COLOR TEXT
SETLOCAL DisableDelayedExpansion
for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do rem"') do (
  set "DEL=%%a"
)

goto :eof

:ColorText
echo off
<nul set /p .=. > "%~2"
findstr /v /a:%1 /R "^$" "%~2" nul
echo(%DEL%%DEL%%DEL%
del "%~2" > nul 2>&1
goto :eof
