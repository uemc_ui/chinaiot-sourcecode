{
    "base_url": "tfc/{device_type}/{device_id}",
    "devicerequest" : "DeviceRequest",
    "deviceresponse" : "DeviceResponse",
    "rest_device_request" : "device_request",
    "rest_device_response" : "device_response",
    "publish_topics": {
        "command_result": "/command/{execution_id}/result",
        "get_shadow": "/shadow/get",
        "update_shadow": "/shadow/update"
    },
    "service.pid": "com.thermofisher.deeplaser.data_processor.DeviceRequestDataProcessorService",
    "shadow_url": "$aws/things/{device_id}",
    "subscribe_topics": {
        "shadow_accepted": "/shadow/get/accepted",
        "shadow_delta": "/shadow/update/delta"
    }
}
