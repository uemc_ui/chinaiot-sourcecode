{
    "heartbeat_topic": "tfc/{device_type}/{device_id}/ping",
    "mqtt_port": 8883,
    "delimiter" : ":",
    "publishers": [
        "tfc/{device_type}/{device_id}/telemetry",
        "tfc/{device_type}/{device_id}",
        "$aws/things/{device_id}/shadow/get",
        "$aws/things/{device_id}/shadow/update",
        "tfc/{device_type}/{device_id}/command/{execution_id}/result"
    ],
    "service.pid": "com.thermofisher.deeplaser.mqtt.MqttService",
    "subscribers": [
        "tfc/{device_type}/{device_id}",
        "$aws/things/{device_id}/shadow/update/delta",
        "$aws/things/{device_id}/shadow/get/accepted"
    ]
}
