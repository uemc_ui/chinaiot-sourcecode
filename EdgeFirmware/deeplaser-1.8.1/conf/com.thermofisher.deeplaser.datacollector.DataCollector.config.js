{
    "service.pid": "com.thermofisher.deeplaser.datacollector.DataCollector",
    "subscribers": [
        {
            "destination": "IoTResponse",
            "request_type": "IoTRequest",
            "topic": "IoTRequest"
        },
        {
            "destination": "tfc/{device_type}/{device_id}/command/{execution_id}/result",
            "request_type": "DeviceRequest",
            "topic": "$aws/things/{device_id}/shadow/update/delta"
        },
        {
            "destination": "tfc/{device_type}/{device_id}/command/{execution_id}/result",
            "request_type": "DeviceRequest",
            "topic": "$aws/things/{device_id}/shadow/get/accepted"
        }
    ]
}
