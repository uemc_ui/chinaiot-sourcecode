{
    "max_retries": 5,
    "ping_rate": 60,
    "service.pid": "com.thermofisher.deeplaser.heartbeat.MQTTHeartbeatService",
    "timeout": 60,
    "network_latency" : 5
}
