{
    "certificates": [
        {
            "group": "device",
            "id": "root_pem",
            "location": "/home/pi/certification-prod.deeplaser/rootca.pem"
        },
        {
            "group": "device",
            "id": "device_certificate",
            "location": "/home/pi/certification-prod.deeplaser/certificate.pem.cer"
        }
    ],
    "device_certificate_id": "1b83f20c29fa061472d0b4a1d60df2a3a4ea35c44a4a4d8abb56c52511ef4565",
    "device_shared_token": "MODBOX||Model-2016-01||123223",
    "service.pid": "com.thermofisher.deeplaser.certificate.CertificateManagerService"
}
