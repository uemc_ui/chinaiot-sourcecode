#@author Sachin Burange
if test -f setuppath.sh 
then
    source setuppath.sh
fi

echo "[INFO] Deeplaser Home is set to $DEEPLASER_HOME"
echo "[INFO] Python path is set to $PYTHONPATH"

read -r -p "Are you sure? [y/N] " response
if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]
then
# Setup Deep Laser Home Directory.
if [  -d "$DEEPLASER_HOME" ]; then
   echo "[INFO] Found deeplaser install directory. Deleting it"
   rm -rf $DEEPLASER_HOME
fi

echo "[INFO] Removing DeepLaser libraries from PYTHONPATH"
# Remove DeepLaser libraries from PYTHONPATH
python -c "import sys; sys.path.remove('$DEEPLASER_HOME/lib')"
unset PYTHONPATH
export PYTHONPATH=$(python -c "import sys; print(':'.join(x for x in sys.path if x))")
export PATH="$PYTHONPATH":$PATH

#Unsetting earlier env variable
echo "[INFO] Removing DEEPLASER specific environment variable's from shell environment."
unset DEEPLASER_HOME
echo "[INFO] Deeplaser has successfully been uninstalled."

else
    echo "[INFO] No changes made. Exiting now."
fi
