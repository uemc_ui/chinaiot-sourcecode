#!/bin/bash

if test -f setuppath.sh 
then
    source setuppath.sh
fi

echo "[INFO] Deeplaser Home is set to $DEEPLASER_HOME"
echo "[INFO] Python path is set to $PYTHONPATH"

if test -z "$DEEPLASER_HOME"
then
  echo
  echo "[ERROR] the system environment variable DEEPLASER_HOME is not defined!"
  echo
  exit
fi

bash $DEEPLASER_HOME/bin/deeplaser-start $*