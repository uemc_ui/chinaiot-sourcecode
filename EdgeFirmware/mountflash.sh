#!/bin/bash

if [ -b "/dev/usbmemorystick" ]
then
    /home/pi/Desktop/FwUpdScript/flashled.sh &
    mkdir /media/flash
    chmod 777 /media/flash
    chown pi /media/flash
    mount -o uid=pi /dev/usbmemorystick /media/flash

    #Check for HashList.txt existence in USB
    if [ -f /media/flash/Haslist.txt ]
    then            
        echo 'Hash file found in usb device'
    else            
        echo 'Hash file not found in usb device'
        exit            
    fi  

  
#   if /usr/bin/sha256sum -c /media/flash/Haslist.txt 2>&1 | grep OK; then

   echo 'The Hash key matched'
   
   echo 'remove existing file in modbox'
   rm -rf /home/pi/Desktop/EdgeFirmware

   echo 'exstract the file'
   for a in /media/flash/EdgeFirmware.tar.gz
   do
      tar -xzf "$a" -C  /media/flash/
    done

   sleep 10

   echo 'copy text file from usb to desktop'      
   cp -avr /media/flash/EdgeFirmware /home/pi/Desktop/
   chmod 777 /home/pi/Desktop/EdgeFirmware/*
   sleep 45
   sudo reboot

#else
    echo 'The Hash key is not matched : usb software upgrade fail'
    #exit
#fi

fi
