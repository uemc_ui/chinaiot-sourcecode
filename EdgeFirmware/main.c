#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "i2c_BQ24295.h"
#include <wiringSerial.h>
#include <sched.h>
#include <pthread.h>
#include <linux/spi/spidev.h>
#include <math.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <math.h>
#include <netinet/tcp.h>

/*
    C ECHO client example using sockets
*/
#include<sys/socket.h>  //socket
#include<arpa/inet.h>   //inet_addr
int sock;
int sockRetVal;
int sockfd;
struct sockaddr_in server;
char server_reply[2000];


//Disable all printf's
#define DEBUG

/*If Enable the battery uncomment the line 
After compile reboot the Devicelink*/ 
//#ifdef BAT_ENABLE 

#define NORMAL 			1
#define LOGGING 		2
#define ALARM	 		3
#define BATTERY 		4
#define ALARMANDBATT		5
#define ALARMANDLOG		6
#define BATTLOG			7
#define ALARMBATTLOG		8


#define  RLED 			27
#define  GLED 			22
#define  YLED   		26
#define  BUZZR   		12
#define  SWIN   		5
#define  ADC_RST   		14
#define  ADC_CS 		16
#define  ADC_STRT 		18
#define  ADC_CALIB1 		6
#define  ADC_CALIB2 		15
#define  ADC_CALIB3		23

#define  DIR_IN	         	0
#define  DIR_OUT	     	1


#define   PIN_H			1
#define   PIN_L			0


//New hardware
#define CALIB_RESISTOR_LOW_VALUE         (560.0F)
#define CALIB_RESISTOR_MID_VALUE         (750.0f)
#define CALIB_RESISTOR_HIGH_VALUE        (1000.0f)


float B0_Value;
float B1_Value;
float B2_Value;

#define device "/dev/spidev1.0"

//ADC Registers configuration
uint8_t GPIOCFG_TX[3]    = {0X4C,0X00,0X03},GPIOCFG_RX[3];
uint8_t GPIODIR_TX[3]    = {0X4D,0X00,0X00},GPIODIR_RX[3];
uint8_t GPIODATLOW_TX[3] = {0X4E,0X00,0X00},GPIODATLOW_RX[3];
uint8_t GPIODATHIG_TX[3] = {0X4E,0X00,0X03},GPIODATHIG_RX[3];


//Transmit
uint8_t setmux0_tx[3]  = {0x40,0x00,0x01},setmux0_rx[3]; 
uint8_t setmux1_tx[3]  = {0x42,0x00,0x30},setmux1_rx[3];   //Internal reference selected
uint8_t setsys0_tx[3]  = {0x43,0x00,0x02},setsys0_rx[3]; 
uint8_t setidac0_tx[3] = {0x4A,0x00,0x08},setidac0_rx[3];  //0x08
uint8_t setidac1_tx[3] = {0x4B,0x00,0xFF},setidac1_rx[3];  //0xFF
uint8_t setdatac_tx[1] = {0x16},setdatac_rx[1];
uint8_t readdata_tx[4] = {0x12,0xFF,0xFF,0xFF},readdata_rx[4];
uint8_t sync_tx[1]     = {0x04},sync_rx[4];
uint8_t resetcmd_tx[1] = {0x06},resetcmd_rx[4];


#define  A_COEFF                    ((double)(0.0039083))
#define  B_COEFF                    ((double)(-0.0000005775))
#define  C_COEFF                    ((double)(-0.000000000004183))
#define  DENOMINATOR   ((double)((A_COEFF) + (2*B_COEFF*t1) - (300 * C_COEFF * t1 * t1) + (4 * C_COEFF * t1 * t1 * t1)))
#define  SUCC_APPROX_CNT              3

#define  VALUE_MAX 30
#define  PT100_RES_AT_ZERO_DEG       100.0F
#define  PT1000_RES_AT_ZERO_DEG      1000.0F


uint8_t rddata[18] = {0x20,0x14,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF},adcread[18];
uint8_t recvdata[3];

typedef struct Webserver_Struct
{
    char parametersBuffer[40];
    int index;
} Qt_WebServer_Sock;


Qt_WebServer_Sock Client_Webserver;

typedef enum
{

  PT1000 = 0,
  PT100,

  MAX_NUM_OF_SENS_TYPES

}Sensor_Types;


float AdcVal_V,AdcVal_I; //,avg;
float RTDResistance,Voltage,temp;
static uint32_t mode;
static uint8_t bits = 8;
static uint32_t speed = 500000;
static uint16_t delay;


char cabinetTemp[20];
char setPointTemp[20];
char alarmsStrings[20][100];
int totalAlarmsCount;
char modeOfOper[25];
char totalAlarmString[1000];

extern int alarmModeSetFlag;
extern int batteryModeSetFlag;
extern int loggingModeSetFlag;
extern int alarmBatModeSetFlag;
extern int alarmLogModeSetFlag;
extern int alarmBatLogModeSetFlag;
extern int batteryLoggingModeSetFlag;

int alarmWithBatteryFlag;
int alarmWithLoggingFlag;
int loggingWithBatteryFlag;
int alarmBatteryLogFlag;

extern int currentMode;
char timeinUTC[50];
static DRV_IFACE_t bq2x;
static DRVD_BQ24295_t bq2x_data;
char external_temp[sizeof(float)];
char tempBuff[10];
float actual_temp;
extern int guiUpdateFile(void);
extern int readycount;
extern int CloudPost_4min(int count);
extern int postToCloud(int counter);
int alarmActiveFlag = 0;
int timeinsecond;
int sdtimeinsecond;
int battrymodebuttonpress = 0;
int LedstatusFlg=0;
float setHighTemp;
float setLowTemp;
float setpoint;
int fd_spi=0;
#define CUSTOMERTD 1
extern void * check_batterystatus(void * arg);	//need to add logic to update into qt gui

float B0_Value,B1_Value,B2_Value;
float Low_Value,Mid_Value,High_Value,Sensor_Resistance;
float Sensor_Res,B0_Val,B1_Val,B2_Val,finalsensor_Res;
float RTD_B0_VALUE,RTD_B1_VALUE,RTD_B2_VALUE;
float RTD_Rx0_VALUE,RTD_Rx1_VALUE,RTD_Rx2_VALUE;

/////////////////SET GPIO //////////////////////////
#define PIN   5 /* P1-18 */
#define POUT  4  /* P1-07 */
#define IN    0
#define OUT   1
#define LOW   0
#define HIGH  1


int keypress = 0;
int startAlarm = 0;
pthread_t thread, thread1, thread2;
int NETWORKstatusFlag = 0;
int BatterystatusFlag = 0;
char RTDBuffer[20];

int writeToSocket(int index, char* data);


/*
 ============================================================================
 function Name	: transfer
 return type	: int
 Description	: This function is used to transfer the SPI data
 ============================================================================
*/
static int transfer(int fd, uint8_t const *tx, uint8_t const *rx, size_t len)
{
	int ret = -1;
	int out_fd;	
	struct spi_ioc_transfer tr = {
		.tx_buf = (unsigned long)tx,
		.rx_buf = (unsigned long)rx,
		.len = len,
		.delay_usecs = delay,
		.speed_hz = speed,
		.bits_per_word = bits,
	};


	if (mode & SPI_TX_QUAD)
		tr.tx_nbits = 4;
	else if (mode & SPI_TX_DUAL)
		tr.tx_nbits = 2;
	if (mode & SPI_RX_QUAD)
		tr.rx_nbits = 4;
	else if (mode & SPI_RX_DUAL)
		tr.rx_nbits = 2;
	if (!(mode & SPI_LOOP)) {
		if (mode & (SPI_TX_QUAD | SPI_TX_DUAL))
			tr.rx_buf = 0;
		else if (mode & (SPI_RX_QUAD | SPI_RX_DUAL))
			tr.tx_buf = 0;
	}


	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
	if (ret < 1)
		printf("can't send spi message");

	return ret;
}

/*
 ============================================================================
 function Name  : GPIOREAD
 return type    : int
 Description    : read the GPIO pin status 
 ============================================================================
*/
static int GPIORead(int pin)
{
	#define VALUE_MAX 30
	char path[VALUE_MAX];
	char value_str[3];
	int fd;
 
	snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
	fd = open(path, O_RDONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open gpio value for reading!\n");
		return(-1);
	}
 
	if (-1 == read(fd, value_str, 3)) {
		fprintf(stderr, "Failed to read value!\n");
		return(-1);
	}
	close(fd);
	return(atoi(value_str));
}

/*
 ============================================================================
 function Name  : GPIOExport
 return type    : int
 Description    : export thr GPIO pins 
 ============================================================================
*/
static int GPIOExport(int pin)
{
    	#define BUFFER_MAX 3
	char buffer[BUFFER_MAX];
	ssize_t bytes_written;
	int fd;
 
	fd = open("/sys/class/gpio/export", O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open export for writing!\n");
		return(-1);
	}
	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
	write(fd, buffer, bytes_written);
	close(fd);
	return(0);
}
 
/*
 ============================================================================
 function Name  : GPIOUnexport
 return type    : int
 Description    : unexport the GPIO pins 
 ============================================================================
*/
static int GPIOUnexport(int pin)
{
	char buffer[BUFFER_MAX];
	ssize_t bytes_written;
	int fd;
 
	fd = open("/sys/class/gpio/unexport", O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open unexport for writing!\n");
		return(-1);
	}
 
	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
	write(fd, buffer, bytes_written);
	close(fd);
	return(0);
}
 

/*
 ============================================================================
 function Name  : GPIODirection
 return type    : int
 Description    : set the GPIO pins direction (IN/OUT)
 ============================================================================
*/
static int GPIODirection(int pin, int dir)
{
	static const char s_directions_str[]  = "in\0out";
 	#define DIRECTION_MAX 35
	char path[DIRECTION_MAX];
	int fd;
 
	snprintf(path, DIRECTION_MAX, "/sys/class/gpio/gpio%d/direction", pin);
	fd = open(path, O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open gpio direction for writing!\n");
		return(-1);
	}
 
	if (-1 == write(fd, &s_directions_str[IN == dir ? 0 : 3], IN == dir ? 2 : 3)) {
		fprintf(stderr, "Failed to set direction!\n");
		return(-1);
	}
 
	close(fd);
	return(0);
}
 

/*
 ============================================================================
 function Name  : GPIOWrite
 return type    : int
 Description    : set the GPIO pins value (High/Low) 
 ============================================================================
*/
static int GPIOWrite(int pin, int value)
{
	static const char s_values_str[] = "01";
 
	char path[VALUE_MAX];
	int fd;
 
	snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
	fd = open(path, O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open gpio value for writing!\n");
		return(-1);
	}
	if (1 != write(fd, &s_values_str[LOW == value ? 0 : 1], 1)) {
		fprintf(stderr, "Failed to write value!\n");
		return(-1);
	}
	close(fd);
	return(0);
}

/*
 ============================================================================
 function Name  : spi_init
 return type    : void
 Description    : Initilize the SPI bus configurations
 ============================================================================
*/
void spi_init(void)
{

    	int ret=0;
    	#ifdef DEBUG
    	printf("spi init enter\n");
    	#endif
    
	//open the spi device
	fd_spi = open(device, O_RDWR);
	if (fd_spi < 0)
		printf("spi device open failed\n");

	//spi mode	 
	ret = ioctl(fd_spi, SPI_IOC_WR_MODE32, &mode);
	if (ret == -1)
		printf("can't set spi mode\n");

	ret = ioctl(fd_spi, SPI_IOC_RD_MODE32, &mode);
	if (ret == -1)
		printf("can't get spi mode");
	
	//bits per word
	ret = ioctl(fd_spi, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
		printf("can't set bits per word");

	ret = ioctl(fd_spi, SPI_IOC_RD_BITS_PER_WORD, &bits);
	if (ret == -1)
		printf("can't get bits per word");
	
	ret = ioctl(fd_spi, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		printf("can't set max speed hz");

	ret = ioctl(fd_spi, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		printf("can't get max speed hz");

	#ifdef DEBUG
	printf("spi mode: 0x%x\n", mode);
	printf("bits per word: %d\n", bits);
	printf("max speed: %d Hz (%d KHz)\n", speed, speed/1000);
	printf("spi init exit\n");
	#endif

}

/*
 ============================================================================
 function Name  : adc_reset
 return type    : void
 Description    : reset the adc module
 ============================================================================
*/
void adc_reset()
{
	//Reset the ADC module
	GPIOWrite(ADC_RST,PIN_H);
	sleep(1);
	GPIOWrite(ADC_RST,PIN_L);
	sleep(1);
	GPIOWrite(ADC_RST,PIN_H);
	sleep(1);
}


/*
 ===========================================================================
 function Name  : Calc_temp_below_zero_deg
 return type    : double
 Description    : calculate the below zero temperature values
 ============================================================================
*/
double Calc_temp_below_zero_deg( float calc_Res, float zero_degree_res)
{
  
	uint8_t Index = 0;  
  	float t1= 0.0F;
  	float tn = 0.0F;
  
  	if(zero_degree_res != 0)
  	{
    		t1 =  ((calc_Res/zero_degree_res) - 1 )/(A_COEFF + (4* B_COEFF));
    		for(Index = 0; Index < SUCC_APPROX_CNT  ; Index++)
    		{
      			tn = t1 - ((1 + (double)(A_COEFF * t1) + (double)(B_COEFF * t1 * t1) + \
                    	(double)(( C_COEFF * t1 * t1 *t1 ) * (t1 - 100)) - (calc_Res/zero_degree_res))/(DENOMINATOR));  
      			t1 = tn;
    		}
      
  	} 
   	return tn;  
}

/*
 =========================================================================== 
 function Name  : calculate_temperature
 return type    : float
 Description    : calculate the temperature
 ============================================================================
*/
float calculate_temperature(float       *Final_Res_Value, 
                                 Sensor_Types Sensor_Type)
{
	
	float temperature;	  
  	float Temp = 0.0F;
  	float zero_degree_res = 1.0F , Calc_Res = 0.0F;
  
  	if(Final_Res_Value != 0)  // Sanjay NULL_PTR to zero
  	{
     		Calc_Res = *Final_Res_Value;
     
     	switch(Sensor_Type)
     	{  
       		case PT1000 :
            		zero_degree_res  = PT1000_RES_AT_ZERO_DEG;
       			break;
       		case PT100 :
            		zero_degree_res  =  PT100_RES_AT_ZERO_DEG;
       			break;
       		default :
       			break;
     	}  
     
    	if(Calc_Res > zero_degree_res)
    	{   
      		Temp = (double)(sqrt( (double)((double)(A_COEFF * A_COEFF) - (double) (4 * B_COEFF * (1 - (Calc_Res/zero_degree_res))))) - A_COEFF);
      		Temp = Temp/(2 * B_COEFF) ;
    	} 
    	else if(Calc_Res < zero_degree_res)
    	{
      		Temp = Calc_temp_below_zero_deg(Calc_Res,zero_degree_res);
    	}
    	else
    	{
      		Temp = 0.0F;
    	}
 
  }
      
  return Temp;

}

/*
 =========================================================================== 
 function Name  : adc_gpioreinit
 return type    : void
 Description    :This function is reset the gpio pins when adc error hang condition
 ============================================================================
*/
void adc_gpioreinit()
{

	GPIOExport(ADC_RST);   		// Adc_Reset pin
        GPIODirection(ADC_RST,DIR_OUT);

        GPIOExport(ADC_CS);   		// Adc chip slection pin
        GPIODirection(ADC_CS,DIR_OUT);

        GPIOExport(ADC_STRT);   	// Adc_Start pin
        GPIODirection(ADC_STRT,DIR_OUT);
        GPIOWrite(ADC_STRT,PIN_H);

        GPIOExport(ADC_CALIB1);
        GPIODirection(ADC_CALIB1,DIR_OUT);

        GPIOExport(ADC_CALIB2);
        GPIODirection(ADC_CALIB2,DIR_OUT);

        GPIOExport(ADC_CALIB3);
        GPIODirection(ADC_CALIB3,DIR_OUT);

}

/*
 =========================================================================== 
 function Name  : adc_start
 return type    : void
 Description    : start the adc conversion
 ============================================================================
*/
void adc_start(void)
{ 
	//ADC start the conversion
	GPIOWrite(ADC_STRT , PIN_H);
	sleep(1);
	GPIOWrite(ADC_STRT , PIN_L);
	sleep(1);
	GPIOWrite(ADC_STRT , PIN_H);
}

/*
 =========================================================================== 
 function Name  : adc_defaultconfig
 return type    : void
 Description    : configure the adc module
 ============================================================================
*/
void adc_defaultconfig()
{

	double temp;
	int val1=0,val2=0,i=0;
	int ret,ret1,ret2,ret3,ret4,ret5,ret6;
	int Adcval;
	float tmp,voltage,temperature;
  
	GPIOWrite( ADC_CS , PIN_L);
	sleep(.5);
	GPIOWrite( ADC_STRT , PIN_H);
	
	ret = transfer(fd_spi, setmux0_tx, setmux0_rx, sizeof(setmux0_tx));
	if(ret < 0)
		printf("comunciation failed\n");

	ret = transfer(fd_spi, setmux1_tx, setmux1_rx, sizeof(setmux1_tx));
	if(ret < 0)
		printf("comunciation failed\n");
			
	ret = transfer(fd_spi,  setsys0_tx, setsys0_rx, sizeof( setsys0_tx));
	if(ret < 0)
		printf("comunciation failed\n");

	ret = transfer(fd_spi,  setidac0_tx, setidac0_rx, sizeof(setidac0_tx));
	if(ret < 0)
		printf("comunciation failed\n");

    	ret = transfer(fd_spi,  setidac1_tx, setidac1_rx, sizeof(setidac1_tx));
	if(ret < 0)
		printf("comunciation failed\n");

	ret = transfer(fd_spi, setdatac_tx, setdatac_rx, sizeof(setdatac_tx));
	if(ret < 0)
		printf("comunciation failed\n");

	ret = transfer(fd_spi, rddata, recvdata, sizeof(rddata));
	if(ret < 0)
		printf("comunciation failed\n");

	GPIOWrite( ADC_CS ,  PIN_H);

}

/*
 =========================================================================== 
 function Name  : adc_voltage_read
 return type    : float
 Description    : read the differential voltage
 ============================================================================
*/
float adc_voltage_read()
{

	int i=0,j=0,ret=0,cnt=0;
	float AvgVoltage=0,SecondAvgVoltage=0,AvgVoltage390=0,resistance=0,current390=0,temp;

    	GPIOWrite(ADC_CS,PIN_L);
	sleep(.1);
	GPIOWrite(ADC_STRT,PIN_H);
     
	setmux0_tx[2] = 0x02;  
    	ret= transfer(fd_spi, setmux0_tx, setmux0_rx, sizeof(setmux0_tx));
	if(ret < 0)
		printf("spi comunciation failed in adc_voltage_read function_0x02\n");
		
	setsys0_tx[2] = 0x22;	
	ret=transfer(fd_spi,  setsys0_tx, setsys0_rx, sizeof( setsys0_tx));
	if(ret < 0)
		printf("spi comunciation failed in adc_voltage_read function_0x22\n");

	ret=transfer(fd_spi, rddata, adcread, sizeof(rddata));
	if(ret < 0)
		printf("spi comunciation failed in adc_voltage_read function\n");	
	  
	GPIOWrite(ADC_CS,PIN_H);
	GPIOWrite(ADC_STRT,PIN_L);
      	
	adc_start();       

	GPIOWrite(ADC_CS,PIN_L);
	sleep(.1);     
	transfer(fd_spi, readdata_tx, readdata_rx, sizeof(readdata_tx));
	GPIOWrite(ADC_CS,PIN_H); 
       
	AdcVal_V=0;
	AdcVal_V = (float)(readdata_rx[1] << 16 | readdata_rx[2] << 8 | readdata_rx[3]);
	#ifdef DEBUG
	//printf(" AdcVal_V = %f\n", AdcVal_V);
	#endif

	return AdcVal_V;

}

/*
 =========================================================================== 
 function Name  : adc_current_read
 return type    : float
 Description    : read the differential current
 ============================================================================
*/
float adc_current_read()
{
	
	int i=0,j=0,ret=0,cnt=0;
	float AvgVoltage=0,SecondAvgVoltage=0,AvgVoltage390=0,resistance=0,current390=0,temp;
 
 
	GPIOWrite(ADC_CS,PIN_L);
	sleep(.1);
	GPIOWrite(ADC_STRT,PIN_H);
     
	setmux0_tx[2] = 0x13;
	ret=transfer(fd_spi, setmux0_tx, setmux0_rx, sizeof(setmux0_tx));
	if(ret < 0)
		printf("spi comunciation failed in adc_current_read function_0x13\n");

    	setsys0_tx[2] = 0x42;		
    	transfer(fd_spi,  setsys0_tx, setsys0_rx, sizeof( setsys0_tx));
	if(ret < 0)
		printf("spi comunciation failed in adc_current_read function_0x42\n");
       
	transfer(fd_spi, rddata, adcread, sizeof(rddata));      		
   
    	GPIOWrite(ADC_CS,PIN_H);
	GPIOWrite(ADC_STRT,PIN_L);	
    
    	adc_start();

	GPIOWrite(ADC_CS,PIN_L);
    	sleep(.1); 
    	transfer(fd_spi, readdata_tx, readdata_rx, sizeof(readdata_tx));
	GPIOWrite(ADC_CS,PIN_H); 
                  	
    	AdcVal_I=0;
    	AdcVal_I = readdata_rx[1] << 16 | readdata_rx[2] << 8 | readdata_rx[3];
    	#ifdef DEBUG
    	//printf("AdcVal_I = %f\n",AdcVal_I);
    	#endif

	return AdcVal_I;

}

/*
 =========================================================================== 
 function Name  : read_calibration_560
 return type    : float
 Description    : read the calibration resister(560) resistance
 ============================================================================
*/
float read_calibration_560()
{
	float calib1_v,calib1_c,Resistance_calib1;
	int i;

    	GPIOWrite( ADC_CALIB2,  PIN_L);   //15
    	GPIOWrite( ADC_CALIB1,  PIN_H);   //6
    	GPIOWrite( ADC_CALIB3,  PIN_H);   //23
     
	calib1_v = adc_voltage_read();
	calib1_c = adc_current_read();
	
	Resistance_calib1 = (390.0F * (16.0F/4.0F)*(calib1_v/calib1_c));
	#ifdef DEBUG
	printf("Resistance_calib1_560 = %f\n",Resistance_calib1);
	#endif
		
	return Resistance_calib1;
		
}

/*
 =========================================================================== 
 function Name  : read_calibration_750
 return type    : float
 Description    : read the calibration resister(750) resistance
 ============================================================================
*/
float read_calibration_750()
{
	float calib2_v,calib2_c,Resistance_calib2;
	int i;
	
    	GPIOWrite( ADC_CALIB2,  PIN_H);   //15
    	GPIOWrite( ADC_CALIB1,  PIN_L);   //6
    	GPIOWrite( ADC_CALIB3,  PIN_H);   //23
    	
	calib2_v = adc_voltage_read();
	calib2_c = adc_current_read();
	Resistance_calib2 = (390.0F * (16.0F/4.0F)*(calib2_v/calib2_c));
	#ifdef DEBUG
	printf("Resistance_calib2_750 = %f\n",Resistance_calib2);
	#endif
		
	return Resistance_calib2;
			
}

/*
 =========================================================================== 
 function Name  : read_calibration_1000
 return type    : float
 Description    : read the calibration resister(1000) resistance
 ============================================================================
*/
float read_calibration_1000()
{
	float calib3_v,calib3_c,Resistance_calib3;
	int i;
			
    	GPIOWrite( ADC_CALIB2,  PIN_H);   //15
    	GPIOWrite( ADC_CALIB1,  PIN_H);   //6
    	GPIOWrite( ADC_CALIB3,  PIN_H);   //23
      
	calib3_v = adc_voltage_read();
	calib3_c = adc_current_read();
	Resistance_calib3 = (390.0F * (16.0F/4.0F)*(calib3_v/calib3_c));
	#ifdef DEBUG
	printf("Resistance_calib3_1000 = %f\n",Resistance_calib3);
	#endif
		
	return Resistance_calib3;
		
}

/*
 =========================================================================== 
 function Name  : read_rtd_resistance
 return type    : float
 Description    : read the RTD sensor resistance
 ============================================================================
*/
float read_rtd_resistance()
{

	
    	float rtd_v,rtd_c,Resistance_rtd;
    	int i;
	
    	GPIOWrite( ADC_CALIB2,  PIN_L);   //15
    	GPIOWrite( ADC_CALIB1,  PIN_L);   //6
    	GPIOWrite( ADC_CALIB3,  PIN_H);   //23
    
    	rtd_v = adc_voltage_read();
    	rtd_c = adc_current_read();
    	Resistance_rtd = (390.0F * (16.0F/4.0F)*(rtd_v/rtd_c));
    	#ifdef DEBUG
    	//printf("Resistance_rtd = %f\n",Resistance_rtd);
    	#endif
		
	return Resistance_rtd;
		
}

/*
 =========================================================================== 
 function Name  : adcdata_read
 return type    : int
 Description    : remove the calibration error
 ============================================================================
*/
int adcdata_read()
{
	
   	int timecount=0,n=0,i=0,j=0,k=0,suden_cntr=0;	
   	float Def_Low_Value=0,Def_Mid_Value=0,Def_High_Value=0;
   	int  res_arr[10];
	int inf_flag,nan_flag;
   	float Final_Rtd_resistance[10];
    
   	Def_Low_Value  = CALIB_RESISTOR_LOW_VALUE ;   
   	Def_Mid_Value  = CALIB_RESISTOR_MID_VALUE ;   
   	Def_High_Value = CALIB_RESISTOR_HIGH_VALUE ;
    
   	adc_defaultconfig();

	read_calibres:
   
   	Low_Value =  read_calibration_560();	//Read the calibration resistance from 560 resistor 
   	Mid_Value =  read_calibration_750();	//Read the calibration resistance from 750 resistor 
   	High_Value = read_calibration_1000();	//Read the calibration resistance from 1000resistor 

	inf_flag=0;
	nan_flag=0;

	//This condition set flag when calibration resistance infinite or not
	if((isinf(Low_Value) != 0) || (isinf(Mid_Value) != 0) || (isinf(High_Value) !=0) )
        {
		#ifdef DEBUG 
                printf("isinf True\n");
		#endif
                inf_flag=1;
        }
        else
        {
                 inf_flag=0;
        }
	//This condition set flag when calibration resistance nan or not
	if((isnan(Low_Value) !=0) || (isnan(Mid_Value) !=0) || (isnan(High_Value) !=0) )
        {
		#ifdef DEBUG 
                printf("isnan True\n");
		#endif
                nan_flag=1;

        }
        else
        {
                nan_flag=0;

        }
	//This condition check the calibration resistance values within the range or not
	if((inf_flag != 1) && (nan_flag != 1))
        {
		if(Low_Value > 570 || Mid_Value > 760 || High_Value > 1010) 
		{
			#ifdef DEBUG 
			printf("Invalid calibration resistors values\n");
			#endif
			sprintf(external_temp,"NA");                       	
                	writeToSocket(14,external_temp);
			adc_gpioreinit();			//Reset the all adc gpio pins
			adc_reset();				//Reset the adc reset gpio pin
			adc_defaultconfig();			//Set the register configuration
			goto read_calibres;
		}
	}
	else
        {
		#ifdef DEBUG 
		printf("got the inf and nan values\n");
		#endif	
		sprintf(external_temp,"NA");                       	
                writeToSocket(14,external_temp);
		adc_gpioreinit();
		adc_reset();
		adc_defaultconfig();
               	goto read_calibres;
        }
   

   	RTD_Rx0_VALUE  =  Low_Value;   //CALIB_RESISTOR_LOW_VALUE ;   
   	RTD_Rx1_VALUE  =  Mid_Value;   //CALIB_RESISTOR_MID_VALUE ;   
   	RTD_Rx2_VALUE  =  High_Value;  //CALIB_RESISTOR_HIGH_VALUE ;  
   	RTD_B0_VALUE   =  CALIB_RESISTOR_LOW_VALUE ;   
   	RTD_B1_VALUE   =  1.0F ;                      
   	RTD_B2_VALUE    = 0.0F ; 

	restart_adc:

     	while(1)
     	{
		for(i=0;i<5;i++)
		{
			Sensor_Resistance = read_rtd_resistance();	//Read the RTD sensor resistance
			B0_Val = RTD_B0_VALUE;
			B1_Val = (Sensor_Resistance - RTD_Rx0_VALUE*RTD_B1_VALUE);
			B2_Val = (Sensor_Resistance - RTD_Rx0_VALUE)*(Sensor_Resistance - RTD_Rx1_VALUE)*RTD_B2_VALUE; 
			Sensor_Res = B0_Val + B1_Val + B2_Val;
			Final_Rtd_resistance [i] = Sensor_Res;             
			#ifdef DEBUG
			//printf("Final_Rtd_Resistance = %f\n",Final_Rtd_resistance [i]);
			#endif
			//This condition chekc the final rtd resistance is infinite or nan
			if((isnan(Final_Rtd_resistance[i]) == 0) || (isinf(Final_Rtd_resistance[i]) == 0 )) 
			{
				//This condition check the final rtd resistance within the range or not
				if((Final_Rtd_resistance[i] > 1400)||(Final_Rtd_resistance[i] < 180)) 
				{
					printf("Temperature out off range\n");
					sprintf(external_temp,"NA");                       	
                			writeToSocket(14,external_temp);			
                			adc_reset();				//Reset the adc reset gpio pin
                			adc_defaultconfig();			//Set the register configuration
                			goto restart_adc;			
                			break;
		     		}
			}
			else
			{		
					#ifdef DEBUG
					printf("Final_Rtd_Resistance is inf or nan values\n");
					#endif
					sprintf(external_temp,"NA");                       	
               				writeToSocket(14,external_temp);
					adc_gpioreinit();			//Reset the all adc gpio pins
					adc_reset();				//Reset the adc reset gpio pin
					adc_defaultconfig();			//Set the register configuration
					goto read_calibres;
                			break;
			}
		}
	    	for(k=0;k<4;k++)
	    	{
			//This condition check the current sample and previous sample not greater thant 50 resistance 
			if(fabsf(Final_Rtd_resistance[k] - Final_Rtd_resistance[k+1]) > 50)
			{
			
				#ifdef DEBUG
				printf("Resistance sudden change\n");
				#endif
				adc_gpioreinit();				//Reset the all adc gpio pins
				adc_reset();					//Reset the adc reset gpio pin
				adc_defaultconfig();				//Set the register configuration
				goto read_calibres;
				break;
			} 
			else
			{       
				finalsensor_Res = Final_Rtd_resistance[k];
				actual_temp = calculate_temperature(&finalsensor_Res, PT1000);	//Calculate the temperature from rtd sensor resistance
				sprintf(setPointTemp, "%f",setpoint);
				snprintf(external_temp,2*sizeof(float),"%f",actual_temp);
				#ifdef DEBUG
				//printf("external_temp = %s\n",external_temp);
				#endif
				if(k==3)
				{
					writeToSocket(15,external_temp);			
				}
				if(n < 0)
				{
					error("ERROR writing to socket in adcdata read");
				}
            		}

		}

	}		
}
	
/*
 =========================================================================== 
 function Name  : writeToSocket
 return type    : int
 Description    : write data to sockt for displayig QT screen
 ============================================================================
*/
int writeToSocket(int index, char* data)
{
	Client_Webserver.index=index;						
	strcpy(Client_Webserver.parametersBuffer,data);					
	sockRetVal = send(sock,(Qt_WebServer_Sock*)&Client_Webserver,sizeof(Client_Webserver),MSG_DONTWAIT);
	if(sockRetVal < 0)
	{
		printf("ERROR writing to socket in webserver socket\n");
	}
	sleep(1);
	return sockRetVal;
}

/*
 =========================================================================== 
 function Name  : deeplaser
 return type    : void
 Description    : read the wifi signal strength 
 ============================================================================
*/
void * deeplaser(void * arg)
{

	printf("Created Thread for pushing data to cloud\n");
	while(1)
	{
		sleep(59);
	 	//readWifiSignal();
	}
	
}

/*
 =========================================================================== 
 function Name  : aapstatus_check
 return type    : void
 Description    : check the application status for watchdog configuration
 ============================================================================
*/
void * appstatus_check(void * arg)
{

	FILE *fp=0;
	while(1)
	{
		sleep(3);
		fp = fopen("/home/pi/Desktop/EdgeFirmware/miscfiles/appstatus_check.txt","w+");
		if(fp==NULL){
                	printf("Failed to create appstatus_check.txt file\n");         
		}      
		else
		{
			fprintf(fp,"%s\n","application_alive");
			fclose(fp);
		}
	}	
		
}

/*
 =========================================================================== 
 function Name  : LED_init
 return type    : void
 Description    : Initilize the GPIO's (Led's,buzzer,Switch,ADC etc)
 ============================================================================
*/
void LED_init(void)
{

	#ifdef DEBUG
	printf("Enter LED init function \n");
     	#endif
     	
    	//Export GPIO's 	
	GPIOExport(RLED);  //27
	GPIOExport(GLED);  //22
	GPIOExport(YLED);  //26
	GPIOExport(BUZZR); //12
	GPIOExport(SWIN);  //5
	
	//Set GPIO direction
	GPIODirection(RLED,DIR_OUT);
	GPIODirection(GLED,DIR_OUT);
	GPIODirection(YLED,DIR_OUT);
	GPIODirection(BUZZR,DIR_OUT);
	GPIODirection(SWIN,DIR_IN);

	//ADC pins configuration
	#ifdef DEBUG
	printf("gpio init\n");
	#endif
	
	GPIOExport(ADC_RST);   // Adc_Reset pin
	GPIODirection(ADC_RST,DIR_OUT);
	  
	GPIOExport(ADC_CS);   // Adc chip slection pin
	GPIODirection(ADC_CS,DIR_OUT);
	
	GPIOExport(ADC_STRT);   // Adc_Start pin
	GPIODirection(ADC_STRT,DIR_OUT);
	GPIOWrite(ADC_STRT,PIN_H);
	
	GPIOExport(ADC_CALIB1);
	GPIODirection(ADC_CALIB1,DIR_OUT);
	
	GPIOExport(ADC_CALIB2);
	GPIODirection(ADC_CALIB2,DIR_OUT);
	 
	GPIOExport(ADC_CALIB3);
	GPIODirection(ADC_CALIB3,DIR_OUT);
	
	#ifdef DEBUG
	printf("Exit export GPIO \n");
	#endif

}

/*
 =========================================================================== 
 function Name  : configbattery
 return type    : void
 Description    : configurate the battery related parameters
 ============================================================================
*/
void configbattery(void)
{
	
	system("i2cset -y 1 0x6b 0x00 0x44"); 

	system("i2cset -y 1 0x6b 0x01 0x3b");

	system("i2cset -y 1 0x6b 0x02 0x70");

	system("i2cset -y 1 0x6b 0x03 0x11");

	system("i2cset -y 1 0x6b 0x04 0xb2");

	system("i2cset -y 1 0x6b 0x05 0x0e");

	system("i2cset -y 1 0x6b 0x06 0x93");

	system("i2cset -y 1 0x6b 0x07 0x4b");

}

////////////////////////////////////////////////////////


int main()
{

	int result;
	pthread_attr_t tattr;
	int newprio = 20;
	struct sched_param param;
	
    	if (signal(SIGPIPE, SIG_IGN) == SIG_ERR) {
    		perror("Could not ignore the SIGPIPE signal");
    		exit(EXIT_FAILURE);
    	}
    	sock = socket(AF_INET , SOCK_STREAM , 0);  //Create socket
    	if (sock == -1)
    	{
        	printf("Could not create socket");
    	}
    	#ifdef DEBUG
    	puts("Socket created");
    	#endif

    	server.sin_addr.s_addr = inet_addr("127.0.0.1");
    	server.sin_family = AF_INET;
    	server.sin_port = htons( 1234 );
    	sockfd = sock;
   
    	int flag = 1;
    
	result = setsockopt(sock,            /* socket affected */
					IPPROTO_TCP,     /* set option at TCP level */
					TCP_NODELAY,     /* name of option */
					(char *) &flag,  /* the cast is historical cruft */
					sizeof(int));    /* length of option value */

    	//Connect to remote server
	if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
    	{      
		perror("connect failed. Error");
		while(1)
		{
			sleep(1);
			if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
    		{
     			perror("connect failed. RETRY after a second");
			}
			else
			{
				break;
			}
		}
    	}

	#ifdef DEBUG
	puts("Connected\n");
	printf("Socket Des: %d\n",sock);	
    	#endif
    
	#ifdef BAT_ENABLE	
	/*Configure the battery*/
	configbattery();

        /*Check the battery status flag */
	bq2x.read.ptr = &bq2x_data;
	bq2x.read.size = sizeof(bq2x_data);
	create_BQ24295(bq2x);
	#endif

	/*Initilize the spi module*/
	spi_init();

	/*Export the Led's */
	LED_init();

	/*Reset the adc*/
	adc_reset();

    	/**************CREATING THREADs TO ALL THE MODULES ************************/

	#ifdef BAT_ENABLE 
	pthread_create(&thread,NULL,&check_batterystatus, NULL); //Check the battery percentage status
	#endif
	
	pthread_create(&thread1,NULL,&appstatus_check, NULL);    //application check keep alive or not

	while(1)
	{
		adcdata_read();
		sleep(2);
	}
		
	close(sock);	
	return 0;

}


  
