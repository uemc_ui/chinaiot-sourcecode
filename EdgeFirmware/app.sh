#!/bin/sh

cp /home/pi/Desktop/EdgeFirmware/patch/certificate.pem.cer /home/pi/certification-prod.deeplaser

fwfilename="/home/pi/firmwareupdatestatus.txt"	#0-Start DL As usual; 1-Fw update failed.. restore existing version; 2-New Update available.. iniitate

if test -e $fwfilename; then
	while read -r line 
	do
	    fwstatus="$line"
	    #echo "Name read from file - $fwstatus"
	    break;
	done < "$fwfilename"

else	#file not found
echo "firmwareupdatestatus.txt does not exist"
fwstatus=4;
fi

if [ $fwstatus -eq 1 ]; then
	echo "Firmware Update Failed... Restore the existing version"
	sudo /home/pi/retainfw.sh
	sleep 20
	killall -9 fbi
	sudo reboot -f
elif [ $fwstatus -eq 2 ]; then
	echo "New Firmare Update is available. Initiate firmware update now"
	sudo /home/pi/firmware_update.sh
	#sleep 20
	#sudo killall test
	#sudo killall webserver
	#sudo killall raspi2fb
	#sudo killall fbi
	#sudo reboot -f
else
	if [ $fwstatus -eq 0 ]; then
	#echo "Firmware Updated Successfully. No need to initiate update now"
	echo "....."
	elif [ $fwstatus -eq 4 ]; then
	echo "FileNotFound"
	fi

	#filenamecleanup="/home/pi/Desktop/EdgeFirmware/miscfiles/cleanmodbox.txt"
	#if test -e $filenamecleanup; then

	#echo "The cleanmodbox.txt file exists... no need to run clean up"

	#else

	#echo "The file $filenamecleanup does not exists... so need to clean up now"
	#sudo echo 1 > /home/pi/Desktop/EdgeFirmware/miscfiles/cleanmodbox.txt
	#bash /home/pi/modbox_clean.sh
	#fi


	if pgrep -x "start.sh" > /dev/null ; then
	echo "DeepLaser is Running"

	else

	echo "DeepLaser not running...Re-installing and Staring now"
	sudo /home/pi/Desktop/EdgeFirmware/rundl
	fi

fi





