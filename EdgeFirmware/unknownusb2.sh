#!/bin/bash  
#echo 1 > /sys/class/gpio/gpio26/value
ROOTUSB=/sys/devices/platform/soc/3f980000.usb/usb1/1-1  

#Exceptions network device  
EXC="1-1.1 1-1:1.0"  
for host in $ROOTUSB/*  
do  
if [ -f $host/authorized ];then  
disable=1  

# Parse exception to allow them to be enabled (network...)  
for device in $EXC  
do  
# If host ending equal excetion =>> dont change state  
[[ $host =~ \/$device$ ]] && disable=0  
done  

#Otherwise change state ==> disable  
if [ $disable = 1 ];then  
#USB host to disable  
#echo $host

#echo $(cat $host/idVendor)

	if [[ $(< $host/idVendor) == "7392" ]]; then
		echo "This is EDIMAX DONGLE"
	
	else
		echo "NON EDIMAX USB CONNECTED"
		echo 0 >  $host/authorized
	fi

fi  
fi  
done 
