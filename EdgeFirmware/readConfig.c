#include <curl/curl.h>
#include <curl/easy.h>
#include <string.h>
#include <libxml/parser.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>

#include "readConfig.h"

/* function prototypes to define later */

char devName[20];
char postDataFrequency[10];
char ssid[30];
char psk[30];
char user[10];
char pass[10];
char dialing_num[10];
char calibDoneDate[20];
char calibDueDate[20];
char instrumentType[20];
char instrumentSno[20];
char instrumentManufacturer[20];
char instrumentLocation[20];
char cloudUserId[50];
char cloudPasswd[50];
char registerDecisionVal[10];

char macAddSmartPlug[20];
#if 0
char startAdd[5];
char endAdd[5];
char finalIPOctet[20];
#endif // 0

/* the main function invoking */
int readConfigMain()
{

    static int connect3GCnt=0;

    //long long static cnt=0;
    long long int cnt=0;


    /**********************************XML Handling**********************************/
    xmlDoc         *document;
    xmlNode        *root, *first_child, *node;

    document = xmlReadFile("/home/pi/Desktop/EdgeFirmware/configFile.xml", NULL, 0x60);
    root = xmlDocGetRootElement(document);
	printf("XML document return %d\n",root);
    if (root == NULL)
    {
        printf("Waiting to receive config file\n");
        while(!root && cnt<(8*200000))  //200000 give 45 sec, 200000*8 give 360 sec i.e. 6 min
        {
        document = xmlReadFile("/home/pi/Desktop/EdgeFirmware/configFile.xml", NULL, 0x60);
        root = xmlDocGetRootElement(document);
        cnt++;
        }

        printf("Cnt is %lld\n",cnt);

        if(root == NULL)
        {
            printf("Timed Out waiting for config file !!!\n");
            xmlFreeDoc(document);
            return -1;
        }

    }

    first_child = root->children;
    for (node = first_child; node; node = node->next)
    {

        if(xmlNodeListGetString(document, node->xmlChildrenNode, 1)!=NULL)
        {
            #ifdef DEBUG_ON
            fprintf(stdout, "\t %s:\t %s\n", node->name, xmlNodeListGetString(document, node->xmlChildrenNode, 1));
            #endif // DEBUG_ON
#if 0
            if ((!xmlStrcmp(node->name, (const xmlChar *)"device_name")))
            {
                xmlChar *key;
                char devName[30];
                key = xmlNodeListGetString(document, node->xmlChildrenNode, 1);
                strcpy(devName,key);
                printf("*********device_name: %s\n", devName);
                xmlFree(key);
                //printf("*********device_name: %s\n", xmlNodeListGetString(document, node->xmlChildrenNode, 1));
            }
#endif // 0
        }


        if ((!xmlStrcmp(node->name, (const xmlChar *)"device_name")))
        {
            xmlChar *key;
            key = xmlNodeListGetString(document, node->xmlChildrenNode, 1);
            strcpy(devName,key);
            #ifdef DEBUG_ON
            printf("*********devName: %s\n", devName);
            #endif // DEBUG_ON
            xmlFree(key);
        }
        if ((!xmlStrcmp(node->name, (const xmlChar *)"wifi")))
        {
            parsePMConfig(document,node);
        }

        if ((!xmlStrcmp(node->name, (const xmlChar *)"frequency_seconds")))
        {
            xmlChar *key;
            key = xmlNodeListGetString(document, node->xmlChildrenNode, 1);
            strcpy(postDataFrequency,key);
            #ifdef DEBUG_ON
            printf("*********calibDoneDate: %s\n", calibDoneDate);
            #endif // DEBUG_ON
            xmlFree(key);
        }

        if ((!xmlStrcmp(node->name, (const xmlChar *)"calibration_done_date")))
        {
            xmlChar *key;
            key = xmlNodeListGetString(document, node->xmlChildrenNode, 1);
            strcpy(calibDoneDate,key);
            #ifdef DEBUG_ON
            printf("*********calibDoneDate: %s\n", calibDoneDate);
            #endif // DEBUG_ON
            xmlFree(key);
        }

        if ((!xmlStrcmp(node->name, (const xmlChar *)"calibration_due_date")))
        {
            xmlChar *key;
            key = xmlNodeListGetString(document, node->xmlChildrenNode, 1);
            strcpy(calibDueDate,key);
            #ifdef DEBUG_ON
            printf("*********calibDueDate: %s\n", calibDueDate);
            #endif // DEBUG_ON
            xmlFree(key);
        }

        if ((!xmlStrcmp(node->name, (const xmlChar *)"instrument_type")))
        {
            xmlChar *key;
            key = xmlNodeListGetString(document, node->xmlChildrenNode, 1);
            strcpy(instrumentType,key);
            #ifdef DEBUG_ON
            printf("*********instrumentType: %s\n", instrumentType);
            #endif // DEBUG_ON
            xmlFree(key);
        }

        if ((!xmlStrcmp(node->name, (const xmlChar *)"instrument_serial")))
        {
            xmlChar *key;
            key = xmlNodeListGetString(document, node->xmlChildrenNode, 1);
            strcpy(instrumentSno,key);
            #ifdef DEBUG_ON
            printf("*********instrumentSno: %s\n", instrumentSno);
            #endif // DEBUG_ON
            xmlFree(key);
        }

        if ((!xmlStrcmp(node->name, (const xmlChar *)"instrument_manufacturer")))
        {
            xmlChar *key;
            key = xmlNodeListGetString(document, node->xmlChildrenNode, 1);
            strcpy(instrumentManufacturer,key);
            #ifdef DEBUG_ON
            printf("*********instrumentManufacturer: %s\n", instrumentManufacturer);
            #endif // DEBUG_ON
            xmlFree(key);
        }
        if ((!xmlStrcmp(node->name, (const xmlChar *)"instrument_location")))
        {
            xmlChar *key;
            key = xmlNodeListGetString(document, node->xmlChildrenNode, 1);
            strcpy(instrumentLocation,key);
            #ifdef DEBUG_ON
            printf("*********instrumentLocation: %s\n", instrumentLocation);
            #endif // DEBUG_ON
            xmlFree(key);
        }
        if ((!xmlStrcmp(node->name, (const xmlChar *)"cloud_login")))
        {
            xmlChar *key;
            key = xmlNodeListGetString(document, node->xmlChildrenNode, 1);
            strcpy(cloudUserId,key);
            #ifdef DEBUG_ON
            printf("*********cloudUserId: %s\n", cloudUserId);
            #endif // DEBUG_ON
            xmlFree(key);
        }
        if ((!xmlStrcmp(node->name, (const xmlChar *)"cloud_password")))
        {
            xmlChar *key;
            key = xmlNodeListGetString(document, node->xmlChildrenNode, 1);
            strcpy(cloudPasswd,key);
            #ifdef DEBUG_ON
            printf("*********cloudPasswd: %s\n", cloudPasswd);
            #endif // DEBUG_ON
            xmlFree(key);
        }
        if ((!xmlStrcmp(node->name, (const xmlChar *)"registerDecision")))
        {
            xmlChar *key;
            key = xmlNodeListGetString(document, node->xmlChildrenNode, 1);
            strcpy(registerDecisionVal,key);
            #ifdef DEBUG_ON
            printf("*********registerDecisionVal: %s\n", registerDecisionVal);
            #endif // DEBUG_ON
            xmlFree(key);
        }


        if ((!xmlStrcmp(node->name, (const xmlChar *)"three_g_dongle")))
        {
            parsePMConfig(document,node);
        }

        #if 0
         if ((!xmlStrcmp(node->name, (const xmlChar *)"smart_plug_mac")))
        {
            xmlChar *key;
            key = xmlNodeListGetString(document, node->xmlChildrenNode, 1);
            strcpy(macAddSmartPlug,key);
            //#ifdef DEBUG_ON
            printf("*********Smart Plug MAC Address: %s\n", macAddSmartPlug);
            //#endif // DEBUG_ON
            xmlFree(key);
        }

        if ((!xmlStrcmp(node->name, (const xmlChar *)"start_address")))
        {
            xmlChar *key;
            key = xmlNodeListGetString(document, node->xmlChildrenNode, 1);
            strcpy(startAdd,key);
            #ifdef DEBUG_ON
            printf("*********start Address: %s\n", startAdd);
            #endif // DEBUG_ON
            xmlFree(key);
        }
        if ((!xmlStrcmp(node->name, (const xmlChar *)"end_address")))
        {
            xmlChar *key;
            key = xmlNodeListGetString(document, node->xmlChildrenNode, 1);
            strcpy(endAdd,key);
            #ifdef DEBUG_ON
            printf("*********end Address: %s\n", endAdd);
            #endif // DEBUG_ON
            xmlFree(key);
        }
        if ((!xmlStrcmp(node->name, (const xmlChar *)"ip_address")))
        {
            xmlChar *key;
            key = xmlNodeListGetString(document, node->xmlChildrenNode, 1);
            strcpy(finalIPOctet,key);
            #ifdef DEBUG_ON
            printf("*********start Address: %s\n", startAdd);
            #endif // DEBUG_ON
            xmlFree(key);
        }

        FILE *fp;
        char data[200];
        sprintf(data,"#!/bin/sh\n\nCOUNTER=%s\n\nwhile [ $COUNTER -lt %s ]\ndo\n   ping %s$COUNTER -c 1 -w 1\n   COUNTER=$(( $COUNTER + 1 ))\ndone",startAdd,endAdd,finalIPOctet);

        fp = fopen("/home/pi/Desktop/EdgeFirmware/broadcastping.sh","w");
        fwrite(data,sizeof(char),strlen(data),fp);
        fclose(fp);

        system("chmod u+x broadcastping.sh");
        #endif // 0

    }

    /**********************************XML Handling**********************************/
    #if 1       //automating wifi & 3G Connection

    if(connect3GCnt==0)
    {
        write_wifi_wpafile();
        //connectTo3G();

        connect3GCnt++;
    }

    #endif // 0



    return 0;
}

void write_wifi_wpafile()
{
    FILE *fp;
    int writeWPAFlag=1;     //1-need to write ssid details into config file
                            //0-not needed to write ssid details into config file

    /*************Code to check the existence of current SSID in wpa configuration file*************/

    fp = fopen("/etc/wpa_supplicant/wpa_supplicant.conf","r");

    if(fp==NULL)
    {
        printf("File Does not exists\n");
    }
    else
    {
        //read line by line
        const size_t line_size = 300;
        char* line = malloc(line_size);
        while (fgets(line, line_size, fp) != NULL)  {
            //printf("-> %s",line);
            if(strstr(line,ssid)!=0)    //already configuration exists, no need to add the ssid into file
            {
             #ifdef DEBUG_ON
                printf("*********SSID Exists\n");
            #endif // DEBUG_ON
                writeWPAFlag=0;
            }
        }
        free(line);
    }



    /*************Code to check the existence of current SSID in wpa configuration file*************/

    if(writeWPAFlag==1)
    {
        /**********************write wpa_supplicant.conf**********************/
        fp = fopen("/etc/wpa_supplicant/wpa_supplicant.conf","a");
        //fp = fopen("/home/pi/Desktop/EdgeFirmware/wpa_supplicant.conf","w+");

        if(fp==NULL)
        {
            printf("Failed to create config file\n");
        }
    #if 0
        fprintf(fp,"ctrl_interface=/var/run/wpa_supplicant\n");
        fprintf(fp,"ctrl_interface_group=0\n");
        fprintf(fp,"config_methods=virtual_push_button virtual_display push_button keypad\n");
        fprintf(fp,"update_config=1\n");
        fprintf(fp,"fast_reauth=1\n");
        fprintf(fp,"device_name=Edison\n");
        fprintf(fp,"manufacturer=Intel\n");
        fprintf(fp,"model_name=Edison\n\n\n");
    #endif // 0
        fprintf(fp,"network={\n");
        fprintf(fp,"ssid=\"%s\" \n\n",ssid);
        fprintf(fp,"key_mgmt=WPA-PSK\n");
        fprintf(fp,"pairwise=CCMP TKIP\n");
        fprintf(fp,"group=CCMP TKIP WEP104 WEP40\n");
        fprintf(fp,"eap=TTLS PEAP TLS\n");
        fprintf(fp,"psk=\"%s\" \n",psk);
        fprintf(fp,"}\n");

        fclose(fp);
        /**********************write wpa_supplicant.conf**********************/

    }


}

void parsePMConfig(xmlDoc *document,xmlNode* cur)
{
	xmlNode        *node_new;

    for (node_new = cur->xmlChildrenNode; node_new; node_new = node_new->next)
    {
//        fprintf(stdout, "\t Child is <%s> (%i), Value is <%s>\n", node_new->name, node_new->type, xmlNodeListGetString(document, node_new->xmlChildrenNode, 1));

        #ifdef DEBUG_ON
        fprintf(stdout, "\t %s:\t %s\n", node_new->name, xmlNodeListGetString(document, node_new->xmlChildrenNode, 1));
        #endif // DEBUG_ON

        if ((!xmlStrcmp(node_new->name, (const xmlChar *)"ssid")))
        {
            xmlChar *key;
            key = xmlNodeListGetString(document, node_new->xmlChildrenNode, 1);
            strcpy(ssid,key);
            #ifdef DEBUG_ON
            printf("*********ssid: %s\n", ssid);
            #endif // DEBUG_ON
            xmlFree(key);
        }

        if ((!xmlStrcmp(node_new->name, (const xmlChar *)"psk")))
        {
            xmlChar *key;
            key = xmlNodeListGetString(document, node_new->xmlChildrenNode, 1);
            strcpy(psk,key);
            #ifdef DEBUG_ON
            printf("*********psk: %s\n", psk);
            #endif // DEBUG_ON
            xmlFree(key);
        }

        if ((!xmlStrcmp(node_new->name, (const xmlChar *)"user")))
        {
            xmlChar *key;
            key = xmlNodeListGetString(document, node_new->xmlChildrenNode, 1);
            strcpy(user,key);
            #ifdef DEBUG_ON
            printf("*********user: %s\n", user);
            #endif // DEBUG_ON
            xmlFree(key);
        }

        if ((!xmlStrcmp(node_new->name, (const xmlChar *)"pass")))
        {
            xmlChar *key;
            key = xmlNodeListGetString(document, node_new->xmlChildrenNode, 1);
            strcpy(pass,key);
            #ifdef DEBUG_ON
            printf("*********pass: %s\n", pass);
            #endif // DEBUG_ON
            xmlFree(key);
        }

        if ((!xmlStrcmp(node_new->name, (const xmlChar *)"dialing_num")))
        {
            xmlChar *key;
            key = xmlNodeListGetString(document, node_new->xmlChildrenNode, 1);
            strcpy(dialing_num,key);
            #ifdef DEBUG_ON
            printf("*********dialing_num: %s\n", dialing_num);
            #endif // DEBUG_ON
            xmlFree(key);
        }


	}

	return;
}

int connectTo3G()
{
    FILE *fp;

    fp = fopen("/etc/ppp/peers/tmobile","w+");
    //fp = fopen("tmobile","w+");

    if(fp==NULL)
    {
        printf("Failed to create peers/tmobile file\n");
        return -1;
    }

    fprintf(fp,"lock\n");
    fprintf(fp,"modem\n");
    fprintf(fp,"debug\n");
    fprintf(fp,"/dev/ttyUSB0\n");
    fprintf(fp,"460800\n");
    fprintf(fp,"defaultroute\n");
    fprintf(fp,"replacedefaultroute\n");
    fprintf(fp,"noipdefault\n");
    fprintf(fp,"user %s\n",user);
    fprintf(fp,"password %s\n",pass);
    fprintf(fp,"crtscts\n");

    fprintf(fp,"hide-password\n");
    fprintf(fp,"usepeerdns\n");
    fprintf(fp,"holdoff 3\n");
    fprintf(fp,"novj\n");
    fprintf(fp,"novjccomp\n");
    fprintf(fp,"replacedefaultroute\n");
    fprintf(fp,"persist\n");
    fprintf(fp,"connect 'chat -t60 \\\"\\\" ATZ OK ATX3 OK ATDT%s CONNECT'\n",dialing_num);

    fclose(fp);

    /***************check for existence of /etc/ppp/chatscripts directory*************/

    DIR* dir = opendir("/etc/ppp/chatscripts");
    if (dir)
    {
        /* Directory exists. */
        closedir(dir);
        printf("Directory Exist\n");
    }
    else if (ENOENT == errno)
    {
        /* Directory does not exist. */
        printf("Directory does not exist\n");
        system("mkdir /etc/ppp/chatscripts");
    }
    else
    {
        /* opendir() failed for some other reason. */
    }
    /***************check for existence of /etc/ppp/chatscripts directory*************/

    fp = fopen("/etc/ppp/chatscripts/tmobile","w+");
    //fp = fopen("tmobile1","w+");

    if(fp==NULL)
    {
        printf("Failed to create chatscripts/tmobile file\n");
        return -1;
    }

    fprintf(fp,"TIMEOUT 10\n");
    fprintf(fp,"ABORT 'BUSY'\n");
    fprintf(fp,"ABORT 'NO ANSWER'\n");
    fprintf(fp,"ABORT 'ERROR'\n");
    fprintf(fp,"SAY 'Starting 3G connect script\\n'\n");
    fprintf(fp,"\"\"'ATZ'\n");
    fprintf(fp,"SAY 'Setting APN\\n'\n");
    fprintf(fp,"OK 'AT+CGDCONT=1,\"IP\",\"\"'\n");
    fprintf(fp,"ABORT 'NO CARRIER'\n");
    fprintf(fp,"SAY 'Dialing...\\n'\n");
    fprintf(fp,"OK 'ATDT%s'\n",dialing_num);
    fprintf(fp,"CONNECT\n");

    fclose(fp);


    fp = fopen("/etc/ppp/chatscripts/tmobile-disconnect","w+");
    //fp = fopen("tmobile-disconnect","w+");

    if(fp==NULL)
    {
        printf("Failed to create chatscripts/tmobile-disconnect file\n");
        return -1;
    }

    fprintf(fp,"\"\" \"\\K\"\n");
    fprintf(fp,"\"\" \"+++ATH0\"\n");
    fprintf(fp,"SAY \"3G disconnected.\"\n");

    fclose(fp);


    fp = fopen("/etc/ppp/chap-secrets","w+");
    //fp = fopen("chap-secrets","w+");

    if(fp==NULL)
    {
        printf("Failed to create ppp/chap-secrets file\n");
        return -1;
    }

    fprintf(fp,"user \"%s\"\n",user);
    fprintf(fp,"password \"%s\"\n",pass);

    fclose(fp);

    //fp = fopen("/etc/resolv.conf ","w+");
    fp = fopen("/home/pi/Desktop/EdgeFirmware/resolv.conf","w+");

    if(fp==NULL)
    {
        printf("Failed to create resolv.conf file\n");
        return -1;
    }

    fprintf(fp,"nameserver 8.8.8.8\n");


    fclose(fp);

    system("mv resolv.conf /etc/resolv.conf");

    system("pppd nodetach call tmobile &");   //command to start 3G connection

    sleep(5);
    return 0;

}


