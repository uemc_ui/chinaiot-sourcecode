#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mongoose.h"
#include <error.h>
#include <stdlib.h>

#define	PI	1

#define ENGLISH	1

#define CHINESE	2

#define FREEZER	1
#define ULT	2
#define REFRIGERATOR 3
#define OTHERS	4

#define DEBUG_ON	1

static const char *s_http_port = "8500";
static struct mg_serve_http_opts s_http_serve_opts;

struct mg_mgr mgr;




char pinStr[20];
char serialNo[20];
char startAPModeCmd[128];
char wifiIface[6];
char languageSelected[10];

char selectedAPName[128];
char selectedAPNameTmp[128];

char encodedSSIDCmd[256];
char apNameEncoded[100];

FILE * wifiListFile;
FILE * selecedWifiNameFptr;
FILE * firstTimeConfig;
FILE * languageFile;
int lineCount;

char selectedAPPwd[50];
char selecteAPUserName[50];
char startStationModeCmd[50];

char assetModel[50];
char assetSerialNo[50];
char assetCity[50];
char assetLocation[50];
char assetType[50];

char highTempPoint[50];
char lowTempPoint[50];
char setTempPoint[50];

char timeValue[1000];
char dateValue[1000];

char assetTypeCmd[100];
char assetModelCmd[100];
char assetSerialNoCmd[100];
char assetCityCmd[100];
char assetLocationCmd[100];

char highTempCmd[100];
char lowTempCmd[100];
char setPointTempCmd[100];

int sock;
int sockRetVal;
struct sockaddr_in server;

int languageSelectedFlag;


int writeToSocket(int index, char* data);
int readAssetData(struct http_message *hm);
int readWifiName(struct http_message *hm);
void readWifiData(struct http_message *hm);
void readAlarmData(struct http_message *hm);
char* getAssetType();
char* getAssetSerialNum();
void updateSetPoints(char* assettype);
int isFirstTimeConfig();		//0-Return means first time config 	//1-Return means second time config
void switchToStationMode();
void connectToWifi();
char* getCurrentWifiInterface();
int checkWifiType(char *apName);
void updateAssetDetails();
int convertNonLatinNames();


int changedAsset;
int changedWifi;
char appStartStatus[5];
char apNameWriteCmd[100];
int wifiType;	//1-Open	2-WPA	3-WEP	4-EAP
FILE *wpaFile;
static int connectionCnt;
char wifiPwdWriteCmd[100];
char currentAssetType[20];
char currentAssetSNum[50];
int assetUpdatedFlag;	//1-Asset Type Changed
int assetSerialUpdatedFlag;	//1-Asset Serial Changed

//checkwifi parameters
char getApTypeCmd[1024];
FILE* fp;
FILE *sysCmdFp;
char temp[2000],buffer[2000],str[2000],chr;
int find_ssid,find_cell,line_num_ssid,line_num_cell;
int find_encrypt_on,find_encrypt_off,find_eap,find_psk;
char Previous_line[256];
char Previous_line2[256];
char encryption_on[50]="Encryption key:on";
char encryption_off[50]="Encryption key:off";
char authentication_eap[50]="Authentication Suites (1) : 802.1x";	
char authentication_psk[50]="Authentication Suites (1) : PSK";
int i,cnt;
char* token;
char wifiSignalTemp[10];
char wifiSignal[10];
int wifiTypeReturnVal;
int dongleOn;	//0-DongleOff	1-DongleOn
char tmp[8];

FILE *wifiSignalOutput;
char wifiInterfaceName[128];

typedef struct Webserver_Struct
{
    char parametersBuffer[40];
    int index;
} Qt_WebServer_Sock;


Qt_WebServer_Sock Client_Webserver;

void stringReplace(char *dst) {
  int offset = 0;
  do {
    if (dst[offset] == '&') 
	{
	 strncpy( selectedAPNameTmp, dst + offset+1, strlen(selectedAPName-offset) );
        dst[offset] = '\'';
	dst[offset+1] = '\\';
	dst[offset+2] = '&';
	dst[offset+3] = '\'';
	dst[offset+4] = '\0';	
	break;
	}
    *dst = dst[offset];	
  } while (*dst++);
}

int convertNonLatinNames()
{			
	FILE *fptr1, *fptr2;
	char wifiFileName[256] = "/home/pi/Desktop/modbox-webserver/chinaIoT-English/load_wifi.txt";
	char wifiNameStr[256], temp[] = "temp.txt";
	char wifiNameCmd[256];

	fptr1 = fopen(wifiFileName, "r");
	if (!fptr1) 
	{
			printf(" File not found or unable to open the input file!!\n");
			return 0;
	}
	fptr2 = fopen(temp, "w"); // open the temporary file in write mode 
	if (!fptr2) 
	{
			printf("Unable to open a temporary file to write!!\n");
			fclose(fptr1);
			return 0;
	}

	// copy all contents to the temporary file except the specific line with unicode characters
	while (!feof(fptr1)) 
	{
		memset(wifiNameStr, '\0',sizeof(wifiNameStr));

		if(fgets(wifiNameStr, 256, fptr1)!=NULL)
		{
			//ctr++;
			if(strstr(wifiNameStr,"\\")!=NULL)
			{
				memset(wifiNameCmd,'\0',sizeof(wifiNameCmd));
				printf("Input String Contains Unicode Character\n");					
				//wifiNameStr[strlen(wifiNameStr)-1]='\0';		
				
				if(strchr(wifiNameStr,'\n')!=NULL)
				{
					wifiNameStr[strlen(wifiNameStr)-1]='\0';
				}
				else
				{
					wifiNameStr[strlen(wifiNameStr)]='\0';
				}						
				
				sprintf(wifiNameCmd,"echo %s | sed \'s/[\\\\x]//g\' | xxd -r -p >> /home/pi/Desktop/modbox-webserver/chinaIoT-English/load_wifi.txt",wifiNameStr);
				printf("Final String: %s\nUnicode String Size: %ld\n",wifiNameCmd,strlen(wifiNameStr));
				system(wifiNameCmd);		
				
			}
			else
			{					
				fprintf(fptr2, "%s", wifiNameStr);					
			}
			
		}
	}

	fclose(fptr1);
	fclose(fptr2);
	remove(wifiFileName);  		// remove the original file 
	rename(temp, wifiFileName); 	// rename the temporary file to original name
		
	system("echo \"\" >> /home/pi/Desktop/modbox-webserver/chinaIoT-English/load_wifi.txt");	
	system("echo Others >> /home/pi/Desktop/modbox-webserver/chinaIoT-English/load_wifi.txt");	
	system("sudo cp -r /home/pi/Desktop/modbox-webserver/chinaIoT-English/load_wifi.txt /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/");
	
	return 0;
}

void generateWifiList()
{
	 /********************************************/
    #ifdef PI     
    
    system("sudo ifconfig wlan0 up");

	system("rm -rf /home/pi/Desktop/modbox-webserver/chinaIoT-English/load_wifi.txt");
	system("rm -rf /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/load_wifi.txt");
	
	system("sudo rm  /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/check_wifinw.txt");	
	system("sudo rm  /home/pi/Desktop/modbox-webserver/chinaIoT-English/check_wifinw.txt");	

    //system("sudo iwlist wlan0 scan | grep ESSID |  sed 's/ESSID:/  /' | sed -e 's/^\\s*//' -e '/^$/d' | sed 's/\"//g' | sed '/\\x/d' >> /home/pi/Desktop/modbox-webserver/chinaIoT-English/load_wifi.txt");    
    system("sudo iwlist wlan0 scan | grep ESSID |  sed 's/ESSID:/  /' | sed -e 's/^\\s*//' -e '/^$/d' | sed 's/\"//g'  >> /home/pi/Desktop/modbox-webserver/chinaIoT-English/load_wifi.txt");    
    //system("echo Others >> /home/pi/Desktop/modbox-webserver/chinaIoT-English/load_wifi.txt");
    sleep(2.5);
    system("sudo iwlist wlan0 scanning >> /home/pi/Desktop/modbox-webserver/chinaIoT-English/check_wifinw.txt");
    
      
	system("sudo cp -r /home/pi/Desktop/modbox-webserver/chinaIoT-English/load_wifi.txt /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/");
	system("sudo cp -r /home/pi/Desktop/modbox-webserver/chinaIoT-English/check_wifinw.txt /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/");
	
	convertNonLatinNames();
	
    #else
    
	system("rm -rf /home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/load_wifi.txt");

    system("sudo iwlist wlp2s0 scan | grep ESSID |  sed 's/ESSID:/  /' | sed -e 's/^\\s*//' -e '/^$/d' | sed 's/\"//g' >> /home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/load_wifi.txt");

    #endif	//PI
    /********************************************/
}

static void send_file(struct mg_connection *nc, char *file_path){
    char buf[1024];
    int n;
    FILE *fp;
    fp = fopen(file_path, "rb");
    if(fp == NULL){
        printf("Error, file %s not found.", file_path);
    }
    //char *mimeType = "image/jpg";
    char *mimeType = "text/html";
    fseek(fp, 0L, SEEK_END);
    long sz = ftell(fp) + 2;
    fseek(fp, 0L, SEEK_SET);
    mg_printf(nc,
                    "HTTP/1.1 200 OK\r\n"
                    "Cache: no-cache\r\n"
                    "Content-Type: %s\r\n"
                    "Content-Length: %ld\r\n"
                    "\r\n",
                    mimeType,
                    sz);
    while((n = fread(buf, 1, sizeof(buf), fp)) > 0) {
        mg_send(nc, buf, n);
    }
    fclose(fp);
    mg_send(nc, "\r\n", 2);
    nc->flags |= MG_F_SEND_AND_CLOSE;
    
}    
#if 0
static void handle_ssi_call(struct mg_connection *nc, const char *param) {
	
  if (strcmp(param, "setting1") == 0) {
    mg_printf_html_escape(nc, "%s", s_settings.setting1);
  } else if (strcmp(param, "setting2") == 0) {
    mg_printf_html_escape(nc, "%s", s_settings.setting2);
  }
}
#endif	//0

static void ev_handler(struct mg_connection *nc, int ev, void *ev_data){
    struct http_message *hm = (struct http_message *)ev_data;
    
    
    
    switch(ev)
    {
    case MG_EV_HTTP_REQUEST:
        {
            if(mg_vcmp(&hm->uri, "/hello") == 0)
            {
                //handle_recv(nc);
            } 

			else if(mg_vcmp(&hm->uri, "/doneWifi")==0)
            {
			#ifdef	DEBUG_ON
				printf(" Inside donewifi click action\n");
			#endif	//DEBUG_ON
			
				wifiTypeReturnVal = 0;
				wifiTypeReturnVal = readWifiName(hm);
				
			#ifdef	DEBUG_ON
				printf("RETURN TYPE FOR WIFI TYPE IS %d\n",wifiTypeReturnVal);
			#endif	//DEBUG_ON	
				
				
				if(wifiTypeReturnVal==1)	//selected network type is open network..redirect to same wifi setting pages
				{
					#ifdef PI	
					if(languageSelectedFlag==CHINESE)			
					{
						send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/wifi_settings.html");			
					}
					else
					{
						send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-English/wifi_settings.html");			
					}		
					#else
					send_file(nc,"/home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/wifi_settings.html");				
					#endif	//PI

				}
				else if(wifiTypeReturnVal==5)	//unable to find the specified ssid
				{
					system("sudo rm -rf /home/pi/Desktop/modbox-webserver/chinaIoT-English/selectedap.txt");
					system("sudo rm -rf /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/selectedap.txt");
					#ifdef PI	
					if(languageSelectedFlag==CHINESE)			
					{
						send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/wifi_settings.html");			
					}
					else
					{
						send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-English/wifi_settings.html");			
					}		
					#else
					send_file(nc,"/home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/wifi_settings.html");				
					#endif	//PI

				}

				else
				{
					#ifdef PI	
					if(languageSelectedFlag==CHINESE)			
					{
						send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/wifi_settings-password.html");			
					}
					else
					{
						send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-English/wifi_settings-password.html");			
					}		
					#else
					send_file(nc,"/home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/wifi_settings-password.html");				
					#endif	//PI

				}
				
			}
			else if(mg_vcmp(&hm->uri, "/refreshwifilist")==0)
			{
				generateWifiList();
				
				#ifdef PI	
				if(languageSelectedFlag==CHINESE)			
				{
					send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/wifi_settings.html");			
				}
				else
				{
					send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-English/wifi_settings.html");			
				}		
				#else
				send_file(nc,"/home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/wifi_settings.html");				
				#endif	//PI

			}	
			else if(mg_vcmp(&hm->uri, "/doneWifiPwd")==0)
            {
			#ifdef	DEBUG_ON	
				printf(" Inside donewifi click action\n");
			#endif	//DEBUG_ON
				readWifiData(hm);
				
				#ifdef PI		
				if(languageSelectedFlag==CHINESE)	
				{
					send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/link_asset.html");				
				}
				else
				{
					send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-English/link_asset.html");				
				}			
				#else
				send_file(nc,"/home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/link_asset.html");				
				#endif	//PI
				
				system("sudo rm -rf /home/pi/Desktop/modbox-webserver/chinaIoT-English/network_type.txt ");
				system("sudo rm -rf /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/network_type.txt ");
			}	

			else if(mg_vcmp(&hm->uri, "/doneAsset")==0)		//first time config... asset details saved
			{

				//readAssetData(hm);	
				
				if(readAssetData(hm)==0)	//selected asset type and asset city are of valid type
				{
					updateAssetDetails();	

					sleep(2);			
					
					#ifdef PI
					if(languageSelectedFlag==CHINESE)	
					{
						send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/alarm_settings.html");
					}
					else
					{
						send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-English/alarm_settings.html");   
					}				
					#else
					send_file(nc,"/home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/alarm_settings.html");				
					#endif	//PI

				}
				else
				{
					
				}
				

			}
			else if(mg_vcmp(&hm->uri, "/doneAlarm") == 0 )		//first timedevice config completed
			{
				
				
				readAlarmData(hm);
				
				changedAsset =1;
					
				updateAssetDetails();	
					
				#ifdef PI
				if(languageSelectedFlag==CHINESE)	
				{
					send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/settings_complete.html");
				}
				else
				{
					send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-English/settings_complete.html");
				}
				
				
			#ifdef	DEBUG_ON				
				printf("Completed First Time Device Configuration: %d\n",__LINE__);
			#endif	//DEBUG_ON			
				writeToSocket(27 ,"1");		//27. First Time Device Configuration Completed
				
				switchToStationMode();				
					
				

				#else
				//no action here
				#endif	//PI
			}
			else if(mg_vcmp(&hm->uri, "/clickedexit") == 0)
			{
			#ifdef	DEBUG_ON
				printf("Clicked Exit Button from index.html page\n");
			#endif	//DEBUG_ON
			
				#ifdef PI
				if(languageSelectedFlag==CHINESE)	
				{
					send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/settings_complete.html");
				}
				else
				{
					send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-English/settings_complete.html");
				}

				
				if(assetUpdatedFlag==1 || assetSerialUpdatedFlag==1)
				{
					writeToSocket(32 ,"1");		//32. Asset Type/Asset Serial Number Changed	  					
				}				
				else if(changedWifi==1 && changedAsset==0)	//only wifi details changed
				{
					writeToSocket(24 ,"1");		//24. Changed Wifi Settings from mobile browser	  			
					
				}
				else if(changedWifi==0 && changedAsset==1)  //only asset details changed
				{
					writeToSocket(25 ,"1");		//25. Changed Asset Details/Alarm Settings from mobile browser					
					
				}
				else if(changedWifi==1 && changedAsset==1)  //only asset details changed	//only asset details are changed
				{					
					writeToSocket(26 ,"1");		//26. Changed both wifi and Asset Details/Alarm Settings from mobile browser	  					
																	
				}
				else
				{
					writeToSocket(30 ,"1");		//30. Exited from Device Configuration with out making any changes				
					
					
				}  

				switchToStationMode();			//switch back to station mode and release static ip			

				#else
				#endif	//PI
				

			}
			else if(mg_vcmp(&hm->uri, "/timerfired") == 0)
			{
			#ifdef	DEBUG_ON
				printf("TimedOut...\n");
			#endif	//DEBUG_ON	
				
				if(isFirstTimeConfig()==1)	//return 1 means second time device configuration
				{
					#ifdef PI
					if(languageSelectedFlag==CHINESE)	
					{
						send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/timeOut.html");
					}
					else
					{
						send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-English/timeOut.html");
					}
				
					
					if(changedWifi==1 && changedAsset==0)	//only wifi details changed
					{
						writeToSocket(24 ,"1");		//24. Changed Wifi Settings from mobile browser	 					
						

					}
					else if(changedWifi==0 && changedAsset==1)  //only asset details changed
					{
						writeToSocket(25 ,"1");		//25. Changed Asset Details/Alarm Settings from mobile browser					
						
					}
					else if(changedWifi==1 && changedAsset==1)  //only asset details changed	//only asset details are changed
					{					
						writeToSocket(26 ,"1");		//26. Changed both wifi and Asset Details/Alarm Settings from mobile browser	
																						
					}
					else
					{
						writeToSocket(29 ,"1");		//29. Timed out... Device configuration session has expired!				
										
					}  

					switchToStationMode();			//switch back to station mode and release static ip				

					#else
					#endif	//PI
					
				}
				
			}

			else if(mg_vcmp(&hm->uri, "/doneAlarmYes") == 0)
			{
				
				
				readAlarmData(hm);
				
				changedAsset =1;
					
				updateAssetDetails();	
					
				#ifdef PI
				if(languageSelectedFlag==CHINESE)	
				{
					send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/index.html");
				}
				else
				{
					send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-English/index.html");
				}

				#else
				#endif	//PI
			}
		
			else if(mg_vcmp(&hm->uri, "/alarmfirst") == 0)
			{
				//printf("FIRST YES\n");
			}
			else if(mg_vcmp(&hm->uri, "/donehiddenWifi") == 0)
			{
			#ifdef	DEBUG_ON
				printf("INSIDE donehiddenWifi CLICK ACTION\n");
			#endif	//DEBUG_ON	
				readWifiName(hm);
				
				send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/settings_complete.html");
			}
		
			else if(mg_vcmp(&hm->uri, "/donechangeswifipwd") == 0)
			{
			#ifdef	DEBUG_ON
				printf("Done Changes WIFI Password\n");
			#endif	//DEBUG_ON	
				readWifiData(hm);
				
				changedWifi = 1;
				
				//updateAssetDetails();	
				

				
				#ifdef PI
				
				if(languageSelectedFlag==CHINESE)	
				{		
					send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/index.html");  

				}
				else
				{					
					
					send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-English/index.html");  

				}
			
				#else
				send_file(nc,"/home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/settings_complete.html");          
				#endif	//PI
				


			}		
			else if(mg_vcmp(&hm->uri, "/donechangesasset") == 0)		//asset changed for second time
			{
			#ifdef	DEBUG_ON
				printf("Done Changes Asset\n");
			#endif	//DEBUG_ON	
				//readAssetData(hm);
				
				if(readAssetData(hm)==0)	//selected asset type and asset city are of valid type
				{
					changedAsset = 1;
				
					updateAssetDetails();

					
					
					#ifdef PI
					if(languageSelectedFlag==CHINESE)	
					{
						if(assetUpdatedFlag==1)		//asset updated... so redirect to alarm_settings.html page
						{
							sleep(2);
							send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/alarm_settings.html"); 
						}
						else 			//asset not updated... so redirect to index.html page itself
						{
							send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/index.html"); 
						}
						
					}
					else
					{
						if(assetUpdatedFlag==1)		//asset updated... so redirect to alarm_settings.html page
						{
							send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-English/alarm_settings.html"); 
						}
						else 			//asset not updated... so redirect to index.html page itself
						{
							send_file(nc,"/home/pi/Desktop/modbox-webserver/chinaIoT-English/index.html"); 
						}
						
					}	

					#else
					send_file(nc,"/home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/settings_complete.html");          
					#endif	//PI          

				}
				else 		//selected asset type and asset city are invalid
				{
				}
				
			}

			else 
			{
                mg_serve_http(nc, (struct http_message *) ev_data, s_http_serve_opts);
            }
		  
        }
        break;
	case MG_EV_SSI_CALL:	
      //handle_ssi_call(nc, ev_data);
      break;
    case MG_EV_RECV:
    //printf("MG_EV_RECV\n");
        break;
    case MG_EV_CLOSE:
        break;
	case MG_EV_ACCEPT:
		{
		//	printf("MG_EV_ACCEPT: %d\n",connectionCnt);
			if(connectionCnt==0)
			{
				writeToSocket(28,"1");		 //28. Device Configuration Started and client connected
				connectionCnt++;
			}
		}
	        break;
	default :			
		break;
    }
}

void generateRandomOTP()
{
	#ifdef PI
	
	
	memset(serialNo,'\0',sizeof(serialNo));
	memset(startAPModeCmd,'\0',sizeof(startAPModeCmd));
	

	FILE * serialNoFilePtr;
	
	
	serialNoFilePtr = fopen("/home/deviceSerial.txt","r");
	if(serialNoFilePtr)
	{
		fgets(serialNo,sizeof(serialNo), serialNoFilePtr);
		fclose(serialNoFilePtr);
	}
	
	if(strchr(serialNo,'\n')!=NULL)
	{
		serialNo[strlen(serialNo)-1]='\0';
	}
	else
	{
		serialNo[strlen(serialNo)]='\0';
	}
	
	if(strcmp(getCurrentWifiInterface(),"wlan0")==0)		//current active interface is wlan0
	{
		dongleOn = 0; //0-DongleOff	1-DongleOn		
	}
	else if(strcmp(getCurrentWifiInterface(),"wlan1")==0)	//current active interface is wlan1
	{
		dongleOn = 1; //0-DongleOff	1-DongleOn
		system("sudo ifconfig wlan0 down");
		system("sudo ifconfig wlan0 up");
		
	}
	
	sprintf(startAPModeCmd,"sudo ap %s ",serialNo);
#ifdef	DEBUG_ON	
	printf("\n\nStart AP Mode Command is: %s\n",startAPModeCmd);
#endif	//DEBUG_ON

	system(startAPModeCmd);

	#endif //PI


}

int writeToSocket(int index, char* data)
{
	Client_Webserver.index=index;						
	strcpy(Client_Webserver.parametersBuffer,data);				

	sockRetVal = send(sock,(Qt_WebServer_Sock*)&Client_Webserver,sizeof(Client_Webserver),MSG_DONTWAIT);
	
	printf("<-------->Send Socket Return Value for index %d: is %d\n", index,sockRetVal);
	
	if(sockRetVal < 0)
	{
		printf("ERROR writing to socket in webserver socket\n");
		sockRetVal = send(sock,(Qt_WebServer_Sock*)&Client_Webserver,sizeof(Client_Webserver),MSG_DONTWAIT);	//re-attempting again
		printf("Send Socket Second Attempt Return Value for index %d: is %d\n", index,sockRetVal);
	}

	sleep(1);
	
	return sockRetVal;

}

int isFirstTimeConfig()
{
	firstTimeConfig = fopen("/home/pi/Desktop/EdgeFirmware/miscfiles/deviceconfigcompleted.txt","r");
				
	if(firstTimeConfig==NULL)		//file not found, so this is first time configuration
	{
		printf("Unable to open deviceconfigcompleted.txt file\n");
		
		return 0;
	}
	else 					//file found., i.e second time configuration
	{
		fread(appStartStatus,1,1,firstTimeConfig);
		appStartStatus[2] = '\0';
		
		if(strcmp(appStartStatus,"0")==0)		//first time configuration
		{
			printf("First Time Device Configuration\n");
			
			return 0;
			
		}
		else if(strcmp(appStartStatus,"1")==0)	//second time configuration
		{
			printf("Second Time Device Configuration\n");						
			
			return 1;
		}
		
		fclose(firstTimeConfig);
	}
}

char* getAssetType()
{
	firstTimeConfig = fopen("/home/pi/Desktop/modbox-webserver/chinaIoT-English/assettype.txt","r");
				
	if(firstTimeConfig==NULL)		//file not found, so this is first time configuration
	{
		printf("Unable to open assettype.txt file\n");	
		
	}
	else 					//file found., i.e second time configuration
	{
		memset(currentAssetType,'\0',sizeof(currentAssetType));
		
		while (fgets(currentAssetType,sizeof(currentAssetType), firstTimeConfig)!=NULL)
		{
			
		}
		
		printf("Current Asset Type: %s\n",currentAssetType);
		
		fclose(firstTimeConfig);
	}
	
	if(strchr(currentAssetType,'\n')!=NULL)
	{
		currentAssetType[strlen(currentAssetType)-1]='\0';
	}
	else
	{
		currentAssetType[strlen(currentAssetType)]='\0';
	}
	
	
	return currentAssetType;
}

char* getAssetSerialNum()
{
	firstTimeConfig = fopen("/home/pi/Desktop/modbox-webserver/chinaIoT-English/assetserial.txt","r");
				
	if(firstTimeConfig==NULL)		//file not found, so this is first time configuration
	{
		printf("Unable to open assetserial.txt file\n");	
		
	}
	else 					//file found., i.e second time configuration
	{
		memset(currentAssetSNum,'\0',sizeof(currentAssetSNum));
		
		while (fgets(currentAssetSNum,sizeof(currentAssetSNum), firstTimeConfig)!=NULL)
		{
			
		}
		
		printf("Current Asset Serial: %s\n",currentAssetSNum);
		
		fclose(firstTimeConfig);
	}
	
	if(strchr(currentAssetSNum,'\n')!=NULL)
	{
		currentAssetSNum[strlen(currentAssetSNum)-1]='\0';
	}
	else
	{
		currentAssetSNum[strlen(currentAssetSNum)]='\0';
	}
	
	
	return currentAssetSNum;
}

void updateSetPoints(char* assettypeselected)
{
	printf(" Inside updateSetPoints : %s\n",assettypeselected);

	if(strcmp(assettypeselected,"ULT")==0)		//warm: -70 set: -80 cold: -90
	{
		system("echo -70.0 > /home/pi/Desktop/EdgeFirmware/metadata/warmalarm.txt");
		system("echo -80.0 > /home/pi/Desktop/EdgeFirmware/metadata/setpoint.txt");
		system("echo -90.0 > /home/pi/Desktop/EdgeFirmware/metadata/coldalarm.txt");
	}
	else if(strcmp(assettypeselected,"Refrigerator4")==0)		//warm: 5.5 set: 4.0 cold: 2.0
	{
		system("echo 5.5 > /home/pi/Desktop/EdgeFirmware/metadata/warmalarm.txt");
		system("echo 4.0 > /home/pi/Desktop/EdgeFirmware/metadata/setpoint.txt");
		system("echo 2.0 > /home/pi/Desktop/EdgeFirmware/metadata/coldalarm.txt");
	}
	else if(strcmp(assettypeselected,"Freezer2030")==0)		//warm: -25 set: -30 cold: -35
	{
		system("echo -25.0 > /home/pi/Desktop/EdgeFirmware/metadata/warmalarm.txt");
		system("echo -30.0 > /home/pi/Desktop/EdgeFirmware/metadata/setpoint.txt");
		system("echo -35.0 > /home/pi/Desktop/EdgeFirmware/metadata/coldalarm.txt");
	}
	else 			//warm: 5 set: 0 cold: -5
	{
		system("echo 5.0 > /home/pi/Desktop/EdgeFirmware/metadata/warmalarm.txt");
		system("echo 0.0 > /home/pi/Desktop/EdgeFirmware/metadata/setpoint.txt");
		system("echo -5.0 > /home/pi/Desktop/EdgeFirmware/metadata/coldalarm.txt");
	}
	
	system("sudo cp /home/pi/Desktop/EdgeFirmware/metadata/* /home/pi/Desktop/modbox-webserver/chinaIoT-English");
	system("sudo cp /home/pi/Desktop/EdgeFirmware/metadata/* /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese");

}


int main(void)
{	
	  
    //struct mg_mgr mgr;
    struct mg_connection *nc;
    mg_mgr_init(&mgr, NULL);
    nc = mg_bind(&mgr, s_http_port, ev_handler);
    mg_set_protocol_http_websocket(nc);
    
    changedAsset = 0;
    changedWifi = 0;
    connectionCnt=0;
    assetUpdatedFlag = 0;
    assetSerialUpdatedFlag = 0;
    
    dongleOn = 0 ; //0-DongleOff	1-DongleOn
    
    #ifdef PI
    	//socket creation
	//Create socket
	sock = socket(AF_INET , SOCK_STREAM , 0);
	if (sock == -1)
	{
		printf("Could not create socket");
	}
	
#ifdef DEBUG_ON
    printf("Webserver Socket Created...fd is %d\n",sock);
#endif	//DEBUG_ON

    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_family = AF_INET;
    server.sin_port = htons( 1234 );
    
    //Connect to remote server
    if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
    {
        
	perror("connect failed. Error");
		while(1)
		{
			sleep(1);
			if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
				{
					perror("connect failed. RETRY after a second");
			}
			else
			{
				break;
			}
		}


    }

#ifdef DEBUG_ON
	printf("Connected to QT Server from webserver client\n");
	printf("Socket Des: %d\n",sock);	
#endif	//DEBUG_ON
		
	
	Client_Webserver.index=13;						//13. otp button
	strcpy(Client_Webserver.parametersBuffer,"0");				

	sockRetVal = write(sock,(Qt_WebServer_Sock*)&Client_Webserver,sizeof(Client_Webserver));


	if(sockRetVal < 0)
	{
		printf("ERROR writing to socket in webserver socket\n");
		sockRetVal = write(sock,(Qt_WebServer_Sock*)&Client_Webserver,sizeof(Client_Webserver));
	}
	sleep(1);
	
	//identify whether the language selected is english or chinese
	languageFile = fopen("/home/pi/Desktop/EdgeFirmware/miscfiles/languageselection.txt","r");
	
	if(languageFile==NULL)		//file not found, set default lang to english
	{
		printf("Unable to open languageselection.txt file\n");
		
		languageSelectedFlag = 1;		//1-ENGLISH	2-CHINESE	//defaulting to english
	}
	else 					//file found., i.e second time configuration
	{
		fread(appStartStatus,1,1,languageFile);
		appStartStatus[2] = '\0';
		
		if(strcmp(appStartStatus,"1")==0)		//1-ENGLISH 2-CHINESE
		{
			printf("Language Selected is English\n");
			
			languageSelectedFlag = 1;		//1-ENGLISH	2-CHINESE
			
		}
		else if(strcmp(appStartStatus,"2")==0)	
		{
			printf("Language Selected is Chinese\n");
			
			languageSelectedFlag = 2;		//1-ENGLISH	2-CHINESE

		}
		
		fclose(languageFile);
	}
	
	
	
#endif	//PI

	
	wifiType = 0;		//1-Open	2-WPA	3-WEP	4-EAP
    

    
    #ifdef PI        
    system("sudo rm -rf /home/pi/Desktop/modbox-webserver/chinaIoT-English/password.txt");
    system("sudo rm -rf /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/password.txt");
	if(languageSelectedFlag==CHINESE)	
	{
		s_http_serve_opts.document_root = "/home/pi/Desktop/modbox-webserver/chinaIoT-Chinese";
		s_http_serve_opts.dav_document_root = "/home/pi/Desktop/modbox-webserver/chinaIoT-Chinese";
	}
	else
	{
		s_http_serve_opts.document_root = "/home/pi/Desktop/modbox-webserver/chinaIoT-English";
		s_http_serve_opts.dav_document_root = "/home/pi/Desktop/modbox-webserver/chinaIoT-English";		
	}
    #else
        s_http_serve_opts.document_root = "/home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT";
		s_http_serve_opts.dav_document_root = "/home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT";
    #endif	//PI
    
    s_http_serve_opts.enable_directory_listing = "no";
    
    s_http_serve_opts.url_rewrites = "**.txt | **.png | **.css| **.map| **.js=/home/cyient/pavan/DeviceConfiguration/23112017/Nov_22nd_2017/chinaIoT-English_22_nov/timeOut.htm";
    
    s_http_serve_opts.hidden_file_pattern = "**.txt | **.png | **.css | **.map | **.js";

    
        
    /********************************************/
    #ifdef PI  
    
    struct stat st = {0};

	if (stat("/home/pi/Desktop/EdgeFirmware/metadata", &st) == -1) {
		mkdir("/home/pi/Desktop/EdgeFirmware/metadata", 0777);
	}
	#endif	//PI
	
	generateWifiList();
    /********************************************/
    
    generateRandomOTP();
    
    
    printf("Starting web server on port %s\n", s_http_port);
    for(;;) {
        mg_mgr_poll(&mgr, 500);
    }
    mg_mgr_free(&mgr);
    return 0;
}


int readAssetData(struct http_message *hm)
{
	assetUpdatedFlag=0;
	assetSerialUpdatedFlag = 0;
	
	memset(assetType,'\0',sizeof(assetType));				
	memset(assetModel,'\0',sizeof(assetModel));
	memset(assetSerialNo,'\0',sizeof(assetSerialNo));
	memset(assetCity,'\0',sizeof(assetCity));
	memset(assetLocation,'\0',sizeof(assetLocation));	
	
					
	mg_get_http_var(&hm->body, "assetselect", assetType, sizeof(assetType));
	mg_get_http_var(&hm->body, "assetmodel", assetModel, sizeof(assetModel));
	mg_get_http_var(&hm->body, "serialnumber", assetSerialNo, sizeof(assetSerialNo));
	mg_get_http_var(&hm->body, "assetLocation", assetCity, sizeof(assetCity));
	mg_get_http_var(&hm->body, "assetcity", assetLocation, sizeof(assetLocation));
	
	printf("Asset Type: %s \n Asset Model: %s \n Serial Number: %s \n Location: %s \n City: %s \n",assetType, assetModel,assetSerialNo,assetCity,assetLocation);
	
	if((strcmp(assetType,"Freezer2030")==0) || (strcmp(assetType,"ULT")==0 ) || (strcmp(assetType,"Refrigerator4")==0 ) || (strcmp(assetType,"Others")==0 ) )	//selected asset is of known type
	{
		printf("Valid Asset Type Selected\t");
		if((strcmp(assetCity,"China")==0) || (strcmp(assetCity,"Outside China")==0) )	//selected assetcity if of known type
		{
			printf("Valid assetLocation Selected\n");
		}
		else
		{
			return 1;
		}
	}
	else
	{
		return 1;
	}
	
	#ifdef	PI
	if(strlen(getAssetType())>0)
	{
		if(strcmp(getAssetType(),assetType)!=0)
		{
			printf("Current Asset Type [%s] and Selected type [%s] are different.., so updating set points\n",getAssetType(),assetType);
			updateSetPoints(assetType);
			assetUpdatedFlag = 1;
			//system("/home/pi/Desktop/EdgeFirmware/reinstalldl.sh &");
		}

	}
	
	if(strlen(getAssetSerialNum())>0)
	{
		if(strcmp(getAssetSerialNum(),assetSerialNo)!=0)
		{
			printf("Current Asset Serial [%s] and Selected Serial [%s] are different.., so updating set points\n",getAssetSerialNum(),assetSerialNo);			
			assetSerialUpdatedFlag = 1;
			//system("/home/pi/Desktop/EdgeFirmware/reinstalldl.sh &");
		}

	}

	else
	{
		printf("This is first time configuration.. So not updating set points\n");
	}
	#endif	//PI
	
	return 0;
	
}

int readWifiName(struct http_message *hm)
{
	memset(selectedAPName,'\0',sizeof(selectedAPName));                
	mg_get_http_var(&hm->body, "wifiHiddenField", selectedAPName, sizeof(selectedAPName));				
	printf("Selected AP %s\n",selectedAPName);
	
	memset(encodedSSIDCmd,'\0',sizeof(encodedSSIDCmd));
	memset(apNameEncoded,'\0',sizeof(apNameEncoded));
	
	memset(tmp,'\0',sizeof(tmp));
	sprintf(tmp,"%d",selectedAPName[0]);
	
	if( (atoi(tmp)) > 0 && (atoi(tmp)) < 127 )	//ascii from 1 to 127 i.e. english character name
	{
		printf("Selected AP String is english\n");
	}
	else 	//selected ap name is non english character
	{
		printf("Selected AP String is non english\n");
		
		//convert chinese to hex string
				
		sprintf(encodedSSIDCmd,"echo -n %s | xxd -p -u | sed \'s/.\\\{2\\}/&\\\\x/g;s/^\\\(.\\\{0\\}\\)/\\1\\\\x/;s/\\\(.*\\)\\\\x/\\1/\'",selectedAPName);
		#ifdef DEBUG_ON
		printf("String to convert chinese back to hex: %s\n",encodedSSIDCmd);
		#endif	//0
		
		/* Open the command for reading. */
		sysCmdFp = popen(encodedSSIDCmd, "r");
		if (sysCmdFp == NULL) 
		{
			printf("Failed to run command to extract UTF Encoded SSID name\n" );		
		}

		/* Read the output a line at a time - output it. */
		while (fgets(apNameEncoded, sizeof(apNameEncoded)-1, sysCmdFp) != NULL) {
			#ifdef DEBUG_ON
			printf("AP Name Converted From Non English to Hex is: %s", apNameEncoded);
			#endif //DEBUG_ON
		}

		/* close */
		pclose(sysCmdFp);
		
		memset(selectedAPName,'\0',sizeof(selectedAPName));
		strcpy(selectedAPName,apNameEncoded);	//copying the converted hex string on to selectedAPName variable
		
		memset(encodedSSIDCmd,'\0',sizeof(encodedSSIDCmd));
		memset(apNameEncoded,'\0',sizeof(apNameEncoded));
		
		if(strchr(selectedAPName,'\n')!=NULL)
		{
			selectedAPName[strlen(selectedAPName)-1]='\0';
		}
		else
		{
			selectedAPName[strlen(selectedAPName)]='\0';
		}
		
		
		sprintf(encodedSSIDCmd,"echo %s | sed \'s/\\x//g\'",selectedAPName);	
		#ifdef DEBUG_ON
		printf("String from hex string in stripped format: %s\n",encodedSSIDCmd);	
		#endif //DEBUG_ON

		/* Open the command for reading. */
		sysCmdFp = popen(encodedSSIDCmd, "r");
		if (sysCmdFp == NULL) 
		{
			printf("Failed to run command to extract UTF Encoded SSID name\n" );		
		}

		/* Read the output a line at a time - output it. */
		while (fgets(apNameEncoded, sizeof(apNameEncoded)-1, sysCmdFp) != NULL) {
		//printf("%s", apNameEncoded);
		}

		/* close */
		pclose(sysCmdFp);
		
		if(strchr(apNameEncoded,'\n')!=NULL)
		{
			apNameEncoded[strlen(apNameEncoded)-1]='\0';
		}
		else
		{
			apNameEncoded[strlen(apNameEncoded)]='\0';
		}

		
		printf("Final AP Name: %s\n",apNameEncoded);

		selectedAPName[strlen(selectedAPName)]='\0';
		
	}
	
	

	if(strcmp(selectedAPName,"Others")==0)
	{
		memset(selectedAPName,'\0',sizeof(selectedAPName));                
		mg_get_http_var(&hm->body, "ssidName", selectedAPName, sizeof(selectedAPName));				
		printf("Selected AP %s %d\n",selectedAPName,__LINE__);		
		
	}

	memset(apNameWriteCmd,'\0',sizeof(apNameWriteCmd));
	#ifdef PI
	if(strlen(apNameEncoded)>0)		//selected ap name is in non english characters
	{
		memset(encodedSSIDCmd,'\0',sizeof(encodedSSIDCmd));			
				
		sprintf(encodedSSIDCmd,"echo %s | sed \'s/[\\\\x]//g\' | xxd -r -p > /home/pi/Desktop/modbox-webserver/chinaIoT-English/selectedap.txt",selectedAPName);
		printf("selectedap.txt Final String: %s\n",encodedSSIDCmd);
		system(encodedSSIDCmd);		
		
		system("sudo cp /home/pi/Desktop/modbox-webserver/chinaIoT-English/selectedap.txt /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/selectedap.txt");

	}
	else
	{
		sprintf(apNameWriteCmd,"echo \'%s\' > /home/pi/Desktop/modbox-webserver/chinaIoT-English/selectedap.txt",selectedAPName);
		system(apNameWriteCmd);
		
		memset(apNameWriteCmd,'\0',sizeof(apNameWriteCmd));
		
		sprintf(apNameWriteCmd,"echo \'%s\' > /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/selectedap.txt",selectedAPName);
		system(apNameWriteCmd);
	}

	
	#else
	
	sprintf(apNameWriteCmd,"echo \'%s\' > /home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/selectedap.txt",selectedAPName);
	system(apNameWriteCmd);

	#endif	//PI
	


	return checkWifiType(selectedAPName);
	
		
}

void readWifiData(struct http_message *hm)
{
//	memset(selectedAPName,'\0',sizeof(selectedAPName));                
//	mg_get_http_var(&hm->body, "wifiHiddenField", selectedAPName, sizeof(selectedAPName));				
//	printf("Selected AP %s\n",selectedAPName);	

	memset(selecteAPUserName,'\0',sizeof(selecteAPUserName));                
	mg_get_http_var(&hm->body, "wifiName", selecteAPUserName, sizeof(selecteAPUserName));				
	printf("UserName %s\n",selecteAPUserName);	

	memset(selectedAPPwd,'\0',sizeof(selectedAPPwd));                
	mg_get_http_var(&hm->body, "wifiPassword", selectedAPPwd, sizeof(selectedAPPwd));				
	printf("Password %s\n",selectedAPPwd);	

}

void readAlarmData(struct http_message *hm)
{
	memset(highTempPoint,'\0',sizeof(highTempPoint));
	memset(lowTempPoint,'\0',sizeof(lowTempPoint));
	memset(setTempPoint,'\0',sizeof(setTempPoint));
	
	mg_get_http_var(&hm->body, "warmAlarm", highTempPoint, sizeof(highTempPoint));
	mg_get_http_var(&hm->body, "setPoint", setTempPoint, sizeof(setTempPoint));
	mg_get_http_var(&hm->body, "coldAlarm", lowTempPoint, sizeof(lowTempPoint));
	
	//printf("Received Body: %s\n",hm->body.p);
	
	printf("<--doneAlarm-->High Temp: %s\n Set Temp: %s\n Low Temp: %s\n ",highTempPoint,setTempPoint,lowTempPoint);

}

int checkWifiType(char *apName)
{
	find_ssid=0,find_cell=0,line_num_ssid=0,line_num_cell=0;
	find_encrypt_on=0,find_encrypt_off=0,find_eap=0,find_psk=0;	
	i=0,cnt=0;
	
		
	memset(temp,'\0',sizeof(temp));
	memset(buffer,'\0',sizeof(buffer));
	memset(str,'\0',sizeof(str));
	memset(getApTypeCmd,'\0',sizeof(getApTypeCmd));
	memset(Previous_line,'\0',sizeof(Previous_line));
	memset(Previous_line2,'\0',sizeof(Previous_line2));
	memset(wifiSignalTemp,'\0',sizeof(wifiSignalTemp));
	memset(wifiSignal,'\0',sizeof(wifiSignal));
					 
	#ifdef PI
	
	if(languageSelectedFlag==CHINESE)	
	{
		system("sudo rm -rf /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/network_type.txt");
		system("sudo rm -rf /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/lowwifisignal.txt");

	}
	else
	{
		system("sudo rm -rf /home/pi/Desktop/modbox-webserver/chinaIoT-English/network_type.txt");
		system("sudo rm -rf /home/pi/Desktop/modbox-webserver/chinaIoT-English/lowwifisignal.txt");

	}
	
	#else
	system("sudo rm  /home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/check_wifinw.txt");
	system("sudo rm  /home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/network_type.txt");					 
	sprintf(getApTypeCmd,"sudo iwlist wlp2s0 scanning >> /home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/check_wifinw.txt");
	printf("<--PC--> %s\n",getApTypeCmd);
	system(getApTypeCmd);
	#endif	//PI


	#ifdef PI
	if(languageSelectedFlag==CHINESE)	
	{
		fp=fopen("/home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/check_wifinw.txt","r");
	}
	else
	{
		fp=fopen("/home/pi/Desktop/modbox-webserver/chinaIoT-English/check_wifinw.txt","r");
	}
	
	#else
	fp=fopen("/home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/check_wifinw.txt","r");
	#endif	//PI
	
		
	if( fp == NULL )
	{
		printf("Unable to open check_wifinw file\n");
	}
	else
	{
		while(fgets(temp,512,fp) != NULL)
		{
		     if((strstr(temp,apName)) != NULL) {
		           //printf("ssid line number = %d   ssid=%s\n", line_num_ssid,temp);
		           find_ssid++;
			   
	  		   strcat(str,Previous_line);
	  		   strcat(str,Previous_line2);	  		   
			   strcat(str,temp);			   
			   break;
		     }
		     	strcpy(Previous_line,temp);
		     	
		     	if(line_num_ssid%2==0)
		     	{
					memset(Previous_line,'\0',sizeof(Previous_line));
					strcpy(Previous_line,temp);
					//printf("PREVIOUS LINE 1: %s %d\n",Previous_line,strlen(Previous_line));
				}
				else if(line_num_ssid%2==1)
		     	{
					memset(Previous_line2,'\0',sizeof(Previous_line2));
					strcpy(Previous_line2,temp);
					//printf("PREVIOUS LINE 2: %s %d\n",Previous_line2,strlen(Previous_line2));
				}
		     	
		     	line_num_ssid++;  
		}
		while(fgets(temp,512,fp) != NULL)
		{

			if((strstr(temp, "Cell")) != NULL){
					//printf("ssid line number = %d  Cell=%s\n", line_num_cell,temp);
					find_cell++;					
			break;
				}
			strcat(str,temp);
				line_num_cell++;
		}

		fclose(fp);
		
		memset(temp,'\0',sizeof(temp));
		strcpy(temp,str);
		

        	//printf("<----->string = %s\n",str);

		if((strstr(str,encryption_on)) != NULL){		 
			find_encrypt_on++;
		}
		if((strstr(str,encryption_off)) != NULL){
			find_encrypt_off++;
		}
		if((strstr(str,authentication_eap)) != NULL){
			find_eap++;
		}
		if((strstr(str,authentication_psk)) != NULL){
			 find_psk++;
		}
		
				if((strstr(temp, "Signal level")) != NULL)
		{		
			for (token = strtok(temp, "="); token; token = strtok(NULL, "="))
			{
			    if(strstr(token,"dBm"))
			    {
					strcpy(wifiSignalTemp,token);
				}
			}
			
			if(strlen(wifiSignalTemp)>0)
			{
				token = strtok(wifiSignalTemp, " "); 
				strcpy(wifiSignal,token);
				printf("wifiSignal=%s %d\n", wifiSignal,abs(atoi(wifiSignal)));
			}
			
		}
		
		if(abs(atoi(wifiSignal))>70)
		{
			system("echo 1 > /home/pi/Desktop/modbox-webserver/chinaIoT-English/lowwifisignal.txt");
			system("echo 1 > /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/lowwifisignal.txt");
		}

		
		if((find_encrypt_on && find_eap))
		{
			printf("Enterprise Network\n");
			
			wifiType = 4;		//1-Open	2-WPA-PSK	3-WEP	4-EAP
			
			#ifdef PI
			if(languageSelectedFlag==CHINESE)	
			{
				system("sudo echo 2 > /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/network_type.txt");	//2 - show both user name and password field
			}
			else
			{
				system("sudo echo 2 > /home/pi/Desktop/modbox-webserver/chinaIoT-English/network_type.txt");	//2 - show both user name and password field
			}

			#else
			
			system("sudo echo 2 > /home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/network_type.txt");
			
			#endif	//PI
			
			return 4;

		}		
		else if((find_encrypt_on && find_psk))
		{
			printf("PSK Network\n");
			
			wifiType = 2;		//1-Open	2-WPA-PSK	3-WEP	4-EAP
			
			#ifdef PI
			if(languageSelectedFlag==CHINESE)	
			{
				system("sudo echo 1 > /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/network_type.txt");	//1 - show only password field
			}
			else
			{
				system("sudo echo 1 > /home/pi/Desktop/modbox-webserver/chinaIoT-English/network_type.txt");	//1 - show only password field
			}

			#else
			
			system("sudo echo 1 > /home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/network_type.txt");
			
			#endif	//PI
			
			return 2;

		}
		
		else if((find_encrypt_on && find_psk==0 && find_eap==0))
		{
			printf("WEP network Network\n");
		        
			wifiType = 3;		//1-Open	2-WPA-PSK	3-WEP	4-EAP
			
			#ifdef PI
			if(languageSelectedFlag==CHINESE)	
			{
				system("sudo echo 1 > /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/network_type.txt");	//1 - show only password field
			}
			else
			{
				system("sudo echo 1 > /home/pi/Desktop/modbox-webserver/chinaIoT-English/network_type.txt");	//1 - show only password field
			}

			#else
			
			system("sudo echo 1 > /home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/network_type.txt");
			
			#endif	//PI
			
			return 3;

	 	}
	 	
		else if((find_encrypt_off && find_psk==0 && find_eap==0))
		{
			printf("Open network Network\n");
			
			wifiType = 1;		//1-Open	2-WPA-PSK	3-WEP	4-EAP
		
			#ifdef PI
			if(languageSelectedFlag==CHINESE)	
			{
				system("sudo echo 0 > /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/network_type.txt");
			}
			else
			{
				system("sudo echo 0 > /home/pi/Desktop/modbox-webserver/chinaIoT-English/network_type.txt");
			}

			#else
			
			system("sudo echo 0 > /home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/network_type.txt");
			
			#endif	//PI
			return 1;

		}
		else
		{
			printf("Reached default case... so setting default type as Unknown Network\n");
			
			//wifiType = 5;		//1-Open	2-WPA-PSK	3-WEP	4-EAP	5-Unable to find the specified SSID
			
			#ifdef PI
			if(languageSelectedFlag==CHINESE)	
			{
				system("sudo echo 3 > /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/network_type.txt");
			}
			else
			{
				system("sudo echo 3 > /home/pi/Desktop/modbox-webserver/chinaIoT-English/network_type.txt");
			}

			#else
			
			system("sudo echo 1 > /home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/network_type.txt");
			
			#endif	//PI
			
			return 5;
		}

	}

	return 0;

}


void switchToStationMode()
{
	//checking selected ap name for special character
	

		if(wifiType==1)		//open network
		{
			/*if(strlen(selectedAPName)>0)
			{
				memset(startStationModeCmd,'\0',sizeof(startStationModeCmd));				
				sprintf(startStationModeCmd,"sudo sta %s &\n",selectedAPName);
				printf("*************startStationModeCmd is %s\n",startStationModeCmd);
				system(startStationModeCmd);						
								
			}*/
		}
		else if(wifiType==2)		//WPA-PSK network
		{
			if(strlen(selectedAPName)>0 && strlen(selectedAPPwd)>0)
			{
				
				wpaFile = fopen("/home/pi/Desktop/EdgeFirmware/wpa_supplicant.conf","w");
				
				if(wpaFile==NULL)
				{
					printf("Failed to open wpa_supplicant.conf file for update");
				}
				else
				{
					fprintf(wpaFile,"ctrl_interface=/var/run/wpa_supplicant\n");
					fprintf(wpaFile,"update_config=1\n\n");

					fprintf(wpaFile,"network={\n");
					
					if(strlen(apNameEncoded)>0)	//selected ap name is encoded utf8 format., so no need for quotes
					{
						fprintf(wpaFile,"ssid=%s \n",apNameEncoded);
					}
					else 							//selected ap name is normal text
					{
						fprintf(wpaFile,"ssid=\"%s\" \n",selectedAPName);
					}
					
					fprintf(wpaFile,"psk=\"%s\" \n",selectedAPPwd);
					fprintf(wpaFile,"key_mgmt=WPA-PSK\n");
					fprintf(wpaFile,"}\n");
					
					fclose(wpaFile);
					
					system("sudo mv /home/pi/Desktop/EdgeFirmware/wpa_supplicant.conf /etc/wpa_supplicant/");
					
					//system("sudo sta &");					
					connectToWifi();
				}							
				
			}
		}
		else if(wifiType==3)		//WEP network
		{
			if(strlen(selectedAPName)>0 && strlen(selectedAPPwd)>0)
			{
				wpaFile = fopen("/home/pi/Desktop/EdgeFirmware/wpa_supplicant.conf","w");
				
				if(wpaFile==NULL)
				{
					printf("Failed to open wpa_supplicant.conf file for update");
				}
				else
				{
					fprintf(wpaFile,"ctrl_interface=/var/run/wpa_supplicant\n");
					fprintf(wpaFile,"update_config=1\n\n");
					fprintf(wpaFile,"network={\n");

					if(strlen(apNameEncoded)>0)	//selected ap name is encoded utf8 format., so no need for quotes
					{
						fprintf(wpaFile,"ssid=%s \n",apNameEncoded);
					}
					else 							//selected ap name is normal text
					{
						fprintf(wpaFile,"ssid=\"%s\" \n",selectedAPName);
					}

					fprintf(wpaFile,"key_mgmt=NONE\n");
					fprintf(wpaFile,"wep_key0=%s \n",selectedAPPwd);
					fprintf(wpaFile,"}\n");
					
					fclose(wpaFile);
					
					system("sudo mv /home/pi/Desktop/EdgeFirmware/wpa_supplicant.conf /etc/wpa_supplicant/");
					
					//system("sudo sta &");					
					connectToWifi();
				}
			}			
		}
		else if(wifiType==4)		//EAP network
		{
			if(strlen(selectedAPName)>0 && strlen(selecteAPUserName)>0 && strlen(selectedAPPwd)>0 )
			{
				wpaFile = fopen("/home/pi/Desktop/EdgeFirmware/wpa_supplicant.conf","w");
			
				if(wpaFile==NULL)
				{
					printf("Failed to open wpa_supplicant.conf file for update");
				}
				else
				{
					fprintf(wpaFile,"ctrl_interface=/var/run/wpa_supplicant\n");
					fprintf(wpaFile,"update_config=1\n\n");
					fprintf(wpaFile,"network={\n");

					if(strlen(apNameEncoded)>0)	//selected ap name is encoded utf8 format., so no need for quotes
					{
						fprintf(wpaFile,"ssid=%s \n",apNameEncoded);
					}
					else 							//selected ap name is normal text
					{
						fprintf(wpaFile,"ssid=\"%s\" \n",selectedAPName);
					}

					fprintf(wpaFile,"scan_ssid=1\n");
					fprintf(wpaFile,"proto=RSN\n");
					fprintf(wpaFile,"key_mgmt=WPA-EAP\n");
					fprintf(wpaFile,"pairwise=CCMP TKIP\n");
					fprintf(wpaFile,"group=CCMP TKIP WEP104 WEP40\n");
					fprintf(wpaFile,"eap=PEAP\n");
					fprintf(wpaFile,"identity=\"%s\" \n",selecteAPUserName);
					fprintf(wpaFile,"password=\"%s\" \n",selectedAPPwd);
					fprintf(wpaFile,"phase1=\"peapver=0\"\n");
					fprintf(wpaFile,"phase2=\"MSCHAPV2\"\n");	
					fprintf(wpaFile,"}\n");
					
					fclose(wpaFile);
					
					system("sudo mv /home/pi/Desktop/EdgeFirmware/wpa_supplicant.conf /etc/wpa_supplicant/");
					
					system("sudo cp /home/pi/Desktop/EdgeFirmware/interfaces /etc/network");					
					
					//system("sudo sta &");			
					connectToWifi();

				}				
			}			
		}	
		else 	//default case
		{
			printf("Executing Default sta option\n");
			//system("sudo sta &");
			connectToWifi();
		}
}

void connectToWifi()
{
	if(dongleOn==1)	//dongle is connected.., so taking wlan0 to down
	{
		system("sudo ifconfig wlan0 down");	
		dongleOn=0;	
	}
	
	sleep(5);
	
	if(strcmp(getCurrentWifiInterface(),"wlan1")==0)
	{
		printf("*****&&&&&Dongle On Fired Here %d\n",__LINE__);
		system("/home/pi/Desktop/EdgeFirmware/dongleonapp.sh &");
	}
	else if(strcmp(getCurrentWifiInterface(),"wlan0")==0)
	{
		printf("*****&&&&&Dongle Off Fired Here %d\n",__LINE__);
		system("/home/pi/Desktop/EdgeFirmware/dongleoff.sh &");
	}

	else
	{
		system("/home/pi/Desktop/EdgeFirmware/dongleoff.sh &");
	}
	
}

char* getCurrentWifiInterface()
{
	//check if interface active is wlan0 or wlan1
	if(!(wifiSignalOutput = popen("sudo ifconfig |grep wlan1","r")))
	{
		printf("Unable to get interface details on wlan1\n");;
	}
	else
	{
		memset(wifiInterfaceName,'\0',sizeof(wifiInterfaceName));
		fscanf(wifiSignalOutput,"%s",wifiInterfaceName);
		pclose(wifiSignalOutput);

		if(strlen(wifiInterfaceName)>0)     //interface available is wlan1
		{
			printf("Current Present Inteface is wlan1\n");
			
			return "wlan1";

		}
		else
		{
			if(!(wifiSignalOutput = popen("sudo ifconfig |grep wlan0","r")))
			{
				printf("Unable to get interface details on wlan0\n");
			}
			else
			{
				memset(wifiInterfaceName,'\0',sizeof(wifiInterfaceName));
				fscanf(wifiSignalOutput,"%s",wifiInterfaceName);
				pclose(wifiSignalOutput);

				if(strlen(wifiInterfaceName)>0)     //interface available is wlan0
				{
					printf("Current Present Inteface is wlan0\n");
					return "wlan0";
				}
				else    //neither wlan0 or wlan1 is present
				{
					return "wlan0";
				}

			}
		}
	}
}

void updateAssetDetails()
{
	if(strlen(assetModel)>0 && strlen(assetSerialNo)>0 && strlen(assetCity)>0 && strlen(assetLocation)>0 )
	{
		
		memset(assetTypeCmd,'\0',sizeof(assetTypeCmd));
		memset(assetModelCmd,'\0',sizeof(assetModelCmd));
		memset(assetSerialNoCmd,'\0',sizeof(assetSerialNoCmd));
		memset(assetCityCmd,'\0',sizeof(assetCityCmd));
		memset(assetLocationCmd,'\0',sizeof(assetLocationCmd));
		
	
		#ifdef PI				
		sprintf(assetTypeCmd,"echo %s > /home/pi/Desktop/EdgeFirmware/metadata/assettype.txt",assetType);
		sprintf(assetModelCmd,"echo %s > /home/pi/Desktop/EdgeFirmware/metadata/assetmodel.txt",assetModel);
		sprintf(assetSerialNoCmd,"echo %s > /home/pi/Desktop/EdgeFirmware/metadata/assetserial.txt",assetSerialNo);
		sprintf(assetCityCmd,"echo %s > /home/pi/Desktop/EdgeFirmware/metadata/assetcity.txt",assetCity);
		sprintf(assetLocationCmd,"echo %s > /home/pi/Desktop/EdgeFirmware/metadata/assetlocation.txt",assetLocation);
		
		printf("Asset Type Command is %s\n",assetTypeCmd);	
		
		
		#else
		sprintf(assetTypeCmd,"echo %s > /home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/assettype.txt",assetType);
		sprintf(assetModelCmd,"echo %s > /home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/assetmodel.txt",assetModel);
		sprintf(assetSerialNoCmd,"echo %s > /home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/assetserial.txt",assetSerialNo);
		sprintf(assetCityCmd,"echo %s > /home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/assetcity.txt",assetCity);
		sprintf(assetLocationCmd,"echo %s > /home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/assetlocation.txt",assetLocation);
		
		printf("-> %s\n",assetTypeCmd);
		printf("-> %s\n",assetModelCmd);
		printf("-> %s\n",assetSerialNoCmd);
		printf("-> %s\n",assetCityCmd);
		printf("-> %s\n",assetLocationCmd);

		#endif	//PI		
		
		system(assetTypeCmd);
		system(assetModelCmd);
		system(assetSerialNoCmd);
		system(assetCityCmd);
		system(assetLocationCmd);
		
		#ifdef PI
		system("sudo cp /home/pi/Desktop/EdgeFirmware/metadata/* /home/pi/Desktop/modbox-webserver/chinaIoT-English");
		system("sudo cp /home/pi/Desktop/EdgeFirmware/metadata/* /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese");
		#endif	//PI
		
		//once updated into config files, setting the values to null
		memset(assetType,'\0',sizeof(assetType));
		memset(assetModel,'\0',sizeof(assetModel));
		memset(assetSerialNo,'\0',sizeof(assetSerialNo));
		memset(assetCity,'\0',sizeof(assetCity));
		memset(assetLocation,'\0',sizeof(assetLocation));
		

	}				

	if(strlen(highTempPoint)>0 && strlen(setTempPoint)>0 && strlen(lowTempPoint)>0)
	{
		memset(highTempCmd,'\0',sizeof(highTempCmd));
		memset(lowTempCmd,'\0',sizeof(lowTempCmd));
		memset(setPointTempCmd,'\0',sizeof(setPointTempCmd));

		#ifdef PI
		sprintf(highTempCmd,"echo %s > /home/pi/Desktop/EdgeFirmware/metadata/warmalarm.txt",highTempPoint);
		sprintf(setPointTempCmd,"echo %s > /home/pi/Desktop/EdgeFirmware/metadata/setpoint.txt",setTempPoint);
		sprintf(lowTempCmd,"echo %s > /home/pi/Desktop/EdgeFirmware/metadata/coldalarm.txt",lowTempPoint);
		
		printf("\n***********highTempCmd is %s\n",highTempCmd);

		#else
		sprintf(highTempCmd,"echo %s > /home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/warmalarm.txt",highTempPoint);
		sprintf(setPointTempCmd,"echo %s > /home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/setpoint.txt",setTempPoint);
		sprintf(lowTempCmd,"echo %s > /home/cyient/pavan/DeviceConfiguration/webserver/chinaIoT/coldalarm.txt",lowTempPoint);

		#endif	//PI
			
		
		system(highTempCmd);
		system(setPointTempCmd);
		system(lowTempCmd);
		
		#ifdef PI
		system("sudo cp /home/pi/Desktop/EdgeFirmware/metadata/* /home/pi/Desktop/modbox-webserver/chinaIoT-English");
		system("sudo cp /home/pi/Desktop/EdgeFirmware/metadata/* /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese");
		#endif	//PI
		
		//once updated into config files, setting the values to null
		memset(highTempPoint,'\0',sizeof(highTempPoint));
		memset(setTempPoint,'\0',sizeof(setTempPoint));
		memset(lowTempPoint,'\0',sizeof(lowTempPoint));

		
		
	}
}
