#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

pthread_t thread;
FILE *fp1;
char LEDstatus[3];
int keypress = 0;


void dosomething(void)
{
        //printf("Created New Thread for reading GPIO");
	while(1)
	{
		system("cat /sys/class/gpio/gpio5/value > /home/pi/Desktop/EdgeFirmware/fct/status.txt");
		fp1 = fopen("/home/pi/Desktop/EdgeFirmware/fct/status.txt","r");
		fread(LEDstatus,1,1,fp1);
		LEDstatus[2] = '\0';
		fclose(fp1);
		if(strcmp(LEDstatus,"1"))
		{
			system("echo \"0\" > /sys/class/gpio/gpio12/value");  // OFF the BUZZER
			system("echo \"0\" > /sys/class/gpio/gpio27/value");  // OFF the LED
			system("echo \"0\" > /sys/class/gpio/gpio22/value");  // OFF the LED
			system("echo \"0\" > /sys/class/gpio/gpio26/value");  // OFF the LED
			break;
		}
				
	}

}

void main()
{
	int i=0;

	printf("Export GPIO\n");

	system("echo \"27\" > /sys/class/gpio/export");  
	system("echo \"22\" > /sys/class/gpio/export");  
	system("echo \"26\" > /sys/class/gpio/export");  

	printf("Set GPIO Direction\n");
	system("echo \"out\" > /sys/class/gpio/gpio27/direction	");  
	system("echo \"out\" > /sys/class/gpio/gpio22/direction");  
	system("echo \"out\" > /sys/class/gpio/gpio26/direction");  

	system("echo \"12\" > /sys/class/gpio/export");  
	system("echo \"5\" > /sys/class/gpio/export"); 


	printf("Set GPIO Direction\n");
	system("echo \"out\" > /sys/class/gpio/gpio12/direction");  

	system("echo \"in\" > /sys/class/gpio/gpio5/direction");  

	system("echo \"1\" > /sys/class/gpio/gpio27/value");  //  Glow the LED
	system("echo \"1\" > /sys/class/gpio/gpio22/value");  //  Glow the LED
	system("echo \"1\" > /sys/class/gpio/gpio26/value");  //  Glow the LED
	system("echo \"1\" > /sys/class/gpio/gpio12/value");  //  BUZZER ON

	dosomething();


}
