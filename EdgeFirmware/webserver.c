/*
 * Copyright (c) 2014 Cesanta Software Limited
 * All rights reserved
 *
 * To test this server, do
 *   $ curl -d '{"id":1,method:"sum",params:[22,33]}' 127.0.0.1:8000
 */

#include "mongoose.h"
#include <string.h>
#include "frozen.h"
#define _GNU_SOURCE
#include <stdio.h>
//#include <stdlib.h>
#include <curl/curl.h>
#include <sys/stat.h>
#include <sys/socket.h>  //socket
#include <arpa/inet.h>   //inet_addr
#include <netinet/tcp.h>
#include <time.h>


#define DEBUG_ON	1

#define PI	1

#define	PROCESS_UPDATE_METADATA	1
#define	PROCESS_UPDATE_ALARM_SNOOZE	2
#define	PROCESS_UPDATE_ALARM_ACK	3



static const char *s_http_port = "8000";

static int principalIdCnt=0;

char executionId[50];

char *setPoint_se1, *highTemp_se1, *lowTemp_se1;

char *name_se1, *assetType, *assetModel, *assetSerialNo, *assetLocation, *assetCity, *instrumentName, *byUser;

char commandKey[50], alarmId[50], snoozeTime[50], alarmType[10], eventKey[20], principalId[50]; //instrumentName[20], byUser[20];

char receivedCmd[20];
char qrCodeImage[20];
char telemetryResponse[10];
char dispatchResponse[10];
char idInChar[20];
struct json_token_frozen sensor1_token;
char timeinUTC[30];
time_t t;
struct tm tm;
char executionIdPrevious[50];
char assetModelCmd[100], assetSerialNoCmd[100], assetCityCmd[100], assetLocationCmd[100], assetTypeCmd[100], sensorNameCmd[100];

char highTempCmd[80], lowTempCmd[80], setPointTempCmd[80];

char metaDataRespBuffer[500];
char ackCmdRespBuffer[500];


char *friendlyName;
char friendlyNameCmd[50];
char deviceName[20];
char downloadURL[50];
char checksum[50];
char version[10];

int sock;
int sockRetVal;
struct sockaddr_in server;

char buf[100];

char receivedDataBuf[9000];
char *requestIdBuf;
int iterationCnt,i;
char requestIdBufTmp[32];
char principalIdTemp[50];

FILE *principalIdFp;
 FILE *firmwareFileFp;
CURL *curl;
CURLcode res;
char url[]= "http://localhost:9000/v1/IoTRequest";
char urlAck[]= "http://localhost:9000/v1/deviceresponse";
struct curl_slist *headers = NULL;
char metaDataBuffer[500];
char totalMetaDataBuffer[500];
long http_code;

int remoteCmdCounter;

int writeToSocket(int index, char* data);
int sendAckSettRemoteCmd(char * commandExecId, int responseFlag);
int sendRespAckAlarm(char * commandExecId, int responseFlag);
int sendRespSnooze(char * commandExecId, int responseFlag);
int sendAckModifyDevName(char * commandExecId);
int processFWUpdCmd(char *downloadURL,char *checksum,char *version);	

void readUTCTimenow(void);
char* generateIdNum();
char buf0[1000];
char randomNumber[20];

char heartBeat[10];
char heartBeatId[16];

long long int randOTP;

char *checksumMatch;

struct timespec tim, tim2;

char currentAlarmId[50];

/*
 ============================================================================
 function Name	: generateIdNum
 return type	: char pointer
 Description	: This function is to generate the id needed in every command to deeplaser
 ============================================================================
*/
char *generateIdNum()		
{
	sleep(1);
	randOTP = 0;	
	srand(time(NULL)); 
	randOTP = rand()% 9000000000 + 1000000000; 
	memset(randomNumber,'\0',sizeof(randomNumber));
	sprintf(randomNumber,"%llu",randOTP);
	
	return randomNumber;
}



typedef struct Webserver_Struct
{
    char parametersBuffer[40];
    int index;
} Qt_WebServer_Sock;


Qt_WebServer_Sock Client_Webserver;

/*
 ============================================================================
 function Name	: stringReplace
 return type	: none
 Description	: This function is to replace new char in the provided string
 ============================================================================
*/
void stringReplace(char *dst) {
  int offset = 0;
  do {
    while (dst[offset] == '\n') ++offset;
    *dst = dst[offset];
  } while (*dst++);
}


/*
 ============================================================================
 function Name	: write_data
 return type	: int
 Description	: This function is to set callback for writing received data for curl post
 ============================================================================
*/
size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
    size_t written = fwrite(ptr, size, nmemb, stream);
    return written;
}
 
 
 /*
 ============================================================================
 function Name	: WriteMemoryCallback
 return type	: int
 Description	: This function is to set callback that receives header data 
 ============================================================================
*/
static size_t
WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
  size_t realsize = size * nmemb;

  printf("%s",(char*)contents);
#if 0
  struct MemoryStruct *mem = (struct MemoryStruct *)userp;
 
  mem->memory = realloc(mem->memory, mem->size + realsize + 1);
  if(mem->memory == NULL) {
    /* out of memory! */ 
    printf("not enough memory (realloc returned NULL)\n");
    return 0;
  }
 
  memcpy(&(mem->memory[mem->size]), contents, realsize);
  mem->size += realsize;
  mem->memory[mem->size] = 0;
#endif  //0
// printf("Real Size: %d\n",realsize);
  return realsize;
}


 /*
 ============================================================================
 function Name	: ev_handler
 return type	: none
 Description	: This function is to set callback for receiving http requests
 ============================================================================
*/
static void ev_handler(struct mg_connection *nc, int ev, void *ev_data) {
  
  struct http_message *hm = NULL;
  hm = (struct http_message *) ev_data;


  memset(receivedDataBuf,'\0',sizeof(receivedDataBuf));
  
  switch (ev) {
    case MG_EV_HTTP_REQUEST:      
		memset(receivedDataBuf,'\0',sizeof(receivedDataBuf));
		strncpy(receivedDataBuf,hm->body.p,hm->body.len);
	
		printf("\n\n");	
		printf("Received Data: %s\n", receivedDataBuf);			
		
		#if 1		
		memset(receivedCmd,'\0',sizeof(receivedCmd));
				
		json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{ requestType : %s}  }",receivedCmd);
		
		if(strlen(receivedCmd)!=0)
		{
			receivedCmd[strlen(receivedCmd)-2] = '\0';
			
			if(strcmp(receivedCmd,"linkuser")==0 || strcmp(receivedCmd,"getlinkqrcode")==0)	//received command is linkuser request, need to extract principalId
			{
				
				#ifdef DEBUG_ON
				printf("Response Received is for getlinkqrcode/linkuser (IoTRequest)\n");
				#endif //DEBUG_ON
				memset(qrCodeImage,'\0',sizeof(qrCodeImage));
								
				json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{ response:{qrImagePath:%s} } ", qrCodeImage);
				
				if(strlen(qrCodeImage)>10)		//since for linkuser also we are getting requestqrcode implementing this fix
				{
					qrCodeImage[strlen(qrCodeImage)-3]='\0';
					printf("Received QR Image Path is: %s\n",qrCodeImage);
					
	#ifdef PI				
					struct stat st = {0};

					if (stat("/home/pi/Desktop/QR", &st) == -1) {
						mkdir("/home/pi/Desktop/QR", 0777);
					}
					else
					{
						system("sudo chmod 777 /home/pi/Desktop/QR");
					}	
					
				

					writeToSocket(19,qrCodeImage);		//19. QR Code Image Path
	#endif	//PI
					
				}
				
				
				else    //received command is not getqrcode.., so verifying for principal id
				{
					stringReplace(receivedDataBuf);
					memset(principalId,'\0',sizeof(qrCodeImage));
					json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{ requestType : response:{topics:{} certificateMetadata:{} principalId : %s}  }",principalId);
					
					principalId[strlen(principalId)-2] = '\0';
					printf("Received LinkUser Command, pricipal Id is: %s\n",principalId);
					
			
					if(strlen(principalId)>10)	//user linking for this user first time..
					{
						//writeToSocket(11,principalId);		//11. principalId
						
						
						json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{ requestType : response:{topics:{} certificateMetadata:{} friendlyName : %Q}  }",&friendlyName);
											
						
						printf("Received LinkUser Command, friendlyName is: %s\n",friendlyName);					
					

						writeToSocket(12,friendlyName);

						sleep(2);
						
						writeToSocket(31,"1");		//31. Scanning of QR Code Done
						
						#ifdef PI
						
						
						//if(principalIdCnt==0)	//considering only admin user i.e. first linked user and not taking user linked on next reboot
						{
							
							principalIdFp = fopen("/home/pi/Desktop/EdgeFirmware/principalid.txt","r");	//initially open in read mode to check if file exists or not
							if(principalIdFp==NULL)		//no file exists i.e. admin user not there
							{
								printf("Unable to open principalid.txt file for reading... no admin user... so creating now\n");
								
								principalIdFp = fopen("/home/pi/Desktop/EdgeFirmware/principalid.txt","w+");
								if(principalIdFp==NULL)	
								{
									printf("Unable to open principalid.txt file for writing\n");
								}
								else
								{
									fprintf(principalIdFp,"%s",principalId);
									fclose(principalIdFp);
									principalIdCnt++;
									
									
									sprintf(friendlyNameCmd,"echo %s > /home/pi/Desktop/EdgeFirmware/miscfiles/friendlyname.txt",friendlyName);
									system(friendlyNameCmd);
								}
												
							}
							else
							{
								//prinicipal id of admin user already exists... no action needed
															
								memset(principalIdTemp,'\0',sizeof(principalIdTemp));
								fgets(principalIdTemp,sizeof(principalIdTemp), principalIdFp);
								fclose(principalIdFp);
								if(strlen(principalIdTemp)>0)
								{
									printf("Admin User is already Linked!!!\n");
								}
								else
								{
									printf("Principal Id is Dummy File... so creating admin user now\n");
									principalIdFp = fopen("/home/pi/Desktop/EdgeFirmware/principalid.txt","w+");
									if(principalIdFp==NULL)	
									{
										printf("Unable to open principalid.txt file for writing\n");
									}
									else
									{
										fprintf(principalIdFp,"%s",principalId);
										fclose(principalIdFp);
										principalIdCnt++;
										
										sprintf(friendlyNameCmd,"echo %s > /home/pi/Desktop/EdgeFirmware/miscfiles/friendlyname.txt",friendlyName);
										system(friendlyNameCmd);
									}

								}
							}

						}
						
						#endif	//PI
					}
					else 		//user already linked
					{
						printf("User Already Linked!!! No Parsing to be done");
					}
					
					
					
				}	//else part handling link user

			}		//linkuser or linkqrcode if condition ends here
		}
		
		else
		{
			memset(receivedCmd,'\0',sizeof(receivedCmd));
			
			json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{type: %s}", receivedCmd);	//type: DEVICE is remote command which needs to be acknowledged
			
			if(strlen(receivedCmd)!=0)
			{
				receivedCmd[strlen(receivedCmd)-2] = '\0';	//received command is of type: DEVICE
				
				memset(receivedCmd,'\0',sizeof(receivedCmd));
				
				json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{key: %s}", receivedCmd);	//this may with new format.. but not working as of now so using older format 
		
				if(strlen(receivedCmd)!=0)	//received command is either UPDATE_METADATA or UPDATE_ALARM or modifyDeviceName or modifyPIN
				{
					receivedCmd[strlen(receivedCmd)-2] = '\0';
					
					if(strcmp(receivedCmd,"UPDATE_METADATA")==0)	//received command is UPDATE_METADATA
					{
						printf("Recevied Command is UPDATE_METADATA : %s\n",receivedCmd);
					}
					else if(strcmp(receivedCmd,"UPDATE_ALARM")==0)
					{
						printf("Recevied Command is UPDATE_ALARM : %s\n",receivedCmd);
					}
					else if(strcmp(receivedCmd,"modifyDeviceName")==0)
					{
						printf("Recevied Command is modifyDeviceName : %s\n",receivedCmd);
					}								
					else if(strcmp(receivedCmd,"modifyPIN")==0)
					{
						printf("Recevied Command is modifyPIN : %s\n",receivedCmd);
					}
					else if(strcmp(receivedCmd,"UPDATE_DEVICE_FIRMWARE")==0)
					{
						printf("Recevied Command is UPDATE_DEVICE_FIRMWARE : %s\n",receivedCmd);
					}
					else                                    //received remote command is of unknown type
					{
						printf("Recived Remote Command is of UnKnown type...: %s\n",receivedCmd);
						
						//sending acknowledgement for unknown remote command
						
						memset(executionId,'\0',sizeof(executionId));			
			
						json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{executionId: %s}", executionId);
								
						executionId[strlen(executionId)-2]='\0';

						printf("For Remote Command modifyPIN, execId: %s\n", executionId);

						if(strcmp(executionIdPrevious,executionId)==0)	//received duplicate remote command.. so ignore it
						{
							printf("\n\nRECEIVED DUPLICATE REMOTE COMMAND... IGNORING IT\n");
							sendAckModifyDevName(executionId);			//need not respond for duplicate command... but need this temporarily
						}
						else
						{					
							strcpy(executionIdPrevious,executionId);
							sendAckModifyDevName(executionId);	
						}

					}																
				}
				
				
			}
			
						
		}
		
		
		if(strcmp(receivedCmd,"UPDATE_ALARM")==0)
		{
			printf("Received UPDATE_ALARM remote command\n");
						
			memset(eventKey,'\0',sizeof(eventKey));
						
			json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{  parameters:{event { eventKey: %s}",eventKey);
			eventKey[strlen(eventKey)-2] = '\0';
			
			if(strcmp(eventKey,"SNOOZE")==0)	//received remote command is for snooze
			{		
				printf("Received UPDATE_ALARM - SNOOZE remote command\n");
				memset(commandKey,'\0',sizeof(commandKey));
				memset(alarmId,'\0',sizeof(alarmId));
				memset(snoozeTime,'\0',sizeof(snoozeTime));
				memset(alarmType,'\0',sizeof(alarmType));
	
				memset(principalId,'\0',sizeof(principalId));
				memset(executionId,'\0',sizeof(executionId));
				
				json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{parameters:{event:{eventParameters:{instrumentName: %Q, byUser: %Q, snoozeTime: %s, alarmType: %s, alarmId: %s }", &instrumentName, &byUser,snoozeTime,alarmType, alarmId);

				json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{parameters:{event:{eventParameters:{}, principalId: %s}}}", principalId);	//considering first principal id in response as original one sent by cloud

				json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{parameters:{event:{eventParameters:{} }} executionId: %s, key: %s }", executionId,commandKey);				

			
				snoozeTime[strlen(snoozeTime)-1] = '\0';
				alarmType[strlen(alarmType)-2] = '\0';			//as per DL1.8.0.., alarmType ends with ",
				
				principalId[strlen(principalId)-4] = '\0';	//considering first principal id in response as original one sent by cloud for which 4 bytes to be ignored "}},


				if(strlen(alarmId)>5)
				{
					alarmId[strlen(alarmId)-3] = '\0';	//as per DL1.8.0.., alarmId ends with "},
				}
				else
				{
					strcpy(alarmId,"");
				}
							
				
				executionId[strlen(executionId)-2] = '\0';
				commandKey[strlen(commandKey)-2] = '\0';
							
 				printf("UPDATE_ALARM_SNOOZE Parameters: instrumentName: %s, byUser: %s, snoozeTime: %s, alarmType: %s eventKey: %s, principalId: %s, alarmId: %s, executionId: %s, commandKey: %s\n",instrumentName,byUser,snoozeTime,alarmType,eventKey,principalId,alarmId,executionId,commandKey);
 				
				//checking current active alarm id
				firmwareFileFp = fopen("/home/pi/Desktop/EdgeFirmware/miscfiles/currentalarmid.txt", "r");
				if (firmwareFileFp == NULL) 
				{
					printf("Failed to open currentalarmid.txt\n" );    
				}
				else
				{
					memset(currentAlarmId,'\0',sizeof(currentAlarmId));
					/* Read the output a line at a time - output it. */
					while (fgets(currentAlarmId, sizeof(currentAlarmId)-1, firmwareFileFp) != NULL) 
					{
						if(strchr(currentAlarmId,'\n')!=NULL)
						{
							currentAlarmId[strlen(currentAlarmId)-1]='\0';
						}
						else
						{
							currentAlarmId[strlen(currentAlarmId)]='\0';
						}
						
						printf("CURRENT Alarm Id: %s, received snooze for alarm id: %s\n", currentAlarmId,alarmId);
					}
					
					fclose(firmwareFileFp);
				}

				
				if(strcmp(executionIdPrevious,executionId)==0)	//received duplicate remote command.. so ignore it
				{
					printf("\n\nRECEIVED DUPLICATE REMOTE COMMAND FOR SNOOZE ALARM... IGNORING IT\n");
					
					sendRespSnooze(executionId, PROCESS_UPDATE_ALARM_SNOOZE);	//ONLY FOR TESTING TO ENSURE DL WORKS WITH OUT BLOCK
				}
				else
				{					
					strcpy(executionIdPrevious,executionId);
					
					if(strcmp(currentAlarmId,alarmId)==0)
					{
						writeToSocket(10,snoozeTime);			//10. snooze value
					}					
				
					sendRespSnooze(executionId, PROCESS_UPDATE_ALARM_SNOOZE);
				}
				
				//do some action here to snooze the alarm locally
				
			}
			else if(strcmp(eventKey,"ACKNOWLEDGE")==0)		//received remote command is for acknowledgement
			{				

				memset(alarmType,'\0',sizeof(alarmType));				
				memset(principalId,'\0',sizeof(principalId));
				memset(alarmId,'\0',sizeof(alarmId));
				memset(executionId,'\0',sizeof(executionId));
				memset(commandKey,'\0',sizeof(commandKey));									
				
								
				json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{commandType:commandKey: %s parameters:{event:{timestamp:eventParameters:{alarmId: %s alarmType: %s instrumentName: %Q byUser: %Q}}}", commandKey, alarmId, alarmType, &instrumentName, &byUser);

				json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{commandType:parameters:{event:{timestamp:eventParameters:{} principalId: %s }}", principalId);
				
				json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{parameters:{event:{eventParameters:{instrumentName: %Q, byUser: %Q, alarmType: %s}", &instrumentName,&byUser,alarmType);

				json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{parameters:{event:{eventParameters:{} }} executionId: %s, key: %s }", executionId,commandKey);				

				alarmType[strlen(alarmType)-2] = '\0';		//as per DL1.8.1
				
				principalId[strlen(principalId)-4] = '\0';				
				executionId[strlen(executionId)-2] = '\0';
				commandKey[strlen(commandKey)-2] = '\0';
				
				
				if(strlen(alarmId)>5)
				{
					alarmId[strlen(alarmId)-3] = '\0';		//as per DL1.8.1
				}
				else
				{
					strcpy(alarmId,"");
				}
				
				
								
 				printf("UPDATE_ALARM_ACK Parameters: instrumentName: %s, byUser: %s, alarmType: %s, eventKey: %s, principalId: %s, alarmId: %s, executionId: %s, commandKey: %s\n",instrumentName,byUser,alarmType,eventKey,principalId,alarmId,executionId,commandKey);
				
				
				if(strcmp(executionIdPrevious,executionId)==0)	//received duplicate remote command.. so ignore it
				{
					printf("\n\nRECEIVED DUPLICATE REMOTE COMMAND FOR ACKNOWLEDGE ALARM... IGNORING IT\n");
					
					sendRespAckAlarm(executionId, PROCESS_UPDATE_ALARM_ACK);	//ONLY FOR TESTING TO ENSURE DL WORKS WITH OUT BLOCK
				}
				else
				{					
					strcpy(executionIdPrevious,executionId);					
					
					writeToSocket(9, alarmId);					//9. acknowledge for alarm
					
					sendRespAckAlarm(executionId, PROCESS_UPDATE_ALARM_ACK);
				}

				
			}
			

		}
		else if(strcmp(receivedCmd,"UPDATE_METADATA")==0)
		{
			
			memset(executionId,'\0',sizeof(executionId));						
			
			json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{executionId: %s, parameters:{metadata:{assetLocation : %Q assetType: %Q assetModel: %Q assetSerialNumber: %Q assetCity : %Q }}}", executionId,&assetLocation,&assetType,&assetModel,&assetSerialNo,&assetCity);
			
	  
			json_scanf_array_elem(receivedDataBuf, strlen(receivedDataBuf), ".parameters.metadata.sensors", 0, &sensor1_token);
				
			json_scanf(sensor1_token.ptr, sensor1_token.len, "{setPoint: %Q highTemperature: %Q lowTemperature: %Q, name:%Q}", &setPoint_se1,&highTemp_se1, &lowTemp_se1,&name_se1);
		

			executionId[strlen(executionId)-2]='\0';

			printf("<----->setPoint: %s highTemperature: %s lowTemperature: %s name: %s assetLocation: %s assetType: %s assetModel: %s assetSerialNo: %s assetCity: %s execId: %s\n", setPoint_se1,highTemp_se1,lowTemp_se1,name_se1,assetLocation,assetType,assetModel,assetSerialNo,assetCity,executionId);

#ifdef PI

			if(strlen(highTemp_se1)>0 && strlen(setPoint_se1)>0 && strlen(lowTemp_se1)>0 && strlen(assetType)>0 && strlen(assetModel)>0 && strlen(assetSerialNo)>0 && strlen(assetCity)>0 && strlen(assetLocation)>0)
			{
				
				if(strcmp(executionIdPrevious,executionId)!=0)	//checking for duplicate execution Id
				{
				
					memset(highTempCmd,'\0',sizeof(highTempCmd));
					memset(lowTempCmd,'\0',sizeof(lowTempCmd));
					memset(setPointTempCmd,'\0',sizeof(setPointTempCmd));
					
					sprintf(highTempCmd,"echo %s > /home/pi/Desktop/EdgeFirmware/metadata/warmalarm.txt",highTemp_se1);
					sprintf(setPointTempCmd,"echo %s > /home/pi/Desktop/EdgeFirmware/metadata/setpoint.txt",setPoint_se1);
					sprintf(lowTempCmd,"echo %s > /home/pi/Desktop/EdgeFirmware/metadata/coldalarm.txt",lowTemp_se1);
					
					system(highTempCmd);
					
					system(setPointTempCmd);
					
					system(lowTempCmd);	
					
					memset(assetTypeCmd,'\0',sizeof(assetTypeCmd));
					memset(assetModelCmd,'\0',sizeof(assetModelCmd));
					memset(assetSerialNoCmd,'\0',sizeof(assetSerialNoCmd));
					memset(assetCityCmd,'\0',sizeof(assetCityCmd));
					memset(assetLocationCmd,'\0',sizeof(assetLocationCmd));
						
					
					sprintf(assetTypeCmd,"echo %s > /home/pi/Desktop/EdgeFirmware/metadata/assettype.txt",assetType);
					sprintf(assetModelCmd,"echo %s > /home/pi/Desktop/EdgeFirmware/metadata/assetmodel.txt",assetModel);
					sprintf(assetSerialNoCmd,"echo %s > /home/pi/Desktop/EdgeFirmware/metadata/assetserial.txt",assetSerialNo);
					sprintf(assetCityCmd,"echo %s > /home/pi/Desktop/EdgeFirmware/metadata/assetcity.txt",assetCity);
					sprintf(assetLocationCmd,"echo %s > /home/pi/Desktop/EdgeFirmware/metadata/assetlocation.txt",assetLocation);
					sprintf(sensorNameCmd,"echo %s > /home/pi/Desktop/EdgeFirmware/metadata/sensorname.txt",name_se1);
					
					system(assetTypeCmd);
					
					system(assetModelCmd);
					
					system(assetSerialNoCmd);
					
					system(assetCityCmd);
					
					system(assetLocationCmd);
					
					system(sensorNameCmd);
					
					
					writeToSocket(4,assetType);		//4. Asset Type
					//writeToSocket(5,assetModel);		//5. Asset Model
					//writeToSocket(6,assetSerialNo);		//6. Asset Serial Number
					//writeToSocket(7,assetCity);		//7. Asset City
					//writeToSocket(8,assetLocation);		//8. Asset Location
					//writeToSocket(1,setPoint_se1);		//1. Set Point
					//writeToSocket(2,lowTemp_se1);		//2. Low Temp
					//writeToSocket(3,highTemp_se1);		//3. High Temp

					/*
					sleep(5);
					
					writeToSocket(1,setPoint_se1);		//1. Set Point
					writeToSocket(2,lowTemp_se1);		//2. Low Temp
					writeToSocket(3,highTemp_se1);		//3. High Temp
					writeToSocket(4,assetType);		//4. Asset Type
					writeToSocket(5,assetModel);		//5. Asset Model
					writeToSocket(6,assetSerialNo);		//6. Asset Serial Number
					writeToSocket(7,assetCity);		//7. Asset City
					writeToSocket(8,assetLocation);		//8. Asset Location
					*/
	
		
					
					strcpy(executionIdPrevious,executionId);					
				
					sendAckSettRemoteCmd(executionId, PROCESS_UPDATE_METADATA);
					
				}
				else if(strcmp(executionIdPrevious,executionId)==0)
				{
					printf("\n\nRECEIVED DUPLICATE REMOTE COMMAND FOR METADATA... IGNORING IT\n");
					
					sendAckSettRemoteCmd(executionId, PROCESS_UPDATE_METADATA);	//ONLY FOR TESTING TO ENSURE DL WORKS WITH OUT BLOCK
				}		

			}


#endif	//PI
			
		}		
		else if(strcmp(receivedCmd,"modifyDeviceName")==0)
		{		
			memset(executionId,'\0',sizeof(executionId));			
			memset(deviceName,'\0',sizeof(deviceName));
			
			json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{executionId: %s, parameters: {name: %s }}", executionId,deviceName);
					
			executionId[strlen(executionId)-2]='\0';
			deviceName[strlen(deviceName)-3]='\0';

			printf("For Remote Command modifyDeviceName, execId: %s, deviceName: %s\n", executionId, deviceName);

			if(strcmp(executionIdPrevious,executionId)==0)	//received duplicate remote command.. so ignore it
			{
				printf("\n\nRECEIVED DUPLICATE REMOTE COMMAND... IGNORING IT\n");
				sendAckModifyDevName(executionId);			//need not respond for duplicate command... but need this temporarily
			}
			else
			{	
			
				if(strchr(deviceName,'\n')!=NULL)		//it contains a new line character... so remove it
				{
					deviceName[strlen(deviceName)-1]='\0';
				}
								
				//writeToSocket(21,deviceName);		//21. Device Name	//only doing ack for modifydevice name, since updating device name leads to confusion w.r.t label tagged at back of unit
				strcpy(executionIdPrevious,executionId);
				sendAckModifyDevName(executionId);	
			}
		}
		else if(strcmp(receivedCmd,"modifyPIN")==0)
		{
		
			memset(executionId,'\0',sizeof(executionId));			
			
			json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{executionId: %s}", executionId);
					
			executionId[strlen(executionId)-2]='\0';

			printf("For Remote Command modifyPIN, execId: %s\n", executionId);

			if(strcmp(executionIdPrevious,executionId)==0)	//received duplicate remote command.. so ignore it
			{
				printf("\n\nRECEIVED DUPLICATE REMOTE COMMAND... IGNORING IT\n");
				sendAckModifyDevName(executionId);			//need not respond for duplicate command... but need this temporarily
			}
			else
			{					
				strcpy(executionIdPrevious,executionId);
				sendAckModifyDevName(executionId);	
			}
		}
		else if(strcmp(receivedCmd,"UPDATE_DEVICE_FIRMWARE")==0)
		{
	
			memset(principalId,'\0',sizeof(principalId));
			memset(executionId,'\0',sizeof(executionId));
			memset(commandKey,'\0',sizeof(commandKey));		
			memset(downloadURL,'\0',sizeof(downloadURL));		
			memset(checksum,'\0',sizeof(checksum));		
			memset(version,'\0',sizeof(version));		
			
			
			json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{executionId: %s, parameters: {downloadUrl: %s, checkSum: %s, principalId: %s, version: %s}}", executionId, downloadURL, checksum, principalId, version);
			
				
			executionId[strlen(executionId)-2]='\0';
			downloadURL[strlen(downloadURL)-2]='\0';
			checksum[strlen(checksum)-2]='\0';
			principalId[strlen(principalId)-2]='\0';
			version[strlen(version)-2]='\0';

			printf("For Remote Command UPDATE_DEVICE_FIRMWARE, execId: %s, downloadUrl: %s, checkSum: %s, principalId: %s, version: %s\n", executionId, downloadURL, checksum, principalId, version);

			if(strcmp(executionIdPrevious,executionId)==0)	//received duplicate remote command.. so ignore it
			{
				printf("\n\nRECEIVED DUPLICATE REMOTE COMMAND... IGNORING IT\n");
				
				sendAckSettRemoteCmd(executionId, 8);	//to ensure deeplaser works without blocking further remote commands
			}
			else
			{					
				system("chmod 777 /home/pi/LatestFirmware.zip");
				system("yes | rm -rf /home/pi/LatestFirmware.zip");		//removing existing zip file before downloading latest zip
							
				strcpy(executionIdPrevious,executionId);
				
				sendAckSettRemoteCmd(executionId, 8);	//to ensure deeplaser works without blocking further remote commands
				
				//checking if received firmware version and existing firmware version matches
				firmwareFileFp = fopen("/home/pi/Desktop/EdgeFirmware/miscfiles/release-version.txt", "r");
				if (firmwareFileFp == NULL) 
				{
					printf("Failed to open release-version.txt\n" );    
				}
				else
				{
					memset(buf,'\0',sizeof(buf));
					/* Read the output a line at a time - output it. */
					while (fgets(buf, sizeof(buf)-1, firmwareFileFp) != NULL) 
					{
						if(strchr(buf,'\n')!=NULL)
						{
							buf[strlen(buf)-1]='\0';
						}
						else
						{
							buf[strlen(buf)]='\0';
						}
						
						printf("CURRENT RELEASE VERSION: %s, new version: %s\n", buf,version);
					}
					
					fclose(firmwareFileFp);
				}
				
				if(strcmp(buf,version)==0)
				{
					printf("Current Version on Device Link and received version are same.. No need to update firmware\n");
				}
				else
				{				
					printf("New version is available on cloud.. Initiating firmware update\n");
					processFWUpdCmd(downloadURL, checksum, version);	
				}								
			}
		}
		else
		{
			json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{requestType: %s } ", receivedCmd);
			receivedCmd[strlen(receivedCmd)-2]='\0';
			#ifdef DEBUG_ON
			printf("RECEIVED COMMAND: %s\n",receivedCmd);
			#endif	//DEBUG_ON
			if(strcmp(receivedCmd,"getlinkqrcode")==0)
			{
				#if 0 	//due to error in ICMA APP we are handling qr code in linkuser if condition itself.. so commenting now
				#ifdef DEBUG_ON
				printf("Response Received is for getlinkqrcode (IoTRequest)\n");
				#endif //DEBUG_ON
				memset(qrCodeImage,'\0',sizeof(qrCodeImage));
								
				json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{ response:{qrImagePath:%s} } ", qrCodeImage);
				qrCodeImage[strlen(qrCodeImage)-3]='\0';
				printf("Received QR Image Path is: %s ; LINE: %d\n",qrCodeImage,__LINE__);
#ifdef PI				
				struct stat st = {0};

				if (stat("/home/pi/Desktop/QR", &st) == -1) {
					mkdir("/home/pi/Desktop/QR", 0777);
				}
				else
				{
					system("sudo chmod 777 /home/pi/Desktop/QR");
				}
				
				if(strlen(qrCodeImage)>10)	//valid qr code image
				{
					writeToSocket(19,qrCodeImage);		//19. QR Code Image Path
				}
				
				/*FILE *linkUserFP;
				linkUserFP = fopen("/home/pi/Desktop/QR/linkuser.txt","w");
				if(linkUserFP)
				{
					fprintf(linkUserFP,"%s",qrCodeImage);
					fclose(linkUserFP);
				}
				else
				{
					printf("Unable to open linkuser.txt file for writing\n");
				}
				*/
#endif	//PI
			#endif	//0	due to error in ICMA APP we are handling qr code in linkuser if condition itself.. so commenting now
			}
			
			else if(strcmp(receivedCmd,"senddata:telemetry")==0)
			{
				#ifdef DEBUG_ON
				printf("Response Received is for senddata:telemetry (IoTRequest)\n");
				#endif //DEBUG_ON
				memset(telemetryResponse,'\0',sizeof(telemetryResponse));
				json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{ statusCode:%s } ", telemetryResponse);
				telemetryResponse[strlen(telemetryResponse)-2]='\0';
				printf("Telemetry Response: %s\n",telemetryResponse);
			}
			else if(strcmp(receivedCmd,"DataDispatchRequest")==0)
			{
				#ifdef DEBUG_ON
				printf("Response Received is for DataDispatchRequest (IoTRequest)\n");
				#endif //DEBUG_ON
				memset(dispatchResponse,'\0',sizeof(dispatchResponse));
				json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{ statusCode:%s } ", dispatchResponse);
				dispatchResponse[strlen(dispatchResponse)-2]='\0';
				printf("DataDispatchRequest Response: %s\n",dispatchResponse);
				
				
				json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{ requestId:%Q } ", &requestIdBuf);
				
				printf("Received Dispatch Id Length is: %d\n",strlen(requestIdBuf));
				
				if(strlen(requestIdBuf)<40 && strlen(requestIdBuf)>0)
				{
					writeToSocket(33,requestIdBuf);		//33. DataDispatchRequest received
				}
				else
				{
					printf("Received dispatch request id size exceeded max limit of code..,\n");
					
					iterationCnt = strlen(requestIdBuf)/28;
	
					for(i=0;i<iterationCnt+1;i++)
					{
						memset(requestIdBufTmp,'\0',sizeof(requestIdBufTmp));
						strncpy(requestIdBufTmp,requestIdBuf+i*28,28);
						if(strlen(requestIdBufTmp)>0)
						{
							writeToSocket(33,requestIdBufTmp);		//33. DataDispatchRequest received
							printf("DispatchId Partial String is: %s\n",requestIdBufTmp);
							//sleep(10);					
							tim.tv_sec = 2;
							tim.tv_nsec = 0;
   
							nanosleep(&tim , &tim2);

						}
					}
					
				}
				
				

				/*
				if(strcmp(dispatchResponse,"200")==0)
				{
					#ifdef PI			
					system("sudo echo 1 > /home/pi/Desktop/EdgeFirmware/miscfiles/dl_keepalive.txt");
					system("sudo chmod 777 /home/pi/Desktop/EdgeFirmware/miscfiles/dl_keepalive.txt");
					#endif	//PI
				}
				*/
			}
			else if(strcmp(receivedCmd,"tfcheartbeat")==0)
			{
				#ifdef DEBUG_ON
				printf("Response Received is for tfcheartbeat (IoTRequest)\n");
				#endif //DEBUG_ON
				memset(heartBeat,'\0',sizeof(heartBeat));
				memset(heartBeatId,'\0',sizeof(heartBeatId));
				json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{ response:{connected: %s} } ", heartBeat);
				json_scanf(receivedDataBuf, strlen(receivedDataBuf), "{ requestId: %s} ", heartBeatId);
				
				if(strlen(heartBeat)>0)
				{
					heartBeat[strlen(heartBeat)-2]='\0';		
					heartBeatId[strlen(heartBeatId)-2]='\0';				
					printf("HeartBeat Connected Status: %s, HeartBeat RequestId: %s\n",heartBeat, heartBeatId);
					
					if(strcmp(heartBeat,"true")==0)
					{
						printf("Id Written Back to Socket is: %s\n",heartBeatId);
						//writeToSocket(20,"true");		//20. HeartBeat Status true
						writeToSocket(20,heartBeatId);		//20. HeartBeat Status true
					}
					else if(strcmp(heartBeat,"false")==0)
					{
						printf("HeartBeat Connected Status: Failed\n");
						writeToSocket(20,"false");		//20. HeartBeat Status false

					}
					
					
				}
				else
				{
					printf("HeartBeat Connected Status: Failed\n");
					writeToSocket(20,"false");		//20. HeartBeat Status false
				}
					
			}
			
		}
	
		#endif	//0
		
		
		mg_printf(nc, "HTTP/1.0 200 OK\r\nContent-Length: %d\r\n"	"Content-Type: application/json\r\n\r\n%s",	(int) strlen(buf), buf);
		nc->flags |= MG_F_SEND_AND_CLOSE;
		  
		  
		if ((mg_vcmp(&hm->uri, "/v1/devicerequest") == 0) || (mg_vcmp(&hm->uri, "/v1/iotresponse") == 0))
		{
		  
		  mg_printf(nc, "HTTP/1.0 200 OK\r\nContent-Length: %d\r\n"
					"Content-Type: application/json\r\n\r\n%s",
					(int) strlen(buf), buf);
		  nc->flags |= MG_F_SEND_AND_CLOSE;
		}

      break;
    default:
      break;
  }
}

int writeToSocket(int index, char* data)
{
	Client_Webserver.index=index;						
	strcpy(Client_Webserver.parametersBuffer,data);				

		
	sockRetVal = send(sock,(Qt_WebServer_Sock*)&Client_Webserver,sizeof(Client_Webserver),MSG_DONTWAIT);
	
	
	
	if(sockRetVal < 0)
	{
		printf("ERROR writing to socket in webserver socket\n");
	}

	
	
	
	
	
	return sockRetVal;

}

int main(void) {
  struct mg_mgr mgr;
  struct mg_connection *nc;
  
  readUTCTimenow();
  
  remoteCmdCounter = 0;
  
  curl = curl_easy_init();
  
  
  
	
	//Create socket
	sock = socket(AF_INET , SOCK_STREAM , 0);
	if (sock == -1)
	{
		printf("Could not create socket");
	}
	
	#ifdef DEBUG
    printf("Webserver Socket Created...fd is %d\n",sock);
    #endif

    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_family = AF_INET;
    server.sin_port = htons( 1234 );
        
    int flag = 1;
    
	int result = setsockopt(sock,            /* socket affected */
                        IPPROTO_TCP,     /* set option at TCP level */
                        TCP_NODELAY,     /* name of option */
                        (char *) &flag,  /* the cast is historical cruft */
                        sizeof(int));    /* length of option value */
	
    
    if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
    {
        
	perror("connect failed. Error");
		while(1)
		{
			sleep(1);
			if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
				{
					perror("connect failed. RETRY after a second");
			}
			else
			{
				break;
			}
		}


    }

	#ifdef DEBUG
	printf("Connected to QT Server from webserver client\n");
	printf("Socket Des: %d\n",sock);	
	#endif	



  mg_mgr_init(&mgr, NULL);
  nc = mg_bind(&mgr, s_http_port, ev_handler);
  mg_set_protocol_http_websocket(nc);
  
   
  printf("Starting JSON-RPC server on port %s\n", s_http_port);
  for (;;) {
    mg_mgr_poll(&mgr, 1000);	
  }
  mg_mgr_free(&mgr);
  
  close(sock);

  return 0;
}



int sendAckModifyDevName(char *commandExecId)
{
	for(remoteCmdCounter=0;remoteCmdCounter<2;remoteCmdCounter++)
	{
		//send acknowledgement
		memset(ackCmdRespBuffer,'\0',sizeof(ackCmdRespBuffer));
		sprintf(ackCmdRespBuffer,"{\"id\":\"%s\", \"request\":\"DeviceRequest\", \"status\":\"SUCCESS\", \"commandExecutionId\":\"%s\"}",generateIdNum(),commandExecId);
		
		headers = NULL;
		
		headers = curl_slist_append(headers, "Accept: application/json");
		headers = curl_slist_append(headers, "Content-Type: application/json");
		headers = curl_slist_append(headers, "charsets: utf-8");

		if(curl)
		{

			curl_easy_setopt(curl, CURLOPT_URL, urlAck);
			curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);


			#ifdef  DEBUG_ON
			printf("********String to Send to DL for modifyDeviceName/modifyPin/UPDATE_DEVICE_FIRMWARE Cmd is : %s\n",ackCmdRespBuffer);
			#endif  //DEBUG_ON

			curl_easy_setopt(curl, CURLOPT_POSTFIELDS, ackCmdRespBuffer);

		}

		curl_easy_setopt (curl, CURLOPT_HEADERFUNCTION, WriteMemoryCallback);
		//curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);

		res = curl_easy_perform(curl);

		printf("<----->modifyDeviceName AckCmd Output is %d\n",res);
		
		sleep(2);
		
	}
}

int sendAckSettRemoteCmd(char * commandExecId, int responseFlag)		//new command format
{ 

	
	
	for(remoteCmdCounter=0;remoteCmdCounter<2;remoteCmdCounter++)
	{
		
		tim.tv_sec = 1;
        tim.tv_nsec = 0;
   
		nanosleep(&tim , &tim2);
		memset(ackCmdRespBuffer,'\0',sizeof(ackCmdRespBuffer));
		sprintf(ackCmdRespBuffer,"{\"id\":\"%s\", \"request\":\"DeviceRequest\", \"status\":\"SUCCESS\", \"commandExecutionId\":\"%s\"}",generateIdNum(),commandExecId);
		
		headers = NULL;
		
		headers = curl_slist_append(headers, "Accept: application/json");
		headers = curl_slist_append(headers, "Content-Type: application/json");
		headers = curl_slist_append(headers, "charsets: utf-8");
	
	
		if(curl)
		{
	
		curl_easy_setopt(curl, CURLOPT_URL, urlAck);
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);


		#ifdef  DEBUG_ON
		printf("********String to Send to DL for Send Ack Remote Cmd is : %s\n",ackCmdRespBuffer);
		#endif  //DEBUG_ON

		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, ackCmdRespBuffer);

		}
	
		curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, WriteMemoryCallback);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
	
		res = curl_easy_perform(curl);

		printf("<----->Ack for METADATA Remote Cmd Output is %d\n",res);
			
		

	}


	remoteCmdCounter=0;
/***********************************************************************/
    
    if(responseFlag==PROCESS_UPDATE_METADATA)
    {
		memset(metaDataRespBuffer,'\0',sizeof(metaDataRespBuffer));
		sprintf(metaDataRespBuffer,"{\"request\":\"updatemetadata\", \"id\":\"%s\", \"assetType\":\"%s\", \"assetModel\":\"%s\", \"assetSerialNumber\":\"%s\", \"assetLocation\":\"%s\", \"assetCity\":\"%s\", \"sensors\":[ { \"name\":\"%s\", \"setPoint\": %s, \"highTemperature\": %s, \"lowTemperature\": %s }]}",generateIdNum(),assetType,assetModel,assetSerialNo,assetLocation,assetCity,name_se1,setPoint_se1,highTemp_se1,lowTemp_se1);

		headers = NULL;
		
		headers = curl_slist_append(headers, "Accept: application/json");
		headers = curl_slist_append(headers, "Content-Type: application/json");
		headers = curl_slist_append(headers, "charsets: utf-8");

		
		if(curl)
		{
	
		curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);


	#ifdef  DEBUG_ON
		printf("********String to Send to DL for Meta Data Update Cmd is : %s\n",metaDataRespBuffer);
	#endif  //DEBUG_ON

		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, metaDataRespBuffer);

		}

		curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, WriteMemoryCallback);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);

		res = curl_easy_perform(curl);
		
		printf("<----->MetaData Response Cmd Output is %d\n",res);
		
		
		//processFWUpdCmd("https://downloads.thermofisher.com/firmware/Modbox/Firmware_Release_1.2.zip","1","1.2");

		
	}
		

	return 0;

}


int sendRespSnooze(char * commandExecId, int responseFlag )		//new format with out webserver_indicate
{
	for(remoteCmdCounter=0;remoteCmdCounter<2;remoteCmdCounter++)
	{
	
		tim.tv_sec = 1;
        tim.tv_nsec = 0;
   
		nanosleep(&tim , &tim2);
		
		memset(ackCmdRespBuffer,'\0',sizeof(ackCmdRespBuffer));
		sprintf(ackCmdRespBuffer,"{\"id\":\"%s\", \"request\":\"DeviceRequest\", \"status\":\"SUCCESS\", \"commandExecutionId\":\"%s\"}",generateIdNum(),commandExecId);
		
		headers = NULL;
		
		headers = curl_slist_append(headers, "Accept: application/json");
		headers = curl_slist_append(headers, "Content-Type: application/json");
		headers = curl_slist_append(headers, "charsets: utf-8");
	
	
		if(curl)
		{
	
		curl_easy_setopt(curl, CURLOPT_URL, urlAck);
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);


	#ifdef  DEBUG_ON
		printf("********String to Send to DL for sendRespSnooze Cmd is : %s\n",ackCmdRespBuffer);
	#endif  //DEBUG_ON

		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, ackCmdRespBuffer);

		}
	
		curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, WriteMemoryCallback);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
	
		res = curl_easy_perform(curl);

		printf("<----->sendRespSnooze Output is %d\n",res);	

	}

	remoteCmdCounter = 0;
	/******************************************************************/
	
	readUTCTimenow();
	
	memset(buf0,'\0',sizeof(buf0));
    
    sprintf(buf0,"{\"request\":\"senddata\",\"datatype\":\"event\",\"id\":\"%s\",\"eventKey\":\"%s\", \"timestamp\":\"%s\",\"eventParameters\": { \"snoozeTime\":\"%s\",\"alarmType\":\"%s\",\"alarmId\":\"%s\",\"instrumentName\":\"%s\",\"byUser\":\"%s\"}}",generateIdNum(),eventKey,timeinUTC,snoozeTime,alarmType,alarmId,instrumentName,byUser);
    
	headers = NULL;
	
	headers = curl_slist_append(headers, "Accept: application/json");
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, "charsets: utf-8");
	
    if(curl)
    {

    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);


#ifdef  DEBUG_ON
    printf("********String to Send to DL for Snooze Event Ack Cmd is : %s\n",buf0);
#endif  //DEBUG_ON

    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, buf0);

    }

    curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, WriteMemoryCallback);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);

    res = curl_easy_perform(curl);
    
    printf("<----->Snooze Response Cmd Output is %d\n",res);


}



int sendRespAckAlarm(char * commandExecId, int responseFlag)		//new format
{ 
	
	for(remoteCmdCounter=0;remoteCmdCounter<2;remoteCmdCounter++)
	{
		tim.tv_sec = 1;
        tim.tv_nsec = 0;
   
		nanosleep(&tim , &tim2);

		memset(ackCmdRespBuffer,'\0',sizeof(ackCmdRespBuffer));
		sprintf(ackCmdRespBuffer,"{\"id\":\"%s\", \"request\":\"DeviceRequest\", \"status\":\"SUCCESS\", \"commandExecutionId\":\"%s\"}",generateIdNum(),commandExecId);
		
		headers = NULL;
		
		headers = curl_slist_append(headers, "Accept: application/json");
		headers = curl_slist_append(headers, "Content-Type: application/json");
		headers = curl_slist_append(headers, "charsets: utf-8");
	
		
		if(curl)
		{

		curl_easy_setopt(curl, CURLOPT_URL, urlAck);
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);


	#ifdef  DEBUG_ON
		printf("********String to Send to DL for sendRespAckAlarm Remote Cmd is : %s\n",ackCmdRespBuffer);
	#endif  //DEBUG_ON

		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, ackCmdRespBuffer);

		}

		curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, WriteMemoryCallback);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
	
		res = curl_easy_perform(curl);

		printf("<----->Ack Cmd Output is %d\n",res);

		
	}
	
	remoteCmdCounter = 0;
    /*******************************************************************/
    
    
    
    readUTCTimenow();
	
	memset(buf0,'\0',sizeof(buf0));
    
    sprintf(buf0,"{\"request\":\"senddata\",\"datatype\":\"event\",\"id\":\"%s\",\"eventKey\":\"%s\", \"timestamp\":\"%s\",\"eventParameters\": { \"alarmType\":\"%s\",\"alarmId\":\"%s\",\"instrumentName\":\"%s\",\"byUser\":\"%s\"}}",generateIdNum(),eventKey,timeinUTC,alarmType,alarmId,instrumentName,byUser);
    
	headers = NULL;
	
	headers = curl_slist_append(headers, "Accept: application/json");
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, "charsets: utf-8");

    
    if(curl)
    {
		

    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);


#ifdef  DEBUG_ON
    printf("********String to Send to DL for Send Ack Remote Cmd is : %s\n",buf0);
#endif  //DEBUG_ON

    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, buf0);

    }

    curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, WriteMemoryCallback);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);

    res = curl_easy_perform(curl);

    printf("<----->sendRespAckAlarm Cmd Output is %d\n",res);
    

	return 0;
}




void readUTCTimenow(void)
{
	/*
FILE * time_fp;
char * line = NULL;
int len;
int result; 
int first, second;
		time_fp = fopen("/home/pi/Desktop/EdgeFirmware/miscfiles/utctimenow.txt","r");
		if(time_fp != NULL)
		{
			result = getline(&line,&len,time_fp);
			strcpy(timeinUTC,line);
			printf("timeinUTC %s \n",timeinUTC);
			fclose(time_fp);
						
		}
		else
		{
			strcpy(timeinUTC,"2017-05-11T06:43:16Z");
		}
	*/
	//memset(timeinUTC,'\0',sizeof(timeinUTC));	
	t = time(NULL);
	tm = *gmtime(&t);

	sprintf(timeinUTC,"%04d-%02d-%02dT%02d:%02d:%02dZ", tm.tm_year + 1900, tm.tm_mon + 1,tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
	
	printf("<----->UTC TIME READ IS: %s\n",timeinUTC);
}

int processFWUpdCmd(char *downloadURL,char *checksum,char *version)
{
	//download the file 
	
	headers = NULL;

	headers = curl_slist_append(headers, "Accept: application/json");
	headers = curl_slist_append(headers, "Content-Type: application/json");
	headers = curl_slist_append(headers, "charsets: utf-8");
	
	CURL *curl;

    curl = curl_easy_init();
		
	
	 if (curl) {
        firmwareFileFp = fopen("/home/pi/LatestFirmware.zip","wb");
        if(firmwareFileFp!=NULL)
        {
			curl_easy_setopt(curl, CURLOPT_URL, downloadURL);
			curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, firmwareFileFp);
			curl_easy_setopt (curl, CURLOPT_VERBOSE, 1L);
			res = curl_easy_perform(curl);
			
			printf("<----->Firmware Download File Remote Cmd Output is %d\n",res);
			
			/* always cleanup */
			curl_easy_cleanup(curl);
			fclose(firmwareFileFp);
        
	

		    //calculate the checksum and write to file
			memset(buf,'\0',sizeof(buf));
			sprintf(buf,"md5sum %s > /home/pi/md5.txt","/home/pi/LatestFirmware.zip");
			system(buf);
			
			//compare the checksum
			
			/* Open the command for reading. */
			firmwareFileFp = fopen("/home/pi/md5.txt", "r");
			if (firmwareFileFp == NULL) 
			{
				printf("Failed to run md5sum command to verify downloaded file checksum\n" );    
			}
			else
			{
				memset(buf,'\0',sizeof(buf));
				/* Read the output a line at a time - output it. */
				while (fgets(buf, sizeof(buf)-1, firmwareFileFp) != NULL) 
				{
					printf("********%s", buf);
				}

				checksumMatch = strtok(buf," ");
				printf("**********Checksum of Downloaded File is: %s\n",checksumMatch);
				
				if(strcmp(checksumMatch,checksum)==0)				
				{
						printf("Matching Found\n");
						writeToSocket(22,version);		//22. Valid Checksum received, sending verison
						system("echo 2 > /home/pi/firmwareupdatestatus.txt");	//0-Start DL As usual; 1-Fw update failed.. restore existing version; 2-New Update available..
						system("sudo reboot");
						
				}
				else
				{
						printf("No Match Found\n");
						//writeToSocket(22,"0");		//22. Invalid Checksum 
				}

				pclose(firmwareFileFp);

			}


		}
    

	}

	return 0;
}

