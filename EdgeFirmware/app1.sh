#!/bin/bash


# Check if gedit is running
# -x flag only match processes whose name (or command line if -f is
# specified) exactly match the pattern. 

#   cd /home/pi/Desktop/EdgeFirmware/
#                sudo ./test

while true
do        
        if pgrep -x "test" > /dev/null
	then
    		echo "Test is Running"    		
	else
    		echo "Stopped...Staring now"
        	cd /home/pi/Desktop/EdgeFirmware/
        	sudo ./test
	fi
	sleep 10
done
	
