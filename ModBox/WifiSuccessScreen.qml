import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "./assets"

Rectangle {
    width: 480
    height: 320
    color: "#f6f7f7"

//    BusyIndicator {
//        anchors.centerIn: parent
//        running: deltaK.isSetBusy
//    }

    Image {
        id: busyImage
        x: 80
        y: 10
        width: 68
        height: 68
        source: "./assets/Images/spinner.png"
        anchors.centerIn: parent
        visible: deltaK.isSetBusy

        NumberAnimation on rotation {
            from: 0; to: 360; running: busyImage.visible === true;
            loops: Animation.Infinite; duration: 700;
        }
    }


    Button {
        id: requestQRButton
        x: 155
        y: 260
        width: 170
        height: 50
        text: qsTr("")
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        style: ButtonStyle {
            background: Rectangle {
                color: "#2ea2ec"
                radius: 5
            }
        }

         Text {
            id: requestQRButtonText
            x: 18
            y: 10
            width: 78
            height: 30
            color: "#ffffff"
            text: qsTr("Request QR Code") + languageTranslator.translatedText
            FontLoader {
                id: robotoRegular
                source: "./assets/Fonts/roboto.regular.ttf"
            }
            verticalAlignment: Text.AlignVCenter
            font.family: robotoRegular2.name
            font.pixelSize: 18
        }
         onClicked: {
             requestQRButton.enabled = false
             deltaK.readQRImage()

             while(busyImage.visible===true)
             {

             }

             mainStack.pop(StackView.Immediate)
             if(deltaK.readQRStatus === true)    //qr read is success
             {
                 qrCode.startQRAutoExitTimer()
                 mainStack.push(qrCode, StackView.Immediate)
                 console.log('QR Code Screen is displayed')
                 deltaK.readQRStatus = false
             }
             else //if(deltaK.readQRStatus === false)  //qr read failed
             {
                 mainStack.push(readQRFailed, StackView.Immediate)
                 console.log('QR Code Failed !!!')
             }
             requestQRButton.enabled = true

         }

    }

    Label {
        id: apmodeLabel
        x: 160
        y: 24
        width: 160
        height: 18
        //text: qsTr("Device configuration") + languageTranslator.translatedText
        text: qsTr("")
        color: "#344550"
        font.bold: true
        FontLoader { id: robotoMedium3; source: "./assets/Fonts/roboto.medium.ttf" }
        font.family: robotoMedium3.name
        font.pixelSize: 18
        horizontalAlignment: Text.AlignHCenter
    }

    Image {
        id: wifiSuccessImage
        x: 50
        y: 136
        width: 32
        height: 23
        //source: "../assets/Images/Home_Screen/Status_Icon.png"
        source: "./assets/Images/OK-Green.png"
        visible: deltaK.wifiIconSucces
    }

    Label {
        id: ipAddressLabel
        x: 90
        y: 138
        width: 300
        height: 20
        color: "#33444f"
        text: qsTr("Wi-Fi connection successful") + languageTranslator.translatedText
        font.family: robotoRegular2.name
        font.pixelSize: 18
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        FontLoader {
            id: robotoRegular2
            source: "./assets/Fonts/roboto.regular.ttf"
        }
    }

    Label {
        id: deviceConfigInfoLabel
        x: 60
        y: 190
        width: 360
        height: 30
        color: "#33444f"
        text: qsTr("Click \"Request QR Code\" to proceed to Device user linking ") + languageTranslator.translatedText
        wrapMode: Text.WordWrap
        font.family: robotoRegular3.name
        font.pixelSize: 18
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        FontLoader {
            id: robotoRegular3
            source: "./assets/Fonts/roboto.regular.ttf"
        }
    }


    property int timerCnt: 0

    Timer {
        id: devConfigTimer
        running: true
        repeat: false
        interval: 5000
        triggeredOnStart: true
        onTriggered: {
            devConfigTimer.stop()
            delay(500, function() {
                        //console.log('500 m Sec Delay Expired devConfigTimer..Cnt '+timerCnt)

                if(timerCnt>=1)
                {
                    deltaK.setSettingsMouseClickStatus(true);
                    mouseArea.enabled=false
                }

                timerCnt++
                    })

        }
        }


    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked:   {

            devConfigTimer.start()
            mouse.accepted = deltaK.enableOTPButtonClick
        }
    }

    Timer {
            id: timer
        }

        function delay(delayTime, cb) {
            timer.interval = delayTime;
            timer.repeat = false;
            timer.triggered.connect(cb);
            timer.start();
        }


}
