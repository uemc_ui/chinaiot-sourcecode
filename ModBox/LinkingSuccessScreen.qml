import QtQuick 2.0
import QtQuick.Controls 2.0
import "./Backgrounds"

SetupContentBackground {
//    headerLabel: "Success"
    headerLabel: qsTr("Linking Status") + languageTranslator.translatedText
    property var parentPage: parentPage

    Label {
        id: textMessage
        x: 80
        y: 138
        width: 320
        height: 44
        text: deltaK.userLinkingStatusMsg + languageTranslator.translatedText
        wrapMode: Text.NoWrap
        color: "#33444f"
        FontLoader { id: robotoRegular1; source: "../assets/Fonts/roboto.regular.ttf" }
        font.family: robotoRegular1.name
        font.pixelSize: 18
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    doneButton.onClicked: {
        //mainStack.push(homeScreen)

        if(parentPage === "Setting"){
            mainStack.pop(StackView.Immediate)
        }else{
            deltaK.completedDeviceConfig()
            mainStack.push(homeScreen, StackView.Immediate)
        }
    }

    property int timerCnt: 0

    Timer {
        id: qrTimer
        running: true
        repeat: false
        interval: 5000
        //signal otpdonesig()
        triggeredOnStart: true
        onTriggered: {
            qrTimer.stop()
            delay(500, function() {
                        console.log('500 m Sec Delay Expired qrTimer..Cnt '+timerCnt)

                if(timerCnt>=1)
                {
                    deltaK.setSettingsMouseClickStatus(false);
                    mouseArea.enabled=false
                }
                timerCnt++

                    })

        }
        }


    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked:   {
            //console.log('onPressed AP Mode '+deltaK.enableOTPButtonClick)
            qrTimer.start()
            mouse.accepted = deltaK.enableOTPButtonClick
        }
    }

    Timer {
            id: timer
        }

        function delay(delayTime, cb) {
            timer.interval = delayTime;
            timer.repeat = false;
            timer.triggered.connect(cb);
            timer.start();
        }


}
