
#include <QtCore>
#include <QMutexLocker>
#include <unistd.h>

#include "thread.h"


/**
 * @brief Thread::Thread
 */
Thread::Thread()
{
    stopped = false;
}

/**
 * @brief Thread::run
 */
void Thread::run()
{
    forever
    {
       {
            QMutexLocker locker(&mutex);
            if(stopped)
            {
                stopped = false;
                break;
            }
       }

        qDebug()<<"Thread Running";
        //sleep(1);
        buzzerTimerCnt=0;
        //keyPressedFlag=false;
        system("echo \"0\" > /sys/class/gpio/gpio22/value");  //Alarm mode Green led on off state
        system("echo \"0\" > /sys/class/gpio/gpio26/value");  //Alarm mode yellow led on off state


        while(buzzerTimerCnt<3)
        {
         //   QCoreApplication::processEvents();
            //qDebug()<<"KeyPressed Status is: "<<keyPressedFlag;
            system("echo \"1\" > /sys/class/gpio/gpio27/value");    //RED LED ON

            if(keyPressedFlag==false)
            {
                system("echo \"1\" > /sys/class/gpio/gpio12/value");  //  BUZZER ON
            }


            delay(1);

            system("echo \"0\" > /sys/class/gpio/gpio27/value");  //RED LED OFF
            system("echo \"0\" > /sys/class/gpio/gpio12/value");  //  BUZZER OFF

            delay(1);

            buzzerTimerCnt++;

            if(buzzerTimerCnt==3)
            {
                buzzerTimerCnt=0;
                break;
            }

            //added this code to have mutual exclusion between green led and red led.. need to test
            if(stopped)
            {
                stopped = false;
                break;
            }
        }
        delay(60);

    }
}

/**
 * @brief Thread::stop
 */
void Thread::stop()
{
    QMutexLocker locker(&mutex);
    stopped = true;
    qDebug()<<"Thread Stopped";
}

/**
 * @brief Thread::silenceKeyTrue
 */
void Thread::silenceKeyTrue()
{
    keyPressedFlag=true;
}

/**
 * @brief Thread::silenceKeyFalse
 */
void Thread::silenceKeyFalse()
{
    keyPressedFlag=false;
}

/**
 * @brief Thread::delay
 * @param sec
 */
void Thread::delay(int sec)
{
    dieTime= QTime::currentTime().addSecs(sec);
    while (QTime::currentTime() < dieTime && stopped==false)
    {
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }

}
