#ifndef THREAD_H
#define THREAD_H

#include <QThread>
#include <QMutex>
#include <QTime>

class Thread : public QThread
{
    Q_OBJECT

public:
    Thread();
    void setMessage(const QString &message);
    void stop();



private:

    volatile bool stopped;
    QMutex mutex;

    int buzzerTimerCnt=0;
    bool keyPressedFlag=false;
    QTime dieTime;



public slots:

    void silenceKeyTrue();
    void silenceKeyFalse();
    void delay(int sec);

protected:
    void run();

};

#endif



