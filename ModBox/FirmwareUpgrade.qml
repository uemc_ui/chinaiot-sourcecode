import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "./assets"

Rectangle {
    width: 480
    height: 320
    color: "#f6f7f7"


    Button {
        id: startButton
        x: 155
        y: 260
        width: 170
        height: 50
        text: qsTr("")
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        style: ButtonStyle {
            background: Rectangle {
                color: "#2ea2ec"
                radius: 5
            }
        }

         Text {
            id: startButtonText
            x: 63
            y: 10
            width: 78
            height: 30
            color: "#ffffff"
            // color: "#2ea2ec"
            text: qsTr("Start") + languageTranslator.translatedText
            FontLoader {
                id: robotoRegular
                source: "./assets/Fonts/roboto.regular.ttf"
            }
            verticalAlignment: Text.AlignVCenter
            font.family: robotoRegular.name
            font.pixelSize: 18
        }
         onClicked: {

             deltaK.firmwareUpgradeStarted();
             mainStack.pop(StackView.Immediate)
             mainStack.push(fwupgradeCompleteScreen, StackView.Immediate)

         }
    }


    Label {
        id: headerLabel
        x: 130
        y: 24
        width: 220
        height: 22
        text: qsTr("Firmware Upgrade") + languageTranslator.translatedText
        color: "#33444f"
        font.bold: true
        FontLoader { id: robotoMedium3; source: "./assets/Fonts/roboto.medium.ttf" }
        font.family: robotoMedium3.name
        font.pixelSize: 18
        horizontalAlignment: Text.AlignHCenter
    }

    Label {
        id: textMessage
        x: 85
        y: 138
        width: 310
        height: 44
        text: qsTr("New Firmware update is available. Press Start to download and update.") + languageTranslator.translatedText
        wrapMode: Text.WordWrap
        color: "#33444f"
        FontLoader { id: robotoRegular1; source: "../assets/Fonts/roboto.regular.ttf" }
        font.family: robotoRegular1.name
        font.pixelSize: 18
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    Button {
        id: backButton
        x: 24
        y: 140
        width: 24
        height: 40
        text: qsTr("")

        style: ButtonStyle {
            background: Rectangle {
                color: "Transparent"
            }
        }

        Image {
            id: backImage
            x: 0
            y: 0
            width: 24
            height: 40
            source: "./assets/Images/Settings/Back_icon_new.png"
        }

        onClicked: {
            mainStack.pop(StackView.Immediate)
        }
    }

}
