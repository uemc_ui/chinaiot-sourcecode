import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import "./Backgrounds"

ApplicationWindow {
    id: mainWindow
    property var settings
    property var settingsBackup

    visible: true
    width: 480
    height: 320

    function valueChanged(){
        console.log("Value Changed")
        apModeScreen.showPassword();
    }

    function startRedAnimation()
    {
        //console.log("Start Red Animation Function")
        alarmScreen.startRedTextAnimation();
    }


    function stopRedAnimation()
    {
        //console.log("Stop Red Animation Function")
        alarmScreen.stopRedTextAnimation();
    }

    function startYellowAnimation()
    {
        //console.log("Start Yellow Animation Function")
        warningAlarmScreen.startYellowTextAnimation();
    }

    function stopYellowAnimation()
    {
        //console.log("Stop Yellow Animation Function")
        warningAlarmScreen.stopYellowTextAnimation();
    }

    function wifiSuccessScreen()
    {
        otpScreen.showWifiSuccessScreen();
    }

    function wifiFailScreen()
    {
        otpScreen.showWifiFailScreen();
    }

    function showConfigProgressScreen()
    {
        apModeScreen.showConfigProgress();
    }

    function qrDoneClick()
    {
        //console.log("QR Done Click Function")
        qrCode.clickedDoneQR();
    }

    function apDoneButtonExitTimer()
    {
        otpScreen.startAPModeScreenExitTimer();
    }

    function apDoneButtonTimerStop()
    {
        otpScreen.stopAPModeScreenExitTimer();
    }



    StackView {
        id: mainStack
        objectName: "mainStack"
        x: 0
        y: 0
        width: 480
        height: 320
        visible: true
        initialItem: splashScreen


//        MouseArea {
//            id: mouseArea
//            x: 0
//            y: 0
//            width: 480
//            height: 320
////            propagateComposedEvents: false

//            onClicked: {
//                mouse.accepted = true;
//                //console.log("mouse area clicked")
//                //doneButtonOTP.enabled = false
//            }

//            onPressed: mouse.accepted = true;
//            onReleased: mouse.accepted = true;
//            onDoubleClicked: mouse.accepted = true;
//            onPositionChanged: mouse.accepted = true;
//            onPressAndHold: mouse.accepted = true;
//        }
        SplashScreen {
            id: splashScreen
        }

        HomeScreen { id: homeScreen; visible: false }
        SettingsScreen { id: settingsScreen; visible: false }
        AlaramScreen { id: alarmScreen; visible: false }
        Alarm_Silenced { id: alarmSilenced; visible: false }
        ApMode_WifiConnected{ id: apModeWifiConnected; visible: false }
        MemoryStatus{ id: memoryStatus; visible: false }
        Settings_Alarms_Popup{ id: settingsAlarmsPopup; visible: false }
        OTPScreen{ id: otpScreen; visible: false }
        ApModeScreen { id: apModeScreen; visible: false }
        APMode_Config_Completed{ id: apModeConfigCompleted; visible: false }
        LanguageSelection{ id:languageSelection; visible: false }
        QRCode{ id:qrCode; visible: false }
        FirmwareUpgrade{ id:firmwareUpgrade; visible: false }
        FWUpgradeDownloading{ id:fwUpgradeDownloading; visible: false }
        FWUpgradeCompleteScreen { id: fwupgradeCompleteScreen; visible: false}
        WifiFailScreen { id: wifiFailScreen; visible: false}
        WifiSuccessScreen { id: wifiSuccessScreen; visible: false}
        LinkingSuccessScreen { id: linkingSuccessScreen; visible: false}
        ReadQRFailed{ id: readQRFailed; visible: false }
        SettingsAccess{ id: settingsAccess; visible: false }
        NumericKeypad {id: numericKeypad; visible: false}
        WarningAlarmScreen{ id: warningAlarmScreen; visible: false }
        ResetPINScreen{ id: resetPINScreen; visible: false }

    }

}
