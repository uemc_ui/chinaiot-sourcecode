import QtQuick 2.7
import QtQuick.Controls 2.1
import "./assets"

Item {
    id: idNumKeypad
//    property alias info: idTextInfo.text
    property alias pin: idTextInputPin.text

    width: 240
    height: 228+15//198 //160 + 38

//    Label {
//        id: idTextInfo
//        width: parent.width ;
//        height: 15
//        text: qsTr("")
//        color: "#33444f"
//        font.bold: true
//        FontLoader { id: robotoMedium3; source: "./assets/Fonts/roboto.medium.ttf" }
//        font.family: robotoMedium3.name
//        font.pixelSize: 13
//        horizontalAlignment: Text.AlignHCenter
//    }

    Rectangle {
        x: 0
        y: 42
        width: parent.width
        height: idTextInputPin.height + 8
        //y:15
        border.color: Qt.lighter( "grey" ) ;
        color: "white" ;
        border.width: 2
        radius: 4

        TextInput {
            id: idTextInputPin ; //Eight Digit
            height: 30 ;
            //y:15
             anchors.fill: parent
            anchors.margins: 4
            verticalAlignment: TextInput.AlignVCenter
            horizontalAlignment: TextInput.AlignHCenter
            text: "" ;
//            font.wordSpacing: 20
//            font.letterSpacing: 5
            cursorVisible: false ;

            echoMode: TextInput.Password
            passwordMaskDelay: 500 ;
            maximumLength: 4

            MouseArea {
                id: areaText
                x: 4
                y: 4
                width: parent.width
                height: parent.height

                onClicked: { //NKR: Do not Remove
                    idTextInputPin.cursorPosition = idTextInputPin.length
                    idTextInputPin.cursorVisible = false
                }
            }
        }

//        CheckBox {
//            width: 240  //240
//            height: 30  //30
//            y:38    //38
//            checked: false
//            text: qsTr("Show") ;

//            onClicked: {
//                if ( checkState == Qt.Checked ){
//                    checked = true ;
//                    text = qsTr("Hide") ;
//                    console.log ( "Checked" ) ;
//                    idTextInputPin.echoMode = TextInput.Normal
//                }
//                else {
//                    checked = false ;
//                    text = qsTr("Show") ;
//                    console.log ( "Unchecked" ) ;
//                    idTextInputPin.echoMode = TextInput.Password
//                }
//            }
//        }
    }

    Rectangle {
        width: parent.width
        height: parent.height - 20
        y: 68 + 15 ;
        ListModel {
            id:idData ;

            ListElement { img: "";num: "1" }
            ListElement { img: "";num: "2" }
            ListElement { img: "";num: "3" }
            ListElement { img: "";num: "4" }
            ListElement { img: "";num: "5" }
            ListElement { img: "";num: "6" }
            ListElement { img: "";num: "7" }
            ListElement { img: "";num: "8" }
            ListElement { img: "";num: "9" }
            ListElement { img: "../assets/Images/keypad/backspace.png";num: "" }
            ListElement { img: "";num: "0" }
            ListElement { img: "../assets/Images/keypad/keyboard_enter.png"; num: "" }
        }

        GridView {
            id: idKeyPadView
            anchors.fill: parent ;
            clip: true ;
            model: idData
            cellWidth: 80
            cellHeight: 40
            flow: GridView.FlowLeftToRight
            boundsBehavior: Flickable.StopAtBounds ;

            delegate: idKey
        }

        Component {
            id: idKey ;
            Rectangle {
                width: 80
                height: 40
                radius: 6
                color: "#eeeeee"
                border.color: Qt.lighter( color )
                Text {
                    anchors.centerIn: parent ;
                    font.pointSize: 16 ;
                    font.bold: true ;
                    text:num
                    color: "#20a3e5"
                }

                Image {
                    id: name
                    anchors.centerIn: parent ;
                    source: img
                }

                MouseArea {
                    id: areaKeypad
                    anchors.fill: parent ;
                    onClicked: {
                        if ( index == 9 ) { //Backspace

                            if ( idTextInputPin.text.length ) {
                                var temp = idTextInputPin.text ;
                                idTextInputPin.text = temp.substr ( 0, idTextInputPin.text.length -1 ) ;
                                //console.log ( idTextInputPin.text ) ;
                            }
                            else
                                idTextInputPin.text = "" ;
                        }                            
                        else if ( index == 11 ) { //Carriage Return or OK
                            //console.log ( idNumKeypad.pin.toString( ) ) ;
                            //console.log ( idTextInputPin.text ) ;
                            settingsAccess.validatePinEntered();
                        }
                        else {
                            idTextInputPin.text += num ;
                            //console.log ( "Added: " + idTextInputPin.text ) ;
                        }
                    }
                }
            }
        }
    }
}
