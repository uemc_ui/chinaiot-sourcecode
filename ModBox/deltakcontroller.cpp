#include "deltakcontroller.h"
#include <QTimer>
#include <QDebug>

#include <QTcpSocket>
#include <QString>
#include <unistd.h>
#include <curl/curl.h>
#include <QFileInfo>

#include <QNetworkAccessManager>
#include <QUrl>
#include <QNetworkRequest>
#include <QString>
#include <QJsonArray>
#include <QJsonDocument>
#include <QNetworkReply>
#include <QtConcurrent/qtconcurrentrun.h>

//GPIO22 - GREEN LED
//GPIO12 - BUZZER
//GPIO16 - YELLOW LED - ALPHA BOARDS
//GPIO27 - RED LED
//GPIO26 - YELLOW LED - BETA BOARDS

//1. Set Point
//2. Low Temperature
//3. High Temperature
//4. Asset Type
//5. Asset Model
//6. Asset Serial Number
//7. Asset City
//8. Asset Location
//9.  acknowledge for alarm
//10. snooze value
//11. principalId
//12. friendlyName
//13. otpbutton
//14. rtdValue-NA Value (rdt out of range values)
//15. rtdValue
//16. Battery Connectivity Status	//0-Mains power is on	1-Mains power is off.. running on battery
//17. Battery Percentage
//18. Wifi Signal Strength
//19. QR Code Image Path
//20. HeartBeatCommand Status
//21. Device Name
//22. FirmwareUpdateChecksumStatus	//0-Bad Checksum	1-ValidChecksum
//23. DateTime Command received from mobile broswer during device configuration
//24. Changed Wifi Settings from mobile browser
//25. Changed Asset Details/Alarm Settings from mobile browser
//26. Changed both wifi and Asset Details/Alarm Settings from mobile browser
//27. First Time Device Configuration Completed
//28. Device Configuration Started and client connected
//29. Timed out... Device configuration session has expired!
//30. Exited from Device Configuration with out making any changes
//31. Scanning of QR Code Done
//32. Asset Type/Asset Serial Number Changed
//33. DataDispatchRequest received

#define NORMAL          1
#define LOGGING         2
#define ALARM           3
#define BATTERY         4
#define ALARMANDBATT	5
#define ALARMANDLOG		6
#define BATTLOG			7
#define ALARMBATTLOG	8
#define UPDATING        9
#define WARNING         10

#define SENDHBRETRY 10

#define HIGH_TEMP   1
#define LOW_TEMP    2

#define UPDATE_STATUS_THRESHOLD 2

#define BATT_10_PERCENT 1
#define BATT_25_PERCENT 2


#define SETPOINT_CODE  1
#define LOWTEMP_CODE  1
#define HIGHTEMP_CODE  1

#define DEBUG_ON  1

#define DEFAULT_SNOOZE  60      //in minutes

#define MAX_LOG_DURATION    72*3600      //in sec

#define DL_MQTT_ERROR_CHECK_TIME   300*1000    //in sec


CURL *curl;
CURL *curlUpdMData;
CURL *curlEvent;
CURL *curlDevStatus;
CURL *curlRecovery;
CURL *curlTelemetry;
CURL *curlHeartBt;
CURL *curlOutageData;

struct curl_slist *headers;
struct curl_slist *headersUpdMData;
struct curl_slist *headersEvent;
struct curl_slist *headersDevStatus;
struct curl_slist *headersRecovery;
struct curl_slist *headersTelemetry;
struct curl_slist *headersHeartBt;
struct curl_slist *headersOutageData;

CURLcode res;
char url[]= "http://localhost:9000/v1/IoTRequest";
char urlAck[]= "http://localhost:9000/v1/deviceresponse";




typedef struct Webserver_Struct{

    char parametersBuffer[40];
    int index;

} Qt_WebServer_Sock;



Qt_WebServer_Sock info;
char data[sizeof(Qt_WebServer_Sock)];

/**
 * @brief DeltaKController::DeltaKController
 * @param parent
 */
DeltaKController::DeltaKController(QObject *parent) : QObject(parent)
{

    resetKeyTimer = new QTimer(this);
    QObject::connect(resetKeyTimer, SIGNAL(timeout()), this, SLOT(resetSnoozeKeyPressed()));

    outageDataSendTimer = new QTimer(this);
    QObject::connect(outageDataSendTimer, SIGNAL(timeout()), this, SLOT(sendNetworkOutageData()));

    countDownValue = new QTime(0,60,0);

    timeValue = new QTime();

    myserver = new QTcpServer(this);
    QObject::connect(myserver, SIGNAL(newConnection()), this, SLOT(acceptConnection()));
    myserver->listen(QHostAddress::LocalHost,1234);

    this->setReferenceTemperature("--.-");

    this->setTemperature("--.-");

    this->setWifiWarning(false);

    this->setEthernetWarning(true);

    this->setBatteryWarning(false);

    this->setPowerWarning(false);

    this->setTempAlarmIcon(false);

    this->setYellowWarning(false);

    this->setNoProbeDetected(false);

    this->setShowBackApMode(false);

    this->setShowBackKeyPad(false);

    this->setShowForgotPin(false);

    buzzerTimer = new QTimer(this);
    QObject::connect(buzzerTimer, SIGNAL(timeout()), this, SLOT(runBuzzer()));

    yellowLedTimer = new QTimer(this);
    QObject::connect(yellowLedTimer, SIGNAL(timeout()), this, SLOT(yellowLedFlash()));

    syslogClearTimer = new QTimer(this);
    QObject::connect(syslogClearTimer, SIGNAL(timeout()), this, SLOT(clearSysLogs()));
    syslogClearTimer->start(120*60*60000);  //clearing logs every 5 days

    otherLogsClearTimer = new QTimer(this);
    QObject::connect(otherLogsClearTimer,SIGNAL(timeout()),this,SLOT(clearOtherLogs()));
    otherLogsClearTimer->start(60*60000);   //clearing other logs every 1 hour

    keepAliveTimer = new QTimer(this);
    QObject::connect(keepAliveTimer, SIGNAL(timeout()), this, SLOT(keepAliveSlot()));
    keepAliveTimer->start(2*60000);  //updating keep alive timer every 120 seconds

    deviceStatusTimer = new QTimer(this);
    QObject::connect(deviceStatusTimer, SIGNAL(timeout()), this, SLOT(updateDeviceStatusSlot()));

    telemetryTimer = new QTimer(this);
    QObject::connect(telemetryTimer, SIGNAL(timeout()), this, SLOT(telemetryConcurrentCmd()));

    countDownTimer = new QTimer(this);
    connect(countDownTimer,SIGNAL(timeout()),this,SLOT(timeOutSlotForCountDown()));

    networkTimer = new QTimer(this);
    QObject::connect(networkTimer, SIGNAL(timeout()), this, SLOT(networkMonitor()));

    wifiTimer = new QTimer(this);
    QObject::connect(wifiTimer, SIGNAL(timeout()), this, SLOT(wifiSignalRead()));

    apStaTimer = new QTimer(this);
    QObject::connect(apStaTimer, SIGNAL(timeout()), this, SLOT(changeModeSta()));

    syncDateTimer = new QTimer(this);
    QObject::connect(syncDateTimer, SIGNAL(timeout()), this, SLOT(updateDateTime()));

    resendOutageDataTimer = new QTimer(this);
    QObject::connect(resendOutageDataTimer, SIGNAL(timeout()), this, SLOT(recheckOutageStatus()));

    apModeCheckTimer = new QTimer(this);
    QObject::connect(apModeCheckTimer,SIGNAL(timeout()),this,SLOT(recheckApModeStatus()));

    eventSentCheckTimer = new QTimer(this);
    QObject::connect(eventSentCheckTimer,SIGNAL(timeout()),this,SLOT(eventSentCheckSlot()));

    warmAlarmTimer = new QTimer(this);
    QObject::connect(warmAlarmTimer,SIGNAL(timeout()),this,SLOT(warmAlarmSlot()));

    coldAlarmTimer = new QTimer(this);
    QObject::connect(coldAlarmTimer,SIGNAL(timeout()),this,SLOT(coldAlarmSlot()));

    alarmRecoveryTimer = new QTimer(this);
    QObject::connect(alarmRecoveryTimer,SIGNAL(timeout()),this,SLOT(alarmRecoverySlot()));

    dlRestartTimer = new QTimer(this);
    QObject::connect(dlRestartTimer,SIGNAL(timeout()),this,SLOT(dlRestartTimerSlot()));

    dlDataFolderChkTimer = new QTimer(this);
    QObject::connect(dlDataFolderChkTimer,SIGNAL(timeout()),this,SLOT(dlDataFolderSlot()));

    nwThread = new NetworkThread();

    QObject::connect(nwThread,SIGNAL(networkOn()),this,SLOT(internetOnSlot()));
    QObject::connect(nwThread,SIGNAL(networkOff()),this,SLOT(internetOffSlot()));

    nwThread->start();  //need internet status for loggingFlag even during dev config.,

    wifiThrd = new WifiThread();

    QObject::connect(wifiThrd,SIGNAL(wifiValueChanged(int)),this,SLOT(receivedWifiValue(int)));

    QObject::connect(this,SIGNAL(forceWlan1Down()),wifiThrd,SLOT(changeWlan1StateDown()));

    QObject::connect(this,SIGNAL(forceWlan1Up()),wifiThrd,SLOT(changeWlan1StateUp()));

    QObject::connect(this,SIGNAL(startEventSentCheck()),this,SLOT(startEventSentCheckSlot()));

    QObject::connect(this,SIGNAL(startOutageTimerSignal()),this,SLOT(startOutageTimerSlot()));


    devConfigFile.setFileName("/home/pi/Desktop/EdgeFirmware/miscfiles/deviceconfigcompleted.txt");

    if(!devConfigFile.open(QIODevice::ReadOnly)) {

        system("sudo rm -rf /home/pi/Desktop/modbox-webserver/chinaIoT-English/*.txt");
        system("sudo rm -rf /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/*.txt");
        system("sudo rm -rf //home/pi/Desktop/EdgeFirmware/miscfiles/pinnumber.txt");

        //to provide data no device_info.html page
        system("sudo cp /home/pi/Desktop/EdgeFirmware/miscfiles/release-version.txt /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese");
        system("sudo cp /home/pi/Desktop/EdgeFirmware/miscfiles/release-version.txt /home/pi/Desktop/modbox-webserver/chinaIoT-English");

        system("sudo cp /home/deviceSerial.txt /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese");
        system("sudo cp /home/deviceSerial.txt /home/pi/Desktop/modbox-webserver/chinaIoT-English");


#ifdef  DEBUG_ON
       qDebug()<<"Unable to open deviceconfigcompleted.txt file... so starting with languageSelection Screen";
#endif  //DEBUG_ON
       this->setAppStartLang(true);     //no file found... so let the app start with languageSelection screen
       this->setPinNotSet(true);
       emit valueChanged();
    }

    else        //deviceconfigcompleted.txt file exists
    {

       devConfigFile.close();

       system("sudo cp /home/pi/Desktop/EdgeFirmware/metadata/* /home/pi/Desktop/modbox-webserver/chinaIoT-English/");
       system("sudo cp /home/pi/Desktop/EdgeFirmware/metadata/* /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/");

       system("sudo cp /home/pi/Desktop/EdgeFirmware/miscfiles/deviceconfigcompleted.txt /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese");
       system("sudo cp /home/pi/Desktop/EdgeFirmware/miscfiles/deviceconfigcompleted.txt /home/pi/Desktop/modbox-webserver/chinaIoT-English");

       system("sudo cp /home/pi/Desktop/EdgeFirmware/miscfiles/release-version.txt /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese");
       system("sudo cp /home/pi/Desktop/EdgeFirmware/miscfiles/release-version.txt /home/pi/Desktop/modbox-webserver/chinaIoT-English");

       system("sudo cp /home/deviceSerial.txt /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese");
       system("sudo cp /home/deviceSerial.txt /home/pi/Desktop/modbox-webserver/chinaIoT-English");

       this->setAppStartLang(false);
       emit valueChanged();

       networkTimer->start(30000);      //monitor network connectivity every 6 seconds., but 30 sec on boot up
       wifiTimer->start(11000);  //monitoring wifi signal status every 11 sec

       telemetryTimer->start(60000);  //updating telemetry every 60 sec

       deviceStatusTimer->start(9.5*60000);  //updating device status to cloud every 10 min 10*60000


       if(dlDataFolderChkTimer->isActive()==false)
       {
            dlDataFolderChkTimer->start(DL_MQTT_ERROR_CHECK_TIME + 1000);   //check if device registration failed for DL
       }

       if(dlRestartTimer->isActive()==false)
       {
           dlRestartTimer->start(DL_MQTT_ERROR_CHECK_TIME);   //starting timer for 5 min so that if no data from DL endpoint, DL has to restart
       }


    }


    //reading the instrument name
    deviceSerialfile.setFileName("/home/deviceSerial.txt");

    if(!deviceSerialfile.open(QIODevice::ReadOnly)) {
       qDebug()<<"Unable to open deviceSerial.txt file";
    }

    else
    {
       QTextStream devSerialIn(&deviceSerialfile);

       formatIndex=0;

       while(!devSerialIn.atEnd()) {

           formatIndex++;
           if(formatIndex==1)   //index 1 (i.e. line 1) is having instrument name or device serial number
           {
                instrumentName = devSerialIn.readLine();
           }
           else if(formatIndex==2)   //index 2 (i.e. line 2) is device MAC number
           {
                instrumentMac = devSerialIn.readLine();
           }

       }

       deviceSerialfile.close();
#ifdef DEBUG_ON
       qDebug()<<" Instrument Name is: "<< instrumentName << "Instrument MAC is: "<<instrumentMac;
#endif  //DEBUG_ON

    }

    this->setDeviceName(instrumentName);

    memset(instrumentNameChar,'\0',sizeof(instrumentNameChar));

    memset(dateCmd,'\0',sizeof(dateCmd));

    if(instrumentName.size()<50)
    {
        sprintf(instrumentNameChar,"%s",instrumentName.toLocal8Bit().data());
    }

    //reading current firmware version
    deviceSerialfile.setFileName("/home/pi/Desktop/EdgeFirmware/miscfiles/release-version.txt");

    if(!deviceSerialfile.open(QIODevice::ReadOnly)) {
       qDebug()<<"Unable to open release-version.txt file";
    }
    else
    {
       QTextStream releaseVer(&deviceSerialfile);

       while(!releaseVer.atEnd()) {
           fwVersion = releaseVer.readLine();
       }

       deviceSerialfile.close();
#ifdef DEBUG_ON
       qDebug()<<"Firmware Version is: "<< fwVersion;
#endif //DEBUG_ON
    }

    memset(fwVersionChar,'\0',sizeof(fwVersionChar));

    sprintf(fwVersionChar,"%s",fwVersion.toLocal8Bit().data());

    memset(currentAlarmIdChar,'\0',sizeof(currentAlarmIdChar));

    //reading current pin number
    genericFile.setFileName("/home/pi/Desktop/EdgeFirmware/miscfiles/pinnumber.txt");

    if(!genericFile.open(QIODevice::ReadOnly)) {
       qDebug()<<"Unable to open pinnumber.txt file";
       this->setPinNotSet(true);
    }
    else
    {
       this->setPinNotSet(false);

       QTextStream pinTS(&genericFile);

       while(!pinTS.atEnd()) {
           pinNum = pinTS.readLine();
       }

       genericFile.close();

       this->setPinNumber(pinNum);
#ifdef DEBUG_ON
       qDebug()<<"Pin Num is: "<< pinNum;
#endif //DEBUG_ON
    }

    //obtaining count of no of telemetry network outage records to be sent
    genericFile.setFileName("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/telemetry.csv");
    telemetry_LineCnt = 0;
    genericFile.open(QIODevice::ReadOnly);
    QTextStream telemetryIn(&genericFile);
    while( !telemetryIn.atEnd())
    {
        logFileLine=telemetryIn.readLine();
        telemetry_LineCnt++;
    }

    genericFile.close();

    qDebug()<<"Telemetry Outage Record Cnt to be sent to Cloud is: "<<QString::number(telemetry_LineCnt);

    //obtaining count of no of telemetry htbt.csv network outage records to be sent
    genericFile.setFileName("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/telemetry-htbt.csv");
    telemetry_HtBtLineCnt = 0;
    genericFile.open(QIODevice::ReadOnly);
    QTextStream telemetryInHtBt(&genericFile);
    while( !telemetryInHtBt.atEnd())
    {
        logFileLine=telemetryInHtBt.readLine();
        telemetry_HtBtLineCnt++;
    }

    genericFile.close();

    qDebug()<<"Telemetry (htbt.csv)Outage Record Cnt to be sent to Cloud is: "<<QString::number(telemetry_HtBtLineCnt);


    //obtaining count of no of event network outage records to be sent
    genericFile.setFileName("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/event.csv");
    event_LineCnt = 0;
    genericFile.open(QIODevice::ReadOnly);
    QTextStream eventIn(&genericFile);
    while( !eventIn.atEnd())
    {
        logFileLine=eventIn.readLine();
        event_LineCnt++;
    }

    genericFile.close();

    qDebug()<<"Event Outage Record Cnt to be sent to Cloud is: "<<QString::number(event_LineCnt);

    //obtaining count of no of snooze event network outage records to be sent
    genericFile.setFileName("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/snoozeevent.csv");
    snoozeEvent_LineCnt = 0;
    genericFile.open(QIODevice::ReadOnly);
    QTextStream snEventIn(&genericFile);
    while( !snEventIn.atEnd())
    {
        logFileLine=snEventIn.readLine();
        snoozeEvent_LineCnt++;
    }

    genericFile.close();

    qDebug()<<"Snooze Event Outage Record Cnt to be sent to Cloud is: "<<QString::number(snoozeEvent_LineCnt);


    //obtaining count of no of recovery event network outage records to be sent
    genericFile.setFileName("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/recoveryevent.csv");
    recoveryEvent_LineCnt = 0;
    genericFile.open(QIODevice::ReadOnly);
    QTextStream rcEventIn(&genericFile);
    while( !rcEventIn.atEnd())
    {
        logFileLine=rcEventIn.readLine();
        recoveryEvent_LineCnt++;
    }

    genericFile.close();

    qDebug()<<"Recovery Event Outage Record Cnt to be sent to Cloud is: "<<QString::number(recoveryEvent_LineCnt);


    system("sudo chmod 777 -R /home/pi/Desktop/EdgeFirmware");


    QFileInfo check_file("/home/pi/Desktop/EdgeFirmware/miscfiles/fileupdatedone.txt");
    // check if file exists and if yes: Is it really a file and no directory?
    if (!check_file.exists() && !check_file.isFile())   //file does n't exist
    {
        system("sudo bash /home/pi/Desktop/EdgeFirmware/patch/modbox_clean.sh &");

    }

     curl = curl_easy_init();       //create a curl handle

     curlUpdMData = curl_easy_init();       //create a curl handle for meta data

     curlEvent = curl_easy_init();       //create a curl handle for event (high temp/low temp)

     curlDevStatus = curl_easy_init();       //create a curl handle for device status

     curlRecovery = curl_easy_init();       //create a curl handle for recovery event

     curlTelemetry = curl_easy_init();       //create a curl handle for telemetry

     curlHeartBt = curl_easy_init();       //create a curl handle for heart beat

     curlOutageData = curl_easy_init();       //create a curl handle for outage data


     workerThread = new Thread();

     connect(this,SIGNAL(silenceKeyPressTrue()),workerThread,SLOT(silenceKeyTrue()));

     connect(this,SIGNAL(silenceKeyPressFalse()),workerThread,SLOT(silenceKeyFalse()));

     connect(this,SIGNAL(stopCntDwnTimerSignal()),this,SLOT(stopCntDwnTimerSlot()));

//     curlThrd = new CurlThread();
//     connect(this,SIGNAL(sendCurlData(CURL*)),curlThrd,SLOT(getData(CURL*)));


     langFile.setFileName("/home/pi/Desktop/EdgeFirmware/miscfiles/languageselection.txt");


     if(!langFile.open(QIODevice::ReadOnly)) {

 #ifdef  DEBUG_ON
        qDebug()<<"Unable to open languageselection.txt file... so starting with english as default language";
 #endif  //DEBUG_ON

         this->setChineseLang(false);
     }

     else
     {
        QTextStream langIn(&langFile);


        while(!langIn.atEnd()) {
            langStatus = langIn.readLine();
        }

        langFile.close();


        if(langStatus.compare("1")==0)        //1-English	2-Chinese
        {

            this->setChineseLang(false);
            this->setLanguage("English");

        }
        else if(langStatus.compare("2")==0)   //1-English	2-Chinese
        {

             langTranslator = new LanguageTranslator();
             langTranslator->translateToChinese();
             this->setChineseLang(true);
             this->setLanguage("Chinese");
        }
     }

     check_devconfigfile.setFile("/home/pi/Desktop/EdgeFirmware/miscfiles/deviceconfigcompleted.txt");

     // check if file exists and if yes:
     if (check_devconfigfile.exists())   //file exist.,
     {
         if(updModBoxStatusInProg==false)
         {
            updateModBoxStatus();   //need this call to have the setpoints and metadata values to be read when unit is booted up
         }

     }


    apStaTimer->start(1000); //trying to connect to previously connected wifi network

    memset(telemetryBkpBuffer,'\0',sizeof(telemetryBkpBuffer));

    emit valueChanged();

}

/**
 * @brief DeltaKController::appStartLang
 * @return
 */
bool DeltaKController::appStartLang() const
{
    return m_appStartLang;
}

/**
 * @brief DeltaKController::setAppStartLang
 * @param appStartLang
 */
void DeltaKController::setAppStartLang(bool appStartLang)
{
    m_appStartLang = appStartLang;
}

/**
 * @brief DeltaKController::pinNotSet
 * @return
 */
bool DeltaKController::pinNotSet() const
{
    return m_pinNotSet;
}


/**
 * @brief DeltaKController::setPinNotSet
 * @param pinNotSet
 */
void DeltaKController::setPinNotSet(bool pinNotSet)
{
    m_pinNotSet = pinNotSet;
}

/**
 * @brief DeltaKController::showForgotPin
 * @return
 */
bool DeltaKController::showForgotPin() const
{
    return m_showForgotPin;
}

/**
 * @brief DeltaKController::setShowForgotPin
 * @param showForgotPin
 */
void DeltaKController::setShowForgotPin(bool showForgotPin)
{
    m_showForgotPin = showForgotPin;
}

/**
 * @brief DeltaKController::enableOTPButton
 * @return
 */
bool DeltaKController::enableOTPButton() const
{
    return m_enableOTPButton;
}


/**
 * @brief DeltaKController::setEnableOTPButton
 * @param enableOTPButton
 */
void DeltaKController::setEnableOTPButton(bool enableOTPButton)
{
    m_enableOTPButton = enableOTPButton;
}

/**
 * @brief DeltaKController::wifiUpdateSucces
 * @return
 */
bool DeltaKController::wifiUpdateSucces() const
{
    return m_wifiUpdateSucces;
}

/**
 * @brief DeltaKController::setWifiUpdateSucces
 * @param wifiUpdateSucces
 */
void DeltaKController::setWifiUpdateSucces(bool wifiUpdateSucces)
{
    m_wifiUpdateSucces = wifiUpdateSucces;
}

/**
 * @brief DeltaKController::wifiIconSucces
 * @return
 */
bool DeltaKController::wifiIconSucces() const
{
    return m_wifiIconSucces;
}

/**
 * @brief DeltaKController::setWifiIconSucces
 * @param wifiIconSucces
 */
void DeltaKController::setWifiIconSucces(bool wifiIconSucces)
{
    m_wifiIconSucces = wifiIconSucces;
}

/**
 * @brief DeltaKController::showConfigProg
 * @return
 */
bool DeltaKController::showConfigProg() const
{
    return m_showConfigProg;
}

/**
 * @brief DeltaKController::setShowConfigProg
 * @param showConfigProg
 */
void DeltaKController::setShowConfigProg(bool showConfigProg)
{
    m_showConfigProg = showConfigProg;
}

/**
 * @brief DeltaKController::assetIconSucces
 * @return
 */
bool DeltaKController::assetIconSucces() const
{
    return m_assetIconSucces;
}

/**
 * @brief DeltaKController::setAssetIconSucces
 * @param assetIconSucces
 */
void DeltaKController::setAssetIconSucces(bool assetIconSucces)
{
    m_assetIconSucces = assetIconSucces;
}

/**
 * @brief DeltaKController::assetIconFail
 * @return
 */
bool DeltaKController::assetIconFail() const
{
    return m_assetIconFail;
}

/**
 * @brief DeltaKController::setAssetIconFail
 * @param assetIconFail
 */
void DeltaKController::setAssetIconFail(bool assetIconFail)
{
    m_assetIconFail = assetIconFail;
}

/**
 * @brief DeltaKController::wifiIconFail
 * @return
 */
bool DeltaKController::wifiIconFail() const
{
    return m_wifiIconFail;
}

/**
 * @brief DeltaKController::setWifiIconFail
 * @param wifiIconFail
 */
void DeltaKController::setWifiIconFail(bool wifiIconFail)
{
    m_wifiIconFail = wifiIconFail;
}

/**
 * @brief DeltaKController::showBackApMode
 * @return
 */
bool DeltaKController::showBackApMode() const
{
    return m_showBackApMode;
}


/**
 * @brief DeltaKController::setShowBackApMode
 * @param showBackApMode
 */
void DeltaKController::setShowBackApMode(bool showBackApMode)
{
    m_showBackApMode = showBackApMode;
}

/**
 * @brief DeltaKController::showBackKeyPad
 */
bool DeltaKController::showBackKeyPad() const
{
    return m_showBackKeyPad;
}

/**
 * @brief DeltaKController::setShowBackKeyPad
 * @param showBackKeyPad
 */
void DeltaKController::setShowBackKeyPad(bool showBackKeyPad)
{
    m_showBackKeyPad = showBackKeyPad;
}

/**
 * @brief DeltaKController::assetUpdateSucces
 * @return
 */
bool DeltaKController::assetUpdateSucces() const
{
    return m_assetUpdateSucces;
}

/**
 * @brief DeltaKController::setAssetUpdateSucces
 * @param assetUpdateSucces
 */
void DeltaKController::setAssetUpdateSucces(bool assetUpdateSucces)
{
    m_assetUpdateSucces = assetUpdateSucces;
}


/**
 * @brief DeltaKController::assetUpdateFail
 * @return
 */
bool DeltaKController::assetUpdateFail() const
{
    return m_assetUpdateFail;
}

/**
 * @brief DeltaKController::setAssetUpdateFail
 * @param assetUpdateFail
 */
void DeltaKController::setAssetUpdateFail(bool assetUpdateFail)
{
    m_assetUpdateFail = assetUpdateFail;
}
/**
 * @brief DeltaKController::enableOTPButtonClick
 * @return
 */
bool DeltaKController::enableOTPButtonClick() const
{
    return m_enableOTPButtonClick;
}

/**
 * @brief DeltaKController::setEnableOTPButtonClick
 * @param enableOTPButtonClick
 */
void DeltaKController::setEnableOTPButtonClick(bool enableOTPButtonClick)
{
    m_enableOTPButtonClick = enableOTPButtonClick;
}

/**
 * @brief DeltaKController::readQRStatus
 * @return
 */
bool DeltaKController::readQRStatus() const
{
    return m_readQRStatus;
}

/**
 * @brief DeltaKController::setReadQRStatus
 * @param readQRStatus
 */
void DeltaKController::setReadQRStatus(bool readQRStatus)
{
    m_readQRStatus = readQRStatus;
    emit valueChanged();
}

/**
 * @brief DeltaKController::acceptConnection
 */
void DeltaKController::acceptConnection()
{

   client = myserver->nextPendingConnection();
   if(client != NULL)
   {
#ifdef  DEBUG__ON
   qDebug()<<"Client is Connected: ip "<<client->peerAddress();
#endif  //DEBUG__ON
   QObject::connect(client, SIGNAL(readyRead()),this, SLOT(startRead()));

   }
   else
   {
    qDebug()<<"Client is Not Connected";
   }

}


/**
 * @brief DeltaKController::startRead
 */
void DeltaKController::startRead()
{
    readSocket = NULL;
    readSocket = qobject_cast<QTcpSocket*>(sender());


    if (readSocket->bytesAvailable() < sizeof(Qt_WebServer_Sock))
    {
        qDebug()<<"Received bytes less than structure size...So returning: ";
        return;
    }


    info.index=0;
    memset(info.parametersBuffer,'\0',sizeof(info.parametersBuffer));

    readSocket->read((char *)&info,sizeof(info));


    qDebug()<<"Received Index: "<<info.index << "Received Value: "<<info.parametersBuffer;


    if(info.index==1)   //1. set point temp received
    {

    }
    else if(info.index==2)   //2. low temp received
    {
       lowTemp = QString::fromLocal8Bit(info.parametersBuffer);

    }
    else if(info.index==3)   //3. high temp received
    {
        highTemp = QString::fromLocal8Bit(info.parametersBuffer);

    }
    else if(info.index==4)   //4. asset type
    {
        assetType = QString::fromLocal8Bit(info.parametersBuffer);

        if(updModBoxStatusInProg==false)
        {
           updateModBoxStatus();
        }

        system("sudo cp /home/pi/Desktop/EdgeFirmware/metadata/* /home/pi/Desktop/modbox-webserver/chinaIoT-English/");
        system("sudo cp /home/pi/Desktop/EdgeFirmware/metadata/* /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese/");

    }
    else if(info.index==5)   //5. asset model
    {
        assetModel = QString::fromLocal8Bit(info.parametersBuffer);
    }
    else if(info.index==6)   //6. asset serial number
    {
        assetSerial = QString::fromLocal8Bit(info.parametersBuffer);
    }
    else if(info.index==7)   //7. asset city
    {
        assetCity = QString::fromLocal8Bit(info.parametersBuffer);
    }
    else if(info.index==8)   //8. asset location
    {
        assetLocation = QString::fromLocal8Bit(info.parametersBuffer);
    }
    else if(info.index==9)   //9. acknowledge for alarm
    {
        if(totalAlarmIdStr.size()>0)
        {
            ackStat = QString::fromLocal8Bit(info.parametersBuffer) + ";" ;
            qDebug()<<"LINE: "<<__LINE__ << totalAlarmIdStr;
            totalAlarmIdStr.replace(ackStat, QString(""));
            qDebug()<<"LINE: "<<__LINE__ << totalAlarmIdStr;


            if(totalAlarmIdStr.size()==0)
            {
                receivedCloudAck = true;
                verifyRTDData(rtdTempVal.toDouble());
            }
        }
    }
    else if(info.index==10)   //10. snooze value
    {
        snoozeValue = QString::fromLocal8Bit(info.parametersBuffer);

        this->setSnoozeStatus(false);       //resetting snooze if already existing

        this->setSnoozeTimeCounter(0);

        this->setSnoozeTime((snoozeValue.toInt())*60*1000);

        this->setSnoozeStatus(true);
    }

    else if(info.index==11)   //11. principalId
    {

        principalId = QString::fromLocal8Bit(info.parametersBuffer);


        memset(principalIdChar,'\0',sizeof(principalIdChar));

        strcpy(principalIdChar,info.parametersBuffer);
        principalIdChar[32]='\0';

        if(updModBoxStatusInProg==false)
        {
           updateModBoxStatus();
        }

        if(updDevStatusInProgress==false)
        {
            QtConcurrent::run(this,&DeltaKController::updateDeviceStatus);qDebug()<<"Called UpdDevStat: "<<__LINE__;
        }

    }

    else if(info.index==12)   //12. friendlyName
    {
        friendlyName = QString::fromLocal8Bit(info.parametersBuffer);

        if(updModBoxStatusInProg==false)
        {
           updateModBoxStatus();
        }
    }

    else if(info.index==13)   //13. otpbutton
    {

        otpButtStatus = QString::fromLocal8Bit(info.parametersBuffer);

        if(otpButtStatus.compare("1")==0)     //completed configuration in ap mode
        {
            this->setEnableOTPButton(true);

        }
        else if(otpButtStatus.compare("0")==0)     //config not completed in ap mode
        {
            this->setEnableOTPButton(false);

        }


    }

    else if(info.index==14)      //14. rtd Out of range values
    {

        if(unknownRTDCnt>=6)
        {
           this->setTemperature("--.-");
           rtdTempVal = "";

           this->setNoProbeDetected(true);

           if(yellowLedTimer->isActive()==false && this->appStartLang()==false && apModeInProgress==false)
           {
               system("echo \"0\" > /sys/class/gpio/gpio22/value");  //Green led on OFF state
               yellowLedTimer->start(10);
           }
           if(this->yellowWarning()==false)
           {
               this->setYellowWarning(true);
               this->setWarningAlarmMessageText(tr("No Probe Detected"));
               emit startYellowFlash();
           }
        }
        else
        {
            unknownRTDCnt++;
        }


    }

    else if(info.index==15)   //15. rtdValue
    {
        unknownRTDCnt = 0;

        if(this->noProbeDetected()==true)
        {
           this->setNoProbeDetected(false);
        }

        rtdTempVal = QString::fromLocal8Bit(info.parametersBuffer);

        receivedRTD = atof(info.parametersBuffer);      //converting received value to double


        if(receivedRTD!=0)
        {
            if(receivedRTD<0)       //negative rtd value
            {
                if(receivedRTD <= -100)      //values ranging from -100.00 to -999.99
                {
                    //qDebug()<<"Received RTD value is less than -100";     //6 digits ex: -128.3
                    rtdTempVal.truncate(6);
                }
                else if(receivedRTD > -100) //values ranging from -0.01 to -99.99
                {
                    //qDebug()<<"Received RTD value is greater than -100";    //5 digits ex: -28.3

                    if(receivedRTD > -10)           //values between -0.0 to -9.9
                    {
                        rtdTempVal.truncate(4);     //ex: -3.5
                    }
                    else
                    {
                        rtdTempVal.truncate(5);    //ex: -28.3
                    }

                }
            }
            else if(receivedRTD>0)  //positive rtd value
            {
                if(receivedRTD<100)
                {
                    //qDebug()<<"Positive Number is less than 100"; //4 digits ex: 28.3
                    if(receivedRTD<10)      //values between 0.01 and 9.99
                    {
                        rtdTempVal.truncate(3);     //ex: 8.6
                    }
                    else
                    {
                        rtdTempVal.truncate(4);     //ex: 28.3
                    }

                }
                else if(receivedRTD>=100)
                {
                    //qDebug()<<"Positive Number is greater than 100";  //5 digits ex: 128.5
                    rtdTempVal.truncate(5);
                }

            }

            //this->setTemperature(rtdTempVal);   doing this after verifyRTDData();
        }


        rtdDelta = currentRTD - receivedRTD;

        if(rtdDelta < 0 )       //delta value is negative
        {
            rtdDelta = -rtdDelta;

        }



        if(this->appStartLang()==false && apModeInProgress==false)
        {
            if(rtdDelta > UPDATE_STATUS_THRESHOLD)      //call update device status
            {
#ifdef  DEBUG__ON
                qDebug()<<"DELTA IS CROSSING THE THRESHOLD... SO UPDATING DEVICE STATUS";
#endif  //DEBUG_ON
                if(updDevStatusInProgress==false)
                {
                    QtConcurrent::run(this,&DeltaKController::updateDeviceStatus);qDebug()<<"Called UpdDevStat: "<<__LINE__;
                }

                currentRTD =  receivedRTD;
            }


            verifyRTDData(rtdTempVal.toDouble());

            if(receivedRTD!=0)
            {
                this->setTemperature(rtdTempVal);
            }

        }

    }

    else if(info.index==16)   //16. Battery Connectivity Status	//0-Mains power is on	1-Mains power is off.. running on battery
    {

    }

    else if(info.index==17)   //17. Battery Percentage
    {

    }
    else if(info.index==18)   //18. Wifi Signal Strength... NOT USING THIS OPTION NOW
    {


    }

    else if(info.index==19)   //19. QR Code Image with path
    {
        qrPath = QString::fromLocal8Bit(info.parametersBuffer);


        if(qrPath.size()>0)      //obtained the qr image successfully
        {
           this->setReadQRStatus(true);     //   qsTr("Device user linking successful")
           recdQRSuccess = true;
           qrCounter=0;

           QString qrImageStr = "file://";
           qrImageStr = qrImageStr+qrPath;
           qDebug()<<"QR Image Final Path is: "<<qrPath;
           QCoreApplication::processEvents(QEventLoop::AllEvents);
           this->setQrImage(qrImageStr);

           this->setUserLinkingStatusMsg(tr("Device user linking successful"));
           recdQRSuccess= false;

        }
        else         //failed to obtain qr image
        {   qrCounter++;

           if(qrCounter<=3)      //making 3 attempts when qr code generation failed
           {
               readQRImage();
           }
           else
           {
               this->setReadQRStatus(false);
               this->setUserLinkingStatusMsg(tr("User Linking Failed"));
           }

        }

    }
    else if(info.index==20)   //20. HeartBeat Status
    {
        htBtIdRecd = QString::fromLocal8Bit(info.parametersBuffer);

        if(strcmp(info.parametersBuffer,"false")==0)
        {
           //HeartBeat Status is False
           heartBeatStatusFlag = false;
           qDebug()<<"Line: "<< __LINE__ << heartBeatStatusFlag;
        }
        else if(htBtIdRecd.size()>0)
        {
            heartBeatStatusFlag = true;
            devReRegisterFlag = true;
        }
        else
        {
            heartBeatStatusFlag = false;
        }

    }
    else if(info.index==21)   //21. Device Name
    {
        //not updating serial number for now... need to update this code to update deviceSerial file since now mac address is also part of deviceSerial file
        this->setDeviceName(QString::fromLocal8Bit(info.parametersBuffer));

        instrumentName = QString::fromLocal8Bit(info.parametersBuffer);


        memset(instrumentNameChar,'\0',sizeof(instrumentNameChar));

        if(instrumentName.size()<50)
        {
            sprintf(instrumentNameChar,"%s",instrumentName.toLocal8Bit().data());
        }


        deviceSerialfile.setFileName("/home/pi/Desktop/EdgeFirmware/miscfiles/deviceSerial.txt");
        if(!deviceSerialfile.open(QIODevice::WriteOnly)) {
           qDebug()<<"Unable to open deviceSerial.txt file to update devicename";
        }
        else
        {
            //info.parametersBuffer[strlen(info.parametersBuffer)]='\0';
           QTextStream out(&deviceSerialfile);
           out << QString::fromLocal8Bit(info.parametersBuffer) ;
           deviceSerialfile.close();
        }

        system("sudo mv /home/pi/Desktop/EdgeFirmware/miscfiles/deviceSerial.txt /home/deviceSerial.txt");

    }
    else if(info.index==22)   //22. FirmwareUpdateChecksumStatus
    {
        //batteryPer = QString::fromLocal8Bit(info.parametersBuffer);
        strcpy(fwVersionChar,info.parametersBuffer);
        if(strlen(fwVersionChar)>0)
        {
           //doFirmwareUpdate();
        }
    }
    else if(info.index==23)   //23. DateTime Command received from mobile broswer during device configuration
    {
        //Not using this command for now... this was with old device configuration flow        
    }
    else if(info.index==24)   //24. Changed Wifi Settings from mobile browser
    {

        apModeInProgress = false;        //to start validating the received rtd values against the threshold immediately instead of waiting till user clicking on DONE button
        if(apModeCheckTimer->isActive()==true)
        {
            apModeCheckTimer->stop();
        }
        if(dlRestartTimer->isActive()==false)
        {
            dlRestartTimer->start(DL_MQTT_ERROR_CHECK_TIME);   //starting timer for 5 min so that if no data from DL endpoint, DL has to restart
        }


#ifdef DEBUG_ON
        qDebug()<<"Received Changed Wifi Settings event from modbox-webserver";
#endif  //DEBUG_ON

       if(getWifiConnectStatus()==true)     //obtained ip address for wifi
       {
            this->setWifiUpdateSucces(true);
            this->setWifiStatusText(tr("Wi-Fi connection successful"));

            this->setWifiIconSucces(true);  //show success icon
            this->setWifiIconFail(false);   //hide red cross fail icon
            this->setAssetUpdateFail(false);         //hide wifi fail display label
       }
        else      //no network
        {
            this->setWifiIconSucces(false); //hide green tick success icon
            this->setWifiIconFail(true);   //show red cross fail icon

            this->setWifiUpdateSucces(true);        //enable wifi status display label
            this->setWifiStatusText(tr("Wi-Fi connection failed"));
            this->setAssetUpdateFail(true);         //enable wifi fail display label
            this->setFailMessage(tr("Please check your Wi-Fi network credentials! Go to Settings > Device Configuration and try again later!"));
        }

       this->setBusy(false);

       this->setShowConfigProg(false);

       this->setEnableOTPButton(true);



       emit apModeAutoExit();   //to auto navigate back to settings screen

       system("sudo killall modbox-webserver");
       apStaTimer->start(100);

    }
    else if(info.index==25)   //25. Changed Asset Details/Alarm Settings from mobile browser
    {
        apModeInProgress = false;        //to start validating the received rtd values against the threshold immediately instead of waiting till user clicking on DONE button
        if(apModeCheckTimer->isActive()==true)
        {
            apModeCheckTimer->stop();
        }

        if(dlRestartTimer->isActive()==false)
        {
            dlRestartTimer->start(DL_MQTT_ERROR_CHECK_TIME);   //starting timer for 5 min so that if no data from DL endpoint, DL has to restart
        }



#ifdef DEBUG_ON
        qDebug()<<"Received Changed Asset/Alarm Settings event from modbox-webserver";
#endif //DEBUG_ON
        if(getAssetPushStatus()==true)        //obtained ip address for wifi
        {
            this->setAssetUpdateSucces(true);
            this->setAssetStatusText(tr("Asset details pushed to cloud successfully"));

            this->setAssetIconSucces(true);     //show success icon green tick
            this->setAssetIconFail(false);      //hide fail icon red cross
            this->setAssetUpdateFail(false);         //hide wifi/assset fail display label
        }
        else       //no network
        {
            this->setAssetIconSucces(false);     //hide success icon green tick
            this->setAssetIconFail(true);      //show fail icon red cross

            this->setAssetUpdateSucces(true);
            this->setAssetStatusText(tr("Failed to push Asset settings to cloud!"));
            this->setAssetUpdateFail(true);     //show the go to settings and retry text
            this->setFailMessage(tr("Please check your internet connection Go to Settings > Device Configuration and try again later!"));

            cloudPushFail = true;       //to ensure metadata record is not stored on sd card
        }

        this->setBusy(false);

        this->setShowConfigProg(false);

        this->setEnableOTPButton(true);

        emit apModeAutoExit();   //to auto navigate back to settings screen

        system("sudo killall modbox-webserver");
        apStaTimer->start(100);
        if(updModBoxStatusInProg==false)
        {
           updateModBoxStatus();
        }

    }
    else if(info.index==26)   //26. Changed both wifi and Asset Details/Alarm Settings from mobile browser
    {
        apModeInProgress = false;        //to start validating the received rtd values against the threshold immediately instead of waiting till user clicking on DONE button
        if(apModeCheckTimer->isActive()==true)
        {
            apModeCheckTimer->stop();
        }

        if(dlRestartTimer->isActive()==false)
        {
            dlRestartTimer->start(DL_MQTT_ERROR_CHECK_TIME);   //starting timer for 5 min so that if no data from DL endpoint, DL has to restart
        }


#ifdef DEBUG_ON
        qDebug()<<"Received Changed both Wi-Fi and Asset/Alarm Settings event from modbox-webserver";
#endif //DEBUG_ON

        if(getWifiConnectStatus()==true)        //got ip address from wifi access point
        {
            this->setWifiUpdateSucces(true);    //enable wifi status display label
            this->setWifiStatusText(tr("Wi-Fi connection successful"));

            this->setWifiIconSucces(true);  //show wifisuccess icon
            this->setWifiIconFail(false);   //hide wifi red cross fail icon
            this->setAssetUpdateFail(false);         //hide wifi fail display label

            this->setAssetUpdateSucces(true);
            this->setAssetStatusText(tr("Asset details pushed to cloud successfully"));

            this->setAssetIconSucces(true);     //show asset success icon green tick
            this->setAssetIconFail(false);      //hide asset fail icon red cross
            this->setAssetUpdateFail(false);         //hide wifi/assset fail display label
        }
        else       //no network
        {
            this->setWifiIconSucces(false); //hide green tick success icon
            this->setWifiIconFail(true);   //show red cross fail icon

            this->setWifiUpdateSucces(true);        //enable wifi status display label
            this->setWifiStatusText(tr("Wi-Fi connection failed"));
            this->setAssetUpdateFail(true);         //enable wifi fail display label
            this->setFailMessage(tr("Please check your Wi-Fi network credentials! Go to Settings > Device Configuration and try again later!"));


            this->setAssetIconSucces(false);     //hide success icon green tick
            this->setAssetIconFail(true);      //show fail icon red cross

            this->setAssetUpdateSucces(true);
            this->setAssetStatusText(tr("Failed to push Asset settings to cloud!"));

            cloudPushFail = true;       //to ensure metadata record is not stored on sd card
        }

        this->setBusy(false);

        this->setShowConfigProg(false);

        this->setEnableOTPButton(true);

        emit apModeAutoExit();   //to auto navigate back to settings screen

        system("sudo killall modbox-webserver");
        apStaTimer->start(100);
        if(updModBoxStatusInProg==false)
        {
           updateModBoxStatus();
        }


    }
    else if(info.index==27)     //27. First Time Device Configuration Completed
    {
        apModeInProgress = false;        //to start validating the received rtd values against the threshold immediately instead of waiting till user clicking on DONE button
        if(apModeCheckTimer->isActive()==true)
        {
            apModeCheckTimer->stop();
        }

        if(dlRestartTimer->isActive()==false)
        {
            dlRestartTimer->start(DL_MQTT_ERROR_CHECK_TIME);   //starting timer for 5 min so that if no data from DL endpoint, DL has to restart
        }


        system("sudo echo 1 > /home/pi/Desktop/EdgeFirmware/miscfiles/startdl.txt");  //needed only for CM

        if(getWifiConnectStatus()==true)        //got ip address from wifi access point
        {

            emit wifiConnSuccess();

            this->setWifiIconSucces(true);  //show wifisuccess icon
            this->setWifiIconFail(false);   //hide wifi red cross fail icon
        }
        else    //no network
        {
            emit wifiConnFail();

            this->setWifiIconSucces(false);  //show wifisuccess icon
            this->setWifiIconFail(true);   //hide wifi red cross fail icon

            cloudPushFail = true;       //to ensure metadata record is not stored on sd card

        }

        this->setBusy(false);

        this->setShowConfigProg(false);

        this->setEnableOTPButton(true);


        system("sudo killall modbox-webserver");
        apStaTimer->start(100);
        if(updModBoxStatusInProg==false)
        {
           updateModBoxStatus();
        }

    }
    else if(info.index==28)     //28. Device Configuration Started and client connected
    {

        emit configProgress();
        this->setShowBackApMode(false);

    }
    else if(info.index==29)     //29. Timed out... Device configuration session has expired!
    {
        apModeInProgress = false;        //to start validating the received rtd values against the threshold immediately instead of waiting till user clicking on DONE button
        if(apModeCheckTimer->isActive()==true)
        {
            apModeCheckTimer->stop();
        }

        if(dlRestartTimer->isActive()==false)
        {
            dlRestartTimer->start(DL_MQTT_ERROR_CHECK_TIME);   //starting timer for 5 min so that if no data from DL endpoint, DL has to restart
        }


        if(getWifiConnectStatus()==true)        //got ip address from wifi access point
        {
            //needed this delay to display the timed out page
        }


        this->setWifiUpdateSucces(true);    //enable wifi status display label

        this->setWifiStatusText(tr("         Device Configuration has expired!"));

        this->setBusy(false);

        this->setShowConfigProg(false);

        this->setEnableOTPButton(true);



        emit apModeAutoExit();   //to auto navigate back to settings screen

        system("sudo killall modbox-webserver");
        apStaTimer->start(100);
    }
    else if(info.index==30)     //30. Exited from Device Configuration with out making any changes
    {
        apModeInProgress = false;        //to start validating the received rtd values against the threshold immediately instead of waiting till user clicking on DONE button
        if(apModeCheckTimer->isActive()==true)
        {
            apModeCheckTimer->stop();
        }

        if(dlRestartTimer->isActive()==false)
        {
            dlRestartTimer->start(DL_MQTT_ERROR_CHECK_TIME);   //starting timer for 5 min so that if no data from DL endpoint, DL has to restart
        }


        if(getWifiConnectStatus()==true)        //got ip address from wifi access point
        {
            //needed this delay to display the timed out page
        }

        this->setWifiUpdateSucces(true);    //enable wifi status display label

        this->setWifiStatusText(tr("         Completed Device Configuration"));

        this->setBusy(false);

        this->setShowConfigProg(false);

        this->setEnableOTPButton(true);



        emit apModeAutoExit();   //to auto navigate back to settings screen

        system("sudo killall modbox-webserver");
        apStaTimer->start(100);
    }
    else if(info.index==31)     //31. Scanning of QR Code Done
    {
        //emit qrScanDone();    //this is causing to emit even when linked from linkcode

    }
    else if(info.index==32)   //32. Asset Type/Asset Serial Number Changed
    {
        this->setBusy(false);
        this->setPopUpString(tr("DeviceLink connected to new asset, rebooting now"));
        this->showPopUp(true);
        this->setShowConfigProg(false);
        system("sudo rm -rf /home/pi/Desktop/EdgeFirmware/heartbtfaildata/*");
        system("sudo rm -rf /home/pi/Desktop/EdgeFirmware/miscfiles/languageselection.txt");
        system("sudo bash /home/pi/Desktop/EdgeFirmware/reinstalldl.sh &");

    }
    else if(info.index==33)     //33. DataDispatchRequest received
    {

        QtConcurrent::run(this,&DeltaKController::processRecdDispatchId);
        if(apModeInProgress==false)
        {
            if(loggingFlag==false)  //monitor DL status only when network is connected
            {
                if(dlRestartTimer->isActive()==false)
                {
                    dlRestartTimer->start(DL_MQTT_ERROR_CHECK_TIME);   //starting timer for 5 min so that if no data from DL endpoint, DL has to restart
                }
                else if(dlRestartTimer->isActive()==true)
                {
                    qDebug("DLTimer Resetted");
                    dlRestartTimer->stop();
                    dlRestartTimer->start(DL_MQTT_ERROR_CHECK_TIME);
                }
            }
            else if(loggingFlag==true)      //network is down, don't monitor DL
            {
                if(dlRestartTimer->isActive()==true)
                {
                    dlRestartTimer->stop();
                }
            }

        }
    }

    if (readSocket->bytesAvailable() > 0)
    {
        qDebug()<<"*****Read Excess Bytes from Socket is: "<<readSocket->readAll();
    }

   emit valueChanged();

}


/**
 * @brief DeltaKController::processRecdDispatchId
 */
void DeltaKController::processRecdDispatchId()
{
    if(strlen(telemetryBkpBuffer)>0)
    {

        recdReqId = QString::fromLocal8Bit(info.parametersBuffer);
        reqIdCnt=0;

        memset(telemetryBkpBufferTmp,'\0',sizeof(telemetryBkpBufferTmp));
        strcpy(telemetryBkpBufferTmp,telemetryBkpBuffer);

        teleTokenLenBefore = strlen(telemetryBkpBufferTmp);  //retaining current token count

        recdReqIdList = recdReqId.split(",");


        foreach(QString item, recdReqIdList)
        {

            strcpy(reqIdTokens[reqIdCnt], item.remove(QRegExp("[^a-zA-Z\\d]")).toLocal8Bit().data());
#ifdef  DEBUG_ON
            qDebug()<<"Received Dispatch Tokens: "<<QString::fromLocal8Bit(reqIdTokens[reqIdCnt]);
#endif  //DEBUG_ON
            reqIdCnt++;
        }


        for(i=0;i<reqIdCnt;i++)
        {
            memset(telemetryBkpBufferUpd,'\0',sizeof(telemetryBkpBufferUpd));

            telemetryTokenList = QString::fromLocal8Bit(telemetryBkpBufferTmp).split(";");

            foreach(QString itemTelemetry, telemetryTokenList)
            {
                if(itemTelemetry.size()>90)
                {                    
                    if((strstr(itemTelemetry.toLocal8Bit().data(),reqIdTokens[i])==NULL))
                    {
                        sprintf(telemetryBkpBufferUpd+strlen(telemetryBkpBufferUpd),"%s;",itemTelemetry.toLocal8Bit().data());
                    }    
                }
            }

            memset(telemetryBkpBufferTmp,'\0',sizeof(telemetryBkpBufferTmp));
            strcpy(telemetryBkpBufferTmp,telemetryBkpBufferUpd);
        }
#ifdef  DEBUG_ON
        qDebug()<<"Dispatch Request Received, Updated Buf is:"<<telemetryBkpBufferTmp;
#endif  //DEBUG_ON
        teleTokenLenAfter = strlen(telemetryBkpBuffer);     //present token count

        memset(telemetryBkpDiffBuff,'\0',sizeof(telemetryBkpDiffBuff));
        strncpy(telemetryBkpDiffBuff,telemetryBkpBuffer+teleTokenLenBefore,(teleTokenLenAfter-teleTokenLenBefore));

        memset(telemetryBkpBuffer,'\0',sizeof(telemetryBkpBuffer));
        strcpy(telemetryBkpBuffer,telemetryBkpBufferTmp);
        strcat(telemetryBkpBuffer,telemetryBkpDiffBuff);

    }
    else
    {
        qDebug()<<"No Data is available in telemetryBkpBuffer., so unable to process";
    }


}


/**
 * @brief DeltaKController::delay
 * @param sec
 */
void DeltaKController::delayQRReq(int sec)
{
    dieTimeQR= QTime::currentTime().addSecs(sec);
    while ((QTime::currentTime() < dieTimeQR))
    {
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
        if(this->readQRStatus()==true)
        {
            break;
        }
    }
}

/**
 * @brief DeltaKController::delay
 * @param sec
 */
void DeltaKController::delay(int sec)
{
    dieTime= QTime::currentTime().addSecs(sec);
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}


/**
 * @brief DeltaKController::verifyRTDData
 * @param rtdTempVal
 */
void DeltaKController::verifyRTDData(double rtdTempVal)
{

    lowTempFloat = lowTemp.toDouble();
    highTempFloat = highTemp.toDouble();

    if( ((rtdTempVal >= highTempFloat) || (rtdTempVal <= lowTempFloat)) && rtdTempVal!=0 )     //temp alarm active
    {
        this->setYellowWarning(false);
        this->setWarningAlarmMessageText("");
        emit stopYellowFlash();

        if(resetKeyTimer->isActive()==false && keyPressedFlag==false)    //timer to monitor silence key press
        {
            resetKeyTimer->start(100);
        }

        if(rtdTempVal>=highTempFloat)     //high temp condition
        {

            if(batteryAlarmFlag==false)
            {
                this->setAlarmMessage("");
                this->setAlarmMessage(tr("Warm Alarm Active"));
            }

            //handling the case where low temp alarm comes when high temp alarm is active
            if(lowTempTextDispFlag==true && lowTempAlarmFlag==true)
            {
                if(alarmRecoveryInProgress==false)
                {
                    if(recoveryAlarmEventSentFlag==false)  //blocking all further alarms to be received....
                    {
                        thresholdArray =   lowTemp.toLocal8Bit();
                        //QtConcurrent::run(this,&DeltaKController::sendAlarmRecoveryEvent);
                        alarmRecoveryTimer->start(2.5*1000);    //calling this in a timer so that ui does n't block updating when waiting for heartbeat lock inside warmalarm event
                    }
                }
                else
                {
                    qDebug()<<"*****AlarmRecovery Previous Attempt in Progress., not sending now*****";
                }
            }




            if(highTempAlarmEventSentFlag==false)       //sending high temp alarm event
            {
                if(highTempInProgress==false)
                {
                    //QtConcurrent::run(this,&DeltaKController::sendHighTempAlarmEvent);
                    warmAlarmTimer->start(5*1000);  //calling this in a timer so that ui does n't block updating when waiting for heartbeat lock inside warmalarm event
                    qDebug()<<"*****Attempted WarmAlarmEvent*****";
                }
                else
                {
                    qDebug()<<"*****WarmAlarm Previous Attempt in Progress., not sending now*****";
                }

            }

            highTempTextDispFlag=true;
            highTempAlarmFlag = true;
            lowTempAlarmEventSentFlag = false;
            lowTempTextDispFlag = false;

        }
        else if(rtdTempVal<=lowTempFloat)     //low temperature event
        {
            if(batteryAlarmFlag==false)
            {
                this->setAlarmMessage("");
                this->setAlarmMessage(tr("Cold Alarm Active"));
            }

            //handling the case where high temp alarm comes when low temp alarm is active to send event
            if(highTempTextDispFlag==true && highTempAlarmFlag==true)
            {
                if(alarmRecoveryInProgress==false)
                {
                    qDebug()<<"<----->LOW TEMP ALARM FIRED IN MIDDLE OF HIGH TEMP ACTIVE";
                    if(recoveryAlarmEventSentFlag==false)  //blocking all further alarms to be received....
                    {
                        thresholdArray =   highTemp.toLocal8Bit();
                        alarmRecoveryTimer->start(2.5*1000);    //calling this in a timer so that ui does n't block updating when waiting for heartbeat lock inside warmalarm event
                    }

                }
                else
                {
                    qDebug()<<"*****AlarmRecovery Previous Attempt in Progress., not sending now*****";
                }
            }

            if(lowTempAlarmEventSentFlag==false)
            {
                if(lowTempInProgress==false)
                {
                    //QtConcurrent::run(this,&DeltaKController::sendLowTempAlarmEvent);
                    coldAlarmTimer->start(5*1000);  //calling this in a timer so that ui does n't block updating when waiting for heartbeat lock inside warmalarm event
                    qDebug()<<"*****Attempted ColdAlarmEvent*****";
                }
                else
                {
                    qDebug()<<"*****ColdAlarm Previous Attempt in Progress., not sending now*****";
                }
            }

            lowTempTextDispFlag=true;
            lowTempAlarmFlag = true;
            highTempAlarmEventSentFlag = false;
            highTempTextDispFlag = false;
        }

        alarmFlag=true;


       if(isSnooze()==false)        //setting the default snooze timer
       {
            this->setTempAlarm(true);
            this->setSnoozeStatus(false);
            this->setSnoozeTime(DEFAULT_SNOOZE*60*1000);
            this->setTempAlarmIcon(true);
            if(batteryAlarmFlag==false)
            {
                this->setBatteryAlarmIcon(false);
            }

            emit startRedFlash();

            if(highTempAlarmFlag==true && lowTempAlarmFlag==true)   //case where high temp is fired when low temp is active
            {
                lowTempAlarmFlag = false;
                highTempAlarmFlag = false;
                keyPressedFlag = false;emit silenceKeyPressFalse();

                if(workerThread->isRunning()==true)
                {
                  //buzzerTimer->stop();
                    //qDebug()<<"WORKER THREAD STARTED: "<<__LINE__;
                  workerThread->stop(); delay(1);
                  workerThread->start();
                }
                else if(workerThread->isRunning()==false)
                {//qDebug()<<"WORKER THREAD STARTED: "<<__LINE__;
                    workerThread->start();
                }

            }
            else if(workerThread->isRunning()==false)
            {//qDebug()<<"WORKER THREAD STARTED: "<<__LINE__;
                workerThread->start();
            }

         }
         else if(this->isSnooze()==true && bothAlarmBatteryActFlag==true)
         {

         }
         //case where battery alarm came into picture when temp alarm is active...
         else if(this->isSnooze()==true && batteryAlarmFlag==true && snoozeResetTempAlarmFlag==false && snoozeResetBattWarnFlag==false && snoozeResetBattAlarmFlag==false)
         {
           snoozeResetTempAlarmFlag = true;     //flag to ensure snooze does n't get reset for both temp alarm and battery alarm active
           keyPressedFlag = false;emit silenceKeyPressFalse();
           this->setSnoozeStatus(false);
           this->setSnoozeTimeCounter(0);
           emit valueChanged();
           if(workerThread->isRunning()==true)
           {//qDebug()<<"WORKER THREAD STARTED: "<<__LINE__;
               workerThread->stop();delay(1);
               workerThread->start();
           }
           else if(workerThread->isRunning()==false)
           {//qDebug()<<"WORKER THREAD STARTED: "<<__LINE__;
               workerThread->start();
           }
         }
        //case where battery warning came into picture when temp alarm is active...
         else if(this->isSnooze()==true && batteryAlarmFlag==true && snoozeResetTempAlarmFlag==false && (snoozeResetBattWarnFlag==false || snoozeResetBattAlarmFlag==false))
         {

             snoozeResetTempAlarmFlag = true;
             keyPressedFlag = false;emit silenceKeyPressFalse();
             this->setSnoozeStatus(false);
             this->setSnoozeTimeCounter(0);
             emit valueChanged();
             if(workerThread->isRunning()==true)
             {
                 workerThread->stop();delay(1);
                 workerThread->start();
             }
             else if(workerThread->isRunning()==false)
             {//qDebug()<<"WORKER THREAD STARTED: "<<__LINE__;
                 workerThread->start();
             }
         }
        //case for handling high temp fired when low temp is active and snooze timer active... to reset the snooze and start buzzer
         else if(this->isSnooze()==true && highTempAlarmFlag==true && lowTempAlarmFlag==true && batteryAlarmFlag==false && snoozeResetTempAlarmFlag==false && snoozeResetBattWarnFlag==false && snoozeResetBattAlarmFlag==false)
         {

             lowTempAlarmFlag = false;
             highTempAlarmFlag = false;
             keyPressedFlag = false;emit silenceKeyPressFalse();
             this->setSnoozeStatus(false);
             this->setSnoozeTimeCounter(0);
             emit valueChanged();
             if(workerThread->isRunning()==true)
             {
               workerThread->stop();delay(1);
               workerThread->start();
             }
             else if(workerThread->isRunning()==false)
             {
                 workerThread->start();
             }

         }
         //case for handling high temp fired when low temp is active and snooze timer not active
         else if(this->isSnooze()==false && highTempAlarmFlag==true && lowTempAlarmFlag==true && batteryAlarmFlag==false && snoozeResetTempAlarmFlag==false && snoozeResetBattWarnFlag==false && snoozeResetBattAlarmFlag==false)
         {

           lowTempAlarmFlag = false;
           highTempAlarmFlag = false;
           keyPressedFlag = false;emit silenceKeyPressFalse();

           if(workerThread->isRunning()==true)
           {
             workerThread->stop();delay(1);
             workerThread->start();
           }
           else if(workerThread->isRunning()==false)
           {
               workerThread->start();
           }

         }
        else if(workerThread->isRunning()==false)
        {
           workerThread->start();
        }

       //updating the current status to cloud
       if((batteryFlag==false) && (loggingFlag==false))
       {
           updateMode(ALARM);
       }
       else if((batteryFlag==true) && (loggingFlag==false))
       {
           updateMode(ALARMANDBATT);
       }
       else if((batteryFlag==false) && (loggingFlag==true))
       {
           updateMode(ALARMANDLOG);
       }
       else if((batteryFlag==true) && (loggingFlag==true))
       {
           updateMode(ALARMBATTLOG);
       }

       recoveryAlarmEventSentFlag = false;      //resetting the rececovery event sent flag status

    }
    else                                     //temp alarm is not set
    {
        highTempAlarmEventSentFlag = false;  //flag to ensure event is sent only once

        lowTempAlarmEventSentFlag = false;


        dlLinkedUsers.setPath("/home/pi/dl-1.8-prod/deeplaser-1.8.5/data");

        dlLinkedUsers.setFilter( QDir::AllEntries | QDir::NoDotAndDotDot );

        //qDebug()<<"Total File Count is: "<<dlLinkedUsers.count();

        if(dlLinkedUsers.count()<=1)    //no user is linked... so flash a warning message
        {
            noUserLinked = true;
            //qDebug()<<"<-----> USER NOTS LINKED";
        }
        else if(dlLinkedUsers.count()>1)    //more than one folder exists... user already linked
        {
            noUserLinked = false;
            //qDebug()<<"<-----> USER ALREADY LINKED";
        }



        if(alarmFlag==true)
        {
            if(recoveryAlarmEventSentFlag==false)  //blocking all further alarms to be received....
            {
                if(alarmRecoveryInProgress==false)
                {
                    QFuture<long> recoveryStat = QtConcurrent::run(this,&DeltaKController::sendAlarmRecoveryEvent);

                    QFutureWatcher<long> watcher;
                    QEventLoop loop;

                    connect(&watcher, SIGNAL(finished()), &loop, SLOT(quit()),  Qt::QueuedConnection);
                    watcher.setFuture(recoveryStat);
                    loop.exec();

                    if(recoveryStat.result()==200)
                    {
                        alarmSetClearedFlag=true;    //previously alarm was there... but now cleared
                        alarmFlag=false;
                    }
                }
                else
                {
                    qDebug()<<"*****AlarmRecovery Previous Attempt in Progress., not sending now*****";
                }

            }
        }


        if(resetKeyTimer->isActive()==true)
        {
            resetKeyTimer->stop();
        }

        if(highTempTextDispFlag==true)      //warm temp is previously set
        {
            if(receivedCloudAck==false)     //no ack is yet received from the cloud for this alarm
            {
                 unAckHighTempFlag = true;
                 unAckPowerOutFlag = true;
            }
            htAlarmSetClearedFlag = true;
            highTempTextDispFlag = false;

        }

        if(lowTempTextDispFlag==true)       //cold alarm is previously set
        {
            if(receivedCloudAck==false)     //no ack is yet received from the cloud for this alarm
            {
                 unAckLowTempFlag = true;
            }
            ltAlarmSetClearedFlag = true;
            lowTempTextDispFlag = false;

        }

        if(powerOutageSetClrFlag==true)     //power outage warning is previously set
        {
            if(receivedCloudAck==false)     //no ack is yet received from the cloud for this event
            {
                unAckPowerOutFlag = true;
            }
        }

        bothAlarmBatteryActFlag = false;

        snoozeResetTempAlarmFlag = false;    //flag to ensure snooze does n't get reset during both temp and batt alarm are active

        if(keyPressedFlag==true)
        {
            if(batteryAlarmFlag==false)
            {
                 keyPressedFlag=false;emit silenceKeyPressFalse();
            }
        }



        if((alarmSetClearedFlag==true && receivedCloudAck==true && noUserLinked==false) || (unAckAlarmReadFileFlag==true && receivedCloudAck==true))   //all clear... green heart
        {
            this->setYellowWarning(false);
            this->setWarningAlarmMessageText("");
            this->setUnAckAlarmCleared(false);
            emit stopYellowFlash();

            alarmSetClearedFlag = false;
            powerOutageSetClrFlag = false;
            receivedCloudAck = false;
            unAckAlarmReadFileFlag = false;
            unAckLowTempFlag = false;
            unAckHighTempFlag = false;
            unAckPowerOutFlag = false;
            this->setTempAlarmIcon(false);

            if(yellowLedTimer->isActive()==true && batteryFlag==false && loggingFlag==false && this->noProbeDetected()==false)
            {//qDebug()<<"<----->"<<__LINE__;
                yellowLedTimer->stop();
            }




        }
        else if((alarmSetClearedFlag==true && receivedCloudAck==false && noUserLinked==false) || (unAckAlarmReadFileFlag==true && noUserLinked==false))       //only temp alarm cleared with out ack from cloud
        {
            if(yellowLedTimer->isActive()==false && apModeInProgress==false)
            {
                system("echo \"0\" > /sys/class/gpio/gpio22/value");  //Green led on OFF state
                yellowLedTimer->start(10);
            }

            this->setUnAckAlarmCleared(true);

            this->setYellowWarning(true);

            this->setTempAlarmIcon(true);

            this->setUserLinkWarning(false);

            if(unAckHighTempFlag==true && unAckLowTempFlag==false)
            {

                this->setWarningAlarmMessageText(tr("Warm Alarm cleared with out ACK from Cloud"));

                highTempTextDispFlag = false;
            }
            else if(unAckLowTempFlag==true && unAckHighTempFlag==false)
            {

                this->setWarningAlarmMessageText(tr("Cold Alarm cleared with out ACK from Cloud"));

                lowTempTextDispFlag = false;
            }
            else if(unAckHighTempFlag==true && unAckLowTempFlag==true )
            {

                this->setWarningAlarmMessageText(tr("Alarm cleared with out ACK from Cloud"));

                lowTempTextDispFlag = false;
                highTempTextDispFlag = false;
            }
            else
            {
                this->setWarningAlarmMessageText(tr("Alarm cleared with out ACK from Cloud"));

            }
            updateMode(WARNING);
            emit startYellowFlash();
        }
        else if((alarmSetClearedFlag==true && receivedCloudAck==false && noUserLinked==true) || (unAckAlarmReadFileFlag==true && noUserLinked==true))       //both temp alarm cleared with out ack from cloud and no user linked flash message
        {
            if(yellowLedTimer->isActive()==false && apModeInProgress==false)
            {
                system("echo \"0\" > /sys/class/gpio/gpio22/value");  //Green led on OFF state
                yellowLedTimer->start(10);
            }

            this->setUnAckAlarmCleared(true);

            this->setUserLinkWarning(true);

            this->setYellowWarning(true);

            this->setTempAlarmIcon(true);

            if(unAckHighTempFlag==true && unAckLowTempFlag==false)
            {

                this->setWarningAlarmMessageText(tr("Warm Alarm cleared with out ACK") + tr(" & User Not Linked"));

                highTempTextDispFlag = false;
            }
            else if(unAckLowTempFlag==true && unAckHighTempFlag==false)
            {

                this->setWarningAlarmMessageText(tr("Cold Alarm cleared with out ACK from Cloud")+ tr(" & User Not Linked"));

                lowTempTextDispFlag = false;
            }
            else if(unAckHighTempFlag==true && unAckLowTempFlag==true )
            {

                this->setWarningAlarmMessageText(tr("Alarm cleared with out ACK from Cloud")+ tr(" & User Not Linked"));

                lowTempTextDispFlag = false;
                highTempTextDispFlag = false;
            }
            else
            {
                this->setWarningAlarmMessageText(tr("Alarm cleared with out ACK from Cloud")+ tr(" & User Not Linked"));

            }
            updateMode(WARNING);
            emit startYellowFlash();

        }
        else if((alarmSetClearedFlag==true && receivedCloudAck==true && noUserLinked==true) || (unAckAlarmReadFileFlag==true && receivedCloudAck==true) || (alarmSetClearedFlag==false && noUserLinked==true))        //only user linking not done warning flash message
        {

            alarmSetClearedFlag = false;
            receivedCloudAck = false;
            unAckAlarmReadFileFlag = false;

            if(yellowLedTimer->isActive()==false && apModeInProgress==false)
            {
                system("echo \"0\" > /sys/class/gpio/gpio22/value");  //Green led on OFF state
                yellowLedTimer->start(10);
            }

            this->setUnAckAlarmCleared(false);

            this->setUserLinkWarning(true);

            this->setYellowWarning(true);

            this->setTempAlarmIcon(false);

            //write a code here to set this state into ini file for retaining state on boot

            this->setWarningAlarmMessageText(tr("Device user linking incomplete"));

            emit startYellowFlash();
        }
        else if(alarmSetClearedFlag==false && noUserLinked==false)      //no warning messages to flash
        {
            this->setYellowWarning(false);
            this->setWarningAlarmMessageText("");
            this->setUnAckAlarmCleared(false);
            emit stopYellowFlash();

            this->setUserLinkWarning(false);

            if(yellowLedTimer->isActive()==true && batteryFlag==false && loggingFlag==false && this->noProbeDetected()==false)
            {
                yellowLedTimer->stop();
            }

        }


        if(workerThread->isRunning()==true)
        {
            if(batteryAlarmFlag==false)
            {
                 this->setAlarmMessage("");
                 workerThread->stop();
            }
        }


        if(batteryAlarmFlag==false)
        {
             this->setTempAlarm(false);
             this->setBatteryAlarmIcon(false);

             emit stopRedFlash();
        }

        if(this->isSnooze()==true && batteryAlarmFlag==false)
        {
            this->setSnoozeStatus(false);
            this->setSnoozeTimeCounter(0);
        }


        if(batteryFlag==false && loggingFlag==false && batteryAlarmFlag==false && this->unAckAlarmCleared()==false && this->userLinkWarning()==false)
        {
            system("echo \"1\" > /sys/class/gpio/gpio22/value");  //Alarm mode Green led on ON state
            system("echo \"0\" > /sys/class/gpio/gpio26/value");    //yellow led on OFF state
            this->setTempAlarmIcon(false);  //this is needed to handle case where recovery failed to be sent to DL, during which alarmsetcleared will never set to true and so even after recovery we see green led with temp icon.,
            updateMode(NORMAL);
        }
        else if(batteryFlag==false && loggingFlag==false && batteryAlarmFlag==false && this->unAckAlarmCleared()==false && this->userLinkWarning()==true)
        {

            //dont change the led to green yet... only update mode to cloud since no link warning is active
            updateMode(NORMAL);

        }
        else if((batteryFlag==true) && (loggingFlag==false))
        {
            updateMode(BATTERY);
        }


    }

    emit valueChanged();

}


/**
 * @brief DeltaKController::runBuzzer
 */
void DeltaKController::runBuzzer()
{
    buzzerTimer->stop();

    if(workerThread->isRunning()==false)
    {
        workerThread->start();
    }

}


/**
 * @brief DeltaKController::yellowLedFlash
 */
void DeltaKController::yellowLedFlash()
{

    if(workerThread->isRunning()==false)
    {//qDebug()<<__LINE__;
        system("echo \"0\" > /sys/class/gpio/gpio22/value");  //Alarm mode Green led on off state

        while(yellowLedTimerCnt<3)
        {

            system("echo \"1\" > /sys/class/gpio/gpio26/value");    //YELLOW LED ON
            delay(1);
            system("echo \"0\" > /sys/class/gpio/gpio26/value");
            delay(1);

            yellowLedTimerCnt++;

            if(yellowLedTimerCnt==3)
            {
                yellowLedTimerCnt=0;
                break;
            }

            //added this code to have mutual exclusion between green led and red led.. need to test
            if(normalModeActive==true)
            {
                normalModeActive=false;
                system("echo \"0\" > /sys/class/gpio/gpio26/value");
                emit valueChanged();
                break;
            }
        }

        yellowLedTimer->setInterval(60000);

    }
    else
    {

    }

}


 /* @brief DeltaKController::resetSnoozeKeyPressed
 */
void DeltaKController::resetSnoozeKeyPressed()
{

    keyPressStatus = 1;

    if(!(keyPressOutput = popen("cat /sys/class/gpio/gpio5/value","r")))
    {
#ifdef  DEBUG__ON
        qDebug()<<"Unable to get keypress gpio value";
#endif  //DEBUG_ON

    }

    else
    {

        fscanf(keyPressOutput,"%d",&keyPressStatus);

        //qDebug()<< " KeyPress Status is "<<abs(keyPressStatus);

        if(keyPressStatus==0)   //key already pressed... no need to monitor this key any longer till new alarm
        {
            keyPressedFlag=true;
            emit silenceKeyPressTrue();
            resetKeyTimer->stop();
        }
        else    //key not pressed... no action lets continue to monitor and keep running this timer
        {

        }


        pclose(keyPressOutput);
    }


}


/**
 * @brief DeltaKController::temperature
 * @return
 */
QString DeltaKController::temperature()
{
    return m_temperature;
}


/**
 * @brief DeltaKController::setTemperature
 * @param temperature
 */
void DeltaKController::setTemperature(const QString &temperature)
{
    m_temperature = temperature;
}


/**
 * @brief DeltaKController::snoozeTimeMinSec
 * @return
 */
QString DeltaKController::snoozeTimeMinSec()
{
    return m_snoozeTimeMinSec;
}


/**
 * @brief DeltaKController::setSnoozeTimeMinSec
 * @param snoozeTimeMinSec
 */
void DeltaKController::setSnoozeTimeMinSec(const QString &snoozeTimeMinSec)
{
    m_snoozeTimeMinSec = snoozeTimeMinSec;
}

/**
 * @brief DeltaKController::failMessage
 * @return
 */
QString DeltaKController::failMessage()
{
    return m_failMessage;
}

/**
 * @brief DeltaKController::setFailMessage
 * @param failMessage
 */
void DeltaKController::setFailMessage(const QString &failMessage)
{
    m_failMessage = failMessage;
}

/**
 * @brief DeltaKController::wifiStatusText
 * @return
 */
QString DeltaKController::wifiStatusText()
{
    return m_wifiStatusText;
}

/**
 * @brief DeltaKController::setWifiStatusText
 * @param wifiStatusText
 */
void DeltaKController::setWifiStatusText(const QString &wifiStatusText)
{
    m_wifiStatusText = wifiStatusText;
}

/**
 * @brief DeltaKController::assetStatusText
 * @return
 */
QString DeltaKController::assetStatusText()
{
    return m_assetStatusText;
}

/**
 * @brief DeltaKController::setAssetStatusText
 * @param assetStatusText
 */
void DeltaKController::setAssetStatusText(const QString &assetStatusText)
{
    m_assetStatusText = assetStatusText;
}

/**
 * @brief DeltaKController::referenceTemperature
 * @return
 */
QString DeltaKController::referenceTemperature() const
{
    return m_referenceTemperature;
}

/**
 * @brief DeltaKController::setReferenceTemperature
 * @param referenceTemperature
 */
void DeltaKController::setReferenceTemperature(QString referenceTemperature)
{
    m_referenceTemperature = referenceTemperature;
}

/**
 * @brief DeltaKController::wifiWarning
 * @return
 */
bool DeltaKController::wifiWarning() const
{
    return m_wifiWarning;
}

/**
 * @brief DeltaKController::setWifiWarning
 * @param wifiWarning
 */
void DeltaKController::setWifiWarning(bool wifiWarning)
{
    m_wifiWarning = wifiWarning;
}

/**
 * @brief DeltaKController::ethernetWarning
 * @return
 */
bool DeltaKController::ethernetWarning() const
{
    return m_ethernetWarning;
}


/**
 * @brief DeltaKController::setEthernetWarning
 * @param ethernetWarning
 */
void DeltaKController::setEthernetWarning(bool ethernetWarning)
{
    m_ethernetWarning = ethernetWarning;
}

/**
 * @brief DeltaKController::powerWarning
 * @return
 */
bool DeltaKController::powerWarning() const
{
    return m_powerWarning;
}

/**
 * @brief DeltaKController::setPowerWarning
 * @param powerAlarm
 */
void DeltaKController::setPowerWarning(bool powerAlarm)
{
    m_powerWarning = powerAlarm;
}

/**
 * @brief DeltaKController::batteryWarning
 * @return
 */
bool DeltaKController::batteryWarning() const
{
    return m_batteryWarning;
}

/**
 * @brief DeltaKController::setBatteryWarning
 * @param batteryAlarm
 */
void DeltaKController::setBatteryWarning(bool batteryAlarm)
{
    m_batteryWarning = batteryAlarm;
}

/**
 * @brief DeltaKController::tempAlarm
 * @return
 */
bool DeltaKController::tempAlarm() const
{
    return m_tempAlarm;
}

/**
 * @brief DeltaKController::setTempAlarm
 * @param tempAlarm
 */
void DeltaKController::setTempAlarm(bool tempAlarm)
{
    m_tempAlarm = tempAlarm;
}

/**
 * @brief DeltaKController::chineseLang
 * @return
 */
bool DeltaKController::chineseLang() const
{
    return m_chineseLang;
}

/**
 * @brief DeltaKController::setChineseLang
 * @param chineseLang
 */
void DeltaKController::setChineseLang(bool chineseLang)
{
    m_chineseLang = chineseLang;
}

/**
 * @brief DeltaKController::snoozeTime
 * @return
 */
int DeltaKController::snoozeTime() const
{
    return m_snoozeTime;
}

/**
 * @brief DeltaKController::setSnoozeTime
 * @param snooze
 */
void DeltaKController::setSnoozeTime(int snooze)
{
    m_snoozeTime = snooze;
}

/**
 * @brief DeltaKController::isSnooze
 * @return
 */
bool DeltaKController::isSnooze() const
{
    return m_isSnooze;
}

/**
 * @brief DeltaKController::setIsSnooze
 * @param isSnooze
 */
void DeltaKController::setIsSnooze(bool isSnooze)
{    
    m_isSnooze = isSnooze;
}

/**
 * @brief DeltaKController::apName
 * @return
 */
QString DeltaKController::apName() const
{
    return m_apName;
}

/**
 * @brief DeltaKController::setApName
 * @param apName
 */
void DeltaKController::setApName(const QString &apName)
{
    m_apName = apName;
}

/**
 * @brief DeltaKController::apIP
 * @return
 */
QString DeltaKController::apIP() const
{
    return m_apIP;
}

/**
 * @brief DeltaKController::setApIP
 * @param apIP
 */
void DeltaKController::setApIP(const QString &apIP)
{
    m_apIP = apIP;
}

/**
 * @brief DeltaKController::deviceName
 * @return
 */
QString DeltaKController::deviceName() const
{
    return m_deviceName;
}

/**
 * @brief DeltaKController::setDeviceName
 * @param deviceName
 */
void DeltaKController::setDeviceName(const QString &deviceName)
{
    m_deviceName = deviceName;
}

/**
 * @brief DeltaKController::popUpString
 * @return
 */
QString DeltaKController::popUpString() const
{
    return m_popUpString;
}

/**
 * @brief DeltaKController::setPopUpString
 * @param popUpString
 */
void DeltaKController::setPopUpString(const QString &popUpString)
{
    m_popUpString = popUpString;
}

/**
 * @brief DeltaKController::updateModBoxStatus
 */
void DeltaKController::updateModBoxStatus()
{

    updModBoxStatusInProg = true;

    readUTCTimenow();

    setPointFile.setFileName("/home/pi/Desktop/EdgeFirmware/metadata/setpoint.txt");
    if(!setPointFile.open(QIODevice::ReadOnly)) {
        qDebug()<<"Unable to open setpoint.txt";
        setPointTemp = "-80";
    }

    else
    {
        QTextStream in(&setPointFile);

        while(!in.atEnd()) {
            setPointTemp = in.readLine();
        }

        setPointFile.close();
        qDebug()<<"SetPoint: "<<setPointTemp;
    }

    this->setReferenceTemperature(setPointTemp);

    highTempFile.setFileName("/home/pi/Desktop/EdgeFirmware/metadata/warmalarm.txt");
    if(!highTempFile.open(QIODevice::ReadOnly)) {
        qDebug()<<"Unable to open warmalarm.txt";
        highTemp = "-70";
    }
    else
    {
        QTextStream highTempTS(&highTempFile);
           while(!highTempTS.atEnd()) {
            highTemp = highTempTS.readLine();
        }
        highTempFile.close();
        qDebug()<<"HighTemp: "<<highTemp;
    }



    lowTempFile.setFileName("/home/pi/Desktop/EdgeFirmware/metadata/coldalarm.txt");
    if(!lowTempFile.open(QIODevice::ReadOnly)) {
        qDebug()<<"Unable to open coldalarm.txt";
        lowTemp = "-90";
    }

    else
    {
        QTextStream lowTempTS(&lowTempFile);
           while(!lowTempTS.atEnd()) {
            lowTemp = lowTempTS.readLine();
        }

        lowTempFile.close();
        //qDebug()<<"LowTemp: "<<lowTemp;
    }

    //read asset type
    astTypeFile.setFileName("/home/pi/Desktop/EdgeFirmware/metadata/assettype.txt");
    if(!astTypeFile.open(QIODevice::ReadOnly)) {
        //qDebug()<<"Unable to open assettype.txt";
        assetType = "Freezer";
    }

    else
    {
        QTextStream astTypeIn(&astTypeFile);

        while(!astTypeIn.atEnd()) {
            assetType = astTypeIn.readLine();
        }

        astTypeFile.close();

        if(assetType.indexOf("Freezer")==0)
        {
            assetType = "Freezer";

            if(this->language()=="Chinese")
            {
                assetType = "冰柜";
            }
        }
        if(assetType.indexOf("ULT")==0)
        {
            assetType = "ULT";

            if(this->language()=="Chinese")
            {
                assetType = "ULT";
            }
        }
        else if(assetType.indexOf("Refrigerator")==0)
        {
            assetType = "Refrigerator";
            if(this->language()=="Chinese")
            {
                assetType = "冰箱";
            }
        }
        else if(assetType.indexOf("Others")==0)
        {
            assetType = "Others";
            if(this->language()=="Chinese")
            {
                assetType = "其他";
            }
        }
    }

    //read asset model
    astModelFile.setFileName("/home/pi/Desktop/EdgeFirmware/metadata/assetmodel.txt");
    if(!astModelFile.open(QIODevice::ReadOnly)) {
        qDebug()<<"Unable to open assetmodel.txt";
        assetModel = "Freezer 2.0";
    }

    else
    {
        QTextStream astModelIn(&astModelFile);

        while(!astModelIn.atEnd()) {
            assetModel = astModelIn.readLine();
        }

        astModelFile.close();
    }

    //read asset serial  number
    astSnoFile.setFileName("/home/pi/Desktop/EdgeFirmware/metadata/assetserial.txt");
    if(!astSnoFile.open(QIODevice::ReadOnly)) {
        qDebug()<<"Unable to open assetserial.txt";
        assetSerial = "123456789";
    }

    else
    {
        QTextStream astSnoIn(&astSnoFile);

        while(!astSnoIn.atEnd()) {
            assetSerial = astSnoIn.readLine();
        }

        astSnoFile.close();
    }

    //read asset city
    astCityFile.setFileName("/home/pi/Desktop/EdgeFirmware/metadata/assetcity.txt");
    if(!astCityFile.open(QIODevice::ReadOnly)) {
        qDebug()<<"Unable to open assetcity.txt";
        assetCity = "Carlsbad";
    }

    else
    {
        QTextStream assetCityIn(&astCityFile);

        while(!assetCityIn.atEnd()) {
            assetCity = assetCityIn.readLine();
        }

        astCityFile.close();

        if(this->language()=="Chinese")
        {
            if(assetCity.indexOf("China")==0)
            {
                assetCity = "中国";
            }
            else
            {
                assetCity = "在中国以外";
            }


        }
    }


    //read asset location

    astLocFile.setFileName("/home/pi/Desktop/EdgeFirmware/metadata/assetlocation.txt");
    if(!astLocFile.open(QIODevice::ReadOnly)) {
        qDebug()<<"Unable to open assetlocation.txt";
        assetLocation = "US";
    }

    else
    {
        QTextStream assetLocIn(&astLocFile);

        while(!assetLocIn.atEnd()) {
            assetLocation = assetLocIn.readLine();
        }

        astLocFile.close();
    }

    //read sensor name

    sensorNameFile.setFileName("/home/pi/Desktop/EdgeFirmware/metadata/sensorname.txt");
    if(!sensorNameFile.open(QIODevice::ReadOnly)) {
        qDebug()<<"Unable to open sensorname.txt";
        sensorName = "Sensor 1";
    }

    else
    {
        QTextStream sensorNameIn(&sensorNameFile);

        while(!sensorNameIn.atEnd()) {
            sensorName = sensorNameIn.readLine();
        }

        sensorNameFile.close();
    }



    //read friendlyName

    friendlyNameFile.setFileName("/home/pi/Desktop/EdgeFirmware/miscfiles/friendlyname.txt");
    if(!friendlyNameFile.open(QIODevice::ReadOnly)) {
        qDebug()<<"Unable to open friendlyname.txt";
        friendlyName = "Test";
    }

    else
    {
        QTextStream friendlyNameIn(&friendlyNameFile);

        while(!friendlyNameIn.atEnd()) {
            friendlyName = friendlyNameIn.readLine();
        }

        friendlyNameFile.close();
    }

    memset(friendlyNameChar,'\0',sizeof(friendlyNameChar));
    sprintf(friendlyNameChar,"%s",friendlyName.toLocal8Bit().data());



    metadata_file.setFile("/home/pi/Desktop/EdgeFirmware/metadata/setpoint.txt");

    if (metadata_file.exists() && metadata_file.isFile())   //file exist
    {
        if(cloudPushFail==false)
        {

            if(assetType=="Freezer" || assetType=="ULT" || assetType=="Refrigerator" || assetType=="Others" || assetType=="冰柜" || assetType=="冰箱" || assetType=="其他")
            {
                qDebug()<<"<----->Valid Asset Type Obtained";
                if(assetCity=="China" || assetCity=="Outside China" || assetCity=="中国" || assetCity=="在中国以外")
                {
                    qDebug()<<"<----->Valid Asset City Obtained";
                    lowTempFloat = lowTemp.toFloat();
                    highTempFloat = highTemp.toFloat();

                    if(highTempFloat < lowTempFloat)    //invalid set points
                    {
                        qDebug()<<"<----->InValid SetPoints Obtained... Not sending metadata to cloud";
                    }
                    else
                    {

                        QtConcurrent::run(this,&DeltaKController::updateMetaData);
                    }


                }
                else
                {
                    qDebug()<<"<----->InValid Asset City Obtained... Not sending metadata to cloud";
                }
            }
            else
            {
                qDebug()<<"<----->InValid Asset Type Obtained... Not sending metadata to cloud";
            }

        }
    }

    //updating UI
    emit valueChanged();

    updModBoxStatusInProg = false;

}


/**
 * @brief DeltaKController::setSnoozeStatus
 * @param snoozeStatus
 */
void DeltaKController::setSnoozeStatus(bool snoozeStatus)
{
    if(alarmFlag==true && batteryAlarmFlag==true)
    {
        bothAlarmBatteryActFlag = true;

    }


    if(snoozeStatus == true)
    {
        this->timeValue->setHMS(0,0,0);
        this->setSnoozeTimeMinSec(timeValue->toString("mm:ss").insert(2,'m')+"s");
        this->startCountDown();
        this->setTempAlarmIcon(false);
        keyPressedFlag = true;
        emit silenceKeyPressTrue();
    }

    this->setIsSnooze(snoozeStatus);

    emit valueChanged();

}

/**
 * @brief DeltaKController::callSendSnoozeEvent
 */
void DeltaKController::callSendSnoozeEvent()
{

    delay(10);      //to handle the situation where snooze for 10 min pressed on cloud and snooze locally is pushed simultaneously

    if(alarmFlag==true)
    {
        if(highTempTextDispFlag==true)
        {
            QtConcurrent::run(this,&DeltaKController::sendSnoozeEvent,HIGH_TEMP);//sendSnoozeEvent(HIGH_TEMP);
        }
        else if(lowTempTextDispFlag==true)
        {
            QtConcurrent::run(this,&DeltaKController::sendSnoozeEvent,LOW_TEMP);//sendSnoozeEvent(LOW_TEMP);
        }

    }
}

/**
 * @brief DeltaKController::setSettingsMouseClickStatus
 * @param enableStatus
 */
void DeltaKController::setSettingsMouseClickStatus(bool enableStatus)
{
    this->setEnableOTPButtonClick(enableStatus);
}

/**
 * @brief DeltaKController::initPasswordRequest
 */
void DeltaKController::initPasswordRequest()
{

}

/**
 * @brief DeltaKController::startCountDown
 */
void DeltaKController::startCountDown()
{
  #ifdef  DEBUG__ON
  qDebug() << "start Count down";
  #endif  //DEBUG__ON
  this->setSnoozeTimeCounter(this->snoozeTime());

  if(QString::number(this->snoozeTime())=="3600000")
  {
      //qDebug()<<"DEFAULT SNOOZE IS IDENTIFIED AS 60";

      this->timeValue->setHMS(0,DEFAULT_SNOOZE-1,DEFAULT_SNOOZE-1);

      this->setSnoozeTimeMinSec(timeValue->toString("mm:ss").insert(2,'m')+"s");

  }
  else if(QString::number(this->snoozeTime())!="3600000")
  {
      //qDebug()<<"SNOOZE IS IDENTIFIED AS NON 60 VALUE"<<this->snoozeTime();

      this->timeValue->setHMS(0,10,0);

      this->setSnoozeTimeMinSec(timeValue->toString("mm:ss").insert(2,'m')+"s");
  }

  countDownTimer->start(1000);

}

/**
 * @brief DeltaKController::snoozeTimeCounter
 * @return
 */
int DeltaKController::snoozeTimeCounter() const
{
    return m_snoozeTimeCounter;
}


/**
 * @brief DeltaKController::setSnoozeTimeCounter
 * @param snoozeTimeCounter
 */
void DeltaKController::setSnoozeTimeCounter(unsigned int snoozeTimeCounter)
{
    m_snoozeTimeCounter = snoozeTimeCounter;
}


/**
 * @brief DeltaKController::language
 * @return
 */
QString DeltaKController::language() const
{
    return m_language;
}


/**
 * @brief DeltaKController::setLanguage
 * @param language
 */
void DeltaKController::setLanguage(const QString &language)
{    
    m_language = language;
}

/**
 * @brief DeltaKController::pinNumber
 * @return
 */
QString DeltaKController::pinNumber() const
{
    return m_pinNumber;
}


/**
 * @brief DeltaKController::setPinNumber
 * @param pinNumber
 */
void DeltaKController::setPinNumber(const QString &pinNumber)
{    
    m_pinNumber = pinNumber;
}

/**
 * @brief DeltaKController::isOTP
 * @return
 */
bool DeltaKController::isOTP() const
{
    return m_isOTP;
}

/**
 * @brief DeltaKController::setIsOTP
 * @param isOTP
 */
void DeltaKController::setIsOTP(bool isOTP)
{
    m_isOTP = isOTP;
}

/**
 * @brief DeltaKController::isDeeplazer
 * @return
 */
bool DeltaKController::isDeeplazer() const
{
    return m_isDeeplazer;
}

/**
 * @brief DeltaKController::setIsDeeplazer
 * @param isDeeplazer
 */
void DeltaKController::setIsDeeplazer(bool isDeeplazer)
{
    m_isDeeplazer = isDeeplazer;
}

/**
 * @brief DeltaKController::OTP
 * @return
 */
QString DeltaKController::OTP() const
{
    return m_OTP;
}

/**
 * @brief DeltaKController::setOTP
 * @param OTP
 */
void DeltaKController::setOTP(const QString &OTP)
{
    m_OTP = OTP;
}

/**
 * @brief DeltaKController::isFWUpgrade
 * @return
 */
bool DeltaKController::isFWUpgrade() const
{
    return m_isFWUpgrade;
}

/**
 * @brief DeltaKController::setIsFWUpgrade
 * @param isFWUpgrade
 */
void DeltaKController::setIsFWUpgrade(bool isFWUpgrade)
{
    m_isFWUpgrade = isFWUpgrade;
}

/**
 * @brief DeltaKController::progressvalueChanged
 * @return
 */
int DeltaKController::progressvalueChanged() const
{
    return m_progressvalueChanged;
}

/**
 * @brief DeltaKController::setProgressvalueChanged
 * @param progressvalueChanged
 */
void DeltaKController::setProgressvalueChanged(int progressvalueChanged)
{
    m_progressvalueChanged = progressvalueChanged;
}

/**
 * @brief DeltaKController::qrImage
 * @return
 */
QString DeltaKController::qrImage() const
{
    return m_qrImage;
}

/**
 * @brief DeltaKController::setQrImage
 * @param qrImage
 */
void DeltaKController::setQrImage(const QString &qrImage)
{
    m_qrImage = qrImage;
}

/**
 * @brief DeltaKController::alarmMessage
 * @return
 */
QString DeltaKController::alarmMessage() const
{
    return m_alarmMessage;
}

/**
 * @brief DeltaKController::firmwareUpgradeStarted
 */
void DeltaKController::firmwareUpgradeStarted()
{
    system("sudo /home/pi/Desktop/FwUpdScript/patch/mountflash.sh");
}

/**
 * @brief DeltaKController::setAlarmMessage
 * @param alarmMessage
 */
void DeltaKController::setAlarmMessage(const QString &alarmMessage)
{
    m_alarmMessage = alarmMessage;
}

/**
 * @brief DeltaKController::userLinkingStatusMsg
 * @return
 */
QString DeltaKController::userLinkingStatusMsg() const
{
    return m_userLinkingStatusMsg;
}

/**
 * @brief DeltaKController::setUserLinkingStatusMsg
 * @param userLinkingStatusMsg
 */
void DeltaKController::setUserLinkingStatusMsg(const QString &userLinkingStatusMsg)
{
    m_userLinkingStatusMsg = userLinkingStatusMsg;
    emit valueChanged();
}

/**
 * @brief DeltaKController::cloudConnStatusMsg
 * @return
 */
QString DeltaKController::cloudConnStatusMsg() const
{
    return m_cloudConnStatusMsg;
}

/**
 * @brief DeltaKController::setCloudConnStatusMsg
 * @param cloudConnStatusMsg
 */
void DeltaKController::setCloudConnStatusMsg(const QString &cloudConnStatusMsg)
{
    m_cloudConnStatusMsg = cloudConnStatusMsg;
    emit valueChanged();
}


/**
 * @brief DeltaKController::timeOutSlotForCountDown
 */
void DeltaKController::timeOutSlotForCountDown()
{
    QtConcurrent::run(this, &DeltaKController::timeOutForCountDownConcurrent);
}

/**
 * @brief DeltaKController::timeOutForCountDownConcurrent
 */
void DeltaKController::timeOutForCountDownConcurrent()
{
#ifdef  DEBUG__ON
    qDebug() << "timeOutSlotForCountDown time:"<<this->snoozeTimeCounter();
#endif  //DEBUG__ON
    int time = this->snoozeTimeCounter();

    if(time>0)
    {
        time -= 1000; // decrement counter
        this->setSnoozeTimeCounter(time);

        this->timeValue->setHMS(0,this->timeValue->addSecs(-1).minute(),this->timeValue->addSecs(-1).second());
        //qDebug()<<this->timeValue->toString();
        this->setSnoozeTimeMinSec(timeValue->toString("mm:ss").insert(2,'m')+"s");
        emit valueChanged();
    }

    if (time<=0) // countdown has finished
    {
        // timeout
        keyPressedFlag = false;
        emit silenceKeyPressFalse();
        emit stopCntDwnTimerSignal();   //stopping this via signal slot due to error running in concurrency
        //countDownTimer->stop();
        if(alarmFlag==true)
        {
            this->setTempAlarmIcon(true);
        }
        if(batteryAlarmFlag==true)
        {
            this->setBatteryAlarmIcon(true);
        }

        this->setSnoozeStatus(false);
        this->timeValue->setHMS(0,0,0);
        this->setSnoozeTimeMinSec(timeValue->toString("mm:ss").insert(2,'m')+"s");
        emit valueChanged();
    }

}

/**
 * @brief DeltaKController::stopCntDwnTimerSlot
 */
void DeltaKController::stopCntDwnTimerSlot()
{
    countDownTimer->stop();
}

/**
 * @brief DeltaKController::networkMonitor
 */
void DeltaKController::networkMonitor()
{

    readUTCTimenow();

    networkTimer->stop();


    if(!(output = popen("ifconfig  | grep -c inet\\ addr","r")))
    {
#ifdef  DEBUG__ON
        qDebug()<<"Unable to get network connectivity status";
#endif  //DEBUG_ON

    }

    else
    {

        fscanf(output,"%u",&noOfIPAddrCnt);

        //qDebug()<<"NO OF IP ADDRESS OBTAINED IS: " << QString::number(noOfIPAddrCnt);

        noOfIPAddrCnt = noOfIPAddrCnt-1;    //removing the loop back ip address count

        if(noOfIPAddrCnt==0)        //no ipaddress is available at any of the interfaces... network down
        {                                                                //this will return 0 when network connected and 512 when no network connection, and 256 when network is there with no internet connection
            //printf("There is no internet connection\n");

            if(apModeInProgress==false)
            {
                loggingFlag = true;

                this->setWifiWarning(true);
                this->setEthernetWarning(true);

                if(ipCntZeroFlag==false)    //handling case after wifi connected no internet access, lan is disconnected
                {
                    ipCntZeroFlag = true;
                    apStaTimer->start(1000);
                }

                if(yellowLedTimer->isActive()==false && apModeInProgress==false)
                {
                    system("echo \"0\" > /sys/class/gpio/gpio22/value");  //Green led on OFF state
                    yellowLedTimer->start(10);
                }

                if((batteryFlag==false) && alarmFlag==false && watchdogFlag!=false)
                {
//                    updateMode(LOGGING);  //no update of logging mode now.., so commenting now
                }
                else if((batteryFlag==true) && alarmFlag==false)
                {
                    updateMode(BATTLOG);
                }
                else if((batteryFlag==false) && alarmFlag==true)
                {
                    updateMode(ALARMANDLOG);
                }
                else if((batteryFlag==true) && alarmFlag==true)
                {
                    updateMode(ALARMBATTLOG);
                }

            }

        }
        else if(noOfIPAddrCnt>=1 && internetStatus==true)   //more than one ip address is associated with oneof the interfaces... network is up
        {
            //printf("There is internet connection\n");

            ipCntZeroFlag = false;
            nwModuleRestartFlag = false;

            internetNotWorkingCnt = 0;

            if(dlRestartTimer->isActive()==false)
            {
                dlRestartTimer->start(DL_MQTT_ERROR_CHECK_TIME);
            }

            if(yellowLedTimer->isActive()==true && batteryFlag==false && this->unAckAlarmCleared()==false && this->noProbeDetected()==false)
            {
                yellowLedTimer->stop();
            }

            if(noOfIPAddrCnt>1)     //both ethernet and wifi are present
            {
                //qDebug()<<"BOTH ETHERNET AND WIFI PRESENT";
                this->setEthernetWarning(false);
                this->setWifiWarning(false);

            }
            else if(noOfIPAddrCnt==1)   //either wifi or ethernet is connected
            {
                if(abs(wifiSignalStrength)==0 && nwCnt>1)  //wifi is not connected... only etherenet is present
                {
                    //qDebug()<<"ONLY ETHERNET PRESENT";
                    this->setWifiWarning(true);   //no setting this to true since to keep the health status to green
                    this->setEthernetWarning(false);
                }
                else if(abs(wifiSignalStrength)>0)  //wifi is connected.. no ethernet
                {
                    //qDebug()<<"ONLY WIFI PRESENT";
                    this->setWifiWarning(false);
                    this->setEthernetWarning(true);

                }
            }

            emit valueChanged();

            if(loggingFlag==true && wifiStatusInProgress==false)   //unit recovered from outage condition.., so resending logged data if any
            {
                QtConcurrent::run(this,&DeltaKController::startOutageSendSlot);
            }
            //loggingFlag = false;
            nwCnt++;


            if(watchdogFlag==false)     //doing some activities that should happen only once after app start
            {
                delay(15);

                system("sudo python /home/pi/Desktop/EdgeFirmware/tzupdate.py");

                if(system("which watchdog")!=0)     //watchdog not installed
                {
                    system("yes | sudo apt-get install watchdog &");
                }

                if(system("which fbi")!=0)     //fbi not installed - needed for firmwareupdate
                {
                    //system("sudo apt-get update &");
                    system("yes | sudo apt-get install fbi &");
                }



                if(strlen(fwVersionChar)>0)     //update firmware version to cloud
                {
                    QtConcurrent::run(this,&DeltaKController::sendFwVerToCloud);//sendFwVerToCloud();
                }


                watchdogFlag = true;

            }

            delay(15);

            if(cloudPushFail==true)
            {
                cloudPushFail = false;
                if(updModBoxStatusInProg==false)
                {
                   updateModBoxStatus();
                }
            }

        }
        else if(noOfIPAddrCnt>=1 && internetStatus==false)   //atleast one ip address is associated with one of the interfaces... but internet is not working
        {
            //printf("There is no internet connection\n");

            if(internetNotWorkingCnt<2147483647)
            {
                internetNotWorkingCnt++;
            }


            if(internetNotWorkingCnt>=2)    //checking for two iterations before updating status
            {
                if(wifiThrd->isRunning()==true) //network went down., don't monitor wifi signal strength
                {
                    wifiThrd->stop();
                }

                loggingFlag = true;
                heartBeatStatusFlag = false;

                if(dlRestartTimer->isActive()==true)
                {
                    dlRestartTimer->stop();
                }

//qDebug()<<"Internet Not Working Cnt: "<<QString::number(internetNotWorkingCnt);
                this->setWifiWarning(true);
                this->setEthernetWarning(true);

                if(apModeInProgress==false)     //flashing yellow led only when not in ap mode
                {
                    if(noOfIPAddrCnt>1 && internetNotWorkingCnt>6)     //both ethernet and wifi are present, but internet not working
                    {
                        wlan0Active = false;
                        wlan1Active=false;

                        wlan0Active = getWlan0Status();
                        wlan1Active = getWlan1Status();

                        
                        if((wlan0Active==true && wlan1Active==false))   //restarting network interfaces only when dongle not connected
                        {
                            qDebug()<<"BOTH ETHERNET AND WIFI PRESENT BUT NO INTERNET ACCESS";
                            if(nwModuleRestartFlag==false)
                            {
                                //system("sudo wpa_cli disconnect -i wlan0");
                                system("sudo service networking restart &");    //to address issue of switching fail from Wifi to LAN
                                nwModuleRestartFlag = true;
                                qDebug()<<"Restarted Network Interfaces";
                            }

                        }
                        else if((wlan0Active==false && wlan1Active==true))  //dongle is connected
                        {
                            /*
                             if(nwModuleRestartFlag==false)
                             {
                                 nwModuleRestartFlag = true;
                                 //system("sudo ifdown wlan1");
                                 system("sudo killall dongleon.sh");
                                 system("sudo killall dongleoff.sh");
                                 system("/home/pi/Desktop/EdgeFirmware/unknownusb.sh");
                                 wlan1ForceDown = true;
                                 emit forceWlan1Down();

                                 qDebug()<<"Turned Down WLAN1";
                                 //system("sudo wpa_cli disconnect -i wlan1");

                             }
                            */
                        }
                    }

                    if(yellowLedTimer->isActive()==false && apModeInProgress==false)
                    {//qDebug()<<__LINE__;
                        system("echo \"0\" > /sys/class/gpio/gpio22/value");  //Green led on OFF state
                        yellowLedTimer->start(10);
                    }

                    if((batteryFlag==false) && alarmFlag==false  && watchdogFlag!=false)
                    {
                        //updateMode(LOGGING);  //logging mode is not there now.., so commenting
                    }
                    else if((batteryFlag==true) && alarmFlag==false)
                    {
                        updateMode(BATTLOG);
                    }
                    else if((batteryFlag==false) && alarmFlag==true)
                    {//qDebug()<<__LINE__;
                        updateMode(ALARMANDLOG);
                    }
                    else if((batteryFlag==true) && alarmFlag==true)
                    {
                        updateMode(ALARMBATTLOG);
                    }

                }

            }

        }

        pclose(output);

    }


    emit valueChanged();
    networkTimer->start(6000);

}

/**
 * @brief DeltaKController::wifiSignalRead
 */
void DeltaKController::wifiSignalRead()
{

    if(apModeInProgress==false)
    {
        if(wifiThrd->isRunning()==false)
        {
            wifiThrd->start();
        }

    }
    else if(apModeInProgress==true)
    {
        if(wifiThrd->isRunning()==true)
        {
            wifiThrd->stop();
        }
    }
}

/**
 * @brief DeltaKController::readUTCTimenow
 */
void DeltaKController::readUTCTimenow()
{
    timeinUTC = QDateTime::currentDateTime().toTimeSpec(Qt::OffsetFromUTC).toString(Qt::ISODate);
}


/**
 * @brief DeltaKController::updateMode
 * @param mode
 * @return
 */
void DeltaKController::updateMode(int mode)
{
    if(currentMode==mode)   //since mode is same, no need for any updatemode
    {
        //Current Mode and received mode are same., no action to be taken
    }
    else    //current mode and received mode are different.,
    {
        #ifdef  DEBUG_ON
            qDebug()<<"Mode is Changed... Current Mode is "<<QString::number(currentMode)<<"Changed Mode is "<<QString::number(mode);
        #endif  //DEBUG_ON


        currentMode = mode;
        memset(updateModeBuffer,'\0',sizeof(updateModeBuffer));


        memset(idChar,'\0',sizeof(idChar));

        if(generateRandInProgress==false)
        {
            sprintf(idChar,"%s",generateRandNumber().toLocal8Bit().data());
        }
        else
        {
            strcpy(idChar,"1234");
        }


        if(mode==NORMAL)
        {
        #ifdef  DEBUG_ON
            qDebug()<<"Mode is Normal";
        #endif  //DEBUG_ON

            sprintf(updateModeBuffer,"{\"request\":\"updatedevicestatus\",\"status\":\"NORMAL\",\"id\":\"%s\"}",idChar);
            normalModeActive = true;    //added this code to have mutual exclusion between green led and red led.. need to test

        }
        else if(mode==LOGGING)
        {
        #ifdef  DEBUG_ON
            qDebug()<<"Mode is LOGGING";
        #endif  //DEBUG_ON

             //no need to send LOGGING Mode.. so commenting now
            //sprintf(updateModeBuffer,"{\"request\":\"updatedevicestatus\",\"status\":\"LOGGING\",\"id\":\"%s\"}",idChar);

        }
        else if((mode==ALARM) || (mode==ALARMANDBATT))
        {

        #ifdef  DEBUG_ON
            qDebug()<<"Mode is ALARM";
        #endif  //DEBUG_ON

            sprintf(updateModeBuffer,"{\"request\":\"updatedevicestatus\",\"status\":\"ALARM\",\"id\":\"%s\"}",idChar);

        }
        else if(mode==BATTERY)
        {

        #ifdef  DEBUG_ON
            qDebug()<<"Mode is BATTERY";
        #endif  //DEBUG_ON

            sprintf(updateModeBuffer,"{\"request\":\"updatedevicestatus\",\"status\":\"BATTERY\",\"id\":\"%s\"}",idChar);

        }
        else if(mode==UPDATING)
        {
            sprintf(updateModeBuffer,"{\"request\":\"updatedevicestatus\",\"status\":\"UPDATING\",\"id\":\"%s\"}",idChar);
        }
        else if((mode==ALARMANDBATT) || (mode==ALARMANDLOG) || (mode==BATTLOG) || (mode==ALARMBATTLOG) )
        {

        }
        else if(mode==WARNING)
        {
            sprintf(updateModeBuffer,"{\"request\":\"updatedevicestatus\",\"status\":\"WARNING\",\"id\":\"%s\"}",idChar);
        }


        if((mode!=LOGGING)&&(mode!=BATTERY)&&(mode!=ALARMANDBATT)&&(mode!=ALARMANDLOG)&&(mode!=BATTLOG)&&(mode!=ALARMBATTLOG) )
        {
            if(updDevStatusInProgress==false)
            {
                QtConcurrent::run(this,&DeltaKController::updateDeviceStatus); qDebug()<<"\n\nCalled UpdDevStat: "<<__LINE__;

            }
        }

    }

}



/**
 * @brief WriteMemoryCallback
 * @param contents
 * @param size
 * @param nmemb
 * @param userp
 * @return
 */
static size_t
WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    (void)contents;
    (void)userp;
    size_t realsize = size * nmemb;

#ifdef DEBUG_ON
    //printf("%s",(char*)contents);
#endif  //DEBUG_ON
    return realsize;
}



/**
 * @brief DeltaKController::startApMode
 */
void DeltaKController::startApMode()
{

    apModeInProgress = true;    //to ensure no alarms are popped up when device config is in progress

    if(wifiThrd->isRunning()==true)
    {
        wifiThrd->stop();
    }

   if(dlRestartTimer->isActive()==true)
    {
        dlRestartTimer->stop();
    }


    system("sudo killall dongleoff");
    system("sudo rm /var/lib/misc/udhcpd.leases");
    system("sudo /etc/init.d/hostapd stop &");
    system("sudo /etc/init.d/udhcpd stop &");
    system("sudo /etc/init.d/dnsmasq stop &");
    system("sudo /etc/init.d/dhcpcd stop &");

    devConfigFile.setFileName("/home/pi/Desktop/EdgeFirmware/miscfiles/deviceconfigcompleted.txt");

    qDebug()<<"Application Started";

    if(!devConfigFile.open(QIODevice::ReadOnly)) {
       qDebug()<<"Unable to open deviceconfigcompleted.txt file... so starting with languageSelection Screen";
       this->setAppStartLang(true);     //no file found... so let the app start with languageSelection screen
       emit valueChanged();
    }

    else        //deviceconfigcompleted.txt file exists
    {
       devConfigFile.close();
       this->setShowBackApMode(true);
    }


    system("echo \"0\" > /sys/class/gpio/gpio27/value");    //RED LED OFF
    system("echo \"0\" > /sys/class/gpio/gpio12/value");  //  BUZZER OFF


    if(workerThread->isRunning()==true)
    {
        workerThread->stop();
    }

    if(yellowLedTimer->isActive()==true)
    {
        yellowLedTimer->stop();
    }


    this->setBusy(true);
    this->setEnableOTPButton(false);

    this->setWifiUpdateSucces(false);   //hide wifi update status text
    this->setWifiStatusText(tr(""));

    this->setWifiIconSucces(false);     //hide wifisuccess icon
    this->setWifiIconFail(false);       //hide wifi red cross fail icon
    this->setAssetUpdateFail(false);    //hide wifi fail display label

    this->setAssetUpdateSucces(false);  //hide asset update status text
    this->setAssetStatusText(tr(""));

    this->setAssetIconSucces(false);    //hide asset success icon green tick
    this->setAssetIconFail(false);      //hide asset fail icon red cross
    this->setAssetUpdateFail(false);    //hide wifi/assset fail display label


    this->setApName(instrumentName);
    emit valueChanged();

    system("sudo /home/pi/Desktop/modbox-webserver/modbox-webserver &");

    apModeCheckTimer->start(45*1000);   //90 sec timer to verify unit is really having static ip of 192.168.50.1 on wlan0 interface

    delay(2);

    this->setBusy(false);

}

/**
 * @brief DeltaKController::readOTP
 */
void DeltaKController::readOTP()
{
    this->setBusy(true);
    this->setEnableOTPButton(false);
    this->setShowConfigProg(true);

    emit valueChanged();
}

/**
 * @brief DeltaKController::setBusy
 * @param busyFlag
 */
void DeltaKController::setBusy(bool busyFlag)
{
    m_busy = busyFlag;
    emit valueChanged();
}

/**
 * @brief DeltaKController::isSetBusy
 * @return
 */
bool DeltaKController::isSetBusy() const
{
    return m_busy;
}

/**
 * @brief DeltaKController::showPopUp
 * @param popUpFlag
 */
void DeltaKController::showPopUp(bool popUpFlag)
{
    m_popUp = popUpFlag;
    emit valueChanged();
}

/**
 * @brief DeltaKController::isShowPopUp
 * @return
 */
bool DeltaKController::isShowPopUp() const
{
    return m_popUp;
}


/**
 * @brief DeltaKController::readQRImage
 * @return
 */
int DeltaKController::readQRImage()
{
    this->setBusy(true);    //to show the busy wheel

    system("echo \"0\" > /sys/class/gpio/gpio27/value");    //RED LED OFF
    system("echo \"0\" > /sys/class/gpio/gpio12/value");  //  BUZZER OFF


    //checking for 1-1 heartbeat
    QFuture<QString> htBtIdSent;
    int heartBeatLoopCnt=0;
    int sendHBretry = SENDHBRETRY;
    bool sent = true;
    bool obtainedLock = false;
    bool trylock = false;
    srand(time(NULL));
    int hbfalsedur = (rand() % 33) + 3;
    bool heartBtStatFlgQR = false;

    if(loggingFlag==false)  //check for heartbeat only if internet connection available
    {
        do
        {
            heartBeatLoopCnt=0;
            do
            {
                if(trylock==false)
                {
                    do {
                        trylock = heartBeatLock.tryLock(3);
                    } while ((trylock == false) && (--hbfalsedur>0));
                    if (trylock == true)
                    {
                        QCoreApplication::processEvents(QEventLoop::AllEvents);
                        heartBeatStatusFlag = false;
                        sent = false;
                        obtainedLock = true;
                        qDebug()<<"Inside readQRImage: "<<hbfalsedur;
                        heartBeatLoopCnt = 0;
                        sendHBretry = SENDHBRETRY;
                    }
                    else
                    {
                        srand(time(NULL));
                        hbfalsedur = (rand() % 33) + 3;
                    }
                }
                if(obtainedLock==false)
                {
                    qDebug()<<"Waiting for Lock in readQRImage "<< QString::number(SENDHBRETRY-sendHBretry)<< " " << QString::number(heartBeatLoopCnt);
                }
                if(!sent)
                {
                    sent = true;
                    htBtIdSent = QtConcurrent::run(this,&DeltaKController::sendHeartBeatCommand);
                    htBtVect.append(htBtIdSent);
                }
                QCoreApplication::processEvents(QEventLoop::AllEvents);

                QThread::msleep(500);

                if(obtainedLock == true)
                {
                    if (htBtVect.contains(htBtIdRecd) && (htBtIdSent == htBtIdRecd))
                    {
                        heartBtStatFlgQR = heartBeatStatusFlag;
                        if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                        {
                            htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                        }
                    }
                }

            }while((++heartBeatLoopCnt<10) && heartBtStatFlgQR==false);

            if (obtainedLock)
            {
                if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                {
                    htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                }
                sent = false;
            }

        }while((--sendHBretry > 0) && (heartBtStatFlgQR==false));


         if(obtainedLock==true)
         {
             qDebug()<<"Breaked HtBt Loop In readQRImage with OuterCnt: "<<QString::number(SENDHBRETRY- sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgQR << ":" <<htBtIdSent <<":"<<htBtIdRecd << ": HtBt Vector Count: "<<htBtVect.count();
             heartBeatLock.unlock();
         }
         else if(obtainedLock==false)
         {
            qDebug()<<"Timed Out! Breaked HtBt Loop In readQRImage with OuterCnt: "<<QString::number(SENDHBRETRY- sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgQR;
         }


    }
    else
    {
        qDebug()<<"Network is down., Not checking for heartbeat inside readQRImage";
    }

    check_devconfigfile.setFile("/home/pi/Desktop/EdgeFirmware/miscfiles/deviceconfigcompleted.txt");

    // check if file exists and if yes: Is it really a file and no directory?
    if (!check_devconfigfile.exists() && !check_devconfigfile.isFile())   //file does n't exist., first time configuration.,
    {
        loggingFlag = !internetStatus;   //since nwmonitor is not yet started, getting internet status
    }
    else    //file exists., no need to add delay for ensuring deep laser registration getting done
    {

    }

    qDebug()<< "heartBeatStatusFlag: "<<heartBtStatFlgQR<<"heartBeatLoopCnt: "<<"loggingFlag: "<<loggingFlag;

    if(heartBtStatFlgQR==true && loggingFlag==false)
    {

        memset(idChar,'\0',sizeof(idChar));

        if(generateRandInProgress==false)
        {
            sprintf(idChar,"%s",generateRandNumber().toLocal8Bit().data());
        }
        else
        {
            strcpy(idChar,"12345");
        }

        memset(getQRImageBuffer,'\0',sizeof(getQRImageBuffer));

        sprintf(getQRImageBuffer,"{\"id\":\"%s\",\"request\":\"getlinkqrcode\",\"fileLocation\":\"/home/pi/Desktop/QR\"}",idChar);

        headers = curl_slist_append(headers, "Accept: application/json");
        headers = curl_slist_append(headers, "Content-Type: application/json");
        headers = curl_slist_append(headers, "charsets: utf-8");


        if(curl)
        {
            curl_easy_setopt(curl, CURLOPT_URL, url);
            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
            readUTCTimenow();

            #ifdef  DEBUG_ON
            qDebug()<<"String to Send to DL for RequestQRCode is "<<getQRImageBuffer;
            #endif  //DEBUG_ON

            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, getQRImageBuffer);

        }

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

        curl_easy_setopt(curl, CURLOPT_HEADER,0L);


        retryCntGetQRCode=0;
        do
        {
            QCoreApplication::processEvents(QEventLoop::AllEvents);

            res = curl_easy_perform(curl);

            http_code = 0;
            curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);

            retryCntGetQRCode++;
            delay(2);

        }while(http_code!=200 && retryCntGetQRCode<5);

        if(http_code==200)  //command posted to deep laser successfully
        {
            delayQRReq(60);     //waiting here only when network is on and heart beat is true
        }
        else
        {
            qDebug()<<"Exceeded max attempts to send getlinkqrcode command to dl";
        }

        #ifdef  DEBUG_ON
        qDebug()<<"Get QR Code Output is "<<QString::number(res) <<"Attempt No: "<<QString::number(retryCntGetQRCode);
        qDebug()<<"Get QR Code Response: "<<QString::number(http_code);
        #endif  //DEBUG_ON

    }
    else
    {
        this->setReadQRStatus(false);
        this->setUserLinkingStatusMsg(tr("User Linking Failed !!!"));
        qrCounter=0;

    }

    this->setBusy(false);

    return 0;
}


/**
 * @brief DeltaKController::exitApMode
 */
void DeltaKController::exitApMode()
{
    qDebug()<<"Inside exitApMode";
    apModeInProgress = false;
    if(apModeCheckTimer->isActive()==true)
    {
        apModeCheckTimer->stop();
    }

    if(dlRestartTimer->isActive()==false)
    {
        dlRestartTimer->start(DL_MQTT_ERROR_CHECK_TIME);   //starting timer for 5 min so that if no data from DL endpoint, DL has to restart
    }

    this->setBusy(true);
    system("sudo killall modbox-webserver");
    system("sudo killall ap");
    system("sudo /etc/init.d/hostapd stop");
    system("sudo /etc/init.d/udhcpd stop");
    system("sudo /etc/init.d/dnsmasq stop");


    if(getWlan1Status()==false) //no dongle connected.,so need to backon wifi with onchip wifi only
    {
        system("sudo sta &");
    }
    else
    {
        apStaTimer->start(100);
    }
    delay(20);

    this->setBusy(false);

}

/**
 * @brief DeltaKController::getWlan0Status
 * @return
 */
bool DeltaKController::getWlan0Status()
{
    //check if interface active is wlan0
    if(!(wifiSignalOutput = popen("sudo ifconfig |grep wlan0","r")))
    {
        qDebug()<<"Unable to interface details on wlan0";
    }
    else
    {
        memset(bufGen,'\0',sizeof(bufGen));
        fscanf(wifiSignalOutput,"%s",bufGen);
        pclose(wifiSignalOutput);

        if(strlen(bufGen)>0)     //interface available is wlan0
        {
            return true;
        }
        else
        {

        }
    }
    return false;
}

/**
 * @brief DeltaKController::getWlan1Status
 * @return
 */
bool DeltaKController::getWlan1Status()
{
    //check if interface active is wlan1
    if(!(wifiSignalOutput = popen("sudo ifconfig |grep wlan1","r")))
    {
        qDebug()<<"Unable to interface details on wlan1";
    }
    else
    {
        memset(bufGen,'\0',sizeof(bufGen));
        fscanf(wifiSignalOutput,"%s",bufGen);
        pclose(wifiSignalOutput);

        if(strlen(bufGen)>0)     //interface available is wlan1
        {
            return true;
        }
        else
        {

        }
    }

    return false;
}

/**
 * @brief DeltaKController::changeModeSta
 */
void DeltaKController::changeModeSta()
{
    apStaTimer->stop();

    wlan0Active = false;
    wlan1Active = false;

    //check the current active interface
    //check if interface active is wlan0

    wlan0Active = getWlan0Status();

    wlan1Active = getWlan1Status();

    if(wlan1ForceDown==true)
    {
        wlan1ForceDown = false;
        emit forceWlan1Up();    //to change state in wifi thread to start monitor case of both wlan0 and wlan1 are down
        if(system("pgrep -x \"dongleon.sh\" > /dev/null")!=0) //0-process running; non zero-process not running
        {
            qDebug()<<"*****Dongle On Fired Here*****"<<__LINE__;
            system("/home/pi/Desktop/EdgeFirmware/dongleon.sh &");
        }

    }
    else if(wlan0Active==true && wlan1Active==true)  //both wlan0 and wlan1 are active., so need to have only dongle
    {
        if(system("pgrep -x \"dongleon.sh\" > /dev/null")!=0) //0-process running; non zero-process not running
        {
            qDebug()<<"*****Dongle On Fired Here*****"<<__LINE__;
            system("/home/pi/Desktop/EdgeFirmware/dongleonapp.sh &");
        }

    }
    else if(wlan0Active==true && wlan1Active==false)    //only wlan0 active
    {
        if(system("pgrep -x \"dongleoff.sh\" > /dev/null")!=0) //0-process running; non zero-process not running
        {
            qDebug()<<"*****Dongle Off Fired Here*****"<<__LINE__;
            system("/home/pi/Desktop/EdgeFirmware/dongleoff.sh &");
        }

    }
    else if(wlan0Active==false && wlan1Active==true)        //only wlan1 active
    {
        if(system("pgrep -x \"dongleon.sh\" > /dev/null")!=0) //0-process running; non zero-process not running
        {
            qDebug()<<"*****Dongle On Fired Here*****"<<__LINE__;
            system("/home/pi/Desktop/EdgeFirmware/dongleon.sh &");
        }

    }
    else if(wlan0Active==false && wlan1Active==false)   //no interfaces active
    {
        if(system("pgrep -x \"dongleoff.sh\" > /dev/null")!=0) //0-process running; non zero-process not running
        {
                qDebug()<<"*****Dongle Off Fired Here*****"<<__LINE__;
            system("/home/pi/Desktop/EdgeFirmware/dongleoff.sh &");
        }
    }


}

/**
 * @brief DeltaKController::startDLForDevReg
 */
void DeltaKController::startDLForDevReg()
{
    emit apModeAutoExitTimerStop(); //since user explictly pressed done, stop the auto exit timer

    system("echo \"0\" > /sys/class/gpio/gpio27/value");    //RED LED OFF
    system("echo \"0\" > /sys/class/gpio/gpio12/value");  //  BUZZER OFF

    this->setBusy(true);
    this->setEnableOTPButtonClick(true);
    QThread::msleep(50);
    QCoreApplication::processEvents();

    delay(20);  //to ensure next ap mode is done smoothly

    apModeInProgress = false;

    if(updModBoxStatusInProg==false)
    {
       updateModBoxStatus();
    }

    this->setBusy(false);

}

/**
 * @brief DeltaKController::generateRandNumber
 * @return
 */
QString DeltaKController::generateRandNumber()
{
    generateRandInProgress = true;

    static uint msgId = 0;

    if (msgId == 0) {
     ++msgId;
    }
    else if(msgId>2147483647)
    {
        msgId = 1;
    }

    generateRandInProgress = false;
    return QString::number(++msgId);
}

/**
 * @brief DeltaKController::updateMetaData
 * @return
 */
void DeltaKController::updateMetaData()
{

    readUTCTimenow();

    //checking for 1-1 heartbeat
    QFuture<QString> htBtIdSent;
    int heartBeatLoopCnt=0;
    int sendHBretry = SENDHBRETRY;
    bool sent = true;
    bool obtainedLock = false;
    bool trylock = false;
    srand(time(NULL));
    int hbfalsedur = (rand() % 33) + 3;
    bool heartBtStatFlgUpdMtDt = false;

    if(loggingFlag==false)  //check for heartbeat only if network is available
    {
        do
        {
            heartBeatLoopCnt=0;
            do
            {
                if(trylock==false)
                {
                    do {
                        trylock = heartBeatLock.tryLock(3);
                    } while ((trylock == false) && (--hbfalsedur>0));
                    if (trylock == true)
                    {

                        QCoreApplication::processEvents(QEventLoop::AllEvents);
                        heartBeatStatusFlag = false;
                        sent = false;
                        obtainedLock = true;
                        qDebug()<<"Inside updateMetaData: "<<hbfalsedur;
                        heartBeatLoopCnt = 0;
                        sendHBretry = SENDHBRETRY;
                    }
                    else
                    {
                        srand(time(NULL));
                        hbfalsedur = (rand() % 33) + 3;
                    }
                }
                if(obtainedLock==false)
                {
                    qDebug()<<"Waiting for Lock in updateMetaData "<< QString::number(SENDHBRETRY-sendHBretry)<< " " << QString::number(heartBeatLoopCnt);
                }
                if(!sent)
                {
                    sent = true;
                    htBtIdSent = QtConcurrent::run(this,&DeltaKController::sendHeartBeatCommand);
                    htBtVect.append(htBtIdSent);
                }
                QCoreApplication::processEvents(QEventLoop::AllEvents);

                QThread::msleep(500);

                if(obtainedLock == true)
                {
                    if (htBtVect.contains(htBtIdRecd) && (htBtIdSent == htBtIdRecd))
                    {
                        heartBtStatFlgUpdMtDt = heartBeatStatusFlag;
                        if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                        {
                            htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                        }
                    }
                }

            }while((++heartBeatLoopCnt<10) && heartBtStatFlgUpdMtDt==false);

            if (obtainedLock)
            {
                if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                {
                    htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                }
                sent = false;
            }

        }while((--sendHBretry > 0) && (heartBtStatFlgUpdMtDt==false));


        if(obtainedLock==true)
        {
            qDebug()<<"Breaked HtBt Loop In updateMetaData with OuterCnt: "<<QString::number(SENDHBRETRY- sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":" <<heartBtStatFlgUpdMtDt << ":" <<htBtIdSent <<":"<<htBtIdRecd << "HtBt Vector Count: "<<htBtVect.count();
            heartBeatLock.unlock();
        }
        else if(obtainedLock==false)
        {
            qDebug()<<"Timed Out! Breaked HtBt Loop In updateMetaData with OuterCnt: "<<QString::number(SENDHBRETRY- sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":" <<heartBtStatFlgUpdMtDt;
        }

    }
    else
    {
        qDebug()<<"Network is down., Not checking for heartbeat inside updateMetaData";
    }



    check_devconfigfile.setFile("/home/pi/Desktop/EdgeFirmware/miscfiles/deviceconfigcompleted.txt");

    // check if file exists and if yes: Is it really a file and no directory?
    if (!check_devconfigfile.exists() && !check_devconfigfile.isFile())   //file does n't exist., first time configuration.,
    {
        loggingFlag = !internetStatus;   //since nwmonitor is not yet started, getting internet status
    }
    else    //file exists., no need to add delay for ensuring deep laser registration getting done
    {

    }

    //need this buffer to be filled here itself since in case of network outage, this should be logged
    memset(setPointChar,'\0',sizeof(setPointChar));
    memset(highTempChar,'\0',sizeof(highTempChar));
    memset(lowTempChar,'\0',sizeof(lowTempChar));
    memset(updateMetaDataBuffer,'\0',sizeof(updateMetaDataBuffer));
    memset(idChar,'\0',sizeof(idChar));
    memset(assetTypeChar,'\0',sizeof(assetTypeChar));
    memset(assetModelChar,'\0',sizeof(assetModelChar));
    memset(assetSerialChar,'\0',sizeof(assetSerialChar));
    memset(assetCityChar,'\0',sizeof(assetCityChar));
    memset(assetLocationChar,'\0',sizeof(assetLocationChar));
    memset(sensorNameChar,'\0',sizeof(sensorNameChar));

    sprintf(setPointChar,"%s",setPointTemp.toLocal8Bit().data());
    sprintf(highTempChar,"%s",highTemp.toLocal8Bit().data());
    sprintf(lowTempChar,"%s",lowTemp.toLocal8Bit().data());
    sprintf(assetTypeChar,"%s",assetType.toLocal8Bit().data());
    sprintf(assetModelChar,"%s",assetModel.toLocal8Bit().data());
    sprintf(assetSerialChar,"%s",assetSerial.toLocal8Bit().data());
    sprintf(assetCityChar,"%s",assetCity.toLocal8Bit().data());
    sprintf(assetLocationChar,"%s",assetLocation.toLocal8Bit().data());
    sprintf(sensorNameChar,"%s",sensorName.toLocal8Bit().data());

    if(generateRandInProgress==false)
    {
        sprintf(idChar,"%s",generateRandNumber().toLocal8Bit().data());
    }
    else
    {
        strcpy(idChar,"123456");
    }

    sprintf(updateMetaDataBuffer,"{\"request\":\"updatemetadata\",\"id\":\"%s\",\"assetType\":\"%s\",\"assetModel\":\"%s\",\"assetSerialNumber\":\"%s\",\"assetLocation\":\"%s\",\"assetCity\":\"%s\",\"sensors\":[ { \"name\":\"%s\",\"setPoint\":\"%s\", \"highTemperature\":\"%s\",\"lowTemperature\":\"%s\" } ] }",idChar,assetTypeChar,assetModelChar,assetSerialChar,assetLocationChar,assetCityChar,sensorNameChar,setPointChar,highTempChar,lowTempChar);


    if(heartBtStatFlgUpdMtDt==true && loggingFlag==false)
    {
        headersUpdMData = NULL;
        headersUpdMData = curl_slist_append(headersUpdMData, "Accept: application/json");
        headersUpdMData = curl_slist_append(headersUpdMData, "Content-Type: application/json");
        headersUpdMData = curl_slist_append(headersUpdMData, "charsets: utf-8");

        if(curlUpdMData)
        {

            curl_easy_setopt(curlUpdMData, CURLOPT_URL, url);
            curl_easy_setopt(curlUpdMData, CURLOPT_HTTPHEADER, headersUpdMData);
            readUTCTimenow();

            #ifdef  DEBUG_ON
            qDebug()<<"String to Send to DL for UpdateMetaDataBuffer is :"<< updateMetaDataBuffer ;
            #endif  //DEBUG_ON

            curl_easy_setopt(curlUpdMData, CURLOPT_POSTFIELDS, updateMetaDataBuffer);

        }

        curl_easy_setopt(curlUpdMData, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

        curl_easy_setopt(curlUpdMData, CURLOPT_HEADER,0L);

        retryCntMetaData=0;

        do
        {
            QCoreApplication::processEvents(QEventLoop::AllEvents);

            res = curl_easy_perform(curlUpdMData);

            http_code = 0;

            curl_easy_getinfo (curlUpdMData, CURLINFO_RESPONSE_CODE, &http_code);

            retryCntMetaData++;

            delay(2);

        }while(http_code!=200 && retryCntMetaData<5);

        #ifdef  DEBUG_ON
            qDebug()<<"UpdateMetaData Output is (deltakctrl) "<<QString::number(res) << QString::number(retryCntMetaData);
            qDebug()<<"UpdateMetaData Command to Cloud Post Response ((deltakctrl)): "<<QString::number(http_code);
        #endif  //DEBUG_ON

    }
    else     //network failed or hearbeat failed or both failed
    {
        //start logging data into file
        updMetaDataFp = fopen("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/event.csv","a");

        if(updMetaDataFp==NULL)
        {
            qDebug()<<"Unable to open event.csv";
        }
        else
        {
            memset(heartBtFailChar,'\0',sizeof(heartBtFailChar));
            strcpy(heartBtFailChar,updateMetaDataBuffer);
            strcat(heartBtFailChar,";");
            fprintf(updMetaDataFp,"%s\n",heartBtFailChar);

            fclose(updMetaDataFp);
            eventCnt++;
        }

    }

}


/**
 * @brief DeltaKController::sendHighTempAlarmEvent
 * @return
 */
void DeltaKController::sendHighTempAlarmEvent()
{
    highTempInProgress = true;
    readUTCTimenow();


    //checking for 1-1 heartbeat
    QFuture<QString> htBtIdSent;
    int heartBeatLoopCnt=0;
    int sendHBretry = SENDHBRETRY;
    bool sent = true;
    bool obtainedLock = false;
    bool trylock = false;
    srand(time(NULL));
    int hbfalsedur = (rand() % 33) + 3;
    bool heartBtStatFlgWarmAlrm = false;

    if(loggingFlag==false)  //check for heartbeat only if internet connection available
    {
        do
        {
            heartBeatLoopCnt=0;
            do
            {
                if(trylock==false)
                {
                    do {
                        trylock = heartBeatLock.tryLock(3);
                    } while ((trylock == false) && (--hbfalsedur>0));
                    if (trylock == true)
                    {

                        QCoreApplication::processEvents(QEventLoop::AllEvents);
                        heartBeatStatusFlag = false;
                        sent = false;
                        obtainedLock = true;
                        qDebug()<<"Inside WarmAlarm: "<<hbfalsedur;
                        heartBeatLoopCnt = 0;
                        sendHBretry = SENDHBRETRY;

                    }
                    else
                    {
                        srand(time(NULL));
                        hbfalsedur = (rand() % 33) + 3;
                    }
                }
                if(obtainedLock==false)
                {
                    qDebug()<<"Waiting for Lock in WarmAlarm "<< QString::number(SENDHBRETRY-sendHBretry)<< " " << QString::number(heartBeatLoopCnt);
                }
                if(!sent)
                {
                    sent = true;
                    htBtIdSent = QtConcurrent::run(this,&DeltaKController::sendHeartBeatCommand);
                    htBtVect.append(htBtIdSent);
                }
                QCoreApplication::processEvents(QEventLoop::AllEvents);

                QThread::msleep(500);

                if(obtainedLock == true)
                {
                    if (htBtVect.contains(htBtIdRecd) && (htBtIdSent == htBtIdRecd))
                    {
                        heartBtStatFlgWarmAlrm = heartBeatStatusFlag;
                        if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                        {
                            htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                        }
                    }
                }

            }while((++heartBeatLoopCnt<10) && heartBtStatFlgWarmAlrm==false);

            if (obtainedLock)
            {
                if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                {
                    htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                }
                sent = false;
            }

        }while((--sendHBretry > 0) && (heartBtStatFlgWarmAlrm==false));



        if(obtainedLock==true)
        {
            qDebug()<<"Breaked HtBt Loop In WarmAlarm with OuterCnt: "<<QString::number(SENDHBRETRY- sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgWarmAlrm <<":"<<htBtIdSent <<":"<<htBtIdRecd<< ": HtBt Vector Count: "<<htBtVect.count();
            heartBeatLock.unlock();
        }
        else if(obtainedLock==false)
        {
            qDebug()<<"Timed Out! Breaked HtBt Loop In WarmAlarm with OuterCnt: "<<QString::number(SENDHBRETRY- sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgWarmAlrm;
        }

    }
    else
    {
        qDebug()<<"Network is down., Not checking for heartbeat inside WarmAlarm";
    }

    //need this buffer to be filled here itself since in case of network outage, this should be logged
    alarmIdArray = QUuid::createUuid().toString().remove(QRegExp("[{}]")).toLocal8Bit();

    memset(rtdTempChar,'\0',sizeof(rtdTempChar));
    memset(highTempChar,'\0',sizeof(highTempChar));
    memset(timeinUTCChar,'\0',sizeof(timeinUTCChar));
    memset(alarmIdChar,'\0',sizeof(alarmIdChar));
    memset(highTempEventBuffer,'\0',sizeof(highTempEventBuffer));
    memset(idChar,'\0',sizeof(idChar));

    sprintf(rtdTempChar,"%s",rtdTempVal.toLocal8Bit().data());
    sprintf(highTempChar,"%s",highTemp.toLocal8Bit().data());
    sprintf(timeinUTCChar,"%s",timeinUTC.toLocal8Bit().data());
    sprintf(alarmIdChar,"%s",alarmIdArray.data());

    if(generateRandInProgress==false)
    {
        sprintf(idChar,"%s",generateRandNumber().toLocal8Bit().data());
    }
    else
    {
        strcpy(idChar,"1234567");
    }

    strcpy(currentAlarmIdChar,alarmIdChar);

    totalAlarmIdStr = totalAlarmIdStr + QString::fromLocal8Bit(alarmIdChar) + ";" ;

    memset(bufGen,'\0',sizeof(bufGen));
    sprintf(bufGen,"echo %s > /home/pi/Desktop/EdgeFirmware/miscfiles/currentalarmid.txt",currentAlarmIdChar);
    system(bufGen);             //storing alarm id onto config to matching corresponding snooze event from cloud

    sprintf(highTempEventBuffer,"{\"request\":\"senddata\", \"datatype\":\"event\", \"id\":\"%s\", \"eventKey\":\"WARM_ALARM\", \"timestamp\":\"%s\", \"eventParameters\": { \"timestamp\":\"%s\",\"temperature\":\"%s\", \"threshold\":\"%s\",\"metric\":\"temp-se1\", \"alarmId\":\"%s\" , \"alarmType\":\"WARM_ALARM\", \"instrumentName\":\"%s\"}  }",idChar,timeinUTCChar,timeinUTCChar,rtdTempChar,highTempChar,alarmIdChar,instrumentNameChar);


    if(heartBtStatFlgWarmAlrm==true && loggingFlag==false)
    {
        headersEvent = NULL;
        headersEvent = curl_slist_append(headersEvent, "Accept: application/json");
        headersEvent = curl_slist_append(headersEvent, "Content-Type: application/json");
        headersEvent = curl_slist_append(headersEvent, "charsets: utf-8");

        if(curlEvent)
        {

            curl_easy_setopt(curlEvent, CURLOPT_URL, url);

            curl_easy_setopt(curlEvent, CURLOPT_HTTPHEADER, headersEvent);

            #ifdef  DEBUG_ON
            qDebug()<<"String to Send to DL for highTempEventBuffer is :"<< highTempEventBuffer <<endl << QString(highTempEventBuffer).size() ;
            #endif  //DEBUG_ON

            curl_easy_setopt(curlEvent, CURLOPT_POSTFIELDS, highTempEventBuffer);

        }

        curl_easy_setopt(curlEvent, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

        curl_easy_setopt(curlEvent, CURLOPT_HEADER,0L);

        res = curl_easy_perform(curlEvent);

        http_code = 0;
        curl_easy_getinfo (curlEvent, CURLINFO_RESPONSE_CODE, &http_code);


        #ifdef  DEBUG_ON
        qDebug()<<"HIGH_TEMP EVENT CMD Output is "<<QString::number(res);
        qDebug()<<"HIGH_TEMP EVENT CMD to Cloud Post Response: "<<QString::number(http_code);
        #endif  //DEBUG_ON

        if(http_code==200)
        {
            highTempAlarmEventSentFlag=true;
            qDebug()<<"HighTempEvent Sent Succefully";
            emit startEventSentCheck();     //to recheck if there is network outage immediately after sending event so that we can log it to sd card

        }

    }

    else     //network failed or hearbeat failed or both failed
    {
        //start logging data into file
        highTempAlarmFp = fopen("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/event.csv","a");

        if(highTempAlarmFp==NULL)
        {
            qDebug()<<"Unable to open event.csv";
        }
        else
        {
            memset(heartBtFailChar,'\0',sizeof(heartBtFailChar));
            strcpy(heartBtFailChar,highTempEventBuffer);
            strcat(heartBtFailChar,";");
            fprintf(highTempAlarmFp,"%s\n",heartBtFailChar);

            fclose(highTempAlarmFp);
            eventCnt++;
            highTempAlarmEventSentFlag=true;
        }

    }

    highTempInProgress = false;

}


/**
 * @brief DeltaKController::sendLowTempAlarmEvent
 * @return
 */
void DeltaKController::sendLowTempAlarmEvent()
{
    lowTempInProgress = true;
    readUTCTimenow();


    //checking for 1-1 heartbeat
    QFuture<QString> htBtIdSent;
    int heartBeatLoopCnt=0;
    int sendHBretry = SENDHBRETRY;
    bool sent = true;
    bool obtainedLock = false;
    bool trylock = false;
    srand(time(NULL));
    int hbfalsedur = (rand() % 33) + 3;
    bool heartBtStatFlgCldAlrm = false;

    if(loggingFlag==false)  //check for heartbeat only if internet connection available
    {
        do
        {
            heartBeatLoopCnt=0;
            do
            {
                if(trylock==false)
                {
                    do {
                        trylock = heartBeatLock.tryLock(3);
                    } while ((trylock == false) && (--hbfalsedur>0));
                    if (trylock == true)
                    {
                        QCoreApplication::processEvents(QEventLoop::AllEvents);
                        heartBeatStatusFlag = false;
                        sent = false;
                        obtainedLock = true;
                        qDebug()<<"Inside ColdAlarm: "<<hbfalsedur;
                        heartBeatLoopCnt = 0;
                        sendHBretry = SENDHBRETRY;

                    }
                    else
                    {
                        srand(time(NULL));
                        hbfalsedur = (rand() % 33) + 3;
                    }
                }
                if(obtainedLock==false)
                {
                    qDebug()<<"Waiting for Lock in ColdAlarm "<< QString::number(SENDHBRETRY-sendHBretry)<< " " << QString::number(heartBeatLoopCnt);
                }
                if(!sent)
                {
                    sent = true;
                    htBtIdSent = QtConcurrent::run(this,&DeltaKController::sendHeartBeatCommand);
                    htBtVect.append(htBtIdSent);
                }
                QCoreApplication::processEvents(QEventLoop::AllEvents);

                QThread::msleep(500);

                if(obtainedLock == true)
                {
                    if (htBtVect.contains(htBtIdRecd) && (htBtIdSent == htBtIdRecd))
                    {
                        heartBtStatFlgCldAlrm = heartBeatStatusFlag;
                        if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                        {
                            htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                        }
                    }
                }

            }while((++heartBeatLoopCnt<10) && heartBtStatFlgCldAlrm==false);

            if (obtainedLock)
            {
                if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                {
                    htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                }
                sent = false;
            }

        }while((--sendHBretry > 0) && (heartBtStatFlgCldAlrm==false));


        if(obtainedLock==true)
        {
            qDebug()<<"Breaked HtBt Loop In ColdAlarm with OuterCnt: "<<QString::number(SENDHBRETRY- sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgCldAlrm <<":"<< htBtIdSent <<":"<<htBtIdRecd << ": HtBt Vector Count: "<<htBtVect.count();
            heartBeatLock.unlock();
        }
        else if(obtainedLock==false)
        {
            qDebug()<<"Timed Out! Breaked HtBt Loop In ColdAlarm with OuterCnt: "<<QString::number(SENDHBRETRY- sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgCldAlrm;
        }
    }
    else
    {
        qDebug()<<"Network is down., Not checking for heartbeat inside ColdAlarm";
    }

    //need this buffer to be filled here itself since in case of network outage, this should be logged
    alarmIdArray = QUuid::createUuid().toString().remove(QRegExp("[{}]")).toLocal8Bit();

    memset(rtdTempChar,'\0',sizeof(rtdTempChar));
    memset(lowTempChar,'\0',sizeof(lowTempChar));
    memset(timeinUTCChar,'\0',sizeof(timeinUTCChar));
    memset(alarmIdChar,'\0',sizeof(alarmIdChar));
    memset(lowTempEventBuffer,'\0',sizeof(lowTempEventBuffer));
    memset(idChar,'\0',sizeof(idChar));

    sprintf(rtdTempChar,"%s",rtdTempVal.toLocal8Bit().data());
    sprintf(lowTempChar,"%s",lowTemp.toLocal8Bit().data());
    sprintf(timeinUTCChar,"%s",timeinUTC.toLocal8Bit().data());
    sprintf(alarmIdChar,"%s",alarmIdArray.data());

    if(generateRandInProgress==false)
    {
        sprintf(idChar,"%s",generateRandNumber().toLocal8Bit().data());
    }
    else
    {
        strcpy(idChar,"12345678");
    }

    strcpy(currentAlarmIdChar,alarmIdChar);

    totalAlarmIdStr = totalAlarmIdStr + QString::fromLocal8Bit(alarmIdChar) + ";" ;

    memset(bufGen,'\0',sizeof(bufGen));
    sprintf(bufGen,"echo %s > /home/pi/Desktop/EdgeFirmware/miscfiles/currentalarmid.txt",currentAlarmIdChar);
    system(bufGen);             //storing alarm id onto config to matching corresponding snooze event from cloud

    sprintf(lowTempEventBuffer,"{\"request\":\"senddata\", \"datatype\":\"event\", \"id\":\"%s\", \"eventKey\":\"COLD_ALARM\", \"timestamp\":\"%s\", \"eventParameters\": { \"timestamp\":\"%s\",\"temperature\":\"%s\", \"threshold\":\"%s\", \"metric\":\"temp-se1\", \"alarmId\":\"%s\", \"alarmType\":\"COLD_ALARM\", \"instrumentName\":\"%s\"}  }",idChar,timeinUTCChar,timeinUTCChar,rtdTempChar,lowTempChar,alarmIdChar,instrumentNameChar);


    if(heartBtStatFlgCldAlrm==true && loggingFlag==false)
    {
        headersEvent = NULL;
        headersEvent = curl_slist_append(headersEvent, "Accept: application/json");
        headersEvent = curl_slist_append(headersEvent, "Content-Type: application/json");
        headersEvent = curl_slist_append(headersEvent, "charsets: utf-8");


        if(curlEvent)
        {

            curl_easy_setopt(curlEvent, CURLOPT_URL, url);
            curl_easy_setopt(curlEvent, CURLOPT_HTTPHEADER, headersEvent);


            #ifdef  DEBUG_ON
            qDebug()<<"String to Send to DL for LowTempEventBuffer is :"<< lowTempEventBuffer <<endl << QString(lowTempEventBuffer).size() ;
            #endif  //DEBUG_ON

            curl_easy_setopt(curlEvent, CURLOPT_POSTFIELDS, lowTempEventBuffer);

        }

        curl_easy_setopt(curlEvent, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

        curl_easy_setopt(curlEvent, CURLOPT_HEADER,0L);

        res = curl_easy_perform(curlEvent);

        http_code = 0;
        curl_easy_getinfo (curlEvent, CURLINFO_RESPONSE_CODE, &http_code);


    #ifdef  DEBUG_ON
        qDebug()<<"LOW_TEMP EVENT CMD Output is "<<QString::number(res);
        qDebug()<<"LOW_TEMP EVENT CMD to Cloud Post Response: "<<QString::number(http_code);
    #endif  //DEBUG_ON

        if(http_code==200)
        {
            lowTempAlarmEventSentFlag=true;

            emit startEventSentCheck();     //to recheck if there is network outage immediately after sending event so that we can log it to sd card

        }

    }
    else     //network failed or hearbeat failed or both failed
    {
        //start logging data into file
        lowTempAlarmFp = fopen("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/event.csv","a");

        if(lowTempAlarmFp==NULL)
        {
            qDebug()<<"Unable to open event.csv";
        }
        else
        {
            memset(heartBtFailChar,'\0',sizeof(heartBtFailChar));
            strcpy(heartBtFailChar,lowTempEventBuffer);
            strcat(heartBtFailChar,";");
            fprintf(lowTempAlarmFp,"%s\n",heartBtFailChar);

            fclose(lowTempAlarmFp);
            eventCnt++;
            lowTempAlarmEventSentFlag=true;
        }

    }

    lowTempInProgress = false;
}


/**
 * @brief DeltaKController::sendSnoozeEvent
 * @param eventKey
 * @return
 */
void DeltaKController::sendSnoozeEvent(int eventKey)
{
    readUTCTimenow();

    //checking for 1-1 heartbeat
    QFuture<QString> htBtIdSent;
    int heartBeatLoopCnt=0;
    int sendHBretry = SENDHBRETRY;
    bool sent = true;
    bool obtainedLock = false;
    bool trylock = false;
    srand(time(NULL));
    int hbfalsedur = (rand() % 33) + 3;
    bool heartBtStatFlgSnz = false;

    if(loggingFlag==false)  //check for heartbeat only if internet connection available
    {
        do
        {
            heartBeatLoopCnt=0;
            do
            {
                if(trylock==false)
                {
                    do {
                        trylock = heartBeatLock.tryLock(3);
                    } while ((trylock == false) && (--hbfalsedur>0));
                    if (trylock == true)
                    {

                        QCoreApplication::processEvents(QEventLoop::AllEvents);
                        heartBeatStatusFlag = false;
                        sent = false;
                        obtainedLock = true;
                        qDebug()<<"Inside sendSnoozeEvent: "<<hbfalsedur;
                        heartBeatLoopCnt = 0;
                        sendHBretry = SENDHBRETRY;

                    }
                    else
                    {
                        srand(time(NULL));
                        hbfalsedur = (rand() % 33) + 3;
                    }
                }
                if(obtainedLock==false)
                {
                    qDebug()<<"Waiting for Lock in sendSnoozeEvent "<< QString::number(SENDHBRETRY-sendHBretry)<< " " << QString::number(heartBeatLoopCnt);
                }
                if(!sent)
                {
                    sent = true;
                    htBtIdSent = QtConcurrent::run(this,&DeltaKController::sendHeartBeatCommand);
                    htBtVect.append(htBtIdSent);
                }
                QCoreApplication::processEvents(QEventLoop::AllEvents);

                QThread::msleep(500);

                if(obtainedLock == true)
                {
                    if (htBtVect.contains(htBtIdRecd) && (htBtIdSent == htBtIdRecd))
                    {
                        heartBtStatFlgSnz = heartBeatStatusFlag;
                        if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                        {
                            htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                        }
                    }
                }

            }while((++heartBeatLoopCnt<10) && heartBtStatFlgSnz==false);

            if (obtainedLock)
            {
                if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                {
                    htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                }
                sent = false;
            }

        }while((--sendHBretry > 0) && (heartBtStatFlgSnz==false));


       if(obtainedLock==true)
       {
           qDebug()<<"Breaked HtBt Loop In sendSnoozeEvent with OuterCnt: "<<QString::number(SENDHBRETRY- sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgSnz <<":"<< htBtIdSent <<":"<<htBtIdRecd << ": HtBt Vector Count: "<<htBtVect.count();
           heartBeatLock.unlock();
       }
       else if(obtainedLock==false)
       {
            qDebug()<<"Timed Out! Breaked HtBt Loop In sendSnoozeEvent with OuterCnt: "<<QString::number(SENDHBRETRY- sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgSnz;
       }
    }
    else
    {
        qDebug()<<"Network is down., Not checking for heartbeat inside sendSnoozeEvent";
    }

   //need this buffer to be filled here itself since in case of network outage, this should be logged
   if(eventKey==HIGH_TEMP)
   {
       sprintf(snoozeEventBuffer,"{\"request\":\"senddata\", \"datatype\":\"event\", \"id\":\"%s\", \"eventKey\":\"SNOOZE\", \"timestamp\":\"%s\", \"eventParameters\": { \"alarmId\":\"%s\", \"snoozeTime\":60 , \"alarmType\":\"WARM_ALARM\", \"instrumentName\":\"%s\", \"byUser\":\"%s\"}  }",idChar,timeinUTCChar,currentAlarmIdChar,instrumentNameChar,friendlyNameChar);
   }
   else if(eventKey==LOW_TEMP)
   {
       sprintf(snoozeEventBuffer,"{\"request\":\"senddata\", \"datatype\":\"event\", \"id\":\"%s\", \"eventKey\":\"SNOOZE\", \"timestamp\":\"%s\", \"eventParameters\": { \"alarmId\":\"%s\", \"snoozeTime\":60 , \"alarmType\":\"COLD_ALARM\", \"instrumentName\":\"%s\", \"byUser\":\"%s\"}  }",idChar,timeinUTCChar,currentAlarmIdChar,instrumentNameChar,friendlyNameChar);
   }



    if(heartBtStatFlgSnz==true && loggingFlag==false)
    {

        headers = curl_slist_append(headers, "Accept: application/json");
        headers = curl_slist_append(headers, "Content-Type: application/json");
        headers = curl_slist_append(headers, "charsets: utf-8");


        if(curl)
        {

            curl_easy_setopt(curl, CURLOPT_URL, url);
            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);


            #ifdef  DEBUG_ON
            qDebug()<<"String to Send to DL for SnoozeEventBuffer is :"<<snoozeEventBuffer <<endl << QString(snoozeEventBuffer).size() ;
            #endif  //DEBUG_ON

            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, snoozeEventBuffer);

        }

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

        curl_easy_setopt(curl, CURLOPT_HEADER,0L);


        retryCntSendSnzEvnt=0;
        do
        {
            QCoreApplication::processEvents(QEventLoop::AllEvents);

            res = curl_easy_perform(curl);

            http_code = 0;

            curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);

            retryCntSendSnzEvnt++;

            delay(2);

        }while(http_code!=200 && retryCntSendSnzEvnt<5);

        #ifdef  DEBUG_ON
        qDebug()<<"SNOOZE EVENT CMD Output is "<<QString::number(res);
        qDebug()<<"SNOOZE EVENT CMD to Cloud Post Response: "<<QString::number(http_code);
        #endif  //DEBUG_ON

        if(http_code==200)
        {
            emit startEventSentCheck();  //to recheck if there is network outage immediately after sending event so that we can log it to sd card
        }


    }
    else     //network failed or hearbeat failed or both failed
    {
        //start logging data into file
        snoozeFp = fopen("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/snoozeevent.csv","a");

        if(snoozeFp==NULL)
        {
            qDebug()<<"Unable to open snoozeevent.csv";
        }
        else
        {
            memset(heartBtFailChar,'\0',sizeof(heartBtFailChar));
            strcpy(heartBtFailChar,snoozeEventBuffer);
            strcat(heartBtFailChar,";");
            fprintf(snoozeFp,"%s\n",heartBtFailChar);

            fclose(snoozeFp);
            snoozeEventCnt++;
        }
    }
}



/**
 * @brief DeltaKController::sendAlarmRecoveryEvent
 * @param eventKey
 * @return
 */
long DeltaKController::sendAlarmRecoveryEvent()
{
    alarmRecoveryInProgress = true;

    readUTCTimenow();

    //checking for 1-1 heartbeat
    QFuture<QString> htBtIdSent;
    int heartBeatLoopCnt=0;
    int sendHBretry = SENDHBRETRY;
    bool sent = true;
    bool obtainedLock = false;
    bool trylock = false;
    srand(time(NULL));
    int hbfalsedur = (rand() % 33) + 3;
    bool heartBtStatFlgRcvry = false;

    if(loggingFlag==false)      //check for heartbeat only if internet connection available
    {
        do
        {
            heartBeatLoopCnt=0;
            do
            {
                if(trylock==false)
                {
                    do {
                        trylock = heartBeatLock.tryLock(3);
                    } while ((trylock == false) && (--hbfalsedur>0));
                    if (trylock == true)
                    {

                        QCoreApplication::processEvents(QEventLoop::AllEvents);
                        heartBeatStatusFlag = false;
                        sent = false;
                        obtainedLock = true;
                        qDebug()<<"Inside sendAlarmRecoveryEvent: "<<hbfalsedur;
                        heartBeatLoopCnt = 0;
                        sendHBretry = SENDHBRETRY;
                    }
                    else
                    {
                        srand(time(NULL));
                        hbfalsedur = (rand() % 33) + 3;
                    }
                }
                if(obtainedLock==false)
                {
                    qDebug()<<"Waiting for Lock in sendAlarmRecoveryEvent "<< QString::number(SENDHBRETRY-sendHBretry)<< " " << QString::number(heartBeatLoopCnt);
                }
                if(!sent)
                {
                    sent = true;
                    htBtIdSent = QtConcurrent::run(this,&DeltaKController::sendHeartBeatCommand);
                    htBtVect.append(htBtIdSent);
                }
                QCoreApplication::processEvents(QEventLoop::AllEvents);

                QThread::msleep(500);

                if(obtainedLock == true)
                {
                    if (htBtVect.contains(htBtIdRecd) && (htBtIdSent == htBtIdRecd))
                    {
                        heartBtStatFlgRcvry = heartBeatStatusFlag;
                        if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                        {
                            htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                        }
                    }
                }

            }while((++heartBeatLoopCnt<10) && heartBtStatFlgRcvry==false);

            if (obtainedLock)
            {
                if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                {
                    htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                }
                sent = false;
            }

        }while((--sendHBretry > 0) && (heartBtStatFlgRcvry==false));


        if(obtainedLock==true)
        {
            qDebug()<<"Breaked HtBt Loop In sendAlarmRecoveryEvent with OuterCnt: "<<QString::number(SENDHBRETRY- sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgRcvry << ":"<<htBtIdSent <<":"<<htBtIdRecd << ": HtBt Vector Count: "<<htBtVect.count();
            heartBeatLock.unlock();
        }
        else if(obtainedLock==false)
        {
            qDebug()<<"Timed Out! Breaked HtBt Loop In sendAlarmRecoveryEvent with OuterCnt: "<<QString::number(SENDHBRETRY- sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgRcvry;
        }
    }
    else
    {
        qDebug()<<"Network is down., Not checking for heartbeat inside AlarmRecovery";
    }

    //need this buffer to be filled here itself since in case of network outage, this should be logged
    if(htAlarmSetClearedFlag==true)
    {
        thresholdArray =   highTemp.toLocal8Bit();

    }
    else if(ltAlarmSetClearedFlag==true)
    {
        thresholdArray =   lowTemp.toLocal8Bit();
    }

    memset(timeinUTCChar,'\0',sizeof(timeinUTCChar));
    memset(alarmRecoveryBuffer,'\0',sizeof(alarmRecoveryBuffer));
    memset(idChar,'\0',sizeof(idChar));
    memset(thresholdChar,'\0',sizeof(thresholdChar));
    memset(rtdTempChar,'\0',sizeof(rtdTempChar));

    sprintf(timeinUTCChar,"%s",timeinUTC.toLocal8Bit().data());
    if(generateRandInProgress==false)
    {
        sprintf(idChar,"%s",generateRandNumber().toLocal8Bit().data());
    }
    else
    {
        strcpy(idChar,"123456789");
    }
    sprintf(thresholdChar,"%s",thresholdArray.data());
    sprintf(rtdTempChar,"%s",rtdTempVal.toLocal8Bit().data());

    sprintf(alarmRecoveryBuffer,"{\"request\":\"senddata\",\"datatype\":\"event\",\"id\":\"%s\",\"eventKey\":\"RECOVERY\",\"timestamp\":\"%s\",\"eventParameters\": { \"alarmId\":\"%s\" ,\"alarmType\":\"RECOVERY\",\"instrumentName\":\"%s\",\"temperature\":\"%s\",\"threshold\":\"%s\"}  }",idChar,timeinUTCChar,currentAlarmIdChar,instrumentNameChar,rtdTempChar,thresholdChar);


    if(heartBtStatFlgRcvry==true && loggingFlag==false)
    {
        headersRecovery = NULL;
        headersRecovery = curl_slist_append(headersRecovery, "Accept: application/json");
        headersRecovery = curl_slist_append(headersRecovery, "Content-Type: application/json");
        headersRecovery = curl_slist_append(headersRecovery, "charsets: utf-8");


        if(curlRecovery)
        {

            curl_easy_setopt(curlRecovery, CURLOPT_URL, url);
            curl_easy_setopt(curlRecovery, CURLOPT_HTTPHEADER, headersRecovery);


            #ifdef  DEBUG_ON
            qDebug()<<"String to Send to DL for AlarmRecoveryBuffer is :"<<alarmRecoveryBuffer <<endl << QString(alarmRecoveryBuffer).size() ;
            #endif  //DEBUG_ON

            curl_easy_setopt(curlRecovery, CURLOPT_POSTFIELDS, alarmRecoveryBuffer);

        }
        curl_easy_setopt(curlRecovery, CURLOPT_HEADER,0L);

        curl_easy_setopt(curlRecovery, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

QCoreApplication::processEvents(QEventLoop::AllEvents);
        res = curl_easy_perform(curlRecovery);
QCoreApplication::processEvents(QEventLoop::AllEvents);
        http_code = 0;
        curl_easy_getinfo (curlRecovery, CURLINFO_RESPONSE_CODE, &http_code);


    #ifdef  DEBUG_ON
        qDebug()<<"ALARM RECOVERY EVENT CMD Output is "<<QString::number(res);
        qDebug()<<"ALARM RECOVERY EVENT CMD to Cloud Post Response: "<<QString::number(http_code);
    #endif  //DEBUG_ON
        if(http_code==200)  //command send to deep laser successfully
        {
            recoveryAlarmEventSentFlag = true;
            ltAlarmSetClearedFlag = false;
            htAlarmSetClearedFlag = false;


            emit startEventSentCheck();    //to recheck if there is network outage immediately after sending event so that we can log it to sd card


            memset(currentAlarmIdChar,'\0',sizeof(currentAlarmIdChar));
        }


    }
    else     //network failed or hearbeat failed or both failed
    {
        //start logging data into file
        alarmRecoveryFp = fopen("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/recoveryevent.csv","a");

        if(alarmRecoveryFp==NULL)
        {
            qDebug()<<"Unable to open recoveryevent.csv";
        }
        else
        {
            memset(heartBtFailChar,'\0',sizeof(heartBtFailChar));
            strcpy(heartBtFailChar,alarmRecoveryBuffer);
            strcat(heartBtFailChar,";");
            fprintf(alarmRecoveryFp,"%s\n",heartBtFailChar);

            fclose(alarmRecoveryFp);
            recoveryEvtCnt++;
            recoveryAlarmEventSentFlag = true;
            ltAlarmSetClearedFlag = false;
            htAlarmSetClearedFlag = false;
            memset(currentAlarmIdChar,'\0',sizeof(currentAlarmIdChar));

        }

    }


    alarmRecoveryInProgress = false;
    return http_code;
}



/**
 * @brief DeltaKController::sendLowSignalEvent
 * @return
 */
void DeltaKController::sendLowSignalEvent()
{
    readUTCTimenow();

    //checking for 1-1 heartbeat
    QFuture<QString> htBtIdSent;
    int heartBeatLoopCnt=0;
    int sendHBretry = SENDHBRETRY;
    bool sent = true;
    bool obtainedLock = false;
    bool trylock = false;
    srand(time(NULL));
    int hbfalsedur = (rand() % 33) + 3;
    bool heartBtStatFlgLowSig = false;

    if(loggingFlag==false)      //check for heartbeat only if internet connection available
    {
        do
        {
            heartBeatLoopCnt=0;
            do
            {
                if(trylock==false)
                {
                    do {
                        trylock = heartBeatLock.tryLock(3);
                    } while ((trylock == false) && (--hbfalsedur>0));
                    if (trylock == true)
                    {

                        QCoreApplication::processEvents(QEventLoop::AllEvents);
                        heartBeatStatusFlag = false;
                        sent = false;
                        obtainedLock = true;
                        qDebug()<<"Inside sendLowSignalEvent: "<<hbfalsedur;
                        heartBeatLoopCnt = 0;
                        sendHBretry = SENDHBRETRY;

                    }
                    else
                    {
                        srand(time(NULL));
                        hbfalsedur = (rand() % 33) + 3;
                    }
                }
                if(obtainedLock==false)
                {
                    qDebug()<<"Waiting for Lock in sendLowSignalEvent "<< QString::number(SENDHBRETRY-sendHBretry)<< " " << QString::number(heartBeatLoopCnt);
                }
                if(!sent)
                {
                    sent = true;
                    htBtIdSent = QtConcurrent::run(this,&DeltaKController::sendHeartBeatCommand);
                    htBtVect.append(htBtIdSent);
                }
                QCoreApplication::processEvents(QEventLoop::AllEvents);

                QThread::msleep(500);

                if(obtainedLock == true)
                {
                    if (htBtVect.contains(htBtIdRecd) && (htBtIdSent == htBtIdRecd))
                    {
                        heartBtStatFlgLowSig = heartBeatStatusFlag;
                        if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                        {
                            htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                        }
                    }
                }

            }while((++heartBeatLoopCnt<10) && heartBtStatFlgLowSig==false);

            if (obtainedLock)
            {
                if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                {
                    htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                }
                sent = false;
            }

        }while((--sendHBretry > 0) && (heartBtStatFlgLowSig==false));


        if(obtainedLock==true)
        {
            qDebug()<<"Breaked HtBt Loop In sendLowSignalEvent with OuterCnt: "<<QString::number(SENDHBRETRY - sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgLowSig << ":"<<htBtIdSent <<":"<<htBtIdRecd << ": HtBt Vector Count: "<<htBtVect.count();
            heartBeatLock.unlock();
        }
        else if(obtainedLock==false)
        {
            qDebug()<<"Timed Out! Breaked HtBt Loop In sendLowSignalEvent with OuterCnt: "<<QString::number(SENDHBRETRY - sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgLowSig;
        }
    }
    else
    {
        qDebug()<<"Network is down., Not checking for heartbeat inside sendLowSignalEvent";
    }


    //need this buffer to be filled here itself since in case of network outage, this should be logged
    alarmIdArray = QUuid::createUuid().toString().remove(QRegExp("[{}]")).toLocal8Bit();

    memset(alarmIdChar,'\0',sizeof(alarmIdChar));
    memset(timeinUTCChar,'\0',sizeof(timeinUTCChar));
    memset(lowSignalEventBuffer,'\0',sizeof(lowSignalEventBuffer));
    memset(idChar,'\0',sizeof(idChar));

    sprintf(alarmIdChar,"%s",alarmIdArray.data());
    sprintf(timeinUTCChar,"%s",timeinUTC.toLocal8Bit().data());

    if(generateRandInProgress==false)
    {
        sprintf(idChar,"%s",generateRandNumber().toLocal8Bit().data());
    }
    else
    {
        strcpy(idChar,"98");
    }

    sprintf(lowSignalEventBuffer,"{\"request\":\"senddata\",\"datatype\":\"event\",\"id\":\"%s\",\"eventKey\":\"LOW_SIGNAL\",\"timestamp\":\"%s\",\"eventParameters\": { \"alarmId\":\"%s\" ,\"alarmType\":\"LOW_SIGNAL\",\"instrumentName\":\"%s\"}  }",idChar,timeinUTCChar,alarmIdChar,instrumentNameChar);


    if(heartBtStatFlgLowSig==true && loggingFlag==false)
    {

        headers = curl_slist_append(headers, "Accept: application/json");
        headers = curl_slist_append(headers, "Content-Type: application/json");
        headers = curl_slist_append(headers, "charsets: utf-8");


        if(curl)
        {
            curl_easy_setopt(curl, CURLOPT_URL, url);
            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

            #ifdef  DEBUG_ON
            qDebug()<<"String to Send to DL for lowSignalEventBuffer is :"<<lowSignalEventBuffer <<endl << QString(lowSignalEventBuffer).size() ;
            #endif  //DEBUG_ON

            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, lowSignalEventBuffer);
        }

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

        curl_easy_setopt(curl, CURLOPT_HEADER,0L);


        retryCntLowSignal=0;
        do
        {
            QCoreApplication::processEvents(QEventLoop::AllEvents);

            res = curl_easy_perform(curl);

            http_code = 0;

            curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);

            retryCntLowSignal++;

            delay(2);

        }while(http_code!=200 && retryCntLowSignal<5);


        #ifdef  DEBUG_ON
            qDebug()<<"LOW SIGNAL EVENT CMD Output is "<<QString::number(res);
            qDebug()<<"LOW SIGNAL EVENT CMD to Cloud Post Response: "<<QString::number(http_code);
        #endif  //DEBUG_ON
    }
    else     //network failed or hearbeat failed or both failed
    {
        //start logging data into file
        lowSignalFp = fopen("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/event.csv","a");

        if(lowSignalFp==NULL)
        {
            qDebug()<<"Unable to open event.csv";
        }
        else
        {
            memset(heartBtFailChar,'\0',sizeof(heartBtFailChar));
            strcpy(heartBtFailChar,lowSignalEventBuffer);
            strcat(heartBtFailChar,";");
            fprintf(lowSignalFp,"%s\n",heartBtFailChar);

            fclose(lowSignalFp);
            eventCnt++;
            lowSigEventSentFlag=true;
        }

    }

}

/**
 * @brief DeltaKController::batteryPercentage
 * @return
 */
QString DeltaKController::batteryPercentage() const
{
    return m_batteryPercentage;
}

/**
 * @brief DeltaKController::setBatteryPercentage
 * @param batteryPercentage
 */
void DeltaKController::setBatteryPercentage(const QString &batteryPercentage)
{
    m_batteryPercentage = batteryPercentage;
}

/**
 * @brief DeltaKController::wifiPercentage
 * @return
 */
QString DeltaKController::wifiPercentage() const
{
    return m_wifiPercentage;
}

/**
 * @brief DeltaKController::setWifiPercentage
 * @param wifiPercentage
 */
void DeltaKController::setWifiPercentage(const QString &wifiPercentage)
{
    m_wifiPercentage = wifiPercentage;
}

/**
 * @brief DeltaKController::warningAlarmMessage
 * @return
 */
QString DeltaKController::warningAlarmMessage() const
{
    return m_warningAlarmMessage;
}

/**
 * @brief DeltaKController::setWarningAlarmMessage
 * @param warningAlarmMessage
 */
void DeltaKController::setWarningAlarmMessage(const QString &warningAlarmMessage)
{
    m_warningAlarmMessage = warningAlarmMessage;
}

/**
 * @brief DeltaKController::warningAlarmMessageText
 * @return
 */
QString DeltaKController::warningAlarmMessageText() const
{
    return m_warningAlarmMessageText;
}

/**
 * @brief DeltaKController::setWarningAlarmMessageText
 * @param warningAlarmMessageText
 */
void DeltaKController::setWarningAlarmMessageText(const QString &warningAlarmMessageText)
{
    m_warningAlarmMessageText = warningAlarmMessageText;
}

/**
 * @brief DeltaKController::tempAlarmIcon
 * @return
 */
bool DeltaKController::tempAlarmIcon() const
{
    return m_tempAlarmIcon;
}

/**
 * @brief DeltaKController::setTempAlarmIcon
 * @param tempAlarmIcon
 */
void DeltaKController::setTempAlarmIcon(bool tempAlarmIcon)
{
    m_tempAlarmIcon = tempAlarmIcon;
}

/**
 * @brief DeltaKController::batteryAlarmIcon
 * @return
 */
bool DeltaKController::batteryAlarmIcon() const
{
    return m_batteryAlarmIcon;
}

/**
 * @brief DeltaKController::setBatteryAlarmIcon
 * @param batteryAlarmIcon
 */
void DeltaKController::setBatteryAlarmIcon(bool batteryAlarmIcon)
{
    m_batteryAlarmIcon = batteryAlarmIcon;
}

/**
 * @brief DeltaKController::yellowWarning
 * @return
 */
bool DeltaKController::yellowWarning() const
{
    return m_yellowWarning;
}

/**
 * @brief DeltaKController::setYellowWarning
 * @param yellowWarning
 */
void DeltaKController::setYellowWarning(bool yellowWarning)
{
    m_yellowWarning = yellowWarning;
}

/**
 * @brief DeltaKController::redWarning
 * @return
 */
bool DeltaKController::redWarning() const
{
    return m_redWarning;
}

/**
 * @brief DeltaKController::setRedWarning
 * @param redWarning
 */
void DeltaKController::setRedWarning(bool redWarning)
{
    m_redWarning = redWarning;
}

/**
 * @brief DeltaKController::unAckAlarmCleared
 * @return
 */
bool DeltaKController::unAckAlarmCleared() const
{
    return m_unAckAlarmCleared;
}

/**
 * @brief DeltaKController::setUnAckAlarmCleared
 * @param unAckAlarmCleared
 */
void DeltaKController::setUnAckAlarmCleared(bool unAckAlarmCleared)
{
    m_unAckAlarmCleared = unAckAlarmCleared;
}

/**
 * @brief DeltaKController::userLinkWarning
 * @return
 */
bool DeltaKController::userLinkWarning() const
{
    return m_userLinkWarning;
}

/**
 * @brief DeltaKController::setUserLinkWarning
 * @param userLinkWarning
 */
void DeltaKController::setUserLinkWarning(bool userLinkWarning)
{
    m_userLinkWarning = userLinkWarning;
}

/**
 * @brief DeltaKController::noProbeDetected
 * @return
 */
bool DeltaKController::noProbeDetected() const
{
    return m_noProbeDetected;
}

/**
 * @brief DeltaKController::setNoProbeDetected
 * @param unAckAlarmCleared
 */
void DeltaKController::setNoProbeDetected(bool noProbeDetected)
{
    m_noProbeDetected = noProbeDetected;
}

/**
 * @brief DeltaKController::completedDeviceConfig
 */
void DeltaKController::completedDeviceConfig()
{
    qDebug()<<"Completed Device Config Fired...";
    system("sudo echo 1 > /home/pi/Desktop/EdgeFirmware/miscfiles/deviceconfigcompleted.txt");
    system("sudo chmod 777 /home/pi/Desktop/EdgeFirmware/miscfiles/deviceconfigcompleted.txt");

    this->setAppStartLang(false);
    apModeInProgress = false;
    if(apModeCheckTimer->isActive()==true)
    {
        apModeCheckTimer->stop();
    }

    system("sudo cp /home/pi/Desktop/EdgeFirmware/miscfiles/deviceconfigcompleted.txt /home/pi/Desktop/modbox-webserver/chinaIoT-Chinese");
    system("sudo cp /home/pi/Desktop/EdgeFirmware/miscfiles/deviceconfigcompleted.txt /home/pi/Desktop/modbox-webserver/chinaIoT-English");

    if(nwThread->isRunning()==false)
    {
        nwThread->start();
    }

    //start network timer since config is already done
    if(networkTimer->isActive()==false)
    {
        networkTimer->start(6000);      //monitor network connectivity every 6 seconds., but 30 sec on boot up or after device config completed
    }

    if(wifiTimer->isActive()==false)
    {
        wifiTimer->start(11000);  //monitoring wifi signal status every 11 sec
    }

    if(wifiThrd->isRunning()==false)
    {
        wifiThrd->start();
    }

    if(telemetryTimer->isActive()==false)
    {
        telemetryTimer->start(60*1000);  //updating telemetry every 60 sec
    }


    if(deviceStatusTimer->isActive()==false)
    {
        deviceStatusTimer->start(9.5*60000);  //updating device status to cloud every 10 min 10*60000
    }

    if(dlDataFolderChkTimer->isActive()==false)
    {
        dlDataFolderChkTimer->start(DL_MQTT_ERROR_CHECK_TIME + 1000);   //check if device registration failed for DL
    }

    if(dlRestartTimer->isActive()==false)
    {
        dlRestartTimer->start(DL_MQTT_ERROR_CHECK_TIME);   //starting timer for 5 min so that if no data from DL endpoint, DL has to restart
    }

}


/**
 * @brief DeltaKController::keepAliveSlot
 */
void DeltaKController::keepAliveSlot()
{
    system("sudo touch /home/pi/Desktop/EdgeFirmware/miscfiles/qt_keepalive.txt");

}


/**
 * @brief DeltaKController::clearSysLogs
 */
void DeltaKController::clearSysLogs()
{
    system("sudo cp /var/log/syslog /home/pi/Desktop/syslog1");
    system("sudo /home/pi/Desktop/EdgeFirmware/clearsyslog");

    system("sudo rm -rf /home/pi/Desktop/QR/*");        //clearing existing qr images on filesystem

}


/**
 * @brief DeltaKController::clearOtherLogs
 */
void DeltaKController::clearOtherLogs()
{//qDebug()<<__LINE__;

    system("sudo /home/pi/Desktop/EdgeFirmware/clearotherlog");
    system("sudo rm -rf /home/pi/Desktop/QR/*");        //clearing existing qr images on filesystem

}


/**
 * @brief DeltaKController::updateDeviceStatusSlot
 */
void DeltaKController::updateDeviceStatusSlot()
{
    if(updDevStatusInProgress==false)
    {
        QtConcurrent::run(this,&DeltaKController::updateDeviceStatus);qDebug()<<"Called UpdDevStat: "<<__LINE__;
    }
    else
    {
        qDebug()<<"Previous updateDeviceStatus still in progress !!!";
    }
}


/**
 * @brief DeltaKController::updateDeviceStatus
 */

void DeltaKController::updateDeviceStatus()
{
    updDevStatusInProgress = true;

    char idChar[30];

    //checking for 1-1 heartbeat
    QFuture<QString> htBtIdSent;
    int heartBeatLoopCnt=0;
    int sendHBretry = SENDHBRETRY;
    bool sent = true;
    bool obtainedLock = false;
    bool trylock = false;
    srand(time(NULL));
    int hbfalsedur = (rand() % 33) + 3;
    bool heartBtStatFlgUpdDevSt = false;

    if(loggingFlag==false)      //check for heartbeat only if internet connection available
    {
        do
        {
            heartBeatLoopCnt=0;
            do
            {
                if(trylock==false)
                {
                    do {
                        trylock = heartBeatLock.tryLock(3);
                    } while ((trylock == false) && (--hbfalsedur>0));
                    if (trylock == true)
                    {

                        QCoreApplication::processEvents(QEventLoop::AllEvents);
                        heartBeatStatusFlag = false;
                        sent = false;
                        obtainedLock = true;
                        qDebug()<<"Inside updateDeviceStatus: "<<hbfalsedur;
                        heartBeatLoopCnt = 0;
                        sendHBretry = SENDHBRETRY;

                    }
                    else
                    {
                        srand(time(NULL));
                        hbfalsedur = (rand() % 33) + 3;
                    }
                }
                if(obtainedLock==false)
                {
                    qDebug()<<"Waiting for Lock in updateDeviceStatus "<< QString::number(SENDHBRETRY-sendHBretry)<< " " << QString::number(heartBeatLoopCnt);
                }
                if(!sent)
                {
                    sent = true;
                    htBtIdSent = QtConcurrent::run(this,&DeltaKController::sendHeartBeatCommand);
                    htBtVect.append(htBtIdSent);
                }
                QCoreApplication::processEvents(QEventLoop::AllEvents);

                QThread::msleep(500);

                if(obtainedLock == true)
                {
                    if (htBtVect.contains(htBtIdRecd) && (htBtIdSent == htBtIdRecd))
                    {
                        heartBtStatFlgUpdDevSt = heartBeatStatusFlag;
                        if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                        {
                            htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                        }
                    }
                }

            }while((++heartBeatLoopCnt<10) && heartBtStatFlgUpdDevSt==false);

            if (obtainedLock)
            {
                if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                {
                    htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                }
                sent = false;
            }

        }while((--sendHBretry > 0) && (heartBtStatFlgUpdDevSt==false));


        if(obtainedLock==true)
        {
            qDebug()<<"Breaked HtBt Loop In updateDeviceStatus with OuterCnt: "<<QString::number(SENDHBRETRY - sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgUpdDevSt << ":"<<htBtIdSent <<":"<<htBtIdRecd << ": HtBt Vector Count: "<<htBtVect.count();
            heartBeatLock.unlock();
        }
        else if (obtainedLock==false)
        {
            qDebug()<<"Timed Out! Breaked HtBt Loop In updateDeviceStatus with OuterCnt: "<<QString::number(SENDHBRETRY - sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgUpdDevSt;
        }
    }
    else
    {
        qDebug()<<"Network is down., Not checking for heartbeat inside updateDeviceStatus";
    }


    if(heartBtStatFlgUpdDevSt==true && loggingFlag==false)
    {

        memset(currentModeChar,'\0',sizeof(currentModeChar));
        memset(currentTempChar,'\0',sizeof(currentTempChar));
        memset(highTempAlarmChar,'\0',sizeof(highTempAlarmChar));
        memset(lowTempAlarmChar,'\0',sizeof(lowTempAlarmChar));
        memset(battLowChar,'\0',sizeof(battLowChar));
        memset(acPowerFailChar,'\0',sizeof(acPowerFailChar));
        memset(idChar,'\0',sizeof(idChar));

        currentTempArray = rtdTempVal.toLocal8Bit();
        sprintf(currentTempChar,"%s",currentTempArray.data());

        //setting status of high temp alarm
        if(highTempTextDispFlag==true)      //...highTempAlarmFlag
        {
            strcpy(highTempAlarmChar,"Yes");
        }
        else if(highTempTextDispFlag==false)
        {
            strcpy(highTempAlarmChar,"No");
        }

        //setting status of low temp alarm
        if(lowTempTextDispFlag==true)          //...lowTempAlarmFlag
        {
            strcpy(lowTempAlarmChar,"Yes");
        }
        else if(lowTempTextDispFlag==false)
        {
            strcpy(lowTempAlarmChar,"No");
        }

        strcpy(battLowChar,"No");   //since no battery was there in current design., setting this by default


        strcpy(acPowerFailChar,"No");   //since no battery was there in current design., setting this by default

        //setting status of current mode
        if(currentMode==1)
        {
            //"NORMAL";
            strcpy(currentModeChar,"NORMAL");
        }
    //    else if(currentMode==2)
    //    {
    //        //"LOGGING";
    //        strcpy(currentModeChar,"LOGGING");
    //    }
        else if(currentMode==3)
        {
             //"ALARM";
             strcpy(currentModeChar,"ALARMING");
        }
        else if(currentMode==4)
        {
             //"BATTERY";
             strcpy(currentModeChar,"BATTERY");
        }
        else if(currentMode==10)
        {
             //"WARNING";
             strcpy(currentModeChar,"WARNING");
        }

        memset(updateDevStatusBuffer,'\0',sizeof(updateDevStatusBuffer));

        if(generateRandInProgress==false)
        {
            sprintf(idChar,"%s",generateRandNumber().toLocal8Bit().data());
        }
        else
        {
            strcpy(idChar,"12");
        }


        if(currentMode==3)
        {
            sprintf(updateDevStatusBuffer,"{\"request\": \"updatedevicestatus\", \"id\": \"%s\", \"status\": \"ALARM\", \"deviceStatus\": \"%s\", \"currentTemp\": \"%s\", \"currentAlarms\": { \"highTemp\": \"%s\", \"lowTemp\": \"%s\", \"batteryLow\": \"%s\", \"ACPowerFailure\": \"%s\" } }",idChar,currentModeChar,currentTempChar,highTempAlarmChar,lowTempAlarmChar,battLowChar,acPowerFailChar);
        }
        else
        {
            sprintf(updateDevStatusBuffer,"{\"request\": \"updatedevicestatus\", \"id\": \"%s\", \"status\": \"%s\", \"deviceStatus\": \"%s\", \"currentTemp\": \"%s\", \"currentAlarms\": { \"highTemp\": \"%s\", \"lowTemp\": \"%s\", \"batteryLow\": \"%s\", \"ACPowerFailure\": \"%s\" } }",idChar,currentModeChar,currentModeChar,currentTempChar,highTempAlarmChar,lowTempAlarmChar,battLowChar,acPowerFailChar);
        }

        headersDevStatus = NULL;
        headersDevStatus = curl_slist_append(headersDevStatus, "Accept: application/json");
        headersDevStatus = curl_slist_append(headersDevStatus, "Content-Type: application/json");
        headersDevStatus = curl_slist_append(headersDevStatus, "charsets: utf-8");


        if(curlDevStatus)
        {
            curl_easy_setopt(curlDevStatus, CURLOPT_URL, url);
            curl_easy_setopt(curlDevStatus, CURLOPT_HTTPHEADER, headersDevStatus);

            #ifdef  DEBUG_ON
            qDebug()<<"String to Send to DL for updateDevStatusBuffer is :"<<updateDevStatusBuffer <<endl << QString(updateDevStatusBuffer).size() ;
            #endif  //DEBUG_ON

            curl_easy_setopt(curlDevStatus, CURLOPT_POSTFIELDS, updateDevStatusBuffer);
        }

        curl_easy_setopt(curlDevStatus, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

         curl_easy_setopt(curlDevStatus, CURLOPT_HEADER,0L);

        retryCntDevStatus=0;
        do
        {
            QCoreApplication::processEvents(QEventLoop::AllEvents);

            res = curl_easy_perform(curlDevStatus);

            http_code = 0;

            curl_easy_getinfo (curlDevStatus, CURLINFO_RESPONSE_CODE, &http_code);

            retryCntDevStatus++;

            delay(2);

        }while(http_code!=200 && retryCntDevStatus<5);

        #ifdef  DEBUG_ON
            qDebug()<<"UPDATE DEVICE STATUS CMD Output is "<<QString::number(res);
            qDebug()<<"UPDATE DEVICE STATUS CMD to Cloud Post Response: "<<QString::number(http_code);
        #endif  //DEBUG_ON

    }

    updDevStatusInProgress = false;

}


/**
 * @brief DeltaKController::sendTelemetryData
 * @return
 */
void DeltaKController::sendTelemetryData()
{
    telemetryInProgress = true;


    //need this buffer to be filled here itself since in case of network outage, this should be logged
    memset(rtdTempValChar,'\0',sizeof(rtdTempValChar));
    memset(telemetryBuffer,'\0',sizeof(telemetryBuffer));
    memset(timeinUTCChar,'\0',sizeof(timeinUTCChar));
    memset(idCharTele,'\0',sizeof(idCharTele));

    sprintf(rtdTempValChar,"%s",rtdTempVal.toLocal8Bit().data());
    sprintf(timeinUTCChar,"%s",QDateTime::currentDateTime().toTimeSpec(Qt::OffsetFromUTC).toString(Qt::ISODate).toLocal8Bit().data());
    if(generateRandInProgress==false)
    {
        sprintf(idCharTele,"%s",generateRandNumber().toLocal8Bit().data());
    }
    else
    {
        static uint backUpMsgId = 0;
        if(backUpMsgId>2147483647)
        {
            backUpMsgId = 1;
        }

        sprintf(idCharTele,"%d",backUpMsgId++);
    }

    sprintf(telemetryBuffer,"{\"request\":\"senddata\",\"datatype\":\"telemetry\",\"id\":\"%s\",\"timestamp\":\"%s\",\"temp-se1\":\"%s\"}",idCharTele,timeinUTCChar,rtdTempValChar);

    if(loggingFlag==false)  //sending telemetry irrespective of heartbeat status
    {
        headersTelemetry = NULL;
        headersTelemetry = curl_slist_append(headersTelemetry, "Accept: application/json");
        headersTelemetry = curl_slist_append(headersTelemetry, "Content-Type: application/json");
        headersTelemetry = curl_slist_append(headersTelemetry, "charsets: utf-8");


        if(curlTelemetry)
        {
            curl_easy_setopt(curlTelemetry, CURLOPT_URL, url);
            curl_easy_setopt(curlTelemetry, CURLOPT_HTTPHEADER, headersTelemetry);

            #ifdef  DEBUG_ON
            qDebug()<<"String to Send to DL for telemetryBuffer is :"<<telemetryBuffer ;
            #endif  //DEBUG_ON

            curl_easy_setopt(curlTelemetry, CURLOPT_POSTFIELDS, telemetryBuffer);

        }

        curl_easy_setopt(curlTelemetry, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

        curl_easy_setopt(curlTelemetry, CURLOPT_HEADER,0L);


        retryCntSendTele=0;
        do
        {
            delay(2);

            QCoreApplication::processEvents(QEventLoop::AllEvents);

            res = curl_easy_perform(curlTelemetry);

            http_code = 0;

            curl_easy_getinfo (curlTelemetry, CURLINFO_RESPONSE_CODE, &http_code);

            retryCntSendTele++;


        }while(http_code!=200 && retryCntSendTele<5);

        #ifdef  DEBUG_ON
        qDebug()<<"Telemetry Output is "<<QString::number(res);
        qDebug()<<"Telemetry Command to Cloud Post Response: "<<QString::number(http_code);
        #endif  //DEBUG_ON

        if(http_code==200)    //data send to deep laser successfully., now need to keep monitor this data for dispatch request
        {
            teleFailRespCnt=0;
//            if(outageSendInProgress==true)  //need to check for more records, since buffer is update on two locations
//            {
//                teleBkpBuffSizeChk=22528;
//            }
//            else if(outageSendInProgress==false)
//            {
//                teleBkpBuffSizeChk=1024;
//            }

            teleBkpBuffSizeChk = 1024;

            if(strlen(telemetryBkpBuffer)<teleBkpBuffSizeChk) //waiting for max of 10 or 70 records storing time to check dispatch request
            {
                qDebug()<<"Updating record on to telemetryBkpBuffer " << QString::number(teleBkpBuffSizeChk) << "Current Buf Size: "<<QString::number(strlen(telemetryBkpBuffer));

                strcat(telemetryBuffer,";");
                strcat(telemetryBkpBuffer,telemetryBuffer);

                qDebug()<<"Record Updated: "<< telemetryBkpBuffer;
                //printf("\n\nRecord Updated: Current Buffer Size %d\n",strlen(telemetryBkpBuffer));
            }
            else    //buffer size exceeded max records i.e. no dispacth received., so need to log the data to sdcard
            {
                if(outageTeleWriteProgress==false)
                {

                    //start logging data into file
                    teleFpOn = fopen("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/telemetry.csv","a");

                    if(teleFpOn==NULL)
                    {
                        qDebug()<<"Unable to open telemetry.csv";
                    }
                    else
                    {

                        //logic to append the data in telemetryBkpBuffer onto telemetry.csv file
                        regTeleWriteProgress = true;
                        qDebug()<<"telemetryBkpBuffer size exceeded..," ;

                        //tokenWriteFile = strtok(telemetryBkpBuffer, ";");
                        tokenWriteFileList = QString::fromLocal8Bit(telemetryBkpBuffer).split(";");

                        //while (tokenWriteFile != NULL)
                        foreach(QString itemTelemetryWrite, tokenWriteFileList)
                        {

                            memset(dispatchErrorLogBuffReg,'\0',sizeof(dispatchErrorLogBuffReg));
                            strcpy(dispatchErrorLogBuffReg,itemTelemetryWrite.toLocal8Bit().data());
                            strcat(dispatchErrorLogBuffReg,";");

                            if(strlen(dispatchErrorLogBuffReg)>75)
                            {
                                fprintf(teleFpOn,"%s\n",dispatchErrorLogBuffReg);
                                qDebug("Data Written to telemetry file is %s\n",dispatchErrorLogBuffReg);
                                telemetryLogCnt++;

                            }
                        }


                        memset(telemetryBkpBuffer,'\0',sizeof(telemetryBkpBuffer));
                        strcat(telemetryBuffer,";");
                        strcat(telemetryBkpBuffer,telemetryBuffer);
                        regTeleWriteProgress = false;
    #ifdef  DEBUG_ON
                        qDebug()<<"Size Exceeded.., Resetted Update Buf: "<<telemetryBkpBuffer;
    #endif  //DEBUG_ON

                        fclose(teleFpOn);

                        if(outageDataSendTimer->isActive()==false && outageSendInProgress==false)   //DL process just restarted., need to send the logged data so far back to DL
                        {
                            QtConcurrent::run(this,&DeltaKController::startOutageSendSlot);
                        }
                        else
                        {
                            qDebug()<<"*** Already Outage Send in Progress.., So not initiating now" << __LINE__;
                        }
                    }

                }
            }
        }
        else        //network is present, but failed to send data to deep laser., so need to log data to sd card
        {
            logDataToSdCard();

            teleFailRespCnt++;  //to address cases where DL is not receiving any data from application

            if(teleFailRespCnt==5)  //obtained consecutive non 200 response to deeplaser post which means deep laser is not running or failing to receive data
            {
               teleFailRespCnt=0;
               if(dlRestartTimer->isActive()==false)    //if already this timer is active, it will take care to restart DL process., but this is needed in cases where the timer never started at all after boot up
               {
                   qDebug()<<"***** DL failed to receive telemetry data!!!  *****";
                   system("sudo systemctl restart tfsdl.service");

               }

            }
        }

    }

    else     //network failed or hearbeat failed or both failed
    {

        logDataToSdCard();

    }

    telemetryInProgress = false;

}

/**
 * @brief DeltaKController::telemetryConcurrentCmd
 */
void DeltaKController::telemetryConcurrentCmd()
{
    qDebug()<<"Telemetry Timer Fired "<<__LINE__;
    if(receivedRTD!=0)
    {
        if(telemetryInProgress==false)  //to avoid stacking up tele requests in case of hb failures
        {
            previousTeleInProgCnt=0;
            if(loggingFlag==false)  //network is on., so use qt concurrent
            {
                QtConcurrent::run(this,&DeltaKController::sendTelemetryData);
            }
            else    //network is down., no need for qt concurrent
            {
                sendTelemetryData();
            }

        }
        else    //add logic to retain the sample somehow
        {
            qDebug()<<"Previous Telemetry Send Still In Progress.., ";
            previousTeleInProgCnt++;
            if(previousTeleInProgCnt==5)    //for some reason the previous tele post never returns., then restart the dl
            {
                previousTeleInProgCnt=0;
                if(dlRestartTimer->isActive()==false)    //if already this timer is active, it will take care to restart DL process., but this is needed in cases where the timer never started at all after boot up
                {
                    qDebug()<<"***** Prev Tele Post Not Returned even after 5 min!!!  *****";
                    system("sudo systemctl restart tfsdl.service");
                }
            }
        }

    }

}


/**
 * @brief DeltaKController::sendHeartBeatCommand
 * @return
 */
QString DeltaKController::sendHeartBeatCommand()
{

    QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    heartBtCmdInProgress = true;

    headersHeartBt = NULL;

    memset(heartBeatBuffer,'\0',sizeof(heartBeatBuffer));
    memset(idChar,'\0',sizeof(idChar));

    htBtRandomId = generateRandNumber();

    sprintf(idChar,"%s",htBtRandomId.toLocal8Bit().data());

    sprintf(heartBeatBuffer,"{\"id\":\"%s\",\"request\":\"tfcheartbeat\"}",idChar);

    headersHeartBt = curl_slist_append(headersHeartBt, "Accept: application/json");
    headersHeartBt = curl_slist_append(headersHeartBt, "Content-Type: application/json");
    headersHeartBt = curl_slist_append(headersHeartBt, "charsets: utf-8");

    if(curlHeartBt)
    {
        curl_easy_setopt(curlHeartBt, CURLOPT_URL, url);
        curl_easy_setopt(curlHeartBt, CURLOPT_HTTPHEADER, headersHeartBt);

        #ifdef  DEBUG_ON
        qDebug()<<"String to Send to DL for tfcheartbeat is :"<<heartBeatBuffer ;
        #endif  //DEBUG_ON

        curl_easy_setopt(curlHeartBt, CURLOPT_POSTFIELDS, heartBeatBuffer);

    }

    curl_easy_setopt(curlHeartBt, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

    curl_easy_setopt(curlHeartBt, CURLOPT_HEADER,0L);

    retryCntSendHtBt=0;
    //do
    //{
         res = curl_easy_perform(curlHeartBt);

        http_code = 0;

        curl_easy_getinfo (curlHeartBt, CURLINFO_RESPONSE_CODE, &http_code);

        retryCntSendHtBt++;

//        delay(2);

    //}while(http_code!=200 && retryCntSendHtBt<5);

    #ifdef  DEBUG_ON
    qDebug()<<"TFCHearBeat Output is "<<QString::number(res)<<" "<<QString::number(retryCntSendHtBt);
    qDebug()<<"TFCHeartBeat Command to Cloud Post Response: "<<QString::number(http_code);
    #endif  //DEBUG_ON


    heartBtCmdInProgress = false;

    return htBtRandomId;
}


/**
 * @brief DeltaKController::updateDateTime
 */
void DeltaKController::updateDateTime()
{
    syncDateTimer->stop();

    //updating date time after network recovered so that if there is any drift in current time due to any reason time will be in sync again
    //system("sudo date -s \"$(wget -qSO- --max-redirect=0 www.thermofisher.com/cn/zh/home.html 2>&1 | grep Date: | cut -d' ' -f5-8)Z\"");
    //system("sudo hwclock -w");

    system("sudo /home/pi/Desktop/EdgeFirmware/setdatetime.sh &");

}



/**
 * @brief DeltaKController::sendNetworkOutageData
 */
void DeltaKController::sendNetworkOutageData()
{
    syncDateTimer->start(20000);
    outageDataSendTimer->stop();
    QtConcurrent::run(this,&DeltaKController::sendNetworkOutageDataConcurrent);
}


/**
 * @brief DeltaKController::sendNetworkOutageDataConcurrent
 */
void DeltaKController::sendNetworkOutageDataConcurrent()
{

    qDebug()<<"********** STARTED SENDING OUTAGE DATA **********";
    outageSendInProgress = true;

//    if(updDevStatusInProgress==false) //since we are sending updatedevstatus in nw monitor after unit recovers from logging., no need to send before sending logged data
//    {
//        QtConcurrent::run(this,&DeltaKController::updateDeviceStatus);
//    }


    //check file sizes
    headersOutageData = NULL;

    headersOutageData = curl_slist_append(headersOutageData, "Accept: application/json");
    headersOutageData = curl_slist_append(headersOutageData, "Content-Type: application/json");
    headersOutageData = curl_slist_append(headersOutageData, "charsets: utf-8");

    recoverySendFp = fopen("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/event.csv", "r");

    if(recoverySendFp==NULL)
    {
        qDebug()<<"Unable to open event.csv file for reading";
    }
    else
    {
        memset(nwRecoveryBuffer,'\0',sizeof(nwRecoveryBuffer));

        while (fgets(nwRecoveryBuffer, 512, recoverySendFp) )
        {
            QCoreApplication::processEvents(QEventLoop::AllEvents, 100);

            if(loggingFlag==false)
            {
                recoveryCounter++;

                nwRecoveryBuffer[strlen(nwRecoveryBuffer)-2]='\0';

                #ifdef  DEBUG_ON
                qDebug()<<"Event Outage Data to be send is "<< QString::fromLocal8Bit(nwRecoveryBuffer);
                #endif  //DEBUG_ON

                if(curlOutageData)
                {

                    curl_easy_setopt(curlOutageData, CURLOPT_URL, url);

                    curl_easy_setopt(curlOutageData, CURLOPT_HTTPHEADER, headersOutageData);

                    curl_easy_setopt(curlOutageData, CURLOPT_POSTFIELDS, nwRecoveryBuffer);
                }

                curl_easy_setopt(curlOutageData, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

                curl_easy_setopt(curlOutageData, CURLOPT_HEADER,0L);

                retryCnt = 0;
                do
                {
                    res = curl_easy_perform(curlOutageData);

                    http_code = 0;

                    curl_easy_getinfo (curlOutageData, CURLINFO_RESPONSE_CODE, &http_code);

                    retryCnt++;


                }while(http_code!=200 && retryCnt<5);

                #ifdef  DEBUG_ON
                qDebug()<<"Event Outage Send Output is "<<QString::number(res);
                qDebug()<<"Event Outage Command to DeepLaser Post Response: "<<QString::number(http_code);
                #endif  //DEBUG_ON

                memset(nwRecoveryBuffer,'\0',sizeof(nwRecoveryBuffer));

                if(http_code!=200 || apModeInProgress==true)  //unable to send outage record even after 5 attempts.., so breaking out
                {
                    qDebug()<<"Not sending back the logged event data...breaking out";
                    break;
                }
            }
            else
            {
                qDebug()<<"Not sending back the logged event data...breaking out";
                break;
            }

        }

        fclose(recoverySendFp);
#ifdef  DEBUG_ON
        qDebug()<<"Total No Event Outage Records Sent : "<<QString::number(recoveryCounter);
#endif  //DEBUG_ON
        if(recoveryCounter==eventCnt + event_LineCnt)
        {
            eventCnt = 0;

            event_LineCnt = 0;

            //deleting the outage files since they have been already send back to deeplaser
            system("yes | rm -rf /home/pi/Desktop/EdgeFirmware/heartbtfaildata/event.csv");
        }

        recoveryCounter = 0;
    }

    //sending snooze events
    recoverySendFp = fopen("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/snoozeevent.csv", "r");

    if(recoverySendFp==NULL)
    {
        qDebug()<<"Unable to open snoozeevent.csv file for reading";
    }
    else
    {
        memset(nwRecoveryBuffer,'\0',sizeof(nwRecoveryBuffer));

        while (fgets(nwRecoveryBuffer, 512, recoverySendFp) )
        {
            QCoreApplication::processEvents(QEventLoop::AllEvents, 100);

            //if(loggingFlag==false && heartBtStatFlgOutageData==true)
            if(loggingFlag==false)
            {
                recoveryCounter++;

                nwRecoveryBuffer[strlen(nwRecoveryBuffer)-2]='\0';

                #ifdef  DEBUG_ON
                qDebug()<<"Snooze Outage Data to be send is "<< QString::fromLocal8Bit(nwRecoveryBuffer);
                #endif  //DEBUG_ON

                if(curlOutageData)
                {

                    curl_easy_setopt(curlOutageData, CURLOPT_URL, url);

                    curl_easy_setopt(curlOutageData, CURLOPT_HTTPHEADER, headersOutageData);

                    curl_easy_setopt(curlOutageData, CURLOPT_POSTFIELDS, nwRecoveryBuffer);
                }

                curl_easy_setopt(curlOutageData, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

                curl_easy_setopt(curlOutageData, CURLOPT_HEADER,0L);

                retryCnt = 0;
                do
                {
                    res = curl_easy_perform(curlOutageData);

                    http_code = 0;

                    curl_easy_getinfo (curlOutageData, CURLINFO_RESPONSE_CODE, &http_code);

                    retryCnt++;

                }while(http_code!=200 && retryCnt<5);



                #ifdef  DEBUG_ON
                qDebug()<<"Snooze Outage Send Output is "<<QString::number(res);
                qDebug()<<"Snooze Outage Command to DeepLaser Post Response: "<<QString::number(http_code);
                #endif  //DEBUG_ON


                memset(nwRecoveryBuffer,'\0',sizeof(nwRecoveryBuffer));

                if(http_code!=200 || apModeInProgress==true)  //unable to send outage record even after 5 attempts.., so breaking out
                {
                    qDebug()<<"Not sending back the logged snooze data...breaking out";
                    break;
                }

            }
            else
            {
                qDebug()<<"Not sending back the logged snooze data...breaking out";
                break;
            }
        }

        fclose(recoverySendFp);
#ifdef  DEBUG_ON
        qDebug()<<"Total No Snooze Outage Records Sent is: "<<QString::number(recoveryCounter);
#endif  //DEBUG_ON
        if(recoveryCounter==snoozeEventCnt + snoozeEvent_LineCnt)
        {
            snoozeEventCnt = 0;

            snoozeEvent_LineCnt = 0;

            //deleting the outage files since they have been already send back to deeplaser
            system("yes | rm -rf /home/pi/Desktop/EdgeFirmware/heartbtfaildata/snoozeevent.csv");
        }

        recoveryCounter = 0;
    }



    //send recovery events if any
    recoverySendFp = fopen("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/recoveryevent.csv", "r");

    if(recoverySendFp==NULL)
    {
        qDebug()<<"Unable to open recoveryevent.csv file for reading";
    }
    else
    {

        memset(nwRecoveryBuffer,'\0',sizeof(nwRecoveryBuffer));

        while (fgets(nwRecoveryBuffer, 512, recoverySendFp) )
        {
            QCoreApplication::processEvents(QEventLoop::AllEvents, 100);

            if(loggingFlag==false)
            {
                recoveryCounter++;

                nwRecoveryBuffer[strlen(nwRecoveryBuffer)-2]='\0';

                #ifdef  DEBUG_ON
                qDebug()<<"Recovery Outage Data to be send is "<< QString::fromLocal8Bit(nwRecoveryBuffer);
                #endif  //DEBUG_ON

                if(curlOutageData)
                {

                    curl_easy_setopt(curlOutageData, CURLOPT_URL, url);

                    curl_easy_setopt(curlOutageData, CURLOPT_HTTPHEADER, headersOutageData);

                    curl_easy_setopt(curlOutageData, CURLOPT_POSTFIELDS, nwRecoveryBuffer);
                }

                curl_easy_setopt(curlOutageData, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

                curl_easy_setopt(curlOutageData, CURLOPT_HEADER,0L);
                retryCnt = 0;
                do
                {
                    res = curl_easy_perform(curlOutageData);

                    http_code = 0;

                    curl_easy_getinfo (curlOutageData, CURLINFO_RESPONSE_CODE, &http_code);

                    retryCnt++;

                }while(http_code!=200 && retryCnt<5);


                #ifdef  DEBUG_ON
                qDebug()<<"Recovery Outage Send Output is "<<QString::number(res);
                qDebug()<<"Recovery Outage Command to DeepLaser Post Response: "<<QString::number(http_code);
                #endif  //DEBUG_ON

                memset(nwRecoveryBuffer,'\0',sizeof(nwRecoveryBuffer));

                if(http_code!=200 || apModeInProgress==true)  //unable to send outage record even after 5 attempts.., so breaking out
                {
                    qDebug()<<"Not sending back the logged recoveryevent data...breaking out";
                    break;
                }
            }
            else
            {
                qDebug()<<"Not sending back the logged recoveryevent data...breaking out";
                break;
            }

        }

        fclose(recoverySendFp);
#ifdef  DEBUG_ON
        qDebug()<<"Total No Recovery Outage Records Sent is: "<<QString::number(recoveryCounter);
#endif  //DEBUG_ON
        if(recoveryCounter==recoveryEvtCnt + recoveryEvent_LineCnt)
        {
            recoveryEvtCnt = 0;

            recoveryEvent_LineCnt = 0;

            //deleting the outage files since they have been already send back to deeplaser
            system("yes | rm -rf /home/pi/Desktop/EdgeFirmware/heartbtfaildata/recoveryevent.csv");

        }

        recoveryCounter = 0;
    }


    recoverySendFp = fopen("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/telemetry.csv", "r");

    if(recoverySendFp==NULL)
    {
        qDebug()<<"Unable to open telemetry.csv file for reading";
    }
    else
    {


        memset(nwRecoveryBuffer,'\0',sizeof(nwRecoveryBuffer));

        while (fgets(nwRecoveryBuffer, 512, recoverySendFp) )
        {
            QCoreApplication::processEvents(QEventLoop::AllEvents, 100);

            if(loggingFlag==false)
             {
                 recoveryCounter++;

                 nwRecoveryBuffer[strlen(nwRecoveryBuffer)-2]='\0';

                 qDebug()<<"Telemetry Outage Data to be send is "<< QString::fromLocal8Bit(nwRecoveryBuffer);


                if(curlOutageData)
                {

                    curl_easy_setopt(curlOutageData, CURLOPT_URL, url);

                    curl_easy_setopt(curlOutageData, CURLOPT_HTTPHEADER, headersOutageData);

                    curl_easy_setopt(curlOutageData, CURLOPT_POSTFIELDS, nwRecoveryBuffer);
                }

                curl_easy_setopt(curlOutageData, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

                curl_easy_setopt(curlOutageData, CURLOPT_HEADER,0L);

                retryCnt = 0;
                do
                {
                    res = curl_easy_perform(curlOutageData);

                    http_code = 0;

                    curl_easy_getinfo (curlOutageData, CURLINFO_RESPONSE_CODE, &http_code);

                    retryCnt++;

                }while(http_code!=200 && retryCnt<5);

                if(http_code!=200 || apModeInProgress==true)  //unable to send outage record even after 5 attempts.., so breaking out
                {
                    qDebug()<<"Not sending back the logged telemetry data...breaking out";
                    break;
                }
//                else if(http_code==200)        //record send to deep laser succesfully., so start monitoring
//                {

//                    if(strlen(telemetryBkpBuffer)<22528) //waiting for max of 30 records storing time to check dispatch request
//                    {
//                        qDebug()<<"Updating record on to telemetryBkpBuffer-HtBtFailData" ;

//                        strcat(nwRecoveryBuffer,";");
//                        strcat(telemetryBkpBuffer,nwRecoveryBuffer);
//                        //printf("\n\nRecord Updated:(HtBtFailData) %s\n",telemetryBkpBuffer);
//                        printf("\n\nRecord Updated:(HtBtFailData) Current Buffer Size %d\n",strlen(telemetryBkpBuffer));
//                    }
//                    else    //buffer size exceeded max records i.e. no dispacth received., so need to log the data to sdcard
//                    {
//                        //logic to append the data in telemetryBkpBuffer onto telemetry-htbt.csv file

//                        qDebug()<<"telemetryBkpBuffer size exceeded while sending outagedata..," ;

//                        tokenWriteFileOutageData = strtok(telemetryBkpBuffer, ";");

//                        while (tokenWriteFileOutageData != NULL)
//                        {

//                            //printf("%s\n", tokenWriteFile);

//                            memset(dispatchErrorLogBuff,'\0',sizeof(dispatchErrorLogBuff));
//                            strcpy(dispatchErrorLogBuff,"echo '");
//                            strcat(dispatchErrorLogBuff,tokenWriteFileOutageData);
//                            strcat(dispatchErrorLogBuff,";'");
//                            strcat(dispatchErrorLogBuff," >> /home/pi/Desktop/EdgeFirmware/heartbtfaildata/telemetry-htbt.csv");

//                            if(strlen(dispatchErrorLogBuff)>100)
//                            {
//                                system(dispatchErrorLogBuff);
//                                qDebug("Data Written to telemetry file (HtBtFailData) is %s\n",dispatchErrorLogBuff);
//                                telemetryHtBtLogCnt++;

//                            }

//                            tokenWriteFileOutageData = strtok(NULL, ";");

//                        }


//                        memset(telemetryBkpBuffer,'\0',sizeof(telemetryBkpBuffer));

//                        strcat(nwRecoveryBuffer,";");
//                        strcat(telemetryBkpBuffer,nwRecoveryBuffer);


//                    }

//                }



                #ifdef  DEBUG_ON
                qDebug()<<"Telemetry Outage Send Output is "<<QString::number(res);
                qDebug()<<"Telemetry Outage Command to DeepLaser Post Response: "<<QString::number(http_code);
                #endif  //DEBUG_ON

                memset(nwRecoveryBuffer,'\0',sizeof(nwRecoveryBuffer));


             }
             else
             {
                 qDebug()<<"Not sending back the logged telemetry data...breaking out";
                 break;
             }

        }

        fclose(recoverySendFp);
#ifdef  DEBUG_ON
        qDebug()<<"<----->Total No Telemetery Outage Records Sent is: "<<QString::number(recoveryCounter);
        qDebug()<<"<----->Total No Telemetery Queued Records : "<<QString::number(telemetryLogCnt);
        qDebug()<<"<----->Total No Default Telemetery Outage Records Sent is: "<<QString::number(telemetry_LineCnt);
#endif  //DEBUG_ON

        if(recoveryCounter==telemetryLogCnt + telemetry_LineCnt)
        {
            telemetryLogCnt = 0; logSDCardStarted = false;

            telemetry_LineCnt = 0;

            //deleting the outage files since they have been already send back to deeplaser
            system("yes | rm -rf /home/pi/Desktop/EdgeFirmware/heartbtfaildata/telemetry.csv");

        }

        recoveryCounter = 0;

    }

    recoverySendFp = fopen("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/telemetry-htbt.csv", "r");

    if(recoverySendFp==NULL)
    {
        qDebug()<<"Unable to open telemetry-htbt.csv file for reading";
    }
    else
    {

        memset(nwRecoveryBuffer,'\0',sizeof(nwRecoveryBuffer));

        while (fgets(nwRecoveryBuffer, 512, recoverySendFp) )
        {
            QCoreApplication::processEvents(QEventLoop::AllEvents, 100);

            if(loggingFlag==false)
             {
                 recoveryCounter++;

                 nwRecoveryBuffer[strlen(nwRecoveryBuffer)-2]='\0';

                 qDebug()<<"Telemetry Outage Data to be send is "<< QString::fromLocal8Bit(nwRecoveryBuffer);


                if(curlOutageData)
                {
                    curl_easy_setopt(curlOutageData, CURLOPT_URL, url);

                    curl_easy_setopt(curlOutageData, CURLOPT_HTTPHEADER, headersOutageData);

                    curl_easy_setopt(curlOutageData, CURLOPT_POSTFIELDS, nwRecoveryBuffer);
                }

                curl_easy_setopt(curlOutageData, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

                curl_easy_setopt(curlOutageData, CURLOPT_HEADER,0L);

                retryCnt = 0;
                do
                {
                    res = curl_easy_perform(curlOutageData);

                    http_code = 0;

                    curl_easy_getinfo (curlOutageData, CURLINFO_RESPONSE_CODE, &http_code);

                    retryCnt++;

                }while(http_code!=200 && retryCnt<5);

                if(http_code!=200 || apModeInProgress==true)  //unable to send outage record even after 5 attempts.., so breaking out
                {
                    qDebug()<<"Not sending back the logged telemetry-htbt data...breaking out";
                    break;
                }
//                else if(http_code==200)       //record send to deep laser succesfully., so start monitoring
//                {

//                    if(strlen(telemetryBkpBuffer)<22528) //waiting for max of 30 records storing time to check dispatch request
//                    {
//                        qDebug()<<"Updating record on to telemetryBkpBuffer-HtBtFailData (htbt.csv)" ;

//                        strcat(nwRecoveryBuffer,";");
//                        strcat(telemetryBkpBuffer,nwRecoveryBuffer);
//                        //printf("\n\nRecord Updated:(HtBtFailData csv) %s\n",telemetryBkpBuffer);
//                        printf("\n\nRecord Updated:(HtBtFailData csv) Current Buffer Size %d\n",strlen(telemetryBkpBuffer));
//                    }
//                    else    //buffer size exceeded max records i.e. no dispacth received., so need to log the data to sdcard
//                    {
//                        if(regTeleWriteProgress==false)
//                        {
//                            //logic to append the data in telemetryBkpBuffer onto telemetry.csv file
//                            outageTeleWriteProgress = true;
//                            qDebug()<<"telemetryBkpBuffer size exceeded in telemetry-htbt.csv..," ;

//                            tokenWriteFileOutageData = strtok(telemetryBkpBuffer, ";");

//                            while (tokenWriteFileOutageData != NULL)
//                            {

//                                //printf("%s\n", tokenWriteFile);


//                                memset(dispatchErrorLogBuff,'\0',sizeof(dispatchErrorLogBuff));
//                                strcpy(dispatchErrorLogBuff,"echo '");
//                                strcat(dispatchErrorLogBuff,tokenWriteFileOutageData);
//                                strcat(dispatchErrorLogBuff,";'");
//                                strcat(dispatchErrorLogBuff," >> /home/pi/Desktop/EdgeFirmware/heartbtfaildata/telemetry.csv");

//                                if(strlen(dispatchErrorLogBuff)>100)
//                                {
//                                    system(dispatchErrorLogBuff);
//                                    qDebug("Data Written to telemetry file (HtBtFailData) is %s\n",dispatchErrorLogBuff);
//                                    telemetryLogCnt++;



//                                }
//                                tokenWriteFileOutageData = strtok(NULL, ";");
//                            }


//                            memset(telemetryBkpBuffer,'\0',sizeof(telemetryBkpBuffer));
//                            strcat(nwRecoveryBuffer,";");
//                            strcat(telemetryBkpBuffer,nwRecoveryBuffer);

//                            outageTeleWriteProgress = false;
//                            printf("Size Exceeded.., Resetted Update Buf(htbt.csv)\n");
//                        }
//                        else
//                        {
//                            qDebug()<<"HIT THE CONDITION OF BOTH REGULAR BACK UP WRITING TO CSV FILE";
//                        }
//                    }

//                }


                #ifdef  DEBUG_ON
                qDebug()<<"Telemetry Outage Send Output is (htbt.csv)"<<QString::number(res);
                qDebug()<<"Telemetry Outage Command to DeepLaser Post Response(htbt.csv): "<<QString::number(http_code);
                #endif  //DEBUG_ON

                memset(nwRecoveryBuffer,'\0',sizeof(nwRecoveryBuffer));

             }
             else
             {
                 qDebug()<<"Not sending back the logged telemetry data (htbt.csv)...breaking out";
                 break;
             }

        }

        fclose(recoverySendFp);
#ifdef  DEBUG_ON
        qDebug()<<"<----->Total No Telemetery Outage Records Sent is (htbt.csv): "<<QString::number(recoveryCounter);
        qDebug()<<"<----->Total No Telemetery Queued Records (htbt.csv): "<<QString::number(telemetryHtBtLogCnt);
        qDebug()<<"<----->Total No Default Telemetery Outage Records Sent is (htbt.csv): "<<QString::number(telemetry_HtBtLineCnt);
#endif  //DEBUG_ON

        if(recoveryCounter==telemetryHtBtLogCnt + telemetry_HtBtLineCnt)
        {
            telemetryHtBtLogCnt = 0;

            telemetry_HtBtLineCnt = 0;

            //deleting the outage files since they have been already send back to deeplaser
            system("yes | rm -rf /home/pi/Desktop/EdgeFirmware/heartbtfaildata/telemetry-htbt.csv");

        }

        recoveryCounter = 0;

    }

    outageSendInProgress = false;


}

/**
 * @brief DeltaKController::copyPath
 * @param sourceDir
 * @param destinationDir
 * @param overWriteDirectory
 * @return
 */

bool DeltaKController::copyPath(QString sourceDir, QString destinationDir, bool overWriteDirectory)
{
    QDir originDirectory(sourceDir);

    if (! originDirectory.exists())
    {
        return false;
    }

    QDir destinationDirectory(destinationDir);

    if(destinationDirectory.exists() && !overWriteDirectory)
    {
        return false;
    }
    else if(destinationDirectory.exists() && overWriteDirectory)
    {
        destinationDirectory.removeRecursively();
    }

    originDirectory.mkpath(destinationDir);

    foreach (QString directoryName, originDirectory.entryList(QDir::Dirs | \
                                                              QDir::NoDotAndDotDot))
    {
        QString destinationPath = destinationDir + "/" + directoryName;
        originDirectory.mkpath(destinationPath);
        copyPath(sourceDir + "/" + directoryName, destinationPath, overWriteDirectory);
    }

    foreach (QString fileName, originDirectory.entryList(QDir::Files))
    {
        QFile::copy(sourceDir + "/" + fileName, destinationDir + "/" + fileName);
    }

    /*! Possible race-condition mitigation? */
    QDir finalDestination(destinationDir);
    finalDestination.refresh();

    if(finalDestination.exists())
    {
        return true;
    }

    return false;
}


/**
 * @brief DeltaKController::sendFwVerToCloud
 * @return
 */
void DeltaKController::sendFwVerToCloud()
{

    readUTCTimenow();


    //checking for 1-1 heartbeat
    QFuture<QString> htBtIdSent;
    int heartBeatLoopCnt=0;
    int sendHBretry = SENDHBRETRY;
    bool sent = true;
    bool obtainedLock = false;
    bool trylock = false;
    srand(time(NULL));
    int hbfalsedur = (rand() % 33) + 3;
    bool heartBtStatFlgFwVer = false;

    if(loggingFlag==false)      //check for heartbeat only if internet connection available
    {
        do
        {
            heartBeatLoopCnt=0;
            do
            {
                if(trylock==false)
                {
                    do {
                        trylock = heartBeatLock.tryLock(3);
                    } while ((trylock == false) && (--hbfalsedur>0));
                    if (trylock == true)
                    {

                        QCoreApplication::processEvents(QEventLoop::AllEvents);
                        heartBeatStatusFlag = false;
                        sent = false;
                        obtainedLock = true;
                        qDebug()<<"Inside sendFwVerToCloud: "<<hbfalsedur;
                        heartBeatLoopCnt = 0;
                        sendHBretry = SENDHBRETRY;

                    }
                    else
                    {
                        srand(time(NULL));
                        hbfalsedur = (rand() % 33) + 3;
                    }
                }
                if(obtainedLock==false)
                {
                    qDebug()<<"Waiting for Lock in sendFwVerToCloud "<< QString::number(SENDHBRETRY-sendHBretry)<< " " << QString::number(heartBeatLoopCnt);
                }
                if(!sent)
                {
                    sent = true;
                    htBtIdSent = QtConcurrent::run(this,&DeltaKController::sendHeartBeatCommand);
                    htBtVect.append(htBtIdSent);
                }
                QCoreApplication::processEvents(QEventLoop::AllEvents);

                QThread::msleep(500);

                if(obtainedLock == true)
                {
                    if (htBtVect.contains(htBtIdRecd) && (htBtIdSent == htBtIdRecd))
                    {
                        heartBtStatFlgFwVer = heartBeatStatusFlag;
                        if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                        {
                            htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                        }
                    }
                }

            }while((++heartBeatLoopCnt<10) && heartBtStatFlgFwVer==false);

            if (obtainedLock)
            {
                if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                {
                    htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                }
                sent = false;
            }

        }while((--sendHBretry > 0) && (heartBtStatFlgFwVer==false));


        if(obtainedLock==true)
        {
            qDebug()<<"Breaked HtBt Loop In sendFwVerToCloud with OuterCnt: "<<QString::number(SENDHBRETRY - sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgFwVer << ":"<<htBtIdSent <<":"<<htBtIdRecd << ": HtBt Vector Count: "<<htBtVect.count();
            heartBeatLock.unlock();
        }
        else if(obtainedLock==false)
        {
            qDebug()<<"Timed Out! Breaked HtBt Loop In sendFwVerToCloud with OuterCnt: "<<QString::number(SENDHBRETRY - sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgFwVer ;
        }
    }
    else
    {
        qDebug()<<"Network is down., Not checking for heartbeat inside sendFwVerToCloud";
    }


    //need this buffer to be filled here itself since in case of network outage, this should be logged

    memset(sendFwBuffer,'\0',sizeof(sendFwBuffer));

    memset(idChar,'\0',sizeof(idChar));

    if(generateRandInProgress==false)
    {
        sprintf(idChar,"%s",generateRandNumber().toLocal8Bit().data());
    }
    else
    {
        strcpy(idChar,"987");
    }


    sprintf(sendFwBuffer,"{\"request\":\"sendfirmwareversion\",\"version\":\"%s\",\"id\":\"%s\"}",fwVersionChar,idChar);


    if(heartBtStatFlgFwVer==true && loggingFlag==false)
    {

        headers = curl_slist_append(headers, "Accept: application/json");
        headers = curl_slist_append(headers, "Content-Type: application/json");
        headers = curl_slist_append(headers, "charsets: utf-8");


        if(curl)
        {

            curl_easy_setopt(curl, CURLOPT_URL, url);
            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);


            #ifdef  DEBUG_ON
            qDebug()<<"String to Send to DL for Update Firmware Version is "<<sendFwBuffer;
            #endif  //DEBUG_ON

            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, sendFwBuffer);

        }

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

        curl_easy_setopt(curl, CURLOPT_HEADER,0L);

        retryCntSendFwVer=0;
        do
        {
            QCoreApplication::processEvents(QEventLoop::AllEvents);

            res = curl_easy_perform(curl);

            http_code = 0;

            curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);

            retryCntSendFwVer++;

            delay(2);

        }while(http_code!=200 && retryCntSendFwVer<5);


        #ifdef  DEBUG_ON
            qDebug()<<"UpdateFWVersion to DL Output is "<<QString::number(res);
            qDebug()<<"UpdateFWVersion to Cloud Post Response: "<<QString::number(http_code);
        #endif  //DEBUG_ON


    }

    else    //network failed or hearbeat failed or both failed
    {
        //start logging data into file
        fwVerFp = fopen("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/event.csv","a");

        if(fwVerFp==NULL)
        {
            qDebug()<<"Unable to open event.csv";
        }
        else
        {
            memset(heartBtFailChar,'\0',sizeof(heartBtFailChar));
            strcpy(heartBtFailChar,sendFwBuffer);
            strcat(heartBtFailChar,";");
            fprintf(fwVerFp,"%s\n",heartBtFailChar);

            fclose(fwVerFp);
            eventCnt++;

        }

    }

}


/**
 * @brief DeltaKController::getWifiConnectStatus
 * @return
 */
bool DeltaKController::getWifiConnectStatus()
{
    wifiStatusInProgress = true;

    //need to check if ethernet is already connected... if so need add delay to get connected to access point
    if(!(genFileFP = popen("ifconfig  | grep -c inet\\ addr","r")))
    {
#ifdef  DEBUG__ON
        qDebug()<<"Unable to get network connectivity status";
#endif  //DEBUG_ON

    }

    else
    {

        fscanf(genFileFP,"%u",&noOfIPAddrCntGen);
        if(noOfIPAddrCntGen>2) //already 3 means ethernet is connected.. so provide some time for ap connection
        {
            delay(40);
        }
        pclose(genFileFP);
    }

    netCheckCnt=0;

    do
    {
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
        netCheckCnt++;
        delay(1);

    }while((system("wget -q --spider --tries=5 --timeout=5  http://www.thermofisher.com/us/en/home.html")!=0) && netCheckCnt<30);


    check_devconfigfile.setFile("/home/pi/Desktop/EdgeFirmware/miscfiles/deviceconfigcompleted.txt");

    // check if file exists and if yes: Is it really a file and no directory?
    if (!check_devconfigfile.exists() && !check_devconfigfile.isFile())   //file does n't exist., first time configuration., need to add delay to completed device registration
    {
        qDebug()<<"Device Config not completed.., waiting now to send heartbt command";
        delay(75);
    }\
    else
    {
        qDebug()<<"Device Config already completed.., no need to wait to send heartbt command";
    }


    if (system("wget -q --spider --tries=5 --timeout=5  http://www.thermofisher.com/us/en/home.html") == 0)      //network is present
    {
        //checking to ensure heart beat is up so that qr code can be sent successfully
        netCheckCnt=0;


        //checking for 1-1 heartbeat
        QFuture<QString> htBtIdSent;
        int heartBeatLoopCnt=0;
        int sendHBretry = SENDHBRETRY;
        bool sent = true;
        bool obtainedLock = false;
        bool trylock = false;
        srand(time(NULL));
        int hbfalsedur = (rand() % 33) + 3;
        bool heartBtStatFlgWifi = false;

        do
        {
            heartBeatLoopCnt=0;
            do
            {
                if(trylock==false)
                {
                    do {
                        trylock = heartBeatLock.tryLock(3);
                    } while ((trylock == false) && (--hbfalsedur>0));
                    if (trylock == true)
                    {

                        QCoreApplication::processEvents(QEventLoop::AllEvents);
                        heartBeatStatusFlag = false;
                        sent = false;
                        obtainedLock = true;
                        qDebug()<<"Inside getWifiStatus: "<<hbfalsedur;
                        heartBeatLoopCnt = 0;
                        sendHBretry = SENDHBRETRY;

                    }
                    else
                    {
                        srand(time(NULL));
                        hbfalsedur = (rand() % 33) + 3;
                    }
                }
                if(obtainedLock==false)
                {
                    qDebug()<<"Waiting for Lock in getWifiStatus "<< QString::number(SENDHBRETRY-sendHBretry)<< " " << QString::number(heartBeatLoopCnt);
                }
                if(!sent)
                {
                    sent = true;
                    htBtIdSent = QtConcurrent::run(this,&DeltaKController::sendHeartBeatCommand);
                    htBtVect.append(htBtIdSent);
                }
                QCoreApplication::processEvents(QEventLoop::AllEvents);

                QThread::msleep(500);

                if(obtainedLock == true)
                {
                    if (htBtVect.contains(htBtIdRecd) && (htBtIdSent == htBtIdRecd))
                    {
                        heartBtStatFlgWifi = heartBeatStatusFlag;
                        if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                        {
                            htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                        }
                    }
                }

            }while((++heartBeatLoopCnt<10) && heartBtStatFlgWifi==false);

            if (obtainedLock)
            {
                if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                {
                    htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                }
                sent = false;
            }

        }while((--sendHBretry > 0) && (heartBtStatFlgWifi==false));



        if(obtainedLock==true)
        {
            qDebug()<<"Breaked HtBt Loop In getWifiStatus with OuterCnt: "<<QString::number(SENDHBRETRY - sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgWifi << ":" <<htBtIdSent <<":"<<htBtIdRecd << ": HtBt Vector Count: "<<htBtVect.count();
            heartBeatLock.unlock();
        }
        else if(obtainedLock==false)
        {
            qDebug()<<"Timed Out! Breaked HtBt Loop In getWifiStatus with OuterCnt: "<<QString::number(SENDHBRETRY - sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgWifi ;
        }


        if(heartBtStatFlgWifi==true)
        {
            qDebug()<<"*****Obtained HeartBeat True in attempt no: "<<QString::number(SENDHBRETRY-sendHBretry) <<" *****";

            //need to ensure wifi connection is done... if ethernet is on and no wifi, need to return false


            wifiSignalLocal = 0;

            //check if interface active is wlan0 or wlan1
            if(!(nwCheckDL = popen("sudo ifconfig |grep wlan0","r")))
            {
                qDebug()<<"Unable to interface details on wlan0";
                wifiStatusInProgress = false;
                return false;
            }
            else
            {
                memset(wifiInterfaceDetails,'\0',sizeof(wifiInterfaceDetails));
                fscanf(nwCheckDL,"%s",wifiInterfaceDetails);
                pclose(nwCheckDL);

                if(strlen(wifiInterfaceDetails)>0)     //interface available is wlan0
                {
                    if(!(nwCheckDL = popen("sudo iwconfig wlan0|grep Signal|cut -d\"=\" -f3|cut -d\" \" -f1","r")))
                    {
                        qDebug()<<"Unable to get wifi signal status on wlan0";
                        wifiStatusInProgress = false;
                        return false;
                    }
                    else
                    {

                        fscanf(nwCheckDL,"%d",&wifiSignalLocal);

                        this->setWifiPercentage(QString::number(abs(wifiSignalLocal)));
                        emit valueChanged();

                       //qDebug()<< " WiFi Signal Strength Absolute value in dBm is "<<abs(wifiSignalLocal);

                        if(abs(wifiSignalLocal)==0)      // no wifi signal strenght available i.e. wifi not connected
                        {
                            wifiStatusInProgress = false;
                            return false;
                        }

                        pclose(nwCheckDL);
                    }

                }
                else        //checking for interface wlan1
                {
                    if(!(nwCheckDL = popen("sudo ifconfig |grep wlan1","r")))
                    {
                        qDebug()<<"Unable to interface details on wlan1";
                        wifiStatusInProgress = false;
                        return false;
                    }
                    else
                    {
                        memset(wifiInterfaceDetails,'\0',sizeof(wifiInterfaceDetails));
                        fscanf(nwCheckDL,"%s",wifiInterfaceDetails);
                        pclose(nwCheckDL);

                        if(strlen(wifiInterfaceDetails)>0)     //interface available is wlan1
                        {
                            if(!(nwCheckDL = popen("sudo iwconfig wlan1|grep Signal|cut -d\"=\" -f3|cut -d\" \" -f1","r")))
                            {
                                qDebug()<<"Unable to get wifi signal status on wlan1";
                                wifiStatusInProgress = false;
                                return false;
                            }
                            else
                            {

                                fscanf(nwCheckDL,"%d",&wifiSignalLocal);

                                this->setWifiPercentage(QString::number(abs(wifiSignalLocal)));
                                emit valueChanged();

                               //qDebug()<< " WiFi Signal Strength Absolute value in dBm is "<<abs(wifiSignalLocal);

                                if(abs(wifiSignalLocal)==0)      // no wifi signal strenght available i.e. wifi not connected
                                {
                                    wifiStatusInProgress = false;
                                    return false;
                                }

                                pclose(nwCheckDL);
                            }

                        }
                    }

                }

            }


            wifiStatusInProgress = false;
            return true;
        }
        else
        {
            qDebug()<<"*****Exceeded Max Attempts to Obtain HeartBeat attempt no: "<<QString::number(netCheckCnt) <<" *****";
        }


    }

    wifiStatusInProgress = false;
    return false;
}


/**
 * @brief DeltaKController::getAssetPushStatus
 * @return
 */
bool DeltaKController::getAssetPushStatus()
{
    wifiStatusInProgress = true;
    //need to check if ethernet is already connected... if so need add delay to get connected to access point
    if(!(genFileFP = popen("ifconfig  | grep -c inet\\ addr","r")))
    {
#ifdef  DEBUG__ON
        qDebug()<<"Unable to get network connectivity status";
#endif  //DEBUG_ON

    }

    else
    {

        fscanf(genFileFP,"%u",&noOfIPAddrCntGen);
        if(noOfIPAddrCntGen>2) //already 3 means ethernet is connected.. so provide some time for ap connection
        {
            delay(50);
        }
        pclose(genFileFP);
    }

    netCheckCnt=0;

    do
    {
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
        netCheckCnt++;
        delay(1);

    }while((system("wget -q --spider --tries=5 --timeout=5  http://www.thermofisher.com/us/en/home.html")!=0) && netCheckCnt<30);

    if (system("wget -q --spider --tries=5 --timeout=5  http://www.thermofisher.com/us/en/home.html") == 0)      //network is present
    {
        //checking to ensure heart beat is up so that qr code can be sent successfully
        netCheckCnt=0;



        //checking for 1-1 heartbeat
        QFuture<QString> htBtIdSent;
        int heartBeatLoopCnt=0;
        int sendHBretry = SENDHBRETRY;
        bool sent = true;
        bool obtainedLock = false;
        bool trylock = false;
        srand(time(NULL));
        int hbfalsedur = (rand() % 33) + 3;
        bool heartBtStatFlgAstPush = false;

        do
        {
            heartBeatLoopCnt=0;
            do
            {
                if(trylock==false)
                {
                    do {
                        trylock = heartBeatLock.tryLock(3);
                    } while ((trylock == false) && (--hbfalsedur>0));
                    if (trylock == true)
                    {

                        QCoreApplication::processEvents(QEventLoop::AllEvents);
                        heartBeatStatusFlag = false;
                        sent = false;
                        obtainedLock = true;
                        qDebug()<<"Inside getAssetPushStatus: "<<hbfalsedur;
                        heartBeatLoopCnt = 0;
                        sendHBretry = SENDHBRETRY;

                    }
                    else
                    {
                        srand(time(NULL));
                        hbfalsedur = (rand() % 33) + 3;
                    }
                }
                if(obtainedLock==false)
                {
                    qDebug()<<"Waiting for Lock in getAssetPushStatus "<< QString::number(SENDHBRETRY-sendHBretry)<< " " << QString::number(heartBeatLoopCnt);
                }
                if(!sent)
                {
                    sent = true;
                    htBtIdSent = QtConcurrent::run(this,&DeltaKController::sendHeartBeatCommand);
                    htBtVect.append(htBtIdSent);
                }
                QCoreApplication::processEvents(QEventLoop::AllEvents);

                QThread::msleep(500);

                if(obtainedLock == true)
                {
                    if (htBtVect.contains(htBtIdRecd) && (htBtIdSent == htBtIdRecd))
                    {
                        heartBtStatFlgAstPush = heartBeatStatusFlag;
                        if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                        {
                            htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                        }
                    }
                }

            }while((++heartBeatLoopCnt<10) && heartBtStatFlgAstPush==false);

            if (obtainedLock)
            {
                if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                {
                    htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                }
                sent = false;
            }

        }while((--sendHBretry > 0) && (heartBtStatFlgAstPush==false));



        if(obtainedLock==true)
        {
            qDebug()<<"Breaked HtBt Loop In getAssetPushStatus with OuterCnt: "<<QString::number(SENDHBRETRY - sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgAstPush << ":"<<htBtIdSent <<":"<<htBtIdRecd << ": HtBt Vector Count: "<<htBtVect.count();
            heartBeatLock.unlock();
        }
        if (obtainedLock==false)
        {
            qDebug()<<"Timed Out! Breaked HtBt Loop In getAssetPushStatus with OuterCnt: "<<QString::number(SENDHBRETRY - sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgAstPush ;
        }


        if(heartBtStatFlgAstPush==true)
        {
            //need to ensure wifi connection is done... if ethernet is on and no wifi, need to return false
            wifiStatusInProgress = false;
            return true;
        }


    }
    wifiStatusInProgress = false;
    return false;
}


/**
 * @brief DeltaKController::updateLangFlagEnglish
 */
void DeltaKController::updateLangFlagEnglish()      //1-ENGLISH 2-CHINESE
{
    system("sudo echo 1 > /home/pi/Desktop/EdgeFirmware/miscfiles/languageselection.txt");
    this->setChineseLang(false);
}


/**
 * @brief DeltaKController::updateLangFlagChinese
 */
void DeltaKController::updateLangFlagChinese()      //1-ENGLISH 2-CHINESE
{
    system("sudo echo 2 > /home/pi/Desktop/EdgeFirmware/miscfiles/languageselection.txt");
    this->setChineseLang(true);
}


/**
 * @brief DeltaKController::internetOnSlot
 */
void DeltaKController::internetOnSlot()
{
   internetStatus = true;
}


/**
 * @brief DeltaKController::internetOffSlot
 */
void DeltaKController::internetOffSlot()
{
   internetStatus = false;
}


/**
 * @brief DeltaKController::writePinToFile
 */
void DeltaKController::writePinToFile()
{
   qDebug()<<"Final PIN Set is: "<<this->pinNumber();

   genericFile.setFileName("/home/pi/Desktop/EdgeFirmware/miscfiles/pinnumber.txt");
   if(!genericFile.open(QIODevice::WriteOnly)) {
      qDebug()<<"Unable to open pinnumber.txt file to update devicename";
   }
   else
   {

      QTextStream out(&genericFile);
      out << this->pinNumber() ;
      genericFile.close();
   }
}


/**
 * @brief DeltaKController::readPinFromFile
 */
void DeltaKController::readPinFromFile()
{
    //reading current pin number
    genericFile.setFileName("/home/pi/Desktop/EdgeFirmware/miscfiles/pinnumber.txt");

    if(!genericFile.open(QIODevice::ReadOnly)) {
       qDebug()<<"Unable to open pinnumber.txt file";
       this->setPinNotSet(true);
    }
    else
    {
       this->setPinNotSet(false);

       QTextStream pinTS(&genericFile);

       while(!pinTS.atEnd()) {
           pinNum = pinTS.readLine();
       }

       genericFile.close();

       this->setPinNumber(pinNum);
#ifdef DEBUG_ON
       qDebug()<<"Pin Num is: "<< pinNum;
#endif //DEBUG_ON
    }
}

/**
 * @brief DeltaKController::resetPIN
 */
void DeltaKController::resetPIN()
{
    year = "";
    month = "";
    day = "";
    keyInt = 0;
    keyStr = "";
    generatedKey = "";

    year = (QString::number((QDateTime::currentDateTime().date().year()-2000)*13.7).split(".").takeFirst());
    month = (QString::number((QDateTime::currentDateTime().date().month())*19.3).split(".").takeFirst());
    day = (QString::number((QDateTime::currentDateTime().date().day()-1)*17.1).split(".").takeFirst());

    keyInt = year.toInt()+month.toInt()+day.toInt();

    keyStr = QString::number(keyInt);

    generatedKey = keyStr.right(1)+keyStr.mid(1,1)+keyStr.left(1)+"0";

       this->setPinNumber(generatedKey);

       this->writePinToFile();

       qDebug()<<"PIN RESET DONE";

}


/**
 * @brief DeltaKController::enableForgotPinOption
 */
void DeltaKController::enableForgotPinOption()
{
    this->setShowForgotPin(true);
    emit valueChanged();
}


/**
 * @brief DeltaKController::disableForgotPinOption
 */
void DeltaKController::disableForgotPinOption()
{
    this->setShowForgotPin(false);
    emit valueChanged();
}


/**
 * @brief DeltaKController::enableBackNumKeyPad
 */
void DeltaKController::enableBackNumKeyPad()
{
    this->setShowBackKeyPad(true);
    emit valueChanged();
}


void DeltaKController::receivedWifiValue(int wifiVal)
{
    this->setWifiPercentage(QString::number(wifiVal));
    wifiSignalStrength = wifiVal;
    emit valueChanged();

   //qDebug()<< " WiFi Signal Strength Absolute value in dBm is "<<wifiSignalStrength;

    if(wifiVal>80)      // -80 dBm is unstable connection https://eyesaas.com/wi-fi-signal-strength/
    {
        if(lowSigEventSentFlag==false)
        {
            QtConcurrent::run(this,&DeltaKController::sendLowSignalEvent);//sendLowSignalEvent();
        }
    }
    else
    {
        lowSigEventSentFlag=false;
    }

}


/**
 * @brief DeltaKController::recheckOutageStatus
 */
void DeltaKController::recheckOutageStatus()
{
    heartBeatFailDir.setPath("/home/pi/Desktop/EdgeFirmware/heartbtfaildata");

    heartBeatFailDir.setFilter( QDir::AllEntries | QDir::NoDotAndDotDot );

    //qDebug()<<"Total File Count is: "<<dlLinkedUsers.count();

    if(heartBeatFailDir.count()==0)    //no outage data to be send to cloud
    {
        if(resendOutageDataTimer->isActive()==true)
        {
            resendOutageDataTimer->stop();
        }
        qDebug()<<"<-----> No data to be sent.. So stopping the timer now";
    }
    else if(heartBeatFailDir.count()>0)    //outage data is present to be sent to the cloud
    {


        if(outageSendInProgress==false && heartBeatStatusFlag==true)
        {qDebug()<<"<-----> Sending unsent data to the cloud";
            outageDataSendTimer->start(1000);

            if(resendOutageDataTimer->isActive()==true)
            {
                resendOutageDataTimer->stop();
            }

        }


    }

}


/**
 * @brief DeltaKController::recheckApModeStatus
 */
void DeltaKController::recheckApModeStatus()
{
    apModeCheckTimer->stop();

#if 0   //commenting now for testing purpose

    if(apModeInProgress==true)
    {
        if(getWlan0Status()==true)   //unit has active wlan0 interface.., now need to check for static ip of 192.168.50.1
        {
            if(!(apModeChkFileFP = popen("sudo ifconfig wlan0 | grep \'inet addr\' | cut -d: -f2 | awk \'{print $1}\'","r")))
            {
                qDebug()<<"Unable to interface details on wlan0";
            }
            else
            {
                memset(bufCheckAP,'\0',sizeof(bufCheckAP));
                fscanf(apModeChkFileFP,"%s",bufCheckAP);
                pclose(apModeChkFileFP);
                printf("\n*****VALUE OF bufCheckAP is %s\n",bufCheckAP);
                qDebug()<<"bufCheckAP CurrentDateTIme: "<< QDateTime::currentDateTime().date()<<" : " <<QDateTime::currentDateTime().time();

                if(strcmp(bufCheckAP,"192.168.50.1")==0)     //unit is in ap mode.., no action to be done
                {

                }
                else if(strcmp(bufCheckAP,"192.168.50.1")!=0)    //unit lost static ip when in ap mode
                {
                    qDebug()<<"*****UNIT LOST STATIC IP*****"<<"LINE: "<<__LINE__;
                     memset(bufResetAPCmd,'\0',sizeof(bufResetAPCmd));
                     sprintf(bufResetAPCmd,"sudo ap %s",instrumentNameChar);
                     system(bufResetAPCmd);
                }
            }
        }
        else if(getWlan0Status()==false) //unit does n't have wlan0 interface., may be dongle connected, but wlan0 went down
        {
            if(apModeInProgress==true)
            {
                qDebug()<<"*****UNIT LOST STATIC IP*****"<<"LINE: "<<__LINE__;
                memset(bufResetAPCmd,'\0',sizeof(bufResetAPCmd));
                sprintf(bufResetAPCmd,"sudo ap %s",instrumentNameChar);
                system(bufResetAPCmd);
            }

        }
    }
    else
    {
        qDebug()<<"*****apModeCheckTimer fired..., but AP Mode in not in progress";
    }

#endif  //0
}


/**
 * @brief DeltaKController::eventSentCheckSlot
 */
void DeltaKController::eventSentCheckSlot()
{
    qDebug()<<"*****EVENTSENTCHECKSLOT TIMER FIRED*****";
   eventSentCheckTimer->stop();

   if(loggingFlag==true)    //network is down., so need to log the event data
   {
       //checking for snooze event buffer and writing it to log file accordingly
       if(strlen(snoozeEventBuffer)>0)
       {
           eventResendFp = fopen("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/snoozeevent.csv","a");

           if(eventResendFp==NULL)
           {
               qDebug()<<"Unable to open snoozeevent.csv";
           }
           else
           {
               memset(heartBtFailChar,'\0',sizeof(heartBtFailChar));
               strcpy(heartBtFailChar,snoozeEventBuffer);
               strcat(heartBtFailChar,";");
               fprintf(eventResendFp,"%s\n",heartBtFailChar);

               fclose(eventResendFp);
               snoozeEventCnt++;
           }

       }

       //checking for warm alarm event buffer and writing it to log file accordingly
       if(strlen(highTempEventBuffer)>0)
       {
           eventResendFp = fopen("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/event.csv","a");

           if(eventResendFp==NULL)
           {
               qDebug()<<"Unable to open event.csv";
           }
           else
           {
               memset(heartBtFailChar,'\0',sizeof(heartBtFailChar));
               strcpy(heartBtFailChar,highTempEventBuffer);
               strcat(heartBtFailChar,";");
               fprintf(eventResendFp,"%s\n",heartBtFailChar);

               fclose(eventResendFp);
               eventCnt++;
               highTempAlarmEventSentFlag=true;
           }

       }

       //checking for cold alarm event buffer and writing it to log file accordingly
       if(strlen(lowTempEventBuffer)>0)
       {
           eventResendFp = fopen("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/event.csv","a");

           if(eventResendFp==NULL)
           {
               qDebug()<<"Unable to open event.csv";
           }
           else
           {
               memset(heartBtFailChar,'\0',sizeof(heartBtFailChar));
               strcpy(heartBtFailChar,lowTempEventBuffer);
               strcat(heartBtFailChar,";");
               fprintf(eventResendFp,"%s\n",heartBtFailChar);

               fclose(eventResendFp);
               eventCnt++;
               lowTempAlarmEventSentFlag=true;
           }

       }

       //checking for alarmRecoveryBuffer and writing it to log file accordingly
       if(strlen(alarmRecoveryBuffer)>0)
       {
           eventResendFp = fopen("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/recoveryevent.csv","a");

           if(eventResendFp==NULL)
           {
               qDebug()<<"Unable to open recoveryevent.csv";
           }
           else
           {
               memset(heartBtFailChar,'\0',sizeof(heartBtFailChar));
               strcpy(heartBtFailChar,alarmRecoveryBuffer);
               strcat(heartBtFailChar,";");
               fprintf(eventResendFp,"%s\n",heartBtFailChar);

               fclose(eventResendFp);
               recoveryEvtCnt++;
               recoveryAlarmEventSentFlag = true;
               ltAlarmSetClearedFlag = false;
               htAlarmSetClearedFlag = false;
               memset(currentAlarmIdChar,'\0',sizeof(currentAlarmIdChar));

           }

       }

   }
   else
   {
       qDebug()<<"No Data to be sent inside EVENTSENTCHECKSLOT";
   }
}


/**
 * @brief DeltaKController::startEventSentCheckSlot
 */
void DeltaKController::startEventSentCheckSlot()
{
    //qDebug()<<"*****startEventSentCheckSlot Signal Fired*****";
    if(eventSentCheckTimer->isActive()==false)
    {
        eventSentCheckTimer->start(12*1000);    //to recheck if there is network outage immediately after sending event so that we can log it to sd card
    }

}


/**
 * @brief Task::delayMSec
 * @param msec
 */
void DeltaKController::delayMSec(int msec)
{
    dieTime= QTime::currentTime().addMSecs(msec);
    while(QTime::currentTime() < dieTime)
    {
        QCoreApplication::processEvents(QEventLoop::AllEvents);
    }
}


/**
 * @brief DeltaKController::logDataToSdCard
 */
int DeltaKController::logDataToSdCard()
{
    //start logging data into file

    if(telemetryLogCnt==0 || logSDCardStarted==false)      //logging just started.. so capture the start time
    {
        logStartDateTime = QDateTime::currentDateTime();
        logSDCardStarted = true;
    }

    if(logStartDateTime.secsTo(QDateTime::currentDateTime())<MAX_LOG_DURATION)     //time difference is less than 72 hrs
    {
        //qDebug()<<"<----->Lapsed Time is Less than 72 hrs";
        //logged data is with in time limits..
        telemetryLogCnt++;
    }
    else        //logged duration exceeded MAX_LOG_DURATION hrs .. so removing top log record
    {
        //qDebug()<<"<----->Lapsed Time is more than 60 sec... so deleting line";
        system("sed -i \'1d\' /home/pi/Desktop/EdgeFirmware/heartbtfaildata/telemetry.csv");
    }


    //start logging data into file
    telemetryFp = fopen("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/telemetry.csv","a");

    if(telemetryFp==NULL)
    {
        qDebug()<<"Unable to open telemetry.csv";
    }
    else
    {
        memset(telemetryCmd,'\0',sizeof(telemetryCmd));
        strcpy(telemetryCmd,telemetryBuffer);
        strcat(telemetryCmd,";");
        fprintf(telemetryFp,"%s\n",telemetryCmd);

        fclose(telemetryFp);
    }

    qDebug("Data Written to telemetry file is %s\n",telemetryCmd);


    //since network is down, write the existing data in telemetryBkpBuffer onto sd card and then log the regular data onto sdcard
    //logic to append the data in telemetryBkpBuffer onto telemetry.csv file

    if(strlen(telemetryBkpBuffer)>0 && outageTeleWriteProgress==false)
    {
        telemetryFp = fopen("/home/pi/Desktop/EdgeFirmware/heartbtfaildata/telemetry.csv","a");

        if(telemetryFp==NULL)
        {
            qDebug()<<"Unable to open telemetry.csv";
        }
        else
        {

            qDebug()<<"HeartBeat/Network status changed to false, writing existing telemetryBkpBuffer onto sdcard" ;

            regTeleWriteProgress = true;

            //tokenWriteFile = strtok(telemetryBkpBuffer, ";");
            tokenWriteFileList = QString::fromLocal8Bit(telemetryBkpBuffer).split(";");
            //while (tokenWriteFile != NULL)
            foreach(QString itemTelemetryWriteFile, tokenWriteFileList)
            {
                memset(dispatchErrorLogBuffReg,'\0',sizeof(dispatchErrorLogBuffReg));
                strcpy(dispatchErrorLogBuffReg,itemTelemetryWriteFile.toLocal8Bit().data());
                strcat(dispatchErrorLogBuffReg,";");


                if(strlen(dispatchErrorLogBuffReg)>100)
                {
                    fprintf(telemetryFp,"%s\n",dispatchErrorLogBuffReg);
                    qDebug("Data Written to telemetry file is %s\n",dispatchErrorLogBuffReg);
                    telemetryLogCnt++;
                }

            }


            memset(telemetryBkpBuffer,'\0',sizeof(telemetryBkpBuffer));

            regTeleWriteProgress = false;

            fclose(telemetryFp);
        }


    }

    return 0;
}


/**
 * @brief DeltaKController::startOutageSendSlot
 */
void DeltaKController::startOutageSendSlot()
{
    if(wifiThrd->isRunning()==false)    //if unit is back in network, check and start wifi thread
    {
        wifiThrd->start();
    }

    //doing updatedevicestatus after checking for heartbeat
    if(batteryFlag==false && alarmFlag==false && batteryAlarmFlag==false && this->noProbeDetected()==false && this->unAckAlarmCleared()==false && this->userLinkWarning()==false)
    {
        system("echo \"1\" > /sys/class/gpio/gpio22/value");  //Green led on ON state
        system("echo \"0\" > /sys/class/gpio/gpio26/value");    //yellow led on OFF state
        this->setTempAlarmIcon(false);  //this is needed to handle case where recovery failed to be sent to DL, during which alarmsetcleared will never set to true and so even after recovery we see green led with temp icon.,
        updateMode(NORMAL);
    }
    else if(batteryFlag==false && alarmFlag==false && batteryAlarmFlag==false && this->noProbeDetected()==false && this->unAckAlarmCleared()==false && this->userLinkWarning()==true)
    {
        //dont change the led to green yet... only update mode to cloud since no link warning is active
        updateMode(NORMAL);

    }

    //checking for 1-1 heartbeat

    QFuture<QString> htBtIdSent;
    int heartBeatLoopCnt=0;
    int sendHBretry = SENDHBRETRY;
    bool sent = true;
    bool obtainedLock = false;
    bool trylock = false;
    bool heartBtStatFlgLogg = false;
    srand(time(NULL));
    int hbfalsedur = (rand() % 33) + 3;


    do
    {
        heartBeatLoopCnt=0;
        do
        {
            if(trylock == false)
            {
                do {
                    trylock = heartBeatLock.tryLock(3);
                } while ((trylock == false) && (--hbfalsedur>0));
                if (trylock == true)
                {
                    QCoreApplication::processEvents(QEventLoop::AllEvents);
                    heartBeatStatusFlag = false;
                    sent = false;
                    obtainedLock = true;
                    qDebug()<<"Inside NetworkMonitor :" << hbfalsedur;
                    heartBeatLoopCnt = 0;
                    sendHBretry = SENDHBRETRY;
                    loggingFlag = false;    //setting the status to false only after this thread gets access atleast once to check heartbeat
                }
                else
                {
                    srand(time(NULL));
                    hbfalsedur = (rand() % 33) + 3;
                }
            }
            if(obtainedLock==false)
            {
                qDebug()<<"Waiting for Lock in NetworkMonitor"<< QString::number(SENDHBRETRY-sendHBretry)<< " " << QString::number(heartBeatLoopCnt);
            }
            if(sent==false)
            {
                sent = true;
                htBtIdSent = QtConcurrent::run(this,&DeltaKController::sendHeartBeatCommand);
                htBtVect.append(htBtIdSent);


            }
            QCoreApplication::processEvents(QEventLoop::AllEvents);

            QThread::msleep(500);

            if(obtainedLock == true)
            {
                if (htBtVect.contains(htBtIdRecd) && (htBtIdSent == htBtIdRecd))
                {
                    heartBtStatFlgLogg = heartBeatStatusFlag;
                    if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
                    {
                        htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
                    }
                }

            }

        }while((++heartBeatLoopCnt<10) && heartBtStatFlgLogg==false);

        if (obtainedLock)
        {
            if (htBtVect.lastIndexOf(htBtIdSent) >= 0)
            {
                htBtVect.remove(htBtVect.lastIndexOf(htBtIdSent));
            }
            sent = false;
        }

    }while((--sendHBretry > 0) && (heartBtStatFlgLogg==false));


    if(obtainedLock==true)
    {
        qDebug()<<"Breaked HtBt Loop In NetworMonitor with OuterCnt: "<<QString::number(SENDHBRETRY - sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgLogg <<":"<< htBtIdSent <<":"<<htBtIdRecd << ": HtBt Vector Count: "<<htBtVect.count();
        heartBeatLock.unlock();
    }
    else if(obtainedLock==false)
    {
        qDebug()<<"Timed Out! Breaked HtBt Loop In NetworMonitor with OuterCnt: "<<QString::number(SENDHBRETRY - sendHBretry)<<"InnerCnt: "<<QString::number(heartBeatLoopCnt) << ":"<< heartBtStatFlgLogg;
    }


    if(heartBtStatFlgLogg==true)
    {
        qDebug()<<"HeartBeat Status: "<< heartBeatStatusFlag <<"LINE: "<<__LINE__;
        emit startOutageTimerSignal();
    }

}


/**
 * @brief DeltaKController::startOutageTimerSlot
 */
void DeltaKController::startOutageTimerSlot()
{
    if(outageDataSendTimer->isActive()==false)
    {
         outageDataSendTimer->start(45*1000);
    }

    if(resendOutageDataTimer->isActive()==false)
    {
         resendOutageDataTimer->start(60000);     //start monitoring remaining data to cloud
    }

}


/**
 * @brief DeltaKController::warmAlarmSlot
 */
void DeltaKController::warmAlarmSlot()
{
    warmAlarmTimer->stop();
    QtConcurrent::run(this,&DeltaKController::sendHighTempAlarmEvent);
}


/**
 * @brief DeltaKController::coldAlarmSlot
 */
void DeltaKController::coldAlarmSlot()
{
    coldAlarmTimer->stop();
    QtConcurrent::run(this,&DeltaKController::sendLowTempAlarmEvent);
}


/**
 * @brief DeltaKController::alarmRecoverySlot
 */
void DeltaKController::alarmRecoverySlot()
{
    alarmRecoveryTimer->stop();
    QtConcurrent::run(this,&DeltaKController::sendAlarmRecoveryEvent);
}


/**
 * @brief DeltaKController::dlRestartTimerSlot
 */
void DeltaKController::dlRestartTimerSlot()
{
    if(apModeInProgress==false && loggingFlag==false && this->noProbeDetected()==false)
    {
        //qDebug()<<"***** DL Restart Timer Fired!!!  *****";
        //system("sudo systemctl restart tfsdl.service");

        qDebug()<<"Trying to re-establish MQTT heartbeat";
        QtConcurrent::run(this,&DeltaKController::sendSetRegionCmd);
        system("sudo systemctl restart tfsdl.service");
    }

}


/**
 * @brief DeltaKController::dlDataFolderSlot
 */
void DeltaKController::dlDataFolderSlot()
{
    if(apModeInProgress==false && loggingFlag==false)
    {
        dlLinkedUsers.setPath("/home/pi/dl-1.8-prod/deeplaser-1.8.5/data");

        dlLinkedUsers.setFilter( QDir::AllEntries | QDir::NoDotAndDotDot );

        //qDebug()<<"Total File Count is: "<<dlLinkedUsers.count();

        if(dlLinkedUsers.count()==0 || devReRegisterFlag==false)    //no data folder present., registration failed., need to restart dl
        {
            qDebug()<<"***** DL Reg Failed.., Restarting DL Service!!!  *****";
            system("sudo rm -rf /home/pi/dl-1.8-prod/deeplaser-1.8.5/conf/com.thermofisher.deeplaser.registration.RegistrationService.config.js");
            system("sudo rsync -av /home/pi/Desktop/EdgeFirmware/deeplaser-1.8.5/conf/com.thermofisher.deeplaser.registration.RegistrationService.config.js /home/pi/dl-1.8-prod/deeplaser-1.8.5/conf/");
            system("sudo rm -rf /home/pi/dl-1.8-prod/deeplaser-1.8.5/data/");
            system("sudo systemctl restart tfsdl.service");

            if(outageDataSendTimer->isActive()==false && outageSendInProgress==false) //DL process just restarted., need to send the logged data so far back to DL
            {
                QtConcurrent::run(this,&DeltaKController::startOutageSendSlot);
            }
            else
            {
                qDebug()<<"*** Already Outage Send in Progress.., So not initiating now" << __LINE__;
            }
        }
        else
        {
            qDebug()<<"***** Data Folder Present.., Registration Done *****";

            if(dlDataFolderChkTimer->isActive()==true)
            {
                dlDataFolderChkTimer->stop();
            }
        }
    }
}


/**
 * @brief DeltaKController::sendSetRegionCmd
 */
void DeltaKController::sendSetRegionCmd()
{

    headers = NULL;

    memset(setRegionBuffer,'\0',sizeof(setRegionBuffer));
    memset(idChar,'\0',sizeof(idChar));

    sprintf(idChar,"%s",generateRandNumber().toLocal8Bit().data());

    sprintf(setRegionBuffer,"{\"request\":\"setregion\",\"id\":\"%s\",\"region\":\"CN\"}",idChar);

    headers = curl_slist_append(headersHeartBt, "Accept: application/json");
    headers = curl_slist_append(headersHeartBt, "Content-Type: application/json");
    headers = curl_slist_append(headersHeartBt, "charsets: utf-8");

    if(curl)
    {

        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);


        #ifdef  DEBUG_ON
        qDebug()<<"String to Send to DL for setregion is :"<<setRegionBuffer ;
        #endif  //DEBUG_ON

        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, setRegionBuffer);

    }

    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
    curl_easy_setopt(curl, CURLOPT_HEADER,0L);

    retryCntSetReg=0;
    do
    {
        res = curl_easy_perform(curl);

        http_code = 0;

        curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);

        retryCntSetReg++;

        delay(2);

    }while(http_code!=200 && retryCntSetReg<5);

    #ifdef  DEBUG_ON
    qDebug()<<"SetRegion Output is "<<QString::number(res)<<" "<<QString::number(retryCntSetReg);
    qDebug()<<"SetRegion Command to Cloud Post Response: "<<QString::number(http_code);
    #endif  //DEBUG_ON


}
