import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "./assets"

Rectangle {
    width: 480
    height: 320
    color: "#f6f7f7"


    Button {
        id: okButton
        x: 155
        y: 260
        width: 170
        height: 50
        text: qsTr("")
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        style: ButtonStyle {
            background: Rectangle {
                color: "#2ea2ec"
                radius: 5
            }
        }

         Text {
            id: okButtonText
            x: 73
            y: 10
            width: 78
            height: 30
            color: "#ffffff"
            text: qsTr("OK") + languageTranslator.translatedText
            FontLoader {
                id: robotoRegular
                source: "./assets/Fonts/roboto.regular.ttf"
            }
            verticalAlignment: Text.AlignVCenter
            font.family: robotoRegular2.name
            font.pixelSize: 18
        }
        onClicked: {
         mainStack.pop(StackView.Immediate)
        }

    }

    Label {
        id: apmodeLabel
        x: 160
        y: 24
        width: 160
        height: 18
        text: qsTr("Firmware Upgrade") + languageTranslator.translatedText
        color: "#344550"
        font.bold: true
        FontLoader { id: robotoMedium3; source: "./assets/Fonts/roboto.medium.ttf" }
        font.family: robotoMedium3.name
        font.pixelSize: 18
        horizontalAlignment: Text.AlignHCenter
    }

    Label {
        id: ipAddressLabel
        x: 90
        y: 150
        width: 300
        height: 20
        color: "#33444f"
        text: qsTr("Firmware update completed.") + languageTranslator.translatedText
        font.family: robotoRegular2.name
        font.pixelSize: 18
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        FontLoader {
            id: robotoRegular2
            source: "./assets/Fonts/roboto.regular.ttf"
        }
    }

}
