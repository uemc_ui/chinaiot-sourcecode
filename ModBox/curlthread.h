#ifndef CURLTHREAD_H
#define CURLTHREAD_H

#include <QThread>
#include <QMutex>
#include <QTime>
#include <curl/curl.h>

class CurlThread : public QThread
{
    Q_OBJECT

public:
    CurlThread();
    //void setMessage(const QString &message);
    void stop();



private:

    volatile bool stopped;
    QMutex mutex;


    QTime dieTime;

    bool netOn = false;
    bool netOff = false;
    CURL *recdCurl;
    CURLcode cmdResp;
    long http_code;


signals:
    void networkOn();
    void networkOff();


public slots:

    void delay(int sec);
    void getData(CURL *curl);

protected:
    void run();

};

#endif  //CURLTHREAD_H



