import QtQuick 2.0

Rectangle {
    width: 480
    height: 34
    color: "transparent"

    property alias messageText: messageText.text


    function startYellowTextAnimation(){
        if(yellowTextAnimation.running===false)
        {
            //console.log("start Yellow Text Animation")
            yellowTextAnimation.start()
        }
    }

    function stopYellowTextAnimation(){

        if(yellowTextAnimation.running===true)
        {
             //console.log("stop Yellow Text Animation")
             yellowTextAnimation.stop()
        }
    }

    Rectangle {
        id: messageRectangle
        x: 0
        y: 6
        width: 480
        height: 28
        opacity: 0.8
        color: "#c17e11"

        Text {
            id: messageText
            x: 10
            y: 5
            width: 460
            height: 18
            color: "#ffffff"
            text: deltaK.alarmMessage
            font.pixelSize: 14
            horizontalAlignment: Text.AlignHCenter
            font.family: robotoRegular.name
            FontLoader {
                id: robotoRegular
                source: "../assets/Fonts/roboto.regular.ttf"
            }
            verticalAlignment: Text.AlignVCenter
        }
    }

    Image {
        id: image
        x: 234
        width: 12
        height: 6
        source: "../assets/Images/yellow_arrow.png"
    }

    SequentialAnimation on y {
             id: yellowTextAnimation
             loops: Animation.Infinite
             alwaysRunToEnd: true
             running: true
             //PropertyAnimation { id: animationOne; target: messageRectangle; alwaysRunToEnd: true;property: "opacity"; to: 1; duration: 2000}
             PropertyAnimation { id: animationOne; targets: [messageRectangle,image]; alwaysRunToEnd: true;property: "opacity"; to: 1; duration: 2000}
             PauseAnimation { duration: 2000 }
             //PropertyAnimation { id: animationTwo; target: messageRectangle; alwaysRunToEnd: true;property: "opacity"; to: 0; duration: 2000}
             PropertyAnimation { id: animationTwo; targets: [messageRectangle,image]; alwaysRunToEnd: true;property: "opacity"; to: 0; duration: 2000}
             PauseAnimation { duration: 4000 }
     }

}
