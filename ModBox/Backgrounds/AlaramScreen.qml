import QtQuick 2.0
import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Controls 2.0
import QtQuick.Controls.Styles 1.4
import "../CustomComponents"
import "../assets/Fonts"
import QtQuick.Controls 1.4
import "../Backgrounds"

Rectangle {
    width: 480
    height: 108
    color: "transparent"

    function startRedTextAnimation(){
        if(redTextAnimation.running==false)
        {
            //console.log("start Red Text Animation")
            redTextAnimation.start()
        }
    }

    function stopRedTextAnimation(){

        if(redTextAnimation.running==true)
        {
             //console.log("stop Red Text Animation")
             redTextAnimation.stop()
        }
    }

    Rectangle {
        id: firstRectangle
        x: 0
        y: 0
        width: 480
        height: 80
        color: "#ee3134"

        HeaderLabel {
            id: dateLabel
            x: 120
            y: 44
            width: 58
            height: 18
            //text: Qt.formatDateTime(new Date(), "d/M/yyyy")
            text: Qt.formatDateTime(new Date(), "MM/dd/yyyy")
        }

        Rectangle {
            id: separator
            x: 110
            y: 46
            width: 2
            height: 14
            color: "#4f555a"
        }

        HeaderLabel {
            id: timeLabel
            x: 24
            y: 44
            width: 80
            height: 18
        }

        HeaderLabel{
            id: headerLabel
            x: 24
            y: 24
            width: 112
            height: 18
            text: deltaK.deviceName
            verticalAlignment: Text.AlignVCenter
        }

        Image {
            id: image
            x: 210
            y: 10
            width: 60
            height: 60
            source: "../assets/Images/Status_Bell_Alarm.png"
        }


        Button {
            id: snoozeButton
            x: 357
            y: 20
            width: 99
            height: 40
            text: qsTr("")
            anchors.horizontalCenterOffset: 168
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 20
            style: ButtonStyle {
                background: Rectangle {
                    color: "#952f35"
                    radius: 5
                }
            }

             Text {
                id: snoozeButtonText
                x: 23
                y: 5
                width: 78
                height: 30
                color: "#ffffff"
                // color: "#2ea2ec"
                text: qsTr("Snooze") + languageTranslator.translatedText
                FontLoader {
                    id: robotoRegular
                    source: "../assets/Fonts/roboto.regular.ttf"
                }
                verticalAlignment: Text.AlignVCenter
                font.family: robotoRegular.name
                font.pixelSize: 16
            }
             onClicked: {
                    deltaK.setSnoozeStatus(true)
                    deltaK.callSendSnoozeEvent();
             }
        }

        Image {
            id: tempImage
            x: 275
            y: 30
            width: 8
            height: 20
            source: "../assets/Images/Alarm/Temp.png"
            visible: deltaK.tempAlarmIcon
        }

//        Image {
//            id: batteryLowImage
//            x: 288
//            y: 30
//            width: 12
//            height: 20
//            //source: "../assets/Images/Alarm/Battery low_new.png"
//            visible: deltaK.batteryAlarmIcon

//            Connections
//            {
//                target: deltaK
//                onValueChanged:
//                {
//                    var percent = parseInt ( deltaK.batteryPercentage ) ;

//                    if ( percent > 80 && percent <= 100 )
//                        batteryLowImage.source = "../assets/Images/Alarm/Battery_full.png" ;
//                    else if ( percent >= 40 )
//                        batteryLowImage.source = "../assets/Images/Alarm/Battery_medium.png" ;
//                    else if ( percent >= 10 )
//                        batteryLowImage.source = "../assets/Images/Alarm/Battery low_new.png" ;
//                    else
//                        batteryLowImage.source = "../assets/Images/Alarm/Battery_empty.png" ;
//                }
//            }
//        }

        Label {
            id: batteryLabel
            x: 305
            y: 32
            width: 40
            height: 16
            color: "#ffffff"
            text: deltaK.batteryPercentage
            verticalAlignment: Text.AlignVCenter
            FontLoader { id: robotoRegular1; source: "../assets/Fonts/roboto.regular.ttf" }
            font.family: robotoRegular1.name
            font.pixelSize: 12
            visible: deltaK.batteryAlarmIcon
        }

    }

    //    Image {
    //        id: powerOffImage
    //        x: 275
    //        y: 28
    //        width: 20
    //        height: 20
    //        source: "../assets/Images/Alarm/Power failure.png"
    //        visible: deltaK.powerWarning
    //    }

    //    Image {
    //        id: wifiOffImage
    //        x: 300
    //        y: 28
    //        width: 20
    //        height: 20
    //        source: "../assets/Images/Alarm/No Wifi.png"
    //        visible: deltaK.wifiWarning
    //    }


    Image {
        id: arrowImage
        x: 234
        y: 74
        width: 12
        height: 6
        source: "../assets/Images/red_arrow.png"
    }

    Timer {
        repeat: true
        running: true
        interval: 1000
        onTriggered:
        {

            timeLabel.text = Qt.formatDateTime(new Date(), "hh:mm:ssAP")
            //dateLabel.text = Qt.formatDateTime(new Date(), "d/M/yyyy")
            dateLabel.text =  Qt.formatDateTime(new Date(), "MM/dd/yyyy")
        }
    }

    Rectangle {
        id: secondRectangle
        x: 0
        y: 80
        width: 480
        height: 28
        opacity: 0.8
        color: "#bd2f39"
        //visible: deltaK.redWarning
        Text {
            id: messageText
            x: 10
            y: 5
            width: 460
            height: 18
            color: "#ffffff"
            text: deltaK.alarmMessage
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            FontLoader {
                id: robotoRegular2
                source: "../assets/Fonts/roboto.regular.ttf"
            }
            font.family: robotoRegular2.name
            font.pixelSize: 14
        }

        SequentialAnimation on y {
                 id: redTextAnimation
                 loops: Animation.Infinite
                 alwaysRunToEnd: true
                 running: true
                 PropertyAnimation { id: animationOne; targets: [secondRectangle,arrowImage]; alwaysRunToEnd: true;property: "opacity"; to: 1; duration: 2000}
                 PauseAnimation { duration: 2000 }
                 PropertyAnimation { id: animationTwo; targets: [secondRectangle,arrowImage]; alwaysRunToEnd: true;property: "opacity"; to: 0; duration: 2000}
                 PauseAnimation { duration: 4000 }
         }
    }


}

