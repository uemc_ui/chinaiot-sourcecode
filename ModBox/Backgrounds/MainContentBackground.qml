import QtQuick 2.0
import QtQuick.Controls 2.0
import "../CustomComponents"

Rectangle {
    id: contentFrame
    width: 480
    height: 320
    color: "#1f2d36"
    property alias time: timeLabel.text
    property alias date: dateLabel.text
    property alias header: headerLabel.text
    property alias greenImage: greenImage.source
    property alias warningImage: warningImage
    property alias alarmImage: alarmImage
//    property alias powerWarningImage: powerWarningImage
//    property alias batteryWarningImage: batteryWarningImage
//    property alias wifiWarningImage: wifiWarningImage

    HeaderLabel{
        id: headerLabel
        x: 24
        y: 26
        width: 112
        height: 18
        text: ""
        verticalAlignment: Text.AlignVCenter
    }

    HeaderLabel {
        id: timeLabel
        x: 24
        y: 44
        width: 80
        height: 18
        text: ""
    }

    Rectangle {
        id: separator
        x: 110
        y: 46
        width: 2
        height: 14
        color: "#4f555a"
    }

    HeaderLabel {
        id: dateLabel
        x: 120
        y: 44
        width: 58
        height: 18
        text: Qt.formatDateTime(new Date(), "MM/dd/yyyy")
    }

    Image {
        id: warningImage
        x: 210
        y: 10
        width: 60
        height: 60
        visible: deltaK.batteryWarning || (deltaK.wifiWarning && deltaK.ethernetWarning) || deltaK.powerWarning || deltaK.noProbeDetected || deltaK.userLinkWarning ||deltaK.unAckAlarmCleared && !deltaK.tempAlarm
        source: "../assets/Images/Status_Warning_Dark_BG.png"
    }

    Image {
        id: silencedAlarmImage
        x: 210
        y: 10
        width: 60
        height: 60
        visible: deltaK.tempAlarm && deltaK.isSnooze
        source: "../assets/Images/Status_Bell_Snooze_Alarm_Red_BG.png"
    }

    Image {
        id: tempImageSnooze
        x: 275
        y: 5
        width: 8
        height: 20
        source: "../assets/Images/Alarm/Temp.png"
        visible: deltaK.tempAlarm && deltaK.isSnooze
    }

    Image {
        id: greenImage
        x: 210
        y: 10
        width: 58
        height: 58
        visible: !deltaK.batteryWarning && (!deltaK.wifiWarning || !deltaK.ethernetWarning) && !deltaK.powerWarning && !deltaK.tempAlarm && !deltaK.noProbeDetected && !deltaK.userLinkWarning &&!deltaK.unAckAlarmCleared
        //source: "../assets/Images/circle_right_new2.png"
        source: "../assets/Images/HealthIcon_Heart.png"
    }

    Image {
        id: alarmImage
        x: 210
        y: 10
        width: 60
        height: 60
        visible: deltaK.tempAlarm && !deltaK.isSnooze && !deltaK.wifiWarning && !deltaK.batteryWarning && !deltaK.powerWarning
        source: "../assets/Images/Alarm/Status_Icon_Alarm.png"
    }

    Timer {
        repeat: true
        running: true
        interval: 1000
        onTriggered:
        {
            timeLabel.text = Qt.formatDateTime(new Date(), "hh:mm:ssAP")
            dateLabel.text = Qt.formatDateTime(new Date(), "MM/dd/yyyy")
        }


    }

    Image {
        id: tempImage
        x: 275
        y: 30
        width: 8
        height: 20
        source: "../assets/Images/Alarm/Temp.png"
        visible: deltaK.tempAlarmIcon
    }

//    Image {
//        id: batteryLowImage
//        x: 248
//        y: 82
//        width: 12
//        height: 20
//        //source: "../assets/Images/Alarm/Battery low_new.png"
//        visible: deltaK.powerWarning


//        Connections
//        {
//            target: deltaK
//            onValueChanged:
//            {
//                var percent = parseInt ( deltaK.batteryPercentage ) ;

//                if ( percent > 80 && percent <= 100 )
//                    batteryLowImage.source = "../assets/Images/Alarm/Battery_full.png" ;
//                else if ( percent >= 40 )
//                    batteryLowImage.source = "../assets/Images/Alarm/Battery_medium.png" ;
//                else if ( percent >= 10 )
//                    batteryLowImage.source = "../assets/Images/Alarm/Battery low_new.png" ;
//                else
//                    batteryLowImage.source = "../assets/Images/Alarm/Battery_empty.png" ;
//            }
//        }
//    }

    Label {
        id: batteryLabel
        x: 264
        y: 82
        width: 40
        height: 20
        color: "#ffffff"
        text: deltaK.batteryPercentage
        verticalAlignment: Text.AlignVCenter
        FontLoader { id: robotoRegular1; source: "../assets/Fonts/roboto.regular.ttf" }
        font.family: robotoRegular1.name
        font.pixelSize: 12
        visible: deltaK.powerWarning
    }

//    Image {
//        id: powerWarningImage
//        x: 275
//        y: 30
//        width: 20
//        height: 20
//        visible: deltaK.powerWarning
//        source: "../assets/Images/Alarm/Power failure.png"
//    }

//    Image {
//        id: wifiWarningImage
//        x: 300
//        y: 30
//        width: 20
//        height: 20
//        visible: deltaK.wifiWarning
//        source: "../assets/Images/Alarm/No Wifi.png"
//    }

//    Image {
//        id: batteryWarningImage
//        x: 325
//        y: 30
//        width: 12
//        height: 20
//        visible: deltaK.batteryWarning
//        source: "../assets/Images/Alarm/Battery low.png"
//    }

    Image {
        id: timeImage
        x: 285
        y: 30
        width: 16
        height: 20
        source: "../assets/Images/Alarm_Sielnced/Snooze_Timer.png"
        visible: deltaK.tempAlarm
    }    
    Label {
        id: snoozeTimeMinSecLabel
        x: 307
        y: 34
        width: 42
        height: 16
        color: "#ffffff"
        text: deltaK.snoozeTimeMinSec
        FontLoader { id: robotoRegular; source: "../assets/Fonts/roboto.regular.ttf" }
        font.family: robotoRegular.name
        font.pixelSize: 14
        visible: deltaK.tempAlarm
    }
    Label {
        id: snoozeTimeLabel
        x: 422
        y: 32
        width: 40
        height: 16
        color: "#ffffff"
        text: (deltaK.snoozeTimeCounter/1000) + " sec"
        FontLoader { id: robotoRegular3; source: "../assets/Fonts/roboto.regular.ttf" }
        font.family: robotoRegular3.name
        font.pixelSize: 14
        //visible: deltaK.tempAlarm
        visible: false
    }


}
