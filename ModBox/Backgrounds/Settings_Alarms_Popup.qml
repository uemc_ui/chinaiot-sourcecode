import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "../assets"

Rectangle {
    width: 480
    height: 320
    color: "#1f2d36"

    Rectangle {
        id: rectangle
        x: 78
        y: 60
        width: 324
        height: 200
        color: "#ffffff"
        radius: 5

        Rectangle {
            id: rectangle1
            x: 0
            y: 48
            width: 324
            height: 152
            color: "#f6f6f6"

//            Button {
//                id: okButton
//                x: 87
//                y: 92
//                width: 150
//                height: 50
//                text: qsTr("")

//                style: ButtonStyle {
//                    background: Rectangle {
//                        color: "Transparent"
//                    }
//                }

//                Image {
//                    id: image
//                    x: 0
//                    y: 0
//                    width: 150
//                    height: 50
//                    source: "../assets/Images/ok.png"
//                }
//            }

            Button {
                id: okButton
                x: 87
                y: 92
                width: 150
                height: 50
                text: qsTr("")
                anchors.horizontalCenterOffset: 0
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 10
                style: ButtonStyle {
                    background: Rectangle {
                        color: "#2ea2ec"
                        radius: 5
                    }
                }

                 Text {
                    id: okButtonText
                    x: 63
                    y: 10
                    width: 78
                    height: 30
                    color: "#ffffff"
                    text: qsTr("OK") + languageTranslator.translatedText
                    FontLoader {
                        id: robotoRegular
                        source: "../assets/Fonts/roboto.regular.ttf"
                    }
                    verticalAlignment: Text.AlignVCenter
                    font.family: robotoRegular2.name
                    font.pixelSize: 18
                }

            }

            Label {
                id: messageLabel
                x: 32
                y: 32
                width: 260
                height: 34
                color: "#344550"
                text: "Your settings have been saved"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.NoWrap
                font.family: robotoRegular2.name
                font.pixelSize: 18
                FontLoader {
                    id: robotoRegular2
                    source: "../assets/Fonts/roboto.regular.ttf"
                }
            }
        }

        Label {
            id: headerLabel
            x: 12
            y: 12
            width: 150
            height: 24
            color: "#344550"
            text: "Settings Saved"
            verticalAlignment: Text.AlignVCenter
            font.bold: true
            font.family: robotoMedium1.name
            font.pixelSize: 18
            FontLoader {
                id: robotoMedium1
                source: "../assets/Fonts/roboto.medium.ttf"
            }
        }

        Button {
            id: cancelButton
            x: 286
            y: 10
            width: 28
            height: 28
            text: qsTr("")

            style: ButtonStyle{
                background: Rectangle{
                    color: "Transparent"
                }
            }

            Image {
                id: cancelImage
                x: 0
                y: 0
                width: 28
                height: 28
                source: "../assets/Images/cancel_blue.png"
            }
        }
    }

}
