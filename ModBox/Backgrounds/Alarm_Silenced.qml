import QtQuick 2.0
import QtQuick.Controls 2.0
import "../CustomComponents"

Rectangle {
    id: contentFrame
    width: 480
    height: 80
    color: "#1f2d36"
    property alias time: timeLabel.text
    property alias date: dateLabel.text
    property alias header: headerLabel.text
    property alias logo: image.source

    HeaderLabel{
        id: headerLabel
        x: 24
        y: 24
        width: 112
        height: 18
        text: deltaK.deviceName
        verticalAlignment: Text.AlignVCenter
    }

    HeaderLabel {
        id: timeLabel
        x: 24
        y: 46
        width: 80
        height: 18
    }

    Rectangle {
        id: separator
        x: 110
        y: 48
        width: 2
        height: 14
        color: "#4f555a"
    }

    HeaderLabel {
        id: dateLabel
        x: 118
        y: 46
        width: 58
        height: 18
        //text: Qt.formatDateTime(new Date(), "d/M/yyyy")
        text: Qt.formatDateTime(new Date(), "MM/dd/yyyy")
    }

    Image {
        id: image
        x: 210
        y: 10
        width: 60
        height: 60
        source: "../assets/Images/Status_Bell_Snooze_Alarm_Red_BG.png"
    }

    Image {
        id: powerImage
        x: 275
        y: 16
        width: 20
        height: 20
        source: "../assets/Images/Alarm/Power failure.png"
    }

    Image {
        id: wifiImage
        x: 300
        y: 16
        width: 20
        height: 20
        source: "../assets/Images/Alarm/No Wifi.png"
    }

    Image {
        id: batteryImage
        x: 325
        y: 16
        width: 12
        height: 20
        source: "../assets/Images/Alarm/Battery low.png"
    }

    Image {
        id: timeImage
        x: 276
        y: 44
        width: 16
        height: 20
        source: "../assets/Images/Alarm_Sielnced/Snooze_Timer.png"
    }

    Label {
        id: snoozeTimeLabel
        x: 298
        y: 48
        width: 40
        height: 16
        color: "#ffffff"
        text: "10:32"
        FontLoader { id: robotoRegular; source: "../assets/Fonts/roboto.regular.ttf" }
        font.family: robotoRegular.name
        font.pixelSize: 14
    }

    Timer {
        repeat: true
        running: true
        interval: 1000
        onTriggered: timeLabel.text = Qt.formatDateTime(new Date(), "hh:mm:ssAP")
    }
}
