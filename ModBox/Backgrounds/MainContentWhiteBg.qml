import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "../CustomComponents"
import "../assets"

Rectangle {
    id: contentFrame
    width: 480
    height: 320
    color: "#f6f7f7"
    property alias main: mainLabel.text
    property alias degrees: degreesLabel.text
    property alias time: timeLabel.text
    property alias date: dateLabel.text
    property alias header: nameLabel.text
    property alias logo: image.source

    Label{
        id: nameLabel
        x: 24
        y: 16
        text: ""
        width: 112
        height: 14
        color: "#344550"
        FontLoader { id: robotoRegular; source: "../assets/Fonts/roboto.regular.ttf" }
        font.family: robotoRegular.name
        font.pixelSize: 15
        verticalAlignment: Text.AlignVCenter
    }

//    Label{
//        id: timeLabel
//        x: 24
//        y: 33
//        width: 80
//        height: 14
//        text: ""
//        color: "#344550"
//        FontLoader { id: robotoRegular1; source: "../assets/Fonts/roboto.regular.ttf" }
//        font.family: robotoRegular.name
//        font.pixelSize: 15
//    }

    HeaderLabel{
        id: timeLabel
        x: 24
        y: 33
        width: 80
        height: 14
        text: ""
        color: "#344550"
        FontLoader { id: robotoRegular1; source: "../assets/Fonts/roboto.regular.ttf" }
        font.family: robotoRegular.name
        font.pixelSize: 15
    }

    Rectangle {
        id: separator
        x: 110
        y: 33
        width: 2
        height: 14
        color: "#344550"
    }

    Label{
        id: dateLabel
        x: 118
        y: 33
        width: 58
        height: 14
        text: Qt.formatDateTime(new Date(), "MM/dd/yyyy")
        color: "#344550"
        FontLoader { id: robotoRegular2; source: "../assets/Fonts/roboto.regular.ttf" }
        font.family: robotoRegular.name
        font.pixelSize: 15
    }

    Image {
        id: image
        x: 210
        y: 10
        width: 60
        height: 60
        source: "../assets/Images/Home_Screen/Status_Icon_BG_New.png"
    }

    Label {
        id: mainLabel
        x: 24
        y: 54
        width: 156
        height: 16
        color: "#344550"
        text: ""
        font.bold: true
        FontLoader { id: robotoMedium; source: "../assets/Fonts/roboto.medium.ttf" }
        font.family: robotoMedium.name
        font.pixelSize: 14
    }

    Label {
        id: degreesLabel
        x: 275
        y: 30
        width: 50
        height: 20
        color: "#344550"
        text: ""
        verticalAlignment: Text.AlignVCenter
        font.bold: true
        font.family: robotoMedium1.name
        font.pixelSize: 18
        FontLoader {
            id: robotoMedium1
            source: "../assets/Fonts/roboto.medium.ttf"
        }
    }

    Timer {
        repeat: true
        running: true
        interval: 1000
        onTriggered:
        {
            timeLabel.text = Qt.formatDateTime(new Date(), "hh:mm:ssAP")
            dateLabel.text = Qt.formatDateTime(new Date(), "MM/dd/yyyy")
        }
    }

}

