import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "../assets"

Rectangle {
    width: 480
    height: 320
    color: "#f6f7f7"


    Image {
        id: busyImage
        x: 80
        y: 10
        width: 68
        height: 68
        source: "../assets/Images/spinner.png"
        anchors.centerIn: parent
        visible: deltaK.isSetBusy

        NumberAnimation on rotation {
            from: 0; to: 360; running: busyImage.visible === true;
            loops: Animation.Infinite; duration: 700;
        }
    }

    Timer {
        id: configProgScreenTimer
        repeat: false
        running: false
        interval: 1000
        onTriggered: {
            console.log("Config Progress Timer Started")
            deltaK.readOTP()
            mainStack.pop(StackView.Immediate)
            mainStack.push(otpScreen, StackView.Immediate)
        }
    }

    function showConfigProgress(){

        configProgScreenTimer.start()

    }

    Button {
        id: backButton
        x: 24
        y: 140
        width: 24
        height: 40
        text: qsTr("")
        visible: deltaK.showBackApMode
        style: ButtonStyle {
            background: Rectangle {
                color: "Transparent"
            }
        }

        Image {
            id: backImage
            x: 0
            y: 0
            width: 24
            height: 40
            source: "../assets/Images/Settings/Back_icon_new.png"
            visible: deltaK.showBackApMode
        }

        onClicked: {
            deltaK.exitApMode()
            settingsScreen.settingsAutoExitTimeStart()
            mainStack.pop(StackView.Immediate)
            mainStack.push(settingsScreen, StackView.Immediate)

        }
    }

//    Button {
//        id: doneButton
//        x: 155
//        y: 260
//        width: 170
//        height: 50
//        text: qsTr("")
//        //enabled: deltaK.enableOTPButtonClick
//        anchors.horizontalCenterOffset: 0
//        anchors.horizontalCenter: parent.horizontalCenter
//        anchors.bottom: parent.bottom
//        anchors.bottomMargin: 10
//        style: ButtonStyle {
//            background: Rectangle {
//                color: "#2ea2ec"
//                radius: 5
//            }
//        }

//         Text {
//            id: doneButtonText
//            x: 63
//            y: 10
//            width: 78
//            height: 30
//            color: "#ffffff"
//            // color: "#2ea2ec"
//            text: qsTr("Done") + languageTranslator.translatedText
//            FontLoader {
//                id: robotoRegular
//                source: "../assets/Fonts/roboto.regular.ttf"
//            }
//            verticalAlignment: Text.AlignVCenter
//            font.family: robotoRegular.name
//            font.pixelSize: 18
//        }
//         onClicked: {

// //            if(parentPage === "Setting"){
// //                mainStack.pop(settingsScreen)
// //            }else{
// //                mainStack.push(otpScreen)
// //            }
//             deltaK.readOTP()
//             mainStack.pop(StackView.Immediate)
//             mainStack.push(otpScreen, StackView.Immediate)
//         }
//    }

    Label {
        id: apmodeLabel
        x: 160
        y: 24
        width: 160
        height: 18
        text: qsTr("Device Configuration") + languageTranslator.translatedText
        color: "#344550"
        font.bold: true
        FontLoader { id: robotoMedium3; source: "../assets/Fonts/roboto.medium.ttf" }
        font.family: robotoMedium3.name
        font.pixelSize: 18
        horizontalAlignment: Text.AlignHCenter
    }

    Rectangle {
        id: hotspotRectangle
        x: 70
        y: 145  //145
        width: 340
        height: 18
        color: "transparent"

        Label {
            id: hotspotNameLabel
            x: 0
            y: 0
            width: 170
            height: 18
            text: qsTr("Hotspot Name:") + languageTranslator.translatedText
            color: "#344550"
            FontLoader { id: robotoRegular1; source: "../assets/Fonts/roboto.regular.ttf" }
            font.family: robotoRegular1.name
            font.pixelSize: 18
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
        }

        Label {
            id: hotspotNameValueLabel
            x: 172
            y: 0
            width: 168
            height: 18
            text: deltaK.apName
            verticalAlignment: Text.AlignVCenter
            //            padding: 0.1
            color: "#344550"
            font.bold: true
            FontLoader { id: robotoMedium; source: "../assets/Fonts/roboto.medium.ttf" }
            font.family: robotoMedium.name
            font.pixelSize: 18
        }
    }


    property int timerCnt: 0

    Timer {
        id: apModeTimer
        running: true
        repeat: false
        interval: 5000
        //signal otpdonesig()
        triggeredOnStart: true
        onTriggered: {
            apModeTimer.stop()
            delay(500, function() {
                        //console.log('500 m Sec Delay Expired apModeTimer..Cnt '+timerCnt)

                if(timerCnt>=1)
                {
                    deltaK.setSettingsMouseClickStatus(true);
                    mouseArea.enabled=false
                }
                timerCnt++

                    })

        }
        }

    Label {
        id: apmodeInfoText
        x: 45
        y: 185  //185
        width: 410
        height: 30
        //text: qsTr("Please Use a Wi-Fi enabled device (smart phone, tablet etc.,) to find above hot spot name in Wi-Fi Settings")qsTr("\nOnce the device pairing is complete, open a browser on your device and type in the address http://192.168.50.1:8500 ") + qsTr("\nPlease clear mobile browser history before pairing your mobile to this hotspot")+ languageTranslator.translatedText
        text: qsTr("Please Use a Wi-Fi enabled device (smart phone, tablet etc.,) to find above hot spot name in Wi-Fi Settings\nOnce the device pairing is complete, open a browser on your device and type in the address http://192.168.50.1:8500 \nPlease clear mobile browser history before pairing your mobile to this hotspot")+ languageTranslator.translatedText
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        color: "#344550"
        FontLoader { id: robotoRegular2; source: "../assets/Fonts/roboto.regular.ttf" }
        font.family: robotoRegular2.name
        font.pixelSize: 16
        //verticalAlignment: Text.AlignVCenter
        //horizontalAlignment: Text.AlignLeft
    }
//    Label {
//        id: apmodeInfoText1
//        x: 50
//        y: 220  //185
//        width: 400
//        height: 30
//        text: qsTr("\nOnce the device pairing is complete, open a browser on your device and type in the address http://192.168.50.1:8500 ") + qsTr("\nPlease clear mobile browser history before pairing your mobile to this hotspot")+ languageTranslator.translatedText
//        wrapMode: Text.WordWrap
//        horizontalAlignment: Text.AlignHCenter
//        color: "#344550"
//        FontLoader { id: robotoRegular3; source: "../assets/Fonts/roboto.regular.ttf" }
//        font.family: robotoRegular3.name
//        font.pixelSize: 15
//        //verticalAlignment: Text.AlignVCenter
//        //horizontalAlignment: Text.AlignLeft
//    }

    MouseArea {
        id: mouseArea
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        anchors.fill: parent
        onClicked:   {
            //console.log('onPressed AP Mode '+deltaK.enableOTPButtonClick)
            apModeTimer.start()
            mouse.accepted = deltaK.enableOTPButtonClick
        }
    }

    Timer {
        id: timer
    }


        function delay(delayTime, cb) {
            timer.interval = delayTime;
            timer.repeat = false;
            timer.triggered.connect(cb);
            timer.start();
        }


//    Item  {
//        id: root
//        width: 200; height: 230

//        property double startTime: 0
//        property int secondsElapsed: 0

//        function restartCounter()  {

//                root.startTime = 0;

//            }

//        function timeChanged()  {
//            if(root.startTime==0)
//            {
//                root.startTime = new Date().getTime(); //returns the number of milliseconds since the epoch (1970-01-01T00:00:00Z);
//            }
//            var currentTime = new Date().getTime();
//            root.secondsElapsed = (currentTime-startTime)/1000;
//            if(root.secondsElapsed===10)
//            {
//                console.log('10 Seconds Fired')
//            }
//        }

//        Timer  {
//            id: elapsedTimer
//            interval: 1000;
//            running: true;
//            repeat: false;
//            onTriggered: root.timeChanged()
//        }

//        Text {
//            id: counterText
//            text: root.secondsElapsed
//        }
//    }


//        Component.onCompleted: {
//            console.log('&&&&&&&&AP MODE SCREEN LOADED')
//        }



}




//import QtQuick 2.0
//import QtQuick.Controls 2.0
//import QtQuick.Controls 1.4
//import QtQuick.Controls.Styles 1.4
//import "../assets"

//Rectangle {
//    width: 480
//    height: 320
//    color: "#f6f7f7"

//    property var parentPage: parentPage

//    function showPassword(){
//        console.log("Show password")
//        mainStack.push(otpScreen, StackView.Immediate)
//    }

////    Button {
////        id: doneButton
////        x: 155
////        y: 260
////        width: 170
////        height: 50
////        text: qsTr("Button")

////        style: ButtonStyle {
////            background: Rectangle {
////                color: "Transparent"
////            }
////        }

////        Image {
////            id: image
////            x: 0
////            y: 0
////            width: 170
////            height: 50
////            source: "../assets/Images/Done.png"
////        }

////        onClicked: {
////            if(parentPage === "Setting"){
////                mainStack.pop(settingsScreen)
////            }else{
////                mainStack.push(otpScreen)
////            }

////        }
////    }

//    Label {
//        id: apmodeLabel
//        x: 160
//        y: 24
//        width: 160
//        height: 18
//        text: qsTr("AP Mode")
//        color: "#344550"
//        font.bold: true
//        FontLoader { id: robotoMedium3; source: "../assets/Fonts/roboto.medium.ttf" }
//        font.family: robotoMedium3.name
//        font.pixelSize: 18
//        horizontalAlignment: Text.AlignHCenter
//    }

//    Rectangle {
//        id: hotspotRectangle
//        x: 70
//        y: 151
//        width: 340
//        height: 18
//        color: "transparent"

//        Label {
//            id: hotspotNameLabel
//            x: 0
//            y: 0
//            width: 170
//            height: 18
//            text: qsTr("Hotspot Name:")
//            color: "#344550"
//            FontLoader { id: robotoRegular1; source: "../assets/Fonts/roboto.regular.ttf" }
//            font.family: robotoRegular1.name
//            font.pixelSize: 18
//            verticalAlignment: Text.AlignVCenter
//            horizontalAlignment: Text.AlignRight
//        }

//        Label {
//            id: hotspotNameValueLabel
//            x: 172
//            y: 0
//            width: 168
//            height: 18
//            text: deltaK.apName
//            verticalAlignment: Text.AlignVCenter
////            padding: 0.1
//            color: "#344550"
//            font.bold: true
//            FontLoader { id: robotoMedium; source: "../assets/Fonts/roboto.medium.ttf" }
//            font.family: robotoMedium.name
//            font.pixelSize: 18
//        }
//    }

//    Timer {
//        id: requestPasswordTimerForTest
//        repeat: false
//        running: false
//        interval: 20000
//        onTriggered: {
//            console.log("Init password Timer")
//            deltaK.initPasswordRequest()
//        }
//    }

//    Component.onCompleted: {
//        requestPasswordTimerForTest.start()
//    }
//}
