import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "../Backgrounds"
import "../assets"

MainContentWhiteBg {
    main: "Memory Status"
    time: "11:42:26 AM"
    header: deltaK.deviceName
    degrees: "-80°C"
    date: "5/12/15"

//    Button {
//        id: okButton
//        x: 165
//        y: 260
//        width: 150
//        height: 50
//        text: qsTr("")

//        style: ButtonStyle {
//            background: Rectangle {
//                color: "Transparent"
//            }
//        }

//        Image {
//            id: okImage
//            x: 0
//            y: 0
//            width: 150
//            height: 50
//            source: "../assets/Images/ok.png"
//        }

//        onClicked: {
//            mainStack.pop(StackView.Immediate)
//        }
//    }

    Button {
        id: okButton
        x: 165
        y: 260
        width: 150
        height: 50
        text: qsTr("")
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        style: ButtonStyle {
            background: Rectangle {
                color: "#2ea2ec"
                radius: 5
            }
        }

         Text {
            id: okButtonText
            x: 73
            y: 10
            width: 78
            height: 30
            color: "#ffffff"
            text: qsTr("OK") + languageTranslator.translatedText
            FontLoader {
                id: robotoRegular
                source: "../assets/Fonts/roboto.regular.ttf"
            }
            verticalAlignment: Text.AlignVCenter
            font.family: robotoRegular.name
            font.pixelSize: 18
        }

         onClicked: {
             mainStack.pop(StackView.Immediate)
         }

    }

    ProgressBar {
        id: progressBar
        x: 80
        y: 155
        width: 300
        height: 10

    }

    Label {
        id: minimumLabel
        x: 51
        y: 150
        width: 23
        height: 20
        color: "#344550"
        text: "0%"
        font.bold: true
        font.family: robotoMedium1.name
        font.pixelSize: 18
        FontLoader {
            id: robotoMedium1
            source: "../assets/Fonts/roboto.medium.ttf"
        }
    }

    Label {
        id: maxmumLabel
        x: 386
        y: 150
        width: 47
        height: 20
        color: "#344550"
        text: "100%"
        font.bold: true
        font.family: robotoMedium2.name
        font.pixelSize: 18
        FontLoader {
            id: robotoMedium2
            source: "../assets/Fonts/roboto.medium.ttf"
        }
    }

}
