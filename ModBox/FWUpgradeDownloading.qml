import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "./assets"

Rectangle {
    width: 480
    height: 320
    color: "#f6f7f7"

    Label {
        id: apmodeLabel
        x: 160
        y: 24
        width: 160
        height: 18
        text: qsTr("Firmware Upgrade") + languageTranslator.translatedText
        color: "#344550"
        font.bold: true
        FontLoader { id: robotoMedium3; source: "./assets/Fonts/roboto.medium.ttf" }
        font.family: robotoMedium3.name
        font.pixelSize: 18
        horizontalAlignment: Text.AlignHCenter
    }

    Label {
        id: ipAddressLabel
        x: 140
        y: 70
        width: 200
        height: 20
        color: "#33444f"
        text: qsTr("Downloading..") + languageTranslator.translatedText
        font.family: robotoRegular2.name
        font.pixelSize: 18
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        FontLoader {
            id: robotoRegular2
            source: "./assets/Fonts/roboto.regular.ttf"
        }
    }

    ProgressBar {
        id: progressBar
        x: 80
        y: 150
        width: 320
        height: 20
        value: deltaK.progressvalueChanged
        indeterminate: false
        minimumValue: 0
        maximumValue: 100
        style: ProgressBarStyle {
            background: Rectangle {
                radius: 2
                color: "#e0e0e0"
                implicitWidth: 400
                implicitHeight: 20
            }
            progress: Rectangle {
                color: "#009900"
            }
        }

        onValueChanged: {
            if (progressBar.value == 100)
            {
                mainStack.push(fwupgradeCompleteScreen)
            }
            //progressBar.value < progressBar.maximumValue ? progressBar.value += 1.0 : progressBar.value = progressBar.minimumValue
        }
    }

    Label {
        id: zeroLabel
        x: 48
        y: 151
        width: 30
        height: 18
        color: "#344550"
        text: qsTr("0%")
        verticalAlignment: Text.AlignVCenter
        FontLoader {
            id: robotoMedium4
            source: "./assets/Fonts/roboto.medium.ttf"
        }
        horizontalAlignment: Text.AlignHCenter
        font.bold: true
        font.family: robotoMedium4.name
        font.pixelSize: 18
    }

    Label {
        id: hundredLabel
        x: 402
        y: 151
        width: 44
        height: 18
        color: "#344550"
        text: qsTr("100%")
        FontLoader {
            id: robotoMedium5
            source: "./assets/Fonts/roboto.medium.ttf"
        }
        horizontalAlignment: Text.AlignHCenter
        font.bold: true
        font.family: robotoMedium5.name
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: 18
    }

}
