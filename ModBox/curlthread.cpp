#include <QtCore>
#include <QMutexLocker>
#include <unistd.h>

#include "curlthread.h"


/**
 * @brief CurlThread::CurlThread
 */
CurlThread::CurlThread()
{
    stopped = false;
}

/**
 * @brief CurlThread::run
 */
void CurlThread::run()
{
    forever
    {
       {
            QMutexLocker locker(&mutex);
            if(stopped)
            {
                stopped = false;
                break;
            }
       }

        //qDebug()<<"CurlThread Running";

        cmdResp = curl_easy_perform(recdCurl);

        http_code = 0;
        curl_easy_getinfo (recdCurl, CURLINFO_RESPONSE_CODE, &http_code);


    //#ifdef  DEBUG_ON
        qDebug()<<"CURLTHREAD: UpdateMetaData Output is "<<QString::number(cmdResp);
        qDebug()<<"CURLTHREAD: UpdateMetaData Command to Cloud Post Response: "<<QString::number(http_code);
    //#endif  //DEBUG_ON

        if(http_code==200)
        {
            stopped = true;
        }
        else
        {
            delay(5);
        }






        //delay(4);

    }
}

/**
 * @brief CurlThread::stop
 */
void CurlThread::stop()
{
    QMutexLocker locker(&mutex);
    stopped = true;
}


/**
 * @brief CurlThread::delay
 * @param sec
 */
void CurlThread::delay(int sec)
{
    dieTime= QTime::currentTime().addSecs(sec);
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}

void CurlThread::getData(CURL *curl)
{
    //curl_easy_perform(curl);
    recdCurl = curl;
}
