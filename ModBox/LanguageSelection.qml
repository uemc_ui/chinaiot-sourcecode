import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "./assets"

Rectangle {
    width: 480
    height: 320
    color: "#f6f7f7"

    property var parentPage1: parentPage1

//    BusyIndicator {
//        anchors.centerIn: parent
//        running: deltaK.isSetBusy
//    }

    function setCheckedEnglishRB(){
        //console.log("<----->Inside setCheckedEnglishRB Function")
        englishRadioButton.checked = true
    }

    function setCheckedChineseRB(){
        //console.log("<----->Inside setCheckedChineseRB Function")
        chineseRadioButton.checked = true
    }

    Image {
        id: busyImage
        x: 80
        y: 10
        width: 68
        height: 68
        source: "./assets/Images/spinner.png"
        anchors.centerIn: parent
        visible: deltaK.isSetBusy

        NumberAnimation on rotation {
            from: 0; to: 360; running: busyImage.visible === true;
            loops: Animation.Infinite; duration: 700;
        }
    }

    Button {
        id: doneButton
        x: 155
        y: 260
        width: 170
        height: 50
        text: qsTr("")
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        style: ButtonStyle {
            background: Rectangle {
                color: "#2ea2ec"
                radius: 5
            }
        }

         Text {
            id: doneButtonText
            x: 63
            y: 10
            width: 78
            height: 30
            color: "#ffffff"
            // color: "#2ea2ec"
            text: qsTr("Done") + languageTranslator.translatedText
            FontLoader {
                id: robotoRegular2
                source: "./assets/Fonts/roboto.regular.ttf"
            }
            verticalAlignment: Text.AlignVCenter
            font.family: robotoRegular2.name
            font.pixelSize: 18
        }
         onClicked: {
            doneButton.enabled = false
            englishRadioButton.enabled = false
            chineseRadioButton.enabled = false
             if(parentPage1 === "Setting1"){
                 settingsScreen.settingsAutoExitTimeStart()
                 mainStack.pop(settingsScreen,StackView.Immediate)
             }else{
                 deltaK.startApMode();
                 mainStack.pop(StackView.Immediate)
                 mainStack.push(apModeScreen, StackView.Immediate)
             }
            doneButton.enabled = true
             englishRadioButton.enabled = true
             chineseRadioButton.enabled = true
         }
    }


    Label {
        id: apmodeLabel
        x: 130
        y: 24
        width: 220
        height: 22
        text: qsTr("Language Selection") + languageTranslator.translatedText
        color: "#33444f"
        font.bold: true
        FontLoader { id: robotoMedium3; source: "../assets/Fonts/roboto.medium.ttf" }
        font.family: robotoMedium3.name
        font.pixelSize: 18
        horizontalAlignment: Text.AlignHCenter
    }

    RadioButton{
        id: englishRadioButton
        x: 183
        y: 100
        width: 114
        text: qsTr("English") + languageTranslator.translatedText
//        checked: true
//        checked: position === 0

        style: RadioButtonStyle {
            indicator: Rectangle {
                width: 24
                height: 24
                radius: 12
                border.color: "#cbc6cd"
                border.width: 4
                Rectangle {
                    x: 4
                    y: 4
                    height: 16
                    width: 16
                    visible: control.checked
                    color: "#2ea2ec"
                    radius: 8
                    anchors.margins: 4
                }
            }

            label: Label {
                x: 4
                text: control.text
                color: "#33444f"
                FontLoader {
                    id: robotoLight
                    source: "./assets/Fonts/roboto.light.ttf"
                }
                font.family: robotoLight.name
                font.pixelSize: 20
            }
        }

        onClicked: {
            languageTranslator.translateToEnglish()
            resetRadioButtons()
            englishRadioButton.checked = true
//            position = 0
            deltaK.language = "English"
            deltaK.updateLangFlagEnglish()
        }
    }

    RadioButton{
        id: chineseRadioButton
        x: 183
        y: 150
        text: qsTr("Chinese") + languageTranslator.translatedText
//        enabled: false
//        checked: position === 1

        style: RadioButtonStyle {
            indicator: Rectangle {
                width: 24
                height: 24
                radius: 12
                border.color: "#cbc6cd"
                border.width: 4
                Rectangle {
                    x: 4
                    y: 4
                    height: 16
                    width: 16
                    visible: control.checked
                    color: "#2ea2ec"
                    radius: 8
                    anchors.margins: 4
                }
            }

            label: Label {
                x: 4
                color: "#33444f"
                FontLoader {
                    id: robotoLight1
                    source: "./assets/Fonts/roboto.light.ttf"
                }
                font.family: robotoLight1.name
                font.pixelSize: 20
                text: control.text
            }
        }

        onClicked: {
 	    languageTranslator.translateToChinese()
            resetRadioButtons()
            chineseRadioButton.checked = true
            //            position = 1
            deltaK.language = "Chinese"
            deltaK.updateLangFlagChinese()
        }
    }


    function resetRadioButtons() {
        englishRadioButton.checked = false
        chineseRadioButton.checked = false
    }

}
