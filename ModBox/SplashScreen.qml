import QtQuick 2.4
import QtQuick.Controls 2.0

Rectangle {
    id: rectangle
    width: 480
    height: 320
    color: "#1f2d36"

    property var appStartLang

    Image {
        id: splashImage
        width: 480
        height: 320
        fillMode: Image.PreserveAspectCrop
        source: "./assets/Images/01_Boot_Screen.png"
        scale: 0 // 0.85
        opacity: 0
    }


    ParallelAnimation {
        running: true

        ScaleAnimator {
            target: splashImage
            from: 0
            to: 1
            duration: 1600
        }

        OpacityAnimator {
            target: splashImage
            from: 0
            to: 1
            duration: 1600
        }
    }

    Timer {
        running: true
        repeat: false
        interval: 3600

        onTriggered: {
            if(deltaK.appStartLang === true)       //settings not done... start with languageSelection screen
            {
                //console.log("@@@@@@@AppStartLang is true")

                mainStack.pop(StackView.Immediate)
                mainStack.push(settingsAccess,StackView.Immediate)
                //mainStack.push(languageSelection, StackView.Immediate)
            }
            else if(deltaK.appStartLang === false) //settings already done... start with home screen
            {
                //console.log("@@@@@@@@@AppStartLang is false")
                mainStack.pop(StackView.Immediate)
                mainStack.push(homeScreen, StackView.Immediate)
            }



        }

    }
}
