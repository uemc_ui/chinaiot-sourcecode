import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "./assets"

Rectangle {
    width: 480
    height: 320
    color: "#f6f7f7"

    property var parentPage2: parentPage2

    Image {
        id: busyImage
        x: 80
        y: 10
        width: 68
        height: 68
        source: "./assets/Images/spinner.png"
        anchors.centerIn: parent
        visible: deltaK.isSetBusy

        NumberAnimation on rotation {
            from: 0; to: 360; running: busyImage.visible === true;
            loops: Animation.Infinite; duration: 700;
        }
    }


    Button {
        id: exitButton
        x: 155
        y: 260
        width: 170
        height: 50
        text: qsTr("")
        anchors.horizontalCenterOffset: -115
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        style: ButtonStyle {
            background: Rectangle {
                color: "#2ea2ec"
                radius: 5
            }
        }

         Text {
            id: exitButtonText
            x: 63
            y: 10
            width: 78
            height: 30
            color: "#ffffff"
            // color: "#2ea2ec"
            text: qsTr("Exit") + languageTranslator.translatedText
            FontLoader {
                id: robotoRegular
                source: "./assets/Fonts/roboto.regular.ttf"
            }
            verticalAlignment: Text.AlignVCenter
            font.family: robotoRegular2.name
            font.pixelSize: 18
        }
         onClicked: {
             retryButton.enabled = false
             exitButton.enabled = false

             if(parentPage2 === "Setting2"){
                 settingsScreen.settingsAutoExitTimeStart()
                 mainStack.pop(settingsScreen,StackView.Immediate)

             }else{
                 console.log("Inside ELSE Condition READQRFail");
                deltaK.completedDeviceConfig()
                 mainStack.pop(StackView.Immediate)
                 mainStack.push(homeScreen, StackView.Immediate)

             }
             retryButton.enabled = true
             exitButton.enabled = true

         }
    }


    Label {
        id: apmodeLabel
        x: 160
        y: 24
        width: 160
        height: 18
        text: qsTr("Read QR Status") + languageTranslator.translatedText
        color: "#344550"
        font.bold: true
        FontLoader { id: robotoMedium3; source: "./assets/Fonts/roboto.medium.ttf" }
        font.family: robotoMedium3.name
        font.pixelSize: 18
        horizontalAlignment: Text.AlignHCenter
    }

    Label {
        id: ipAddressLabel
        x: 90
        y: 138
        width: 300
        height: 20
        color: "#33444f"
        text: qsTr("Failed to obtain QR code!") + languageTranslator.translatedText
        font.family: robotoRegular2.name
        font.pixelSize: 18
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        FontLoader {
            id: robotoRegular2
            source: "./assets/Fonts/roboto.regular.ttf"
        }
    }

    Label {
        id: qrFailedMessage
        x: 60
        y: 190
        width: 380
        height: 22
        color: "#33444f"
        text: qsTr("Please check your internet connection and click Retry") + languageTranslator.translatedText
        wrapMode: Text.WordWrap
        font.pixelSize: 18
        font.family: robotoRegular2.name
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }


    Button {
        id: retryButton
        x: 151
        y: 260
        width: 170
        height: 50
        text: qsTr("")
        style: ButtonStyle {
            background: Rectangle {
                color: "#2ea2ec"
                radius: 5
            }
        }
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.horizontalCenterOffset: 114
        Text {
            id: retryButtonText
            x: 63
            y: 10
            width: 78
            height: 30
            color: "#ffffff"
            text: qsTr("Retry") + languageTranslator.translatedText
            font.pixelSize: 18
            font.family: robotoRegular2.name
            verticalAlignment: Text.AlignVCenter
            FontLoader {
                id: robotoRegular1
                source: "./assets/Fonts/roboto.regular.ttf"
            }
        }
        anchors.bottomMargin: 10

        onClicked:{
            retryButton.enabled = false
            exitButton.enabled = false
            deltaK.readQRImage()

            while(busyImage.visible===true)
            {

            }
//            mainStack.pop(StackView.Immediate)
            if(deltaK.readQRStatus === true)    //qr read is success
            {
                qrCode.startQRAutoExitTimer()
                mainStack.push(qrCode, StackView.Immediate)
                console.log('QR Code Screen is displayed')
                deltaK.readQRStatus = false
            }
            retryButton.enabled = true
            exitButton.enabled = true

        }
    }

}
