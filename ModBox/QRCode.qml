import QtQuick 2.0
import QtQuick.Controls 2.0
import "./Backgrounds"
import "./assets"

SetupContentBackground {

    property alias qrCodeImage: qrCodeImage
    headerLabel: qsTr("QR Code") + languageTranslator.translatedText

    property var parentPage2: parentPage2

    function clickedDoneQR(){
        console.log("\n\nInside QR Code DONE Clicked function\n\n")

        qrScreenAutoExitTimer.stop()

        if(parentPage2 === "Setting2"){

            mainStack.pop(settingsScreen,StackView.Immediate)
            mainStack.pop(StackView.Immediate)


        }else{

            deltaK.completedDeviceConfig()
            mainStack.push(homeScreen, StackView.Immediate)

        }

    }

    function startQRAutoExitTimer(){

        qrScreenAutoExitTimer.start()

    }

    Timer {
        id: qrScreenAutoExitTimer
        repeat: false
        running: false
        interval: 60000    //60 sectimer will fire to auto redirect
        onTriggered: {
            console.log('qrScreenAutoExitTimer Fired');

            if(parentPage2 === "Setting2"){

                mainStack.pop(settingsScreen,StackView.Immediate)
                mainStack.pop(StackView.Immediate)


            }else{

                deltaK.completedDeviceConfig()
                mainStack.push(homeScreen, StackView.Immediate)

            }


        }
    }

    doneButton.onClicked: {

        qrScreenAutoExitTimer.stop()

        if(parentPage2 === "Setting2"){
            //settingsScreen.settingsAutoExitTimeStart()
            mainStack.pop(settingsScreen,StackView.Immediate)
            mainStack.pop(StackView.Immediate)
            //qrScreenAutoExitTimer.stop()

        }else{


            deltaK.completedDeviceConfig()
            mainStack.push(homeScreen, StackView.Immediate)


        }
    }

    property int timerCnt: 0

    Timer {
        id: qrTimer
        running: true
        repeat: false
        interval: 5000
        //signal otpdonesig()
        triggeredOnStart: true
        onTriggered: {
            qrTimer.stop()
            delay(500, function() {
                //console.log('500 m Sec Delay Expired apModeTimer..Cnt '+timerCnt)

                if(timerCnt>=1)
                {
                    deltaK.setSettingsMouseClickStatus(false);
                    mouseArea.enabled=false
                }
                timerCnt++

            })

        }
    }

    Label {
        id: qrCodeInfoLabel
        x: 174
        y: 55
        width: 306

        height: 220
        //text: qsTr("Please Scan the below QR Code from Instrument Connect App") + languageTranslator.translatedText
        verticalAlignment: Text.AlignTop
        //text: qsTr("1. Launch the Instrument Connect mobile app\navailable from iTunes and Google Play\n2.Within the app, tap the + icon and select QR code option\n3.Scan the QR code to connect the unit  ") + languageTranslator.translatedText
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignLeft
        color: "#344550"
        text: qsTr("1. Launch the Instrument Connect mobile app\navailable from iTunes and Google Play") + qsTr("\n\n2.Within the app, tap the + icon and select QR code option") + qsTr("\n\n3.Scan the QR code to connect the unit") + qsTr("\nConnection times vary depending upon network speeds\n\nAndroid users in China must download the application from the following link:") +  languageTranslator.translatedText + ("\nhttp://downloads.thermofisher.com/APK/InstrumentConnect.apk")



        FontLoader { id: robotoRegular2; source: "../assets/Fonts/roboto.regular.ttf" }
        font.family: robotoRegular2.name
        font.pixelSize: 12
    }


    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked:   {
            //console.log('onPressed AP Mode '+deltaK.enableOTPButtonClick)
            qrTimer.start()
            mouse.accepted = deltaK.enableOTPButtonClick
        }

        Image {
            id: qrCodeImage
            x: 8
            y: 78
            width: 160
            height: 160
            source: deltaK.qrImage
        }
    }

    Timer {
        id: timer
    }


    function delay(delayTime, cb) {
        timer.interval = delayTime;
        timer.repeat = false;
        timer.triggered.connect(cb);
        timer.start();
    }


}
