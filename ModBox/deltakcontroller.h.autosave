#ifndef DELTAKCONTROLLER_H
#define DELTAKCONTROLLER_H

#include <QObject>
#include <QtCore>
#include <QTcpSocket>
#include <QTcpServer>
#include "thread.h"
#include "networkthread.h"
#include "curlthread.h"
#include "wifithread.h"
#include "languagetranslator.h"


class DeltaKController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString deviceName READ deviceName WRITE setDeviceName NOTIFY valueChanged)
    Q_PROPERTY(QString popUpString READ popUpString WRITE setPopUpString NOTIFY valueChanged)
    Q_PROPERTY(QString apName READ apName WRITE setApName NOTIFY valueChanged)
    Q_PROPERTY(QString apIP READ apIP WRITE setApIP NOTIFY valueChanged)
    Q_PROPERTY(QString referenceTemperature READ referenceTemperature WRITE setReferenceTemperature NOTIFY valueChanged)
    Q_PROPERTY(QString temperature READ temperature WRITE setTemperature NOTIFY valueChanged)
    Q_PROPERTY(QString snoozeTimeMinSec READ snoozeTimeMinSec WRITE setSnoozeTimeMinSec NOTIFY valueChanged)
    Q_PROPERTY(QString failMessage READ failMessage WRITE setFailMessage NOTIFY valueChanged)
    Q_PROPERTY(QString wifiStatusText READ wifiStatusText WRITE setWifiStatusText NOTIFY valueChanged)
    Q_PROPERTY(QString assetStatusText READ assetStatusText WRITE setAssetStatusText NOTIFY valueChanged)
    Q_PROPERTY(bool tempAlarm READ tempAlarm WRITE setTempAlarm NOTIFY valueChanged)
    Q_PROPERTY(bool chineseLang READ chineseLang WRITE setChineseLang NOTIFY valueChanged)
    Q_PROPERTY(bool wifiWarning READ wifiWarning WRITE setWifiWarning NOTIFY valueChanged)
    Q_PROPERTY(bool ethernetWarning READ ethernetWarning WRITE setEthernetWarning NOTIFY valueChanged)
    Q_PROPERTY(bool powerWarning READ powerWarning WRITE setPowerWarning NOTIFY valueChanged)
    Q_PROPERTY(bool batteryWarning READ batteryWarning WRITE setBatteryWarning NOTIFY valueChanged)
    Q_PROPERTY(int snoozeTime READ snoozeTime WRITE setSnoozeTime NOTIFY valueChanged)
    Q_PROPERTY(int snoozeTimeCounter READ snoozeTimeCounter WRITE setSnoozeTimeCounter NOTIFY valueChanged)
    Q_PROPERTY(bool isSnooze READ isSnooze WRITE setIsSnooze NOTIFY valueChanged)
    Q_PROPERTY(QString language READ language WRITE setLanguage NOTIFY valueChanged)
    Q_PROPERTY(QString pinNumber READ pinNumber WRITE setPinNumber NOTIFY valueChanged)
    Q_PROPERTY(bool isOTP READ isOTP WRITE setIsOTP NOTIFY valueChanged)
    Q_PROPERTY(QString OTP READ OTP WRITE setOTP NOTIFY valueChanged)
    Q_PROPERTY(bool isDeeplazer READ isDeeplazer WRITE setIsDeeplazer NOTIFY valueChanged)
    Q_PROPERTY(bool isFWUpgrade READ isFWUpgrade WRITE setIsFWUpgrade NOTIFY valueChanged)
    Q_PROPERTY(int progressvalueChanged READ progressvalueChanged WRITE setProgressvalueChanged NOTIFY valueChanged)
    Q_PROPERTY(QString qrImage READ qrImage WRITE setQrImage NOTIFY valueChanged)
    Q_PROPERTY(QString alarmMessage READ alarmMessage WRITE setAlarmMessage NOTIFY valueChanged)
    Q_PROPERTY(bool isSetBusy READ isSetBusy WRITE setBusy NOTIFY valueChanged)
    Q_PROPERTY(bool isShowPopUp READ isShowPopUp WRITE showPopUp NOTIFY valueChanged)
    Q_PROPERTY(bool appStartLang READ appStartLang WRITE setAppStartLang NOTIFY valueChanged)
    Q_PROPERTY(bool pinNotSet READ pinNotSet WRITE setPinNotSet NOTIFY valueChanged)
    Q_PROPERTY(bool showForgotPin READ showForgotPin WRITE setShowForgotPin NOTIFY valueChanged)
    Q_PROPERTY(QString batteryPercentage READ batteryPercentage WRITE setBatteryPercentage NOTIFY valueChanged)
    Q_PROPERTY(QString wifiPercentage READ wifiPercentage WRITE setWifiPercentage NOTIFY valueChanged)
    Q_PROPERTY(QString warningAlarmMessage READ warningAlarmMessage WRITE setWarningAlarmMessage NOTIFY valueChanged)
    Q_PROPERTY(QString warningAlarmMessageText READ warningAlarmMessageText WRITE setWarningAlarmMessageText NOTIFY valueChanged)
    Q_PROPERTY(bool tempAlarmIcon READ tempAlarmIcon WRITE setTempAlarmIcon NOTIFY valueChanged)
    Q_PROPERTY(bool batteryAlarmIcon READ batteryAlarmIcon WRITE setBatteryAlarmIcon NOTIFY valueChanged)
    Q_PROPERTY(bool yellowWarning READ yellowWarning WRITE setYellowWarning NOTIFY valueChanged)
    Q_PROPERTY(bool unAckAlarmCleared READ unAckAlarmCleared WRITE setUnAckAlarmCleared NOTIFY valueChanged)
    Q_PROPERTY(bool userLinkWarning READ userLinkWarning WRITE setUserLinkWarning NOTIFY valueChanged)
    Q_PROPERTY(bool noProbeDetected READ noProbeDetected WRITE setNoProbeDetected NOTIFY valueChanged)
    Q_PROPERTY(bool enableOTPButton READ enableOTPButton WRITE setEnableOTPButton NOTIFY valueChanged)
    Q_PROPERTY(bool wifiUpdateSucces READ wifiUpdateSucces WRITE setWifiUpdateSucces NOTIFY valueChanged)
    Q_PROPERTY(bool wifiIconSucces READ wifiIconSucces WRITE setWifiIconSucces NOTIFY valueChanged)
    Q_PROPERTY(bool showConfigProg READ showConfigProg WRITE setShowConfigProg NOTIFY valueChanged)
    Q_PROPERTY(bool wifiIconFail READ wifiIconFail WRITE setWifiIconFail NOTIFY valueChanged)
    Q_PROPERTY(bool showBackApMode READ showBackApMode WRITE setShowBackApMode NOTIFY valueChanged)
    Q_PROPERTY(bool showBackKeyPad READ showBackKeyPad WRITE setShowBackKeyPad NOTIFY valueChanged)
    Q_PROPERTY(bool assetIconSucces READ assetIconSucces WRITE setAssetIconSucces NOTIFY valueChanged)
    Q_PROPERTY(bool assetIconFail READ assetIconFail WRITE setAssetIconFail NOTIFY valueChanged)
    Q_PROPERTY(bool assetUpdateSucces READ assetUpdateSucces WRITE setAssetUpdateSucces NOTIFY valueChanged)

    Q_PROPERTY(bool assetUpdateFail READ assetUpdateFail WRITE setAssetUpdateFail NOTIFY valueChanged)
    Q_PROPERTY(bool readQRStatus READ readQRStatus WRITE setReadQRStatus NOTIFY valueChanged)
    Q_PROPERTY(QString userLinkingStatusMsg READ userLinkingStatusMsg WRITE setUserLinkingStatusMsg NOTIFY valueChanged)
    Q_PROPERTY(bool enableOTPButtonClick READ enableOTPButtonClick WRITE setEnableOTPButtonClick NOTIFY valueChanged)
    Q_PROPERTY(bool redWarning READ redWarning WRITE setRedWarning NOTIFY valueChanged)
    Q_PROPERTY(QString cloudConnStatusMsg READ cloudConnStatusMsg WRITE setCloudConnStatusMsg NOTIFY valueChanged)

public:

    QTcpSocket* client;
    QTcpSocket* readSocket;
    explicit DeltaKController(QObject *parent = 0);

    bool isSetBusy() const;
    void setBusy(bool busyFlag);

    bool isShowPopUp() const;
    void showPopUp(bool popUpFlag);

    bool appStartLang() const;
    void setAppStartLang(bool appStartLang);

    bool pinNotSet() const;
    void setPinNotSet(bool pinNotSet);

    bool showForgotPin() const;
    void setShowForgotPin(bool showForgotPin);

    bool enableOTPButton() const;
    void setEnableOTPButton(bool enableOTPButton);

    bool wifiUpdateSucces() const;
    void setWifiUpdateSucces(bool wifiUpdateSucces);

    bool wifiIconSucces() const;
    void setWifiIconSucces(bool wifiIconSucces);

    bool showConfigProg() const;
    void setShowConfigProg(bool showConfigProg);

    bool assetIconSucces() const;
    void setAssetIconSucces(bool assetIconSucces);

    bool wifiIconFail() const;
    void setWifiIconFail(bool wifiIconFail);

    bool showBackApMode() const;
    void setShowBackApMode(bool showBackApMode);

    bool showBackKeyPad() const;
    void setShowBackKeyPad(bool showBackKeyPad);

    bool assetIconFail() const;
    void setAssetIconFail(bool assetIconFail);



    bool assetUpdateSucces() const;
    void setAssetUpdateSucces(bool assetUpdateSucces);


    bool assetUpdateFail() const;
    void setAssetUpdateFail(bool assetUpdateFail);

    bool enableOTPButtonClick() const;
    void setEnableOTPButtonClick(bool enableOTPButtonClick);

    bool readQRStatus() const;
    void setReadQRStatus(bool readQRStatus);

    QString deviceName() const;
    void setDeviceName(const QString &deviceName);

    QString popUpString() const;
    void setPopUpString(const QString &popUpString);

    QString apName() const;
    void setApName(const QString &apName);

    QString apIP() const;
    void setApIP(const QString &apIP);

    QString temperature();
    void setTemperature(const QString &temperature);

    QString snoozeTimeMinSec();
    void setSnoozeTimeMinSec(const QString &snoozeTimeMinSec);


    QString failMessage();
    void setFailMessage(const QString &failMessage);

    QString wifiStatusText();
    void setWifiStatusText(const QString &wifiStatusText);

    QString assetStatusText();
    void setAssetStatusText(const QString &assetStatusText);


    QString referenceTemperature() const;
    void setReferenceTemperature(QString referenceTemperature);

    bool wifiWarning() const;
    void setWifiWarning(bool wifiWarning);

    bool ethernetWarning() const;
    void setEthernetWarning(bool ethernetWarning);


    bool powerWarning() const;
    void setPowerWarning(bool powerWarning);

    bool batteryWarning() const;
    void setBatteryWarning(bool batteryWarning);

    bool tempAlarm() const;
    void setTempAlarm(bool tempAlarm);

    bool chineseLang() const;
    void setChineseLang(bool chineseLang);

    int snoozeTime() const;
    void setSnoozeTime(int snoozeTime);

    bool isSnooze() const;
    void setIsSnooze(bool isSnooze);

    void startCountDown();

    int snoozeTimeCounter() const;
    void setSnoozeTimeCounter(unsigned int snoozeTimeCounter);

    QString language() const;
    void setLanguage(const QString &language);

    QString pinNumber() const;
    void setPinNumber(const QString &pinNumber);


    bool isOTP() const;
    void setIsOTP(bool isOTP);

    bool isDeeplazer() const;
    void setIsDeeplazer(bool isDeeplazer);

    QString OTP() const;
    void setOTP(const QString &OTP);

    bool isFWUpgrade() const;
    void setIsFWUpgrade(bool isFWUpgrade);

    int progressvalueChanged() const;
    void setProgressvalueChanged(int progressvalueChanged);

    QString qrImage() const;
    void setQrImage(const QString &qrImage);

    QString alarmMessage() const;
    void setAlarmMessage(const QString &alarmMessage);

    QString userLinkingStatusMsg() const;
    void setUserLinkingStatusMsg(const QString &userLinkingStatusMsg);

    QString cloudConnStatusMsg() const;
    void setCloudConnStatusMsg(const QString &cloudConnStatusMsg);

    QString batteryPercentage() const;
    void setBatteryPercentage(const QString &batteryPercentage);

    QString wifiPercentage() const;
    void setWifiPercentage(const QString &wifiPercentage);


    QString warningAlarmMessage() const;
    void setWarningAlarmMessage(const QString &warningAlarmMessage);

    QString warningAlarmMessageText() const;
    void setWarningAlarmMessageText(const QString &warningAlarmMessageText);

    bool tempAlarmIcon() const;
    void setTempAlarmIcon(bool tempAlarmIcon);

    bool batteryAlarmIcon() const;
    void setBatteryAlarmIcon(bool batteryAlarmIcon);

    bool yellowWarning() const;
    void setYellowWarning(bool yellowWarning);

    bool redWarning() const;
    void setRedWarning(bool redwWarning);

    bool unAckAlarmCleared() const;
    void setUnAckAlarmCleared(bool unAckAlarmCleared);

    bool userLinkWarning() const;
    void setUserLinkWarning(bool userLinkWarning);

    bool noProbeDetected() const;
    void setNoProbeDetected(bool noProbeDetected);




private:
    QTcpServer *myserver;
    QStringList arguments;
    QString m_deviceName;
    QString m_popUpString;
    QString m_apName;
    QString m_apIP;
    QString m_referenceTemperature;
    QString m_temperature;
    QString m_snoozeTimeMinSec;
    QString m_failMessage;
    QString m_wifiStatusText;
    QString m_assetStatusText;
    QString m_language;
    QString m_pinNumber;
    QString m_OTP;
    QString m_qrImage;
    QString m_alarmMessage;
    QString m_userLinkingStatusMsg;
    QString m_cloudConnStatusMsg;
    QString m_batteryPercentage;
    QString m_wifiPercentage;
    QString m_warningAlarmMessage;
    QString m_warningAlarmMessageText;

    bool m_chineseLang = false;
    bool m_tempAlarm = false;
    bool m_wifiWarning;
    bool m_ethernetWarning;
    bool m_powerWarning;
    bool m_batteryWarning;
    bool m_isSnooze;
    int m_snoozeTimeCounter=0;
    bool m_isOTP;
    bool m_isDeeplazer;
    bool m_isFWUpgrade;
    int m_progressvalueChanged=0;
    bool m_tempAlarmIcon;
    bool m_batteryAlarmIcon;
    bool m_yellowWarning;
    bool m_redWarning;
    bool m_unAckAlarmCleared;
    bool m_userLinkWarning;
    bool m_noProbeDetected;
    volatile bool apModeInProgress=false;

    int m_snoozeTime = 05;

    int unknownRTDCnt=0;
    int internetNotWorkingCnt=0;

    int buzzerTimerCnt=0;
    int yellowLedTimerCnt=0;
    int currentMode=1;
    int recoveryCounter=0;
    int qrCounter=0;
    int qrCounterTemp=0;
    int dlCounter=0;
    int eventCnt=0;
    int snoozeEventCnt=0;
    int recoveryEvtCnt=0;
    int telemetryLogCnt=0;
    int telemetryHtBtLogCnt=0;
    int formatIndex=0;
    double tempVal_temp=0;
    unsigned int noOfIPAddrCnt=0;
    unsigned int noOfIPAddrCntGen=0;
    int netCheckCnt=0;
    int wifiSignalStrength=0;
    int wifiSignalLocal=0;
    int keyPressStatus=1;
    int nwCnt=0;
    int retryCnt=0;
    bool netwifistatus=false;
    bool watchdogFlag = false;
    bool m_appStartLang;
    bool m_pinNotSet;
    bool m_showForgotPin;
    bool m_enableOTPButton;
    bool m_wifiUpdateSucces = false;
    bool m_wifiIconSucces = false;
    bool m_showConfigProg = false;
    bool m_assetIconSucces = false;
    bool m_showBackKeyPad = false;
    bool m_wifiIconFail = false;
    bool m_showBackApMode = false;
    bool m_assetIconFail = false;
    bool m_assetUpdateSucces = false;
    bool m_assetUpdateFail = false;
    bool m_enableOTPButtonClick=false;
    bool m_readQRStatus;
    bool m_busy=false;
    bool m_popUp=false;
    bool alarmFlag =false;
    bool loggingFlag=true;  //setting this default value to true so that after unit is powered on, state is toggled to send any available outage data
    bool batteryFlag=false;
    bool keyPressedFlag=false;
    bool alarmLogFlag = false;
    bool alarmBatteryFlag = false;
    bool batteryLogFlag = false;
    bool alarmBatteryLogFlag = false;
    bool batteryWarnEventSentFlag = false;
    bool batteryAlarmEventSentFlag = false;
    bool highTempAlarmEventSentFlag = false;
    bool lowTempAlarmEventSentFlag = false;
    bool powerOutageEventSentFlag = false;
    bool batteryAlarmFlag = false;
    bool batteryWarnFlag = false;
    bool receivedCloudAck=false;
    bool alarmSetClearedFlag=false;
    bool highTempTextDispFlag=false;
    bool lowTempTextDispFlag=false;
    bool lowSigEventSentFlag=false;
    bool snoozeResetBattAlarmFlag=false;
    bool snoozeResetTempAlarmFlag=false;
    bool snoozeResetBattWarnFlag=false;
    bool unAckAlarmReadFileFlag=false;
    bool highTempAlarmFlag=false;
    bool lowTempAlarmFlag=false;
    bool bothAlarmBatteryActFlag=false;
    bool unAckHighTempFlag=false;
    bool unAckLowTempFlag=false;
    bool unAckPowerOutFlag=false;
    bool htAlarmSetClearedFlag=false;
    bool ltAlarmSetClearedFlag=false;
    bool recoveryAlarmEventSentFlag = false;
    bool powerOutageRecovEvntSentFlg = false;
    bool powerOutageSetClrFlag = false;
    bool heartBeatStatusFlag = false;    //only for testing
//    bool heartBtStatFlgOutageData = false;
//    bool heartBeatStatusFlagQR = false;
//    bool heartBeatStatusFlagMtDt = false;
//    bool heartBeatStatusFlgAlarm = false;
//    bool heartBeatStatusFlgSnooze = false;
//    bool heartBeatStatusFlgRecry = false;
//    bool heartBeatStatusFlgDevStat = false;
//    bool heartBeatStatusFlgTele = false;
//    bool heartBeatStatusFlgFwVer = false;
//    bool heartBeatStatusFlgNw = false;
    bool directoryCopied = false;
    bool cloudPushFail = false;
    bool ipCntZeroFlag=false;
    bool normalModeActive=false;
    bool nwModuleRestartFlag=false;

    QString highTemp;
    QString lowTemp;
    QString setPointTemp;
    QString assetType;
    QString assetModel;
    QString assetSerial;
    QString assetCity;
    QString assetLocation;
    QString sensorName;
    QString rtdTempVal;
    QString randomIdString;
    QString snoozeValue;
    //QString batteryPercent;
    QString batteryPer;
    QString principalId;

    QString otpButtStatus;
    QString instrumentName;
    QString instrumentMac;
    QByteArray thresholdArray;

    QString ackStat;


    char updateModeBuffer[200];
    char getQRImageBuffer[100];
    char updateMetaDataBuffer[500];
    char highTempEventBuffer[500];
    char lowTempEventBuffer[500];
    char snoozeEventBuffer[500];
    char acknowledgeEventBuffer[500];
    char powerOutageEventBuffer[500];
    char battPowerEventBuffer[500];
    char lowSignalEventBuffer[500];
    char principalIdChar[50];
    char updateDevStatusBuffer[500];
    char telemetryBuffer[500];
    char heartBeatBuffer[100];
    char alarmRecoveryBuffer[500];
    char currentAlarmIdChar[50];
    char outageAlarmIdChar[50];
    char outagecurrentAlarmIdChar[50];
    char instrumentNameChar[50];
    char executionIdChar[50];
    char heartBtFailChar[500];
    char writeTelemetryCmd[1024];
    char nwRecoveryBuffer[512];

    QTime dieTime;
    QTime dieTimeQR;
    QTime dieTimeHtBt;
    QString timeinUTC;
    QString alarmIdReceived;

    QString snoozeTimeReceived;
    QString batteryStat;
    QString onlineStatus;
    QString friendlyName;
    //QString snoozeStat;

    //variables for updateDeviceStatus
    char currentModeChar[16];
    char currentTempChar[16];
    char highTempAlarmChar[16];
    char lowTempAlarmChar[16];
    char battLowChar[16];
    char acPowerFailChar[16];
    char idChar[30];
    QByteArray currentTempArray;

    long http_code=0;

    //variables for sendLowSignalEvent
    QByteArray alarmIdArray;
    char alarmIdChar[100];

    char timeinUTCChar[50];

    char rtdTempValChar[20];


    //variables for sendBatteryPowerEvent

    char batPerChar[30];

    //variables for sendAcknowledgeEvent


    char friendlyNameChar[50];

    //variables for sendLowTempAlarmEvent

    char rtdTempChar[20];
    QByteArray lowTempArray;
    char lowTempChar[20];

    char highTempChar[20];
    //long long int qrWaitCnt=0;
    char dateCmd[50];

    char bufGen[500];
    char bufCheckAP[64];
    char bufResetAPCmd[64];

    //variables for updateMetaData

    char setPointChar[20];


    char assetTypeChar[64];


    char assetModelChar[64];


    char assetSerialChar[64];


    char assetCityChar[64];


    char assetLocationChar[64];


    char sensorNameChar[64];

    char thresholdChar[20];

    QFile utcFile;
    FILE *output;
    FILE *wifiSignalOutput;
    FILE *keyPressOutput;
    FILE *genFileFP;
    FILE *apModeChkFileFP;

    QFileInfo check_devconfigfile;
    bool recdQRSuccess=false;

    QFile devConfigFile;
    QFile unAckAlarmfile;
    QFile deviceSerialfile;
    QFile genericFile;
    QFile langFile;

    QString unAckAlarmReadFileStr;
    QFile setPointFile;
    QFile highTempFile;
    QFile lowTempFile;
    QFile astTypeFile;
    QFile astModelFile;
    QFile astSnoFile;
    QFile astCityFile;
    QFile astLocFile;
    QFile sensorNameFile;
    QFile pidFile;
    QFile friendlyNameFile;
    QString randOTPStr;
    QFile randOTPFile;
    FILE *nwCheckDL;
    QFile logModeFile;
    QFile batModeLogFile;

    FILE *updMetaDataFp;
    FILE *highTempAlarmFp;
    FILE *lowTempAlarmFp;
    FILE *snoozeFp;
    FILE *ackFp;
    FILE *alarmRecoveryFp;
    FILE *lowSignalFp;
    FILE *fwVerFp;
    FILE *eventResendFp;

    QString qrPath;
    QString qrImageStr;
    FILE *hearBeatFailFp;
    //FILE *telemetryFp;
    FILE *recoverySendFp;
    QDir fwDir;
    char fwVersionChar[10];
    QString totalAlarmIdStr;
    QString fwVersion;
    QString pinNum;
    QString langStatus;
    QTimer *countDownTimer, *networkTimer , *yellowLedTimer, *unAckFlashTimer, *syslogClearTimer, *keepAliveTimer, *deviceStatusTimer, *telemetryTimer;
    QTimer *buzzerTimer;
    QTimer *resetKeyTimer;
    QTimer *outageDataSendTimer;
    QTimer *wifiTimer;
    QTimer *apStaTimer;
    QTimer *syncDateTimer;
    QTimer *metaDataTimer;
    QTimer *resendOutageDataTimer;
    QTimer *apModeCheckTimer;
    QTimer *eventSentCheckTimer;
    QTimer *otherLogsClearTimer;

    QTime *countDownValue;
    QTime *timeValue;
    QString keyPressStat;
    QFileInfo metadata_file;
    QDateTime logStartDateTime;
    Thread *workerThread;
    NetworkThread *nwThread;
//    CurlThread *curlThrd;
    WifiThread *wifiThrd;
    LanguageTranslator *langTranslator;

    QDir dlLinkedUsers;
    QDir heartBeatFailDir;
    bool noUserLinked=false;
    bool internetStatus=false;
    bool heartBtCmdInProgress = false;
    bool updModBoxStatusInProg = false;
    bool updDevStatusInProgress = false;

    int telemetry_LineCnt=0;
    int telemetry_HtBtLineCnt=0;
    QString logFileLine;

    int event_LineCnt=0;

    int snoozeEvent_LineCnt=0;

    int recoveryEvent_LineCnt=0;

    double receivedRTD;
    double currentRTD=0;
    double rtdDelta;
    double highTempFloat;
    double lowTempFloat;

    bool outageSendInProgress=false;

    QString year;
    QString month;
    QString day;
    int keyInt=0;
    QString keyStr;
    QString generatedKey;

    char wifiInterfaceDetails[100];
    bool wlan1Active;
    bool wlan0Active;
    bool wlan1ForceDown=false;

    char telemetryBkpBuffer[24576];
    char telemetryBkpBufferTmp[24576];
    char telemetryBkpBufferUpd[24576];
    char telemetryBkpDiffBuff[24576];
    char recdReqId[1024];
    char *recdReqIdToken;
    char *telemetryToken;
    char *tokenWriteFile;
    char *tokenWriteFileOutageData;
    char reqIdTokens[10][16];       //provision to store max 10 request id
    unsigned int teleBkpBuffSizeChk;
    int i,j;
    int reqIdCnt;
    int teleTokenLenBefore;
    int teleTokenLenAfter;
    char dispatchErrorLogBuff[512];
    char dispatchErrorLogBuffReg[512];

    bool logSDCardStarted=false;
    bool outageTeleWriteProgress=false;
    bool regTeleWriteProgress = false;

    int retryCntSendSnzEvnt=0;
    int retryCntSendTele=0;
    int retryCntSendHtBt=0;
    int retryCntSendFwVer=0;
    int retryCntDevStatus=0;
    int retryCntLowSignal=0;
    int retryCntGetQRCode=0;
    int retryCntMetaData=0;

    QString htBtRandomId;
    QFuture<QString> htBtIdSent;
    QString htBtIdSentStr;
    QString htBtIdRecd;
    bool heartBeatLock=false;
    int bothInterfaceFountCnt=0;

//    bool outageEventProgress=false;
//    bool outageTeleProgress=false;


signals:
    void valueChanged();
    void OTPChanged();
    void startRedFlash();
    void stopRedFlash();
    void startYellowFlash();
    void stopYellowFlash();
    void wifiConnSuccess();
    void wifiConnFail();
    void configProgress();
    void silenceKeyPressTrue();
    void silenceKeyPressFalse();
    void qrScanDone();
    void apModeAutoExit();
    void apModeAutoExitTimerStop();
    void sendCurlData(CURL*);
    void stopCntDwnTimerSignal();
    void forceWlan1Down();
    void forceWlan1Up();
    void startEventSentCheck();

public slots:

    void firmwareUpgradeStarted();
    void updateModBoxStatus();
    void timeOutSlotForCountDown();
    void timeOutForCountDownConcurrent();
    void setSnoozeStatus(bool snoozeStatus);
    void callSendSnoozeEvent();
    void setSettingsMouseClickStatus(bool enableStatus);
    void initPasswordRequest();
    void acceptConnection();
    void startRead();
    void verifyRTDData(double rtdTempVal);
    void runBuzzer();
    void resetSnoozeKeyPressed();
    void yellowLedFlash();
    void clearSysLogs();
    void clearOtherLogs();
    void networkMonitor();
    int updateMode(int mode);
    long sendModeToCloud();
    void readUTCTimenow();
    void startApMode();
    void readOTP();
    void updateLangFlagEnglish();
    void updateLangFlagChinese();
    int readQRImage();
    void startDLForDevReg();
    void exitApMode();
    QString generateRandNumber();
    int updateMetaData();
    void readBatteryPercentage();
    int sendHighTempAlarmEvent();
    int sendLowTempAlarmEvent();
    int sendSnoozeEvent(int eventKey);
//    int sendAcknowledgeEvent(int eventKey);

//    int sendBatteryPowerEvent(int batPercentage);
    int sendLowSignalEvent();
    void delay(int sec);
    void delayQRReq(int sec);
    bool delayHtBtReq(int sec, QString htBtId);
    void completedDeviceConfig();
    void keepAliveSlot();
    int updateDeviceStatus();
    int sendTelemetryData();
    long sendAlarmRecoveryEvent();
    void telemetryConcurrentCmd();
    QString sendHeartBeatCommand();
    void wifiSignalRead();
    void sendNetworkOutageData();
    void sendNetworkOutageDataConcurrent();
    void doFirmwareUpdate();
    int sendFwVerToCloud();
    bool getWifiConnectStatus();
    bool getAssetPushStatus();
    bool copyPath(QString sourceDir, QString destinationDir, bool overWriteDirectory);
    void internetOnSlot();
    void internetOffSlot();
    void changeModeSta();
    void updateDateTime();
    void writePinToFile();
    void readPinFromFile();
    void resetPIN();
    void enableForgotPinOption();
    void disableForgotPinOption();
    void enableBackNumKeyPad();
    void receivedWifiValue(int wifiVal);
    void recheckOutageStatus();
    void stopCntDwnTimerSlot();
    bool getWlan0Status();
    bool getWlan1Status();
    void recheckApModeStatus();
    void eventSentCheckSlot();
    void startEventSentCheckSlot();
    QString resetHtBtSendId();

};

#endif // DELTAKCONTROLLER_H
