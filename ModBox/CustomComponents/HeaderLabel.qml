import QtQuick 2.0
import QtQuick.Controls 2.0
import "../assets/Fonts"

Label {
    width: 100
    height: 24
    color: "#ffffff"
    FontLoader { id: robotoRegular; source: "../assets/Fonts/roboto.regular.ttf" }
    font.family: robotoRegular.name
    font.pixelSize: 14
}
