import QtQuick 2.0
import QtQuick.Controls 2.0
import "../assets/Fonts"

Label {
    width: 100
    height: 24
    color: "#344550"
    font.bold: true
    FontLoader { id: robotoMedium; source: "../assets/Fonts/roboto.medium.ttf" }
    font.family: robotoMedium.name
    font.pixelSize: 100
}
