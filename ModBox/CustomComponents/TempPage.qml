import QtQuick 2.4
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0
import "../assets/Fonts"

Rectangle {
    id: languageTumblerFrame
    width: 170
    height: 170
    color: "transparent"

    Tumbler {
        id: tensTumbler
        x: 4
        y: 34
        width: 60
        height: 100
        padding: -10
        clip: true
        visibleItemCount: 3
        currentIndex: parseInt(settings.langIndex)

        model: ListModel {
            id: tensModel
            ListElement { value: "-1" }
            ListElement { value: "-2" }
            ListElement { value: "-3" }
            ListElement { value: "-4" }
            ListElement { value: "-5" }
            ListElement { value: "-6" }
            ListElement { value: "-7" }
            ListElement { value: "-8" }
            ListElement { value: "-9" }
        }

        background: Item {
            Rectangle {
                color: "#ffffff"
                anchors.fill: parent
                radius: 5

                gradient: Gradient {
                    GradientStop { position: 0.0;   color: "#313d46" }
                    GradientStop { position: 0.29;  color: "#313d46" }
                    GradientStop { position: 0.291; color: "#273138" }
                    GradientStop { position: 0.71;  color: "#273138" }
                    GradientStop { position: 0.711; color: "#313d46" }
                    GradientStop { position: 0.1;   color: "#313d46" }
                }
            }
        }

        delegate: Text {
            text: value
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            color: "#2EA2EC"
            font.pixelSize: 25
            opacity: 1.0 - Math.abs(Tumbler.displacement) / 2
        }

        LinearGradient {
            id: linerGradient
            x: 4
            y: 0
            width: 52
            height: 100
            start: Qt.point(0, 0)
            end: Qt.point(0, 100)

            gradient: Gradient {
                GradientStop { position: 0.0; color: "#313d46" }
                GradientStop { position: 0.15; color: "transparent" }
                GradientStop { position: 0.85; color: "transparent" }
                GradientStop { position: 1.0; color: "#313d46" }
            }
        }

        onCurrentIndexChanged: {
            console.log("LanguageComponent :: Language selected = " + languageModel.get(currentIndex).language)

            settings.langIndex = currentIndex
            settings.lang = languageModel.get(currentIndex).language
        }
    }

    Button {
        id: downButton
        x: 304
        width: 32
        height: 20
        anchors.horizontalCenter: tensTumbler.horizontalCenter
        anchors.top: tensTumbler.bottom
        anchors.topMargin: 10

        background: Rectangle {
            anchors.fill: parent
            color: "transparent"
            Image {
                anchors.fill: parent
                source: "../assets/Images/WheelPickerArrow_Up.png"
            }
        }

        onClicked: tensTumbler.currentIndex--
    }

    Button {
        id: upButton
        x: 304
        y: 139
        width: 32
        height: 20
        anchors.horizontalCenter: tensTumbler.horizontalCenter
        anchors.bottom: tensTumbler.top
        anchors.bottomMargin: 10

        background: Rectangle {
            anchors.fill: parent
            color: "transparent"
            Image {
                anchors.fill: parent
                source: "../assets/Images/WheelPickerArrow_Down.png"
            }
        }

        onClicked: tensTumbler.currentIndex++
    }

    Tumbler {
        id: unitsTumbler
        x: 66
        y: 34
        width: 60
        height: 100
        background: Item {
            Rectangle {
                color: "#ffffff"
                radius: 5
                anchors.fill: parent
                gradient: Gradient {
                    GradientStop { position: 0.0;   color: "#313d46" }
                    GradientStop { position: 0.29;  color: "#313d46" }
                    GradientStop { position: 0.291; color: "#273138" }
                    GradientStop { position: 0.71;  color: "#273138" }
                    GradientStop { position: 0.711; color: "#313d46" }
                    GradientStop { position: 0.1;   color: "#313d46" }
                }
            }
        }
        currentIndex: parseInt(settings.langIndex)
        visibleItemCount: 3
        clip: true
        padding: -10
        model: ListModel {
            id: unitsModel
            ListElement { value: "0" }
            ListElement { value: "1" }
            ListElement { value: "2" }
            ListElement { value: "3" }
            ListElement { value: "4" }
            ListElement { value: "5" }
            ListElement { value: "6" }
            ListElement { value: "7" }
            ListElement { value: "8" }
            ListElement { value: "9" }
        }
        delegate: Text {
            color: "#2ea2ec"
            text: value
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 25
            opacity: 1.0 - Math.abs(Tumbler.displacement) / 2
            verticalAlignment: Text.AlignVCenter
        }
        LinearGradient {
            id: linearGradient1
            x: 4
            y: 0
            width: 52
            height: 100
            end: Qt.point(0, 100)
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#313d46"
                }

                GradientStop {
                    position: 0.15
                    color: "#00000000"
                }

                GradientStop {
                    position: 0.85
                    color: "#00000000"
                }

                GradientStop {
                    position: 1
                    color: "#313d46"
                }
            }
            start: Qt.point(0, 0)
        }
    }

    Button {
        id: upButton1
        x: 295
        y: 25
        width: 32
        height: 20
        anchors.bottom: unitsTumbler.top
        anchors.bottomMargin: 10
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: unitsTumbler.horizontalCenter
        background: Rectangle {
            color: "#00000000"
            anchors.fill: parent
            Image {
                source: "../assets/Images/WheelPickerArrow_Down.png"
                anchors.fill: parent
            }
        }
        onClicked: unitsTumbler.currentIndex++
    }

    Button {
        id: downButton1
        x: 295
        width: 32
        height: 20
        anchors.top: unitsTumbler.bottom
        anchors.topMargin: 10
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: unitsTumbler.horizontalCenter
        background: Rectangle {
            color: "#00000000"
            anchors.fill: parent
            Image {
                source: "../assets/Images/WheelPickerArrow_Up.png"
                anchors.fill: parent
            }
        }
        onClicked: unitsTumbler.currentIndex--
    }

    Label{
        id: timeLabel
        x: 132
        y: 69
        width: 30
        height: 30
        text: qsTr("°C")
        color: "#344550"
        FontLoader { id: robotoRegular2; source: "../assets/Fonts/roboto.regular.ttf" }
        font.family: robotoRegular2.name
        font.pixelSize: 25
    }

}
