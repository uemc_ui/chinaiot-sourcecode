//import QtQuick 2.4
//import QtQuick.Controls 1.3
//import QtQuick.Controls.Styles 1.3
//import QtQuick.Controls 1.1
//import "../QMLLib"

import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4
import Qt.labs.settings 1.0
import QtQuick.Layouts 1.0
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.0
import "../assets"

Rectangle{
    id: tempPage
    color: "transparent"
    height: 200
    property int tumfntsize: 30
    property int paddingTop: -15
    property int paddingBottom: -10
    property alias vis1: snoozeRadioBtn.visible
    property alias visText1: text5.visible

    property bool saveVisible: true

    property Component compFrame: Rectangle {
        id: rectangle
        x: 0
        y: 0
        width: parent.width
        height: parent.height
        radius: 5
        clip: true
        anchors.margins: -1
        gradient: Gradient {
            GradientStop { position: 0.102;   color: "#313d46" }
            GradientStop { position: 0.28;  color: "#313d46" }
            GradientStop { position: 0.285; color: "#273138" }
            GradientStop { position: 0.703;  color: "#273138" }
            GradientStop { position: 0.708; color: "#313d46" }
            //            GradientStop { position: 0.9; color: "#313d46" }
            //            GradientStop {position: 0.99; color: "#ffffff"}

            GradientStop { position: 0.1;   color: "#313d46" }
        }
    }

    property Component tumblerDelegate :Item {
        implicitHeight: (140 - paddingTop - paddingBottom) / 3

        Text {
            id: label
            text: styleData.value
            color: "#2EA2EC"
            font.weight: Font.Normal
            font.family: "Roboto"
            font.pixelSize: 41
            opacity: 0.4 + Math.max(0, 1 - Math.abs(styleData.displacement)) * 0.6
            anchors.centerIn: parent
        }
    }
    width: 640

    ExclusiveGroup{
        id: timeGroup
    }

    CheckBox{
        id: ukRadioBtn
        width: 55
        height: 20
        anchors{left: parent.left;top: parent.top;topMargin: 28}
        text: "-80°C"
        anchors.leftMargin: 45
        exclusiveGroup: dateGroup
        visible: true

        style: CheckBoxStyle {
//            indicator: Rectangle {
//                width: 20
//                height: 20
//                border.color: "#cbc6cd"
//                border.width: 4
//                Rectangle {
//                    x: 4.5
//                    y: 4.5
//                    height: 11
//                    width: 11
//                    visible: control.checked
//                    color: "#2ea2ec"
//                    radius: 5.5
//                    anchors.margins: 4
//                }
//            }

            label: Label {
                x: 4
                FontLoader {
                    id: robotoBold1
                    source: "../../assets/Fonts/roboto.bold.ttf"
                }
                font.family: robotoBold1.name
                font.bold: true
                font.pixelSize: 16
                text: control.text
                color: "#333333"
            }
        }
    }

    CheckBox{
        id: indRadioBtn
        width: 55
        text: "-70°C"
        anchors.left: parent.left
        anchors.leftMargin: 45
        anchors.top: ukRadioBtn.bottom
        anchors.topMargin: 20
        exclusiveGroup: dateGroup
        visible: true

        style: CheckBoxStyle {
//            indicator: Rectangle {
//                width: 20
//                height: 20
//                border.color: "#cbc6cd"
//                border.width: 4
//                Rectangle {
//                    x: 4.5
//                    y: 4.5
//                    height: 11
//                    width: 11
//                    visible: control.checked
//                    color: "#2ea2ec"
//                    radius: 5.5
//                    anchors.margins: 4
//                }
//            }

            label: Label {
                x: 4
                FontLoader {
                    id: robotoBold2
                    source: "../../assets/Fonts/roboto.bold.ttf"
                }
                font.family: robotoBold2.name
                font.bold: true
                font.pixelSize: 16
                text: control.text
                color: "#333333"
            }
        }
    }

    CheckBox{
        id: usRadioBtn
        width: 55
        height: 20
        anchors{left: parent.left;}
        text: "-90°C"
        anchors.leftMargin: 45
        anchors.top: indRadioBtn.bottom
        anchors.topMargin: 20
        exclusiveGroup: dateGroup
        visible: true

        style: CheckBoxStyle {
//            indicator: Rectangle {
//                width: 20
//                height: 20
//                border.color: "#cbc6cd"
//                border.width: 4
//                Rectangle {
//                    x: 4.5
//                    y: 4.5
//                    height: 11
//                    width: 11
//                    visible: control.checked
//                    color: "#2ea2ec"
//                    radius: 5.5
//                    anchors.margins: 4
//                }
//            }

            label: Label {
                x: 4
                FontLoader {
                    id: robotoBold3
                    source: "../../assets/Fonts/roboto.bold.ttf"
                }
                font.family: robotoBold3.name
                font.bold: true
                font.pixelSize: 16
                text: control.text
                color: "#333333"
            }
        }
    }

    CheckBox{
        id: snoozeRadioBtn
        width: 55
        height: 20
        anchors{left: parent.left;}
        text: "10 min"
        anchors.leftMargin: 45
        anchors.top: usRadioBtn.bottom
        anchors.topMargin: 20
        exclusiveGroup: dateGroup
        visible: true

        style: CheckBoxStyle {
//            indicator: Rectangle {
//                width: 20
//                height: 20
//                border.color: "#cbc6cd"
//                border.width: 4
//                Rectangle {
//                    x: 4.5
//                    y: 4.5
//                    height: 11
//                    width: 11
//                    visible: control.checked
//                    color: "#2ea2ec"
//                    radius: 1
//                    anchors.margins: 4
//                }
//            }

            label: Label {
                x: 4
                FontLoader {
                    id: robotoBold4
                    source: "../../assets/Fonts/roboto.bold.ttf"
                }
                font.family: robotoBold4.name
                font.bold: true
                font.pixelSize: 16
                text: control.text
                color: "#333333"
            }
        }
    }

    ListModel {
        id: tensModel
        ListElement { value: "0" }
        ListElement { value: "1" }
        ListElement { value: "2" }
        ListElement { value: "3" }
        ListElement { value: "4" }
        ListElement { value: "5" }
        ListElement { value: "6" }
        ListElement { value: "7" }
        ListElement { value: "8" }
        ListElement { value: "9" }

        Component.onCompleted: {
            tensColumn.model = tensModel
        }
    }

    ListModel {
        id: unitsModel
        ListElement { value: "0" }
        ListElement { value: "1" }
        ListElement { value: "2" }
        ListElement { value: "3" }
        ListElement { value: "4" }
        ListElement { value: "5" }
        ListElement { value: "6" }
        ListElement { value: "7" }
        ListElement { value: "8" }
        ListElement { value: "9" }

        Component.onCompleted: {
           unitsColumn.model = unitsModel
        }
    }


    Image{
        id: tensUp
        width: 38
        height: 27
        anchors.bottom: tensTumbler.top
        anchors.bottomMargin: 5
        source: "../assets/Images/WheelPickerArrow_Down.png"
        anchors.horizontalCenter: tensTumbler.horizontalCenter

        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(tensColumn.currentIndex+1 >= tensColumn.model.length)
                    tensTumbler.setCurrentIndexAt(0,0)
                else
                    tensTumbler.setCurrentIndexAt(0,tensColumn.currentIndex+1);
            }
        }
    }

    Tumbler {
        id: tensTumbler
        y: 36
//        width: 0
        clip: true
        height: 125
        anchors.left: ukRadioBtn.right
        anchors.leftMargin: 170

        TumblerColumn {
            id: tensColumn
            width: 63
        }

        style: TumblerStyle {
            padding.top: -21
            padding.bottom: -21
            property Component frame: compFrame
            property Component foreground: Item {}
            delegate: tumblerDelegate
        }
        }

    Image{
        id: tensDown
        width: 38
        height: 27
        source: "../assets/Images/WheelPickerArrow_Up.png"
        anchors.horizontalCenter: tensTumbler.horizontalCenter
        anchors{  top: tensTumbler.bottom;topMargin: 5 }

        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(tensColumn.currentIndex===0)
                    tensTumbler.setCurrentIndexAt(0,tensColumn.model.length-1)
                else
                    tensTumbler.setCurrentIndexAt(0,tensColumn.currentIndex-1)
            }
        }
    }

    Image{
        id: unitsUp
        width: 38
        height: 27
        anchors.bottom: unitsTumbler.top
        anchors.bottomMargin: 5
        source: "../assets/Images/WheelPickerArrow_Down.png"
        anchors.horizontalCenter: unitsTumbler.horizontalCenter

        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(unitsColumn.currentIndex+1 >= unitsColumn.model.length)
                    unitsTumbler.setCurrentIndexAt(0,0)
                else
                    unitsTumbler.setCurrentIndexAt(0,unitsColumn.currentIndex+1);
            }
        }
    }

    Text {
        id: text1
        y: 28
        height: 20
        FontLoader {
            id: robotoRegular1
            source: "../../assets/Fonts/roboto.regular.ttf"
        }
        font.family: robotoRegular1.name
        text: qsTr("Setpoint")
        anchors.left: ukRadioBtn.right
        anchors.leftMargin: 20
        font.pixelSize: 16
    }

    Tumbler {
        id: unitsTumbler
        y: 36
//        width: 0
        clip: true
        height: 125
        anchors.left: tensTumbler.right
        anchors.leftMargin: 2

        TumblerColumn {
            id: unitsColumn
            width: 63
        }

        style: TumblerStyle {
            padding.top: -21
            padding.bottom: -21
            property Component frame: compFrame
            property Component foreground: Item {}
            delegate: tumblerDelegate
        }
        }

    Image{
        id: unitsDown
        width: 38
        height: 27
        source: "../assets/Images/WheelPickerArrow_Up.png"
        anchors.horizontalCenter: unitsTumbler.horizontalCenter
        anchors{  top: unitsTumbler.bottom;topMargin: 5 }

        MouseArea{
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                if(unitsColumn.currentIndex===0)
                    unitsTumbler.setCurrentIndexAt(0,unitsColumn.model.length-1)
                else
                    unitsTumbler.setCurrentIndexAt(0,minsColumn.currentIndex-1)
            }
        }
    }

    Text {
        id: text4
        y: 44
        width: 57
        height: 125
        color: "#c6c9ce"
        FontLoader {
            id: robotoRegular4
            source: "../../assets/Fonts/roboto.regular.ttf"
        }
        font.family: robotoRegular4.name
        text: qsTr("min")
        fontSizeMode: Text.Fit
        verticalAlignment: Text.AlignVCenter
        anchors.left: unitsTumbler.right
        anchors.leftMargin: 6
        font.pixelSize: 25
    }

    Item{
        id: dataPopUpTemplate
        signal receiveOK()
        signal receiveCancel()

        onReceiveOK: {
            if(fw_popup.firmwarestage===0)
            {
                fw_popup.visible=false
                shader.visible=false
                auto_close_timer.stop()
                menuBar.processBackImage()
            }
            else {
                fw_popup.visible=false
                shader.visible=false
                auto_close_timer.stop()
            }
        }

        onReceiveCancel: {
            parentRegion.settingsSaved=true
            menuBar.processBackImage()
        }
    }
    Timer{
        id: auto_close_timer
        interval: 5000;repeat: false;running: false
        onTriggered: {
            fw_popup.visible=false
            shader.visible=false
            menuBar.processBackImage()
        }
    }

    Text {
        id: text2
        width: 63
        //        x: 0
//        width: 63
        height: 20
        FontLoader {
            id: robotoRegular2
            source: "../../assets/Fonts/roboto.regular.ttf"
        }
        font.family: robotoRegular2.name
        text: qsTr("Warm Alarm")
        anchors.top: text1.bottom
        anchors.topMargin: 19
        font.pixelSize: 16
        anchors.leftMargin: 20
        anchors.left: indRadioBtn.right
    }

    Text {
        id: text3
        width: 63
        //        x: 0
//        width: 63
        height: 20
        FontLoader {
            id: robotoRegular3
            source: "../../assets/Fonts/roboto.regular.ttf"
        }
        font.family: robotoRegular3.name
        text: qsTr("Cold  Alarm")
        anchors.top: text2.bottom
        anchors.topMargin: 21
        font.pixelSize: 16
        anchors.leftMargin: 20
        anchors.left: usRadioBtn.right
    }

    Text {
        id: text5
        height: 20
        FontLoader {
            id: robotoRegular5
            source: "../../assets/Fonts/roboto.regular.ttf"
        }
        font.family: robotoRegular5.name
        text: qsTr("Snooze Time Out")
        anchors.top: text3.bottom
        anchors.topMargin: 20
        anchors.left: snoozeRadioBtn.right
        anchors.leftMargin: 30
        font.pixelSize: 16
    }




    Component.onCompleted: {
        fw_popup.signalItem=dataPopUpTemplate
    }
}




