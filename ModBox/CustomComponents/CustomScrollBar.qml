import QtQuick 2.0

/*  This is a library component which acts as customized scroll bar
 *  The Scroll Bar contains down chevron icon and up chevron icon with
 *  active mouse area to accept events. Scroll Bar also contains Background
 *  swipe bar and vertical swipe Inset Bar for movement .The asociated
 *  functionality is swipe movement , up down button click and click on
 *  scroll bar.
*/

Rectangle {
    id: scrollbar
    color: "transparent"
    property Flickable flickable
    //Up Icon for Customized Scroll Bar
    property alias insetHt: vbar.height
//    property alias handleWt:handle.width
    property int stepSizeupDown:4

    Image    {
        width: 34
        height: 20
        anchors{bottom: vbar.top;bottomMargin: 20}
        anchors.horizontalCenter: vbar.horizontalCenter
        source:"../assets/Icons_Misc/Icon_Chevron_Swipe_Up.png"
        MouseArea{
            anchors.fill: parent
            onClicked: {
                scrollUp()
            }
        }
    }

    Image    {
        id: vbar
        width: 25
        height: 190
        source:"../assets/Icons_Misc/Icon_Vertical_Swipe_Bar.png"
        MouseArea {
            id: clicker
            width: 25
            height: 190
            anchors { fill: parent }

            drag {
                target: handle
                minimumY: 0
                maximumY: (vbar.height - handle.height )
                axis: Drag.YAxis
            }
            onClicked: {
                flickable.contentY = ((mouse.y / vbar.height) * (flickable.contentHeight - flickable.height))
            }
        }
    }

    Item {
        id: handle
        height: 90
        x: vbar.x
        width: 25
        Image
        {
            id: insetblue
            width: 25
            height: 90
            source:"../assets/Icons_Misc/Icon_Vertical_Swipe_Bar_Inset.png"
        }
    }

    //Down Icon for Customized Scroll Bar
    Image    {
        width: 34
        height: 20
        anchors{top: vbar.bottom;topMargin: 20}
        anchors.horizontalCenter: vbar.horizontalCenter
        source:"../assets/Icons_Misc/Icon_Chevron_Swipe_Down.png"
        MouseArea{
            anchors.fill: parent
            onClicked: {
                scrollDown();
            }
        }
    }

    function scrollDown () {
        flickable.contentY = Math.min (flickable.contentY + (flickable.height/stepSizeupDown ), flickable.contentHeight - flickable.height);
    }

    function scrollUp () {
        flickable.contentY = Math.max (flickable.contentY - (flickable.height/stepSizeupDown ), 0)
    }

    Binding {
        target: handle
        property: "y"
        value: ((flickable.contentY * clicker.drag.maximumY) / (flickable.contentHeight - flickable.height))
        when: (!clicker.drag.active)
    }

    Binding {
        target: flickable
        property: "contentY"
        value: (handle.y * (flickable.contentHeight - flickable.height) / clicker.drag.maximumY)
        when: (clicker.drag.active)
    }
}
