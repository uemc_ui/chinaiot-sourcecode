#include <QtCore>
#include <QMutexLocker>
#include <unistd.h>

#include "networkthread.h"


/**
 * @brief NetworkThread::NetworkThread
 */
NetworkThread::NetworkThread()
{
    stopped = false;
}

/**
 * @brief NetworkThread::run
 */
void NetworkThread::run()
{
    forever
    {
       {
            QMutexLocker locker(&mutex);
            if(stopped)
            {
                stopped = false;
                break;
            }
       }

//        qDebug()<<"NetworkThread Running";



        if(system("wget -q --spider --tries=5 --timeout=5  http://www.thermofisher.com/us/en/home.html")==0)    //network is present
        {
            if(netOn==false)
            {
                qDebug()<<"Network On";
                emit networkOn();
                netOn = true;

                netOff = false;
            }

        }
        else if(system("wget -q --spider --tries=5 --timeout=5  https://apps.thermofisher.com")==0)    //backup standard website to check network is present
        {
            if(netOn==false)
            {
                qDebug()<<"Network On";
                emit networkOn();
                netOn = true;

                netOff = false;
            }

        }
        else if(system("wget -q --spider --tries=5 --timeout=5  https://china.apps.thermofisher.com")==0)    //backup standard website to check network is present
        {
            if(netOn==false)
            {
                qDebug()<<"Network On";
                emit networkOn();
                netOn = true;

                netOff = false;
            }

        }
        else if(system("wget -q --spider --tries=5 --timeout=5  http://www.gov.cn/index.htm")==0)    //backup standard website to check network is present
        {
            if(netOn==false)
            {
                qDebug()<<"Network On";
                emit networkOn();
                netOn = true;

                netOff = false;
            }

        }
        else if(system("wget -q --spider --tries=5 --timeout=5  http://www.e-gov.org.cn")==0)    //backup standard website to check network is present
        {
            if(netOn==false)
            {
                qDebug()<<"Network On";
                emit networkOn();
                netOn = true;

                netOff = false;
            }

        }

        else if(system("wget -q --spider --tries=5 --timeout=5  http://www.thermofisher.com/us/en/home.html")!=0)   //network outage mode
        {
            if(netOff==false)
            {
                qDebug()<<"Network Off";
                emit networkOff();
                netOff = true;

                netOn = false;
            }

        }

        delay(4);

    }
}

/**
 * @brief NetworkThread::stop
 */
void NetworkThread::stop()
{
    QMutexLocker locker(&mutex);
    stopped = true;
}


/**
 * @brief NetworkThread::delay
 * @param sec
 */
void NetworkThread::delay(int sec)
{
    dieTime= QTime::currentTime().addSecs(sec);
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}
