#include "languagetranslator.h"
#include <QTranslator>
#include <QGuiApplication>
#include <QCoreApplication>
#include <QQmlContext>

LanguageTranslator::LanguageTranslator(QObject *parent) : QObject(parent)
{

}

QString LanguageTranslator::translatedText() const
{
    return m_translatedText;
}
void LanguageTranslator::translateToChinese()
{
    QTranslator *t=new QTranslator;
    t->load(":/chinese.qm");
    QCoreApplication::instance()->installTranslator(t);
    QGuiApplication::instance()->installTranslator(t);
    emit translatedTextChanged();
}
void LanguageTranslator::translateToEnglish()
{
    QTranslator *t=new QTranslator;
    t->load(":/english.qm");
    QCoreApplication::instance()->installTranslator(t);
    QGuiApplication::instance()->installTranslator(t);
    emit translatedTextChanged();
}
