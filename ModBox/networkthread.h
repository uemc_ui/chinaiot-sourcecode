#ifndef NETWORKTHREAD_H
#define NETWORKTHREAD_H

#include <QThread>
#include <QMutex>
#include <QTime>

class NetworkThread : public QThread
{
    Q_OBJECT

public:
    NetworkThread();
    //void setMessage(const QString &message);
    void stop();



private:

    volatile bool stopped;
    QMutex mutex;


    QTime dieTime;

    bool netOn = false;
    bool netOff = false;


signals:
    void networkOn();
    void networkOff();

public slots:

    void delay(int sec);

protected:
    void run();

};

#endif  //NETWORKTHREAD_H



