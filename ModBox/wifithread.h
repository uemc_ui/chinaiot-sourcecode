#ifndef WIFITHREAD_H
#define WIFITHREAD_H

#include <QThread>
#include <QMutex>
#include <QTime>
#include <curl/curl.h>

class WifiThread : public QThread
{
    Q_OBJECT

public:
    WifiThread();
 
    void stop();



private:

    volatile bool stopped;
    QMutex mutex;


    QTime dieTime;

    FILE *wifiSignalOutput;
    int wifiSignalStrength=0;
    char wifiInterfaceName[100];
    int wifiSignalDongle=0;
    int noInterfaceFoundCnt=0;
    int bothInterfaceFountCnt=0;
    bool wlan0Active;
    bool wlan1Active;
    bool wlan1ForcedDown=false;

signals:
    void wifiValueChanged(int wifiVal);


public slots:

    void delay(int sec);
    void changeWlan1StateDown();
    void changeWlan1StateUp();
  

protected:
    void run();

};

#endif  //WIFITHREAD_H



