#include <QtCore>
#include <QMutexLocker>
#include <unistd.h>

#include "wifithread.h"


/**
 * @brief WifiThread::WifiThread
 */
WifiThread::WifiThread()
{
    stopped = false;
}

/**
 * @brief WifiThread::run
 */
void WifiThread::run()
{
    forever
    {
       {
            QMutexLocker locker(&mutex);
            if(stopped)
            {
                stopped = false;
                break;
            }
       }

        //qDebug()<<"WifiThread Running";

        wifiSignalStrength = 0;
        wlan0Active = false;
        wlan1Active = false;


        /************************************************/
        //check if interface active is wlan0
        if(!(wifiSignalOutput = popen("sudo ifconfig |grep wlan0","r")))
        {
            qDebug()<<"Unable to interface details on wlan0";
        }
        else
        {
            memset(wifiInterfaceName,'\0',sizeof(wifiInterfaceName));
            fscanf(wifiSignalOutput,"%s",wifiInterfaceName);
            pclose(wifiSignalOutput);

            if(strlen(wifiInterfaceName)>0)     //interface available is wlan0
            {
                noInterfaceFoundCnt = 0;    //to ensure dongleoff script is not run in the gap after dongle is connected before wlan1 is populated

                //qDebug()<<"Current Present Inteface is wlan0";
                wlan0Active = true;

            }
            else
            {
                wlan0Active = false;
            }
        }

        //check if interface active is wlan1
        if(!(wifiSignalOutput = popen("sudo ifconfig |grep wlan1","r")))
        {
            qDebug()<<"Unable to interface details on wlan1";
        }
        else
        {
            memset(wifiInterfaceName,'\0',sizeof(wifiInterfaceName));
            fscanf(wifiSignalOutput,"%s",wifiInterfaceName);
            pclose(wifiSignalOutput);

            if(strlen(wifiInterfaceName)>0)     //interface available is wlan1
            {
                //qDebug()<<"Current Present Inteface is wlan1";

                noInterfaceFoundCnt = 0;    //to ensure dongleoff script is not run in the gap after dongle is connected before wlan1 is populated

                wlan1Active = true;
            }
            else
            {
                wlan1Active = false;
            }
        }

        //action to be done according to current active interface
        if(wlan0Active==true && wlan1Active==true)     //both interfaces are active
        {

            if(bothInterfaceFountCnt<2147483647)
            {
                bothInterfaceFountCnt++;
            }
            qDebug()<<"*****BothInterfaceFountCnt*****"<<QString::number(bothInterfaceFountCnt);
            if(bothInterfaceFountCnt>3)
            {
                if(system("pgrep -x \"dongleon.sh\" > /dev/null")!=0) //0-process running; non zero-process not running
                {
                    qDebug()<<"*****Dongle On Fired Here*****"<<__LINE__;
//                    system("sudo ifconfig wlan1 down");
//                    system("sudo ifconfig wlan1 up");
//                    delay(5);
                    system("/home/pi/Desktop/EdgeFirmware/dongleonapp.sh &");
                    bothInterfaceFountCnt = 0;
                }
            }
            else
            {
                //bothInterfaceFountCnt++;
            }
        }
        else if(wlan0Active==true && wlan1Active==false) //only wlan0 is active
        {
            if(!(wifiSignalOutput = popen("sudo iwconfig wlan0|grep Signal|cut -d\"=\" -f3|cut -d\" \" -f1","r")))
            {
        #ifdef  DEBUG__ON
                qDebug()<<"Unable to get wifi signal status on wlan0";
        #endif  //DEBUG_ON

            }

            else
            {

                fscanf(wifiSignalOutput,"%d",&wifiSignalStrength);

                emit wifiValueChanged(abs(wifiSignalStrength));

    //            qDebug()<< " WiFi Signal Strength Absolute value in dBm is "<<abs(wifiSignalStrength);


                pclose(wifiSignalOutput);
            }
        }
        else if(wlan0Active==false && wlan1Active==true)    //only wlan1 is active i.e. dongle connected
        {
            if(!(wifiSignalOutput = popen("sudo iwconfig wlan1|grep Signal|cut -d\"=\" -f3|cut -d\" \" -f1","r")))
            {
        #ifdef  DEBUG__ON
                qDebug()<<"Unable to get wifi signal status on wlan1";
        #endif  //DEBUG_ON

            }
            else
            {

                fscanf(wifiSignalOutput,"%d",&wifiSignalStrength);

                wifiSignalDongle = abs(wifiSignalStrength);

                if(wifiSignalDongle>85) //full signal
                {
                    emit wifiValueChanged(39);      //20-40 dbm
                }
                else if(wifiSignalDongle>70 && wifiSignalDongle<=85)    //medium signal
                {
                    emit wifiValueChanged(41);      //40-50 dbm
                }
                else if(wifiSignalDongle>50 && wifiSignalDongle<=70)    //low signal
                {
                    emit wifiValueChanged(51);      //50-70 dbm
                }
                else if(wifiSignalDongle<50 && wifiSignalDongle>0)    //poor signal
                {
                    emit wifiValueChanged(71);      //above 70 dbm
                }
                else if(wifiSignalDongle==0)    //not connected to any ssid
                {
                    emit wifiValueChanged(0);
                }

#ifdef  DEBUG__ON
                qDebug()<< " WiFi Signal Strength Absolute value in dBm is "<<abs(wifiSignalStrength);
#endif  //DEBUG__ON

                pclose(wifiSignalOutput);
            }
        }
        else if(wlan0Active==false && wlan1Active==false)   //both interfaces are down
        {
            emit wifiValueChanged(0);
            if(wlan1ForcedDown==false)
            {
                if(noInterfaceFoundCnt<2147483647)
                {
                    noInterfaceFoundCnt++;
                }
qDebug()<<"*****NoInterfaceFoundCnt*****"<<QString::number(noInterfaceFoundCnt);
                if(noInterfaceFoundCnt>8)      //ensuring no interfaces are active for more duration
                {
                    if(system("pgrep -x \"dongleoff.sh\" > /dev/null")!=0) //0-process running; non zero-process not running
                    {
                        qDebug()<<"*****Dongle Off Fired Here*****"<<__LINE__;
                        system("/home/pi/Desktop/EdgeFirmware/dongleoff.sh &");
                    }

                }
                else
                {
                    //noInterfaceFoundCnt++;

                }

            }
        }





        delay(11);

    }
}

/**
 * @brief WifiThread::changeWlan1StateDown
 */
void WifiThread::changeWlan1StateDown()
{
    wlan1ForcedDown = true;
}

/**
 * @brief WifiThread::changeWlan1StateUp
 */
void WifiThread::changeWlan1StateUp()
{
    wlan1ForcedDown = false;
}

/**
 * @brief WifiThread::stop
 */
void WifiThread::stop()
{
    QMutexLocker locker(&mutex);
    stopped = true;
    //qDebug()<<"WifiThread Stopped";
}


/**
 * @brief WifiThread::delay
 * @param sec
 */
void WifiThread::delay(int sec)
{
    dieTime= QTime::currentTime().addSecs(sec);
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}


