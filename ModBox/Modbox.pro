QT += qml quick

CONFIG += c++11

QT += network

INCLUDEPATH +=I /usr/include/curl
LIBS += -L/usr/lib/arm-linux-gnueabihf -lcurl

SOURCES += main.cpp \
    deltakcontroller.cpp \
    languagetranslator.cpp \
    thread.cpp \
    networkthread.cpp \
    curlthread.cpp \
    wifithread.cpp

RESOURCES += qml.qrc \
    res.qrc

lupdate_only{
SOURCES += Backgrounds/AlaramScreen.qml Backgrounds/Alarm_Silenced.qml Background/APMode_Config_Completed.qml
SOURCES += Backgrounds/ApMode_WifiConnected.qml Backgrounds/ApModeScreen.qml Backgrounds/MainContentBackground.qml
SOURCES += Backgrounds/MainContentWhiteBg.qml Backgrounds/MemoryStatus.qml Backgrounds/Settings_Alarms_Popup.qml
SOURCES += Backgrounds/SetupContentBackground.qml Backgrounds/WarningAlarmScreen.qml
SOURCES += CustomComponents/CustomScrollBar.qml CustomComponents/HeaderLabel.qml CustomComponents/HeaderLabelMedium.qml
SOURCES += CustomComponents/TempPage.qml CustomComponents/TimePage.qml
SOURCES += WifiFailScreen.qml WifiSuccessScreen.qml FirmwareUpgrade.qml FWUpgradeCompleteScreen.qml FWUpgradeDownloading.qml
SOURCES += HomeScreen.qml LanguageSelection.qml LinkingSuccessScreen.qml main.qml NumericKeypad.qml OTPScreen.qml QRCode.qml
SOURCES += ReadQRFailed.qml ResetPINScreen.qml SettingsAccess.qml SettingsScreen.qml SplashScreen.qml
}

#lupdate_only{
#SOURCES += Backgrounds/AlaramScreen.qml Backgrounds/Alarm_Silenced.qml Background/APMode_Config_Completed.qml
#SOURCES += Backgrounds/ApMode_WifiConnected.qml Backgrounds/ApModeScreen.qml Backgrounds/MainContentBackground.qml
#SOURCES += Backgrounds/MainContentWhiteBg.qml Backgrounds/MemoryStatus.qml Backgrounds/Settings_Alarms_Popup.qml
#SOURCES += Backgrounds/SetupContentBackground.qml Backgrounds/WarningAlarmScreen.qml
#SOURCES += CustomComponents/CustomScrollBar.qml CustomComponents/HeaderLabel.qml CustomComponents/HeaderLabelMedium.qml
#SOURCES += CustomComponents/TempPage.qml CustomComponents/TimePage.qml
#SOURCES += WifiFailScreen.qml WifiSuccessScreen.qml FirmwareUpgrade.qml FWUpgradeCompleteScreen.qml FWUpgradeDownloading.qml
#SOURCES += HomeScreen.qml LanguageSelection.qml LinkingSuccessScreen.qml main.qml OTPScreen.qml QRCode.qml
#SOURCES += SettingsScreen.qml SplashScreen.qml
#}

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

QT +=

HEADERS += \
    deltakcontroller.h \
    languagetranslator.h \
    thread.h \
    networkthread.h \
    curlthread.h \
    wifithread.h

DISTFILES +=
