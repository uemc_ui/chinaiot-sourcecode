<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="zh" sourcelanguage="en">
<context>
    <name>AlaramScreen</name>
    <message>
        <source>Button</source>
        <translation type="obsolete">按键</translation>
    </message>
    <message>
        <location filename="Backgrounds/AlaramScreen.qml" line="123"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="Backgrounds/AlaramScreen.qml" line="114"/>
        <source>Snooze</source>
        <translation>貪睡</translation>
    </message>
</context>
<context>
    <name>ApModeScreen</name>
    <message>
        <source>Button</source>
        <translation type="obsolete">按键</translation>
    </message>
    <message>
        <location filename="Backgrounds/ApModeScreen.qml" line="56"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="obsolete">完成</translation>
    </message>
    <message>
        <source>AP Mode</source>
        <translation type="obsolete">AP模式</translation>
    </message>
    <message>
        <location filename="Backgrounds/ApModeScreen.qml" line="136"/>
        <source>Device Configuration</source>
        <translation>设备配置</translation>
    </message>
    <message>
        <location filename="Backgrounds/ApModeScreen.qml" line="159"/>
        <source>Hotspot Name:</source>
        <translation>热点名称：</translation>
    </message>
    <message>
        <location filename="Backgrounds/ApModeScreen.qml" line="219"/>
        <source>Please Use a Wi-Fi enabled device (smart phone, tablet etc.,) to find above hot spot name in Wi-Fi Settings
Once the device pairing is complete, open a browser on your device and type in the address http://192.168.50.1:8500 
Please clear mobile browser history before pairing your mobile to this hotspot</source>
        <translation>请使用支持Wi-Fi的设备（智能手机，平板电脑等）在Wi-Fi设置中查找上述热点名称
设备配对完成后，打开设备上的浏览器并输入地址http://192.168.50.1:8500
在将您的手机与此热点配对之前，请清除手机浏览器历史记录</translation>
    </message>
    <message>
        <source>Please Use a Wi-Fi enabled device (smart phone, tablet etc.,) to find above hot spot name in Wi-Fi Settings</source>
        <translation type="obsolete">请使用Wi-Fi设备（智能手机，平板电脑等）在Wi-Fi设置中查找以上热点名称</translation>
    </message>
    <message>
        <source>
Please clear mobile browser history before pairing your mobile to this hotspot</source>
        <translation type="obsolete">
请在将手机配对到此热点之前清除手机浏览器历史记录</translation>
    </message>
    <message>
        <source>
Once the device pairing is complete, open a browser on your device and type in the address http://192.168.50.1:8500 </source>
        <translation type="obsolete">设备配对完成后，打开设备上的浏览器并输入地址http://192.168.50.1:8500</translation>
    </message>
</context>
<context>
    <name>ApMode_WifiConnected</name>
    <message>
        <location filename="Backgrounds/ApMode_WifiConnected.qml" line="35"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="Backgrounds/ApMode_WifiConnected.qml" line="54"/>
        <source>OK</source>
        <translation>好</translation>
    </message>
</context>
<context>
    <name>DeeplazerScreen</name>
    <message>
        <source>Deep Laser is successfully connected</source>
        <translation type="obsolete">深激光连接成功</translation>
    </message>
    <message>
        <source>Cloud Connection</source>
        <translation type="obsolete">雲連接</translation>
    </message>
    <message>
        <location filename="DeeplazerScreen.qml" line="65"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="obsolete">完成</translation>
    </message>
    <message>
        <source>Cloud Connection established successfully</source>
        <translation type="obsolete">雲連接建立成功</translation>
    </message>
</context>
<context>
    <name>DeltaKController</name>
    <message>
        <source>High Temperature Alarm Active</source>
        <translation type="obsolete">高溫報警有效</translation>
    </message>
    <message>
        <source>Low Temperature Alarm Active</source>
        <translation type="obsolete">低溫報警有效</translation>
    </message>
    <message>
        <source>High Temp Alarm was cleared with out ACK from Cloud</source>
        <translation type="obsolete">来自Cloud的ACK消息被清除</translation>
    </message>
    <message>
        <source>Low Temp Alarm was cleared with out ACK from Cloud</source>
        <translation type="obsolete">来自Cloud的ACK消息被清除</translation>
    </message>
    <message>
        <source>Alarm was cleared with out ACK from Cloud</source>
        <translation type="obsolete">來自Cloud的ACK消息被清除</translation>
    </message>
    <message>
        <source>Text that describes primary critical cause of alarm. May be tricker.</source>
        <translation type="obsolete">描述主要危險因素的文字。可能是詭計</translation>
    </message>
    <message>
        <source>High Temperature Alarm &amp; Battery Power Warning Active</source>
        <translation type="obsolete">高溫報警和電池電源警告有效</translation>
    </message>
    <message>
        <source>Low Temperature Alarm &amp; Battery Power Warning Active</source>
        <translation type="obsolete">低溫報警和電池電源警告有效</translation>
    </message>
    <message>
        <source>Battery Power Warning Active</source>
        <translation type="obsolete">電池電量警告有效</translation>
    </message>
    <message>
        <source>Battery Power Alarm Active</source>
        <translation type="obsolete">電池電量報警有效</translation>
    </message>
    <message>
        <source>High Temperature Alarm &amp; Battery Power Alarm Active</source>
        <translation type="obsolete">高溫報警主動報警和電池電源</translation>
    </message>
    <message>
        <source>Low Temperature Alarm &amp; Battery Power Alarm Active</source>
        <translation type="obsolete">低溫報警主動報警和電池電源</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="1272"/>
        <source>User Linking Failed</source>
        <translation>用戶連接失敗</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="1259"/>
        <source>Device user linking successful</source>
        <translation>成功處置鏈接設備</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="1360"/>
        <location filename="deltakcontroller.cpp" line="1442"/>
        <source>Wi-Fi connection successful</source>
        <translation>Wi-Fi连接成功</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="1372"/>
        <location filename="deltakcontroller.cpp" line="1461"/>
        <source>Wi-Fi connection failed</source>
        <translation>Wi-Fi连接失败</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="1374"/>
        <location filename="deltakcontroller.cpp" line="1463"/>
        <source>Please check your Wi-Fi network credentials! Go to Settings &gt; Device Configuration and try again later!</source>
        <translation>请检查您的Wi-Fi网络凭据！ 进入设置&gt;设备配置，然后再试一次！</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="1399"/>
        <location filename="deltakcontroller.cpp" line="1449"/>
        <source>Asset details pushed to cloud successfully</source>
        <translation>资产详细信息成功推送到云端</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="1411"/>
        <location filename="deltakcontroller.cpp" line="1470"/>
        <source>Failed to push Asset settings to cloud!</source>
        <translation>无法将资产设置推送到云端！</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="1413"/>
        <source>Please check your internet connection Go to Settings &gt; Device Configuration and try again later!</source>
        <translation>请检查您的互联网连接进入设置&gt;设备配置，然后再试一次！</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="1545"/>
        <source>         Device Configuration has expired!</source>
        <translation>设备配置已过期！</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="1569"/>
        <source>         Completed Device Configuration</source>
        <translation>已完成设备配置</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="1593"/>
        <source>DeviceLink connected to new asset, rebooting now</source>
        <translation>DeviceLink连接到新的资产，现在重新启动</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="2001"/>
        <location filename="deltakcontroller.cpp" line="2058"/>
        <source>Cold Alarm cleared with out ACK from Cloud</source>
        <translation>寒冷的警报清除了从云的ACK</translation>
    </message>
    <message>
        <source>Device Configuration has expired!</source>
        <translation type="obsolete">设备配置已过期！</translation>
    </message>
    <message>
        <source>Completed Device Configuration</source>
        <translation type="obsolete">已完成设备配置</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="1654"/>
        <source>Warm Alarm Active</source>
        <translation>温暖警报激活</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="1684"/>
        <source>Cold Alarm Active</source>
        <translation>冷警报激活</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="1991"/>
        <source>Warm Alarm cleared with out ACK from Cloud</source>
        <translation>温度警报已清除，并且没有来自云端的ACK</translation>
    </message>
    <message>
        <source>Warm Alarm cleared with out ACK from Cloud &amp; Low Batt Warn Active</source>
        <translation type="obsolete">警报已清除，并且Cloud＆Low Batt警告Active ACK</translation>
    </message>
    <message>
        <source>Cold Alarm cleared with out ACK from Cloud &amp; Low Batt Warn Active</source>
        <translation type="obsolete">通过Cloud＆Low Batt警报激活的ACK来清除冷警报</translation>
    </message>
    <message>
        <source>High Temp Alarm cleared with out ACK from Cloud</source>
        <translation type="obsolete">来自Cloud的ACK消息被清除</translation>
    </message>
    <message>
        <source>Low Temp Alarm cleared with out ACK from Cloud</source>
        <translation type="obsolete">来自Cloud的ACK消息被清除</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="2011"/>
        <location filename="deltakcontroller.cpp" line="2019"/>
        <location filename="deltakcontroller.cpp" line="2068"/>
        <location filename="deltakcontroller.cpp" line="2076"/>
        <source>Alarm cleared with out ACK from Cloud</source>
        <translation>来自云的ACK消息被清除</translation>
    </message>
    <message>
        <source>Power Outage Event cleared with out ACK from Cloud</source>
        <translation type="obsolete">停电事件通过Cloud的ACK发出清除</translation>
    </message>
    <message>
        <source>Alarm cleared with out ACK from Cloud &amp; Low Batt Warn Active</source>
        <translation type="obsolete">报警通过来自Cloud＆Low Batt的“ACK”报警清除</translation>
    </message>
    <message>
        <source>High Temp Alarm cleared with out ACK</source>
        <translation type="obsolete">高温报警被清除，无ACK</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="2048"/>
        <location filename="deltakcontroller.cpp" line="2058"/>
        <location filename="deltakcontroller.cpp" line="2068"/>
        <location filename="deltakcontroller.cpp" line="2076"/>
        <source> &amp; User Not Linked</source>
        <translation>＆用户未链接</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="2048"/>
        <source>Warm Alarm cleared with out ACK</source>
        <translation>温度报警被清除，无ACK</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="2106"/>
        <source>Device user linking incomplete</source>
        <translation>设备用户链接不完整</translation>
    </message>
    <message>
        <source>High Temp Alarm cleared with out ACK from Cloud &amp; Low Batt Warn Active</source>
        <translation type="obsolete">高温报警通过从“云”和“低”按钮发出ACK报警清除</translation>
    </message>
    <message>
        <source>Low Temp Alarm cleared with out ACK from Cloud &amp; Low Batt Warn Active</source>
        <translation type="obsolete">低温报警通过来自云端的低电平报警清除，低电平警告激活</translation>
    </message>
    <message>
        <source>Power Outage Event cleared with out ACK from Cloud &amp; Low Batt Warn Active</source>
        <translation type="obsolete">断电事件通过来自Cloud＆Low Batt的ACK发出消息通知</translation>
    </message>
    <message>
        <source>Low Batt Warn Active</source>
        <translation type="obsolete">低蝙蝠警告活动</translation>
    </message>
    <message>
        <source>Warm Alarm &amp; Battery Power Alarm Active</source>
        <translation type="obsolete">暖警报和电池电量警报激活</translation>
    </message>
    <message>
        <source>Cold Alarm &amp; Battery Power Alarm Active</source>
        <translation type="obsolete">冷报警和电池电量报警激活</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="4099"/>
        <source>User Linking Failed !!!</source>
        <translation>用户链接失败!!!</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="4284"/>
        <source>Cloud Connection established successfully</source>
        <translation>雲連接建立成功</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="4295"/>
        <source>Cloud Connection Failed</source>
        <translation>雲連接失敗</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="6265"/>
        <source>Firmware Update in Progress...
Do not turnoff the unit</source>
        <translation>正在进行固件更新...
请勿关闭本机</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="6312"/>
        <source>Firmware Updated Successfully</source>
        <translation>固件更新成功</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="6333"/>
        <source>Firmware Update Failed...
Restoring Current Version...</source>
        <translation>固件更新失败...
恢复当前版本</translation>
    </message>
    <message>
        <location filename="deltakcontroller.cpp" line="6344"/>
        <source>Unit will be rebooted now...</source>
        <translation>单位现在将重新启动...</translation>
    </message>
</context>
<context>
    <name>DeviceConfigScreen</name>
    <message>
        <location filename="DeviceConfigScreen.qml" line="62"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Request QR Code</source>
        <translation type="obsolete">請求QR碼</translation>
    </message>
    <message>
        <source>Device configuration</source>
        <translation type="obsolete">设备配置</translation>
    </message>
    <message>
        <source>Device configuration completed</source>
        <translation type="obsolete">设备配置已完成</translation>
    </message>
    <message>
        <source>Please install Instrument Connect app on your Smart Phone or Tablet and click on Request QR Code button</source>
        <translation type="obsolete">请在您的智能手机或平板电脑上安装Instrument Connect应用程序，然后单击“请求QR码”按钮</translation>
    </message>
</context>
<context>
    <name>FWUpgradeCompleteScreen</name>
    <message>
        <location filename="FWUpgradeCompleteScreen.qml" line="45"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="FWUpgradeCompleteScreen.qml" line="38"/>
        <source>OK</source>
        <translation>好</translation>
    </message>
    <message>
        <location filename="FWUpgradeCompleteScreen.qml" line="59"/>
        <source>Firmware Upgrade</source>
        <translation>固件升级</translation>
    </message>
    <message>
        <location filename="FWUpgradeCompleteScreen.qml" line="75"/>
        <source>Firmware update completed.</source>
        <translation>固件更新已完成</translation>
    </message>
</context>
<context>
    <name>FWUpgradeDownloading</name>
    <message>
        <location filename="FWUpgradeDownloading.qml" line="18"/>
        <source>Firmware Upgrade</source>
        <translation>固件升级</translation>
    </message>
    <message>
        <location filename="FWUpgradeDownloading.qml" line="34"/>
        <source>Downloading..</source>
        <translation>下载..</translation>
    </message>
    <message>
        <location filename="FWUpgradeDownloading.qml" line="83"/>
        <source>0%</source>
        <translation>0%</translation>
    </message>
    <message>
        <location filename="FWUpgradeDownloading.qml" line="102"/>
        <source>100%</source>
        <translation>100%</translation>
    </message>
</context>
<context>
    <name>FirmwareUpgrade</name>
    <message>
        <location filename="FirmwareUpgrade.qml" line="39"/>
        <source>Start</source>
        <translation>開始</translation>
    </message>
    <message>
        <location filename="FirmwareUpgrade.qml" line="64"/>
        <source>Firmware Upgrade</source>
        <translation>固件升级</translation>
    </message>
    <message>
        <location filename="FirmwareUpgrade.qml" line="79"/>
        <source>New Firmware update is available. Press Start to download and update.</source>
        <translation>新的固件更新可用。按开始下载并更新</translation>
    </message>
    <message>
        <location filename="FirmwareUpgrade.qml" line="76"/>
        <source></source>
        <translation></translation>
    </message>
</context>
<context>
    <name>HomeScreen</name>
    <message>
        <location filename="HomeScreen.qml" line="39"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="HomeScreen.qml" line="69"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="HomeScreen.qml" line="188"/>
        <source>Setpoint</source>
        <translation>设定点</translation>
    </message>
</context>
<context>
    <name>LanguageSelection</name>
    <message>
        <location filename="LanguageSelection.qml" line="25"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="LanguageSelection.qml" line="71"/>
        <source>Done</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="LanguageSelection.qml" line="101"/>
        <source>Language Selection</source>
        <translation>语言选择</translation>
    </message>
    <message>
        <location filename="LanguageSelection.qml" line="165"/>
        <source>Chinese</source>
        <translation>中文</translation>
    </message>
    <message>
        <source>Language Selection Test</source>
        <translation type="obsolete">語言選擇測試</translation>
    </message>
    <message>
        <location filename="LanguageSelection.qml" line="115"/>
        <source>English</source>
        <translation>英語</translation>
    </message>
</context>
<context>
    <name>LinkingSuccessScreen</name>
    <message>
        <source>Device user linking successful</source>
        <translation type="obsolete">设备用户链接成功</translation>
    </message>
    <message>
        <location filename="LinkingSuccessScreen.qml" line="7"/>
        <source>Linking Status</source>
        <translation>鏈接狀態</translation>
    </message>
</context>
<context>
    <name>MemoryStatus</name>
    <message>
        <location filename="Backgrounds/MemoryStatus.qml" line="21"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="Backgrounds/MemoryStatus.qml" line="68"/>
        <source>OK</source>
        <translation>好</translation>
    </message>
</context>
<context>
    <name>OTPScreen</name>
    <message>
        <source>OTP</source>
        <translation type="obsolete">OTP</translation>
    </message>
    <message>
        <source>Enter this OTP as password in mobile device:</source>
        <translation type="obsolete">在移动设备中输入此OTP作为密码：</translation>
    </message>
    <message>
        <location filename="OTPScreen.qml" line="85"/>
        <source>Device configuration in progress</source>
        <translation>设备配置正在进行中</translation>
    </message>
    <message>
        <location filename="OTPScreen.qml" line="133"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="OTPScreen.qml" line="191"/>
        <source>Done</source>
        <translation>完成</translation>
    </message>
</context>
<context>
    <name>QRCode</name>
    <message>
        <location filename="QRCode.qml" line="9"/>
        <source>QR Code</source>
        <translation>二维码</translation>
    </message>
    <message>
        <location filename="QRCode.qml" line="123"/>
        <source>1. Launch the Instrument Connect mobile app
available from iTunes and Google Play</source>
        <translation>1.启动Instrument Connect移动应用程序
可从iTunes和Google Play获得</translation>
    </message>
    <message>
        <location filename="QRCode.qml" line="123"/>
        <source>

2.Within the app, tap the + icon and select QR code option</source>
        <translation>

2.在应用程序中，点击+图标，然后选择QR码选项</translation>
    </message>
    <message>
        <location filename="QRCode.qml" line="123"/>
        <source>

3.Scan the QR code to connect the unit</source>
        <translation>

3.扫描QR码连接设备</translation>
    </message>
    <message>
        <location filename="QRCode.qml" line="123"/>
        <source>
Connection times vary depending upon network speeds

Android users in China must download the application from the following link:</source>
        <translation>
连接时间取决于网络速度

中国的Android用户必须从以下链接下载应用程序：</translation>
    </message>
    <message>
        <source>
Connection times vary depending upon network speeds</source>
        <translation type="obsolete">
连接时间取决于网络速度</translation>
    </message>
    <message>
        <source>
http://downloads.thermofisher.com/APK/InstrumentConnect.apk</source>
        <translation type="obsolete">

中国的Android用户必须从以下链接下载应用程序：
http://downloads.thermofisher.com/APK/InstrumentConnect.apk</translation>
    </message>
    <message>
        <source>

Android™ users in China must download the application from the following link: 
http://downloads.thermofisher.com/APK/InstrumentConnect.apk</source>
        <oldsource>

Android™ users in China must download the application from the following link: http://downloads.thermofisher.com/APK/InstrumentConnect.apk</oldsource>
        <translation type="obsolete">

中国的Android™用户必须从以下链接下载应用程序：
http：//downloads.thermofisher.com/APK/InstrumentConnect.apk</translation>
    </message>
    <message>
        <source>1. Launch the Instrument Connect mobile app
available from iTunes and Google Play 

2.Within the app, tap the + icon and select QR code option 

3.Scan the QR code to connect the unit
Connection times vary depending upon network speeds 

Android™ users in China must download the application from the following

link: http://downloads.thermofisher.com/APK/InstrumentConnect.apk</source>
        <translation type="obsolete">1.启动Instrument Connect移动应用程序
可从iTunes和Google Play获得

2.在应用程序中，点击+图标，然后选择QR码选项

3.扫描QR码连接设备
连接时间取决于网络速度

中国的Android™用户必须从下面下载应用程序

链接：http：//downloads.thermofisher.com/APK/InstrumentConnect.apk</translation>
    </message>
    <message>
        <source>1. Launch the Instrument Connect mobile app
Available from iTunes and Google Play 

2.Within the app, tap the + icon and select QR code option 

3.Scan the QR code to connect the unit
Connection times vary depending upon network speeds 

Android™ users in China users must download the application from the following

link: http://downloads.thermofisher.com/APK/InstrumentConnect.apk</source>
        <translation type="obsolete">1.启动Instrument Connect移动应用程序\ n从iTunes和Google Play中可用\ n \ n2。在应用程序中，点击+图标并选择QR码选项\ n \ n3。扫描QR码以连接设备\ n连接时间不同取决于网络速度\ n \ n中国的Android用户必须从以下网站下载应用程序

链接：http：//downloads.thermofisher.com/APK/InstrumentConnect.apk</translation>
    </message>
    <message>
        <source>Please Scan the below QR Code from Instrument Connect App</source>
        <translation type="obsolete">请从Instrument Connect应用程序扫描以下QR码</translation>
    </message>
</context>
<context>
    <name>ReadQRFailed</name>
    <message>
        <location filename="ReadQRFailed.qml" line="57"/>
        <source>Exit</source>
        <translation>出口</translation>
    </message>
    <message>
        <location filename="ReadQRFailed.qml" line="62"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="ReadQRFailed.qml" line="110"/>
        <source>Failed to obtain QR code!</source>
        <translation>无法获得QR码！</translation>
    </message>
    <message>
        <location filename="ReadQRFailed.qml" line="128"/>
        <source>Please check your internet connection and click Retry</source>
        <translation>请检查您的互联网连接，然后单击重试</translation>
    </message>
    <message>
        <location filename="ReadQRFailed.qml" line="160"/>
        <source>Retry</source>
        <translation>重试</translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="obsolete">完成</translation>
    </message>
    <message>
        <location filename="ReadQRFailed.qml" line="94"/>
        <source>Read QR Status</source>
        <translation>閱讀QR狀態</translation>
    </message>
    <message>
        <source>Failed to obtain QR Code from Cloud</source>
        <translation type="obsolete">無法從雲端獲取QR碼</translation>
    </message>
</context>
<context>
    <name>ResetPINScreen</name>
    <message>
        <location filename="ResetPINScreen.qml" line="7"/>
        <source>Reset PIN Status</source>
        <translation>重置PIN状态</translation>
    </message>
    <message>
        <location filename="ResetPINScreen.qml" line="18"/>
        <source>Your PIN has been reset. Please contact customer care for obtaining the same</source>
        <oldsource>Your PIN has been reset. Please contact customer care @xxxxxxxxxx for obtaining the same</oldsource>
        <translation>您的PIN码已被重置。请联系客户服务部门获取相同的信息</translation>
    </message>
</context>
<context>
    <name>SettingsAccess</name>
    <message>
        <location filename="SettingsAccess.qml" line="27"/>
        <location filename="SettingsAccess.qml" line="255"/>
        <source>Exceeded max attempts ! Try after 5 min</source>
        <translation>超过最大的尝试！ 5分钟后再试</translation>
    </message>
    <message>
        <source>PIN must contain at least 4 digits</source>
        <translation type="obsolete">PIN码必须至少包含4位数字</translation>
    </message>
    <message>
        <location filename="SettingsAccess.qml" line="51"/>
        <location filename="SettingsAccess.qml" line="78"/>
        <location filename="SettingsAccess.qml" line="124"/>
        <location filename="SettingsAccess.qml" line="170"/>
        <location filename="SettingsAccess.qml" line="279"/>
        <location filename="SettingsAccess.qml" line="306"/>
        <location filename="SettingsAccess.qml" line="352"/>
        <location filename="SettingsAccess.qml" line="398"/>
        <source>PINs don&apos;t match</source>
        <translation>PIN码不匹配</translation>
    </message>
    <message>
        <location filename="SettingsAccess.qml" line="86"/>
        <location filename="SettingsAccess.qml" line="188"/>
        <location filename="SettingsAccess.qml" line="314"/>
        <location filename="SettingsAccess.qml" line="416"/>
        <source>Incorrect PIN entered</source>
        <translation>输入的PIN码不正确</translation>
    </message>
    <message>
        <location filename="SettingsAccess.qml" line="103"/>
        <location filename="SettingsAccess.qml" line="150"/>
        <location filename="SettingsAccess.qml" line="331"/>
        <location filename="SettingsAccess.qml" line="378"/>
        <source>Confirm your PIN</source>
        <translation>确认您的PIN码</translation>
    </message>
    <message>
        <location filename="SettingsAccess.qml" line="209"/>
        <source>Locked ! Try after 5 min</source>
        <translation>锁定！ 5分钟后再试</translation>
    </message>
    <message>
        <source>Please enter your pin</source>
        <translation type="obsolete">请输入您的PIN码</translation>
    </message>
    <message>
        <location filename="SettingsAccess.qml" line="213"/>
        <location filename="SettingsAccess.qml" line="236"/>
        <location filename="SettingsAccess.qml" line="560"/>
        <source>Please enter your PIN</source>
        <translation>请输入您的PIN码</translation>
    </message>
    <message>
        <location filename="SettingsAccess.qml" line="56"/>
        <location filename="SettingsAccess.qml" line="128"/>
        <location filename="SettingsAccess.qml" line="225"/>
        <location filename="SettingsAccess.qml" line="245"/>
        <location filename="SettingsAccess.qml" line="284"/>
        <location filename="SettingsAccess.qml" line="356"/>
        <source>Choose your 4-digit PIN</source>
        <translation>选择你的4位数密码</translation>
    </message>
    <message>
        <location filename="SettingsAccess.qml" line="46"/>
        <location filename="SettingsAccess.qml" line="72"/>
        <location filename="SettingsAccess.qml" line="274"/>
        <location filename="SettingsAccess.qml" line="300"/>
        <source>PIN must contain 4 digits</source>
        <translation>PIN码必须包含4位数字</translation>
    </message>
    <message>
        <location filename="SettingsAccess.qml" line="404"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="SettingsAccess.qml" line="486"/>
        <source>Forgot PIN?</source>
        <translation>忘记密码？</translation>
    </message>
    <message>
        <location filename="SettingsAccess.qml" line="505"/>
        <source>Reset PIN Confirmation</source>
        <translation>重置PIN码确认</translation>
    </message>
    <message>
        <location filename="SettingsAccess.qml" line="518"/>
        <source>Are you sure to reset PIN?</source>
        <translation>你确定要重置PIN吗？</translation>
    </message>
    <message>
        <location filename="SettingsAccess.qml" line="530"/>
        <source>YES</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="SettingsAccess.qml" line="574"/>
        <source>NO</source>
        <translation>没有</translation>
    </message>
</context>
<context>
    <name>SettingsScreen</name>
    <message>
        <location filename="SettingsScreen.qml" line="18"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <source>AP Mode</source>
        <translation type="obsolete">AP模式</translation>
    </message>
    <message>
        <location filename="SettingsScreen.qml" line="108"/>
        <source>Device Configuration</source>
        <translation>设备配置</translation>
    </message>
    <message>
        <location filename="SettingsScreen.qml" line="175"/>
        <source>Language</source>
        <translation>语言</translation>
    </message>
    <message>
        <location filename="SettingsScreen.qml" line="253"/>
        <source>User Linking</source>
        <translation>用户链接</translation>
    </message>
    <message>
        <location filename="SettingsScreen.qml" line="335"/>
        <source>Change PIN</source>
        <oldsource>Update PIN</oldsource>
        <translation>更改PIN码</translation>
    </message>
    <message>
        <source>QR Code</source>
        <translation type="obsolete">二维码</translation>
    </message>
    <message>
        <source>Firmware upgrade</source>
        <translation type="obsolete">固件升级</translation>
    </message>
    <message>
        <location filename="SettingsScreen.qml" line="207"/>
        <source></source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Settings_Alarms_Popup</name>
    <message>
        <location filename="Backgrounds/Settings_Alarms_Popup.qml" line="35"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="Backgrounds/Settings_Alarms_Popup.qml" line="78"/>
        <source>OK</source>
        <translation>好</translation>
    </message>
</context>
<context>
    <name>SetupContentBackground</name>
    <message>
        <location filename="Backgrounds/SetupContentBackground.qml" line="48"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="Backgrounds/SetupContentBackground.qml" line="68"/>
        <source>Done</source>
        <translation>完成</translation>
    </message>
</context>
<context>
    <name>TempPage</name>
    <message>
        <location filename="CustomComponents/TempPage.qml" line="255"/>
        <source>°C</source>
        <translation>°C</translation>
    </message>
</context>
<context>
    <name>TimePage</name>
    <message>
        <location filename="CustomComponents/TimePage.qml" line="377"/>
        <source>Setpoint</source>
        <translation>设定点</translation>
    </message>
    <message>
        <location filename="CustomComponents/TimePage.qml" line="436"/>
        <source>min</source>
        <translation>分</translation>
    </message>
    <message>
        <location filename="CustomComponents/TimePage.qml" line="490"/>
        <source>Warm Alarm</source>
        <translation>暖警报</translation>
    </message>
    <message>
        <location filename="CustomComponents/TimePage.qml" line="509"/>
        <source>Cold  Alarm</source>
        <translation>冷报警</translation>
    </message>
    <message>
        <location filename="CustomComponents/TimePage.qml" line="525"/>
        <source>Snooze Time Out</source>
        <translation>延后超时</translation>
    </message>
</context>
<context>
    <name>WifiFailScreen</name>
    <message>
        <location filename="WifiFailScreen.qml" line="58"/>
        <source>Wi-Fi connection failed</source>
        <translation>Wi-Fi连接失败</translation>
    </message>
    <message>
        <location filename="WifiFailScreen.qml" line="74"/>
        <source>Please check your Wi-Fi network credentials and try again</source>
        <translation>请检查您的Wi-Fi网络凭据，然后重试</translation>
    </message>
    <message>
        <location filename="WifiFailScreen.qml" line="72"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="WifiFailScreen.qml" line="109"/>
        <source>Exit</source>
        <translation>出口</translation>
    </message>
    <message>
        <location filename="WifiFailScreen.qml" line="196"/>
        <source>Retry</source>
        <translation>重试</translation>
    </message>
</context>
<context>
    <name>WifiSuccessScreen</name>
    <message>
        <location filename="WifiSuccessScreen.qml" line="24"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="WifiSuccessScreen.qml" line="59"/>
        <source>Request QR Code</source>
        <translation>請求QR碼</translation>
    </message>
    <message>
        <source>Device configuration</source>
        <translation type="obsolete">设备配置</translation>
    </message>
    <message>
        <location filename="WifiSuccessScreen.qml" line="129"/>
        <source>Wi-Fi connection successful</source>
        <translation>Wi-Fi连接成功</translation>
    </message>
    <message>
        <location filename="WifiSuccessScreen.qml" line="147"/>
        <source>Click &quot;Request QR Code&quot; to proceed to Device user linking </source>
        <oldsource>Click Request QR Code to proceed to Device user linking </oldsource>
        <translation>点击请求QR码以继续进行设备用户链接</translation>
    </message>
</context>
</TS>
