import QtQuick 2.0
import QtQuick.Controls 2.0
import "./Backgrounds"

import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "./assets"

//import QtQuick.Controls.Material 2.0
import QtQuick.Dialogs 1.2

SetupContentBackground {
    id: idKeyPad
    width: 480
    height: 320
    color: "#f6f7f7"

    property var parentPageSettAcc: parentPageSettAcc
    property int passwdCnt: 0

    function validatePinEntered(){
        //console.log ( "Pin Entered is: " + idPinValue.pin ) ;

        if(passwdCnt===2 && deltaK.pinNumber !== idPinValue.pin)
        {
            //console.log("Exceeded Max no of unsuccessful attempts !!!")
            headerLabel = qsTr ( "Exceeded max attempts ! Try after 5 min" ) + languageTranslator.translatedText;
            idPinValue.pin = "";
            idPinValue.enabled = false

            settingsLockedTimer.start()
        }
        else
        {
            idPinValue.enabled = true



            if(idPinValue.pin.length<4)
            {

                if(deltaK.appStartLang === true)    //entered less than 4 digits during first time device config
                {
                    if(deltaK.pinNotSet === true)   //first attempt
                    {
                        headerLabel = qsTr ( "PIN must contain 4 digits" ) + languageTranslator.translatedText;
                        idPinValue.pin = '';
                    }
                    else if(deltaK.pinNotSet === false)
                    {
                        headerLabel = qsTr ( "PINs don't match" ) + languageTranslator.translatedText;
                        idPinValue.pin = '';

                        delay(1500, function() {
//                                    console.log('500 m Sec Delay Expired Settings Access Screen')
                                    headerLabel = qsTr ( "Choose your 4-digit PIN" ) + languageTranslator.translatedText;

                                })

                        deltaK.pinNumber = ''
                        deltaK.pinNotSet = true

                    }
                }
                else if(deltaK.appStartLang === false)
                {
                    if(parentPageSettAcc === "Setting3")    //entered into this screen from setting menu to reset pin
                    {

                        if(deltaK.pinNotSet === true)
                        {
                            headerLabel = qsTr ( "PIN must contain 4 digits" ) + languageTranslator.translatedText;
                            idPinValue.pin = '';

                        }
                        else if(deltaK.pinNotSet === false)
                        {
                            headerLabel = qsTr ( "PINs don't match" ) + languageTranslator.translatedText;
                            idPinValue.pin = '';

                        }

                    }
                    else
                    {
                        headerLabel = qsTr ( "Incorrect PIN entered" ) + languageTranslator.translatedText;
                        idPinValue.pin = '';
                        passwdCnt++;

                    }
                }
            }
            else if(idPinValue.pin.length>=4)
            {
                if(deltaK.appStartLang === true)    //settings not done... this is during first time configuration
                {

                    if(deltaK.pinNotSet === true)
                    {
                        deltaK.pinNumber = idPinValue.pin

                        idPinValue.pin = '';
                        headerLabel = qsTr ( "Confirm your PIN" ) + languageTranslator.translatedText;

                        deltaK.pinNotSet = false

                    }
                    else if(deltaK.pinNotSet === false)
                    {
                        if ( deltaK.pinNumber === idPinValue.pin )
                        {
                            //console.log("Re-entered PIN Match Success")

                            deltaK.writePinToFile()

                            mainStack.pop(StackView.Immediate)
                            mainStack.push(languageSelection, StackView.Immediate)

                        }
                        else
                        {
                            //console.log("Re-entered PIN Match Failed")
                            idPinValue.pin = '';
                            headerLabel = qsTr ( "PINs don't match" ) + languageTranslator.translatedText;

                            delay(1500, function() {
//                                        console.log('500 m Sec Delay Expired Settings Access Screen')
                                        headerLabel = qsTr ( "Choose your 4-digit PIN" ) + languageTranslator.translatedText;

                                    })

                            deltaK.pinNumber = ''
                            deltaK.pinNotSet = true



                        }
                    }

                }
                else if(deltaK.appStartLang === false)      //entered into this screen from home screen i.e. 2nd time
                {
                    if(parentPageSettAcc === "Setting3")    //entered into this screen from setting menu to reset pin
                    {
                        if(deltaK.pinNotSet === true)
                        {
                            deltaK.pinNumber = idPinValue.pin

                            idPinValue.pin = '';
                            headerLabel = qsTr ( "Confirm your PIN" ) + languageTranslator.translatedText;

                            deltaK.pinNotSet = false

                        }
                        else if(deltaK.pinNotSet === false)
                        {
                            if ( deltaK.pinNumber === idPinValue.pin )
                            {
                                //console.log("Re-entered PIN Match Success")

                                deltaK.writePinToFile()
                                settingsScreen.settingsAutoExitTimeStart()
                                mainStack.pop(settingsScreen,StackView.Immediate)
                                parentPageSettAcc = ""
                            }
                            else
                            {
                                //console.log("Re-entered PIN Match Failed")
                                idPinValue.pin = '';
                                headerLabel = qsTr ( "PINs don't match" ) + languageTranslator.translatedText;

                            }
                        }

                    }
                    else
                    {
                        if ( deltaK.pinNumber === idPinValue.pin )
                        {
                            //Verify the OTP
                            settingsScreen.settingsAutoExitTimeStart()
                            mainStack.pop(StackView.Immediate)
                            mainStack.push(settingsScreen, StackView.Immediate)
                            passwdCnt = 0;
                        }
                        else
                        {
                            headerLabel = qsTr ( "Incorrect PIN entered" ) + languageTranslator.translatedText;
                            idPinValue.pin = '';
                            passwdCnt++
                        }
                    }

                }

            }
        }

    }

    function clearPin(){

        deltaK.enableForgotPinOption()
        deltaK.enableBackNumKeyPad()
        idPinValue.pin = '';

        if(settingsLockedTimer.running===true)
        {
            headerLabel = qsTr ( "Locked ! Try after 5 min" ) + languageTranslator.translatedText;
        }
        else if(settingsLockedTimer.running===false)
        {
            headerLabel = qsTr ( "Please enter your PIN" ) + languageTranslator.translatedText;
        }

    }

    function resetPin(){

        deltaK.disableForgotPinOption()
        deltaK.enableBackNumKeyPad()
        idPinValue.pin = '';
        deltaK.pinNotSet = true

        headerLabel = qsTr ( "Choose your 4-digit PIN" ) + languageTranslator.translatedText;

    }

    Timer {
        id: settingsLockedTimer
        repeat: false
        running: false
        interval: 300000    //300 sectimer will fire to auto redirect
        onTriggered: {
            console.log('settingsLockedTimer Fired');
            headerLabel = qsTr ( "Please enter your PIN" ) + languageTranslator.translatedText;

            idPinValue.enabled=true
            passwdCnt = 0
        }
    }

    Component.onCompleted: {
        doneButton.visible = true ;
        headerLabel = qsTr ( "Choose your 4-digit PIN" ) + languageTranslator.translatedText;
    }


    doneButton.onClicked: {
//        console.log ( "Pin Entered is: " + idPinValue.pin ) ;

        if(passwdCnt===2 && deltaK.pinNumber !== idPinValue.pin)
        {
            //console.log("Exceeded Max no of unsuccessful attempts !!!")
            headerLabel = qsTr ( "Exceeded max attempts ! Try after 5 min" ) + languageTranslator.translatedText;
            idPinValue.pin = "";
            idPinValue.enabled = false

            settingsLockedTimer.start()
        }
        else
        {
            idPinValue.enabled = true



            if(idPinValue.pin.length<4)
            {

                if(deltaK.appStartLang === true)    //entered less than 4 digits during first time device config
                {
                    if(deltaK.pinNotSet === true)   //first attempt
                    {
                        headerLabel = qsTr ( "PIN must contain 4 digits" ) + languageTranslator.translatedText;
                        idPinValue.pin = '';
                    }
                    else if(deltaK.pinNotSet === false)
                    {
                        headerLabel = qsTr ( "PINs don't match" ) + languageTranslator.translatedText;
                        idPinValue.pin = '';

                        delay(1500, function() {
//                                    console.log('500 m Sec Delay Expired Settings Access Screen')
                                    headerLabel = qsTr ( "Choose your 4-digit PIN" ) + languageTranslator.translatedText;

                                })

                        deltaK.pinNumber = ''
                        deltaK.pinNotSet = true

                    }
                }
                else if(deltaK.appStartLang === false)
                {
                    if(parentPageSettAcc === "Setting3")    //entered into this screen from setting menu to reset pin
                    {

                        if(deltaK.pinNotSet === true)
                        {
                            headerLabel = qsTr ( "PIN must contain 4 digits" ) + languageTranslator.translatedText;
                            idPinValue.pin = '';

                        }
                        else if(deltaK.pinNotSet === false)
                        {
                            headerLabel = qsTr ( "PINs don't match" ) + languageTranslator.translatedText;
                            idPinValue.pin = '';

                        }

                    }
                    else
                    {
                        headerLabel = qsTr ( "Incorrect PIN entered" ) + languageTranslator.translatedText;
                        idPinValue.pin = '';
                        passwdCnt++;

                    }
                }
            }
            else if(idPinValue.pin.length>=4)
            {
                if(deltaK.appStartLang === true)    //settings not done... this is during first time configuration
                {

                    if(deltaK.pinNotSet === true)
                    {
                        deltaK.pinNumber = idPinValue.pin

                        idPinValue.pin = '';
                        headerLabel = qsTr ( "Confirm your PIN" ) + languageTranslator.translatedText;

                        deltaK.pinNotSet = false

                    }
                    else if(deltaK.pinNotSet === false)
                    {
                        if ( deltaK.pinNumber === idPinValue.pin )
                        {
                            //console.log("Re-entered PIN Match Success")

                            deltaK.writePinToFile()

                            mainStack.pop(StackView.Immediate)
                            mainStack.push(languageSelection, StackView.Immediate)

                        }
                        else
                        {
                            //console.log("Re-entered PIN Match Failed")
                            idPinValue.pin = '';
                            headerLabel = qsTr ( "PINs don't match" ) + languageTranslator.translatedText;

                            delay(1500, function() {
//                                        console.log('500 m Sec Delay Expired Settings Access Screen')
                                        headerLabel = qsTr ( "Choose your 4-digit PIN" ) + languageTranslator.translatedText;

                                    })

                            deltaK.pinNumber = ''
                            deltaK.pinNotSet = true



                        }
                    }

                }
                else if(deltaK.appStartLang === false)      //entered into this screen from home screen i.e. 2nd time
                {
                    if(parentPageSettAcc === "Setting3")    //entered into this screen from setting menu to reset pin
                    {
                        if(deltaK.pinNotSet === true)
                        {
                            deltaK.pinNumber = idPinValue.pin

                            idPinValue.pin = '';
                            headerLabel = qsTr ( "Confirm your PIN" ) + languageTranslator.translatedText;

                            deltaK.pinNotSet = false

                        }
                        else if(deltaK.pinNotSet === false)
                        {
                            if ( deltaK.pinNumber === idPinValue.pin )
                            {
                                //console.log("Re-entered PIN Match Success")

                                deltaK.writePinToFile()
                                settingsScreen.settingsAutoExitTimeStart()
                                mainStack.pop(settingsScreen,StackView.Immediate)
                                parentPageSettAcc = ""
                            }
                            else
                            {
                                //console.log("Re-entered PIN Match Failed")
                                idPinValue.pin = '';
                                headerLabel = qsTr ( "PINs don't match" ) + languageTranslator.translatedText;

                            }
                        }

                    }
                    else
                    {
                        if ( deltaK.pinNumber === idPinValue.pin )
                        {
                            //Verify the OTP
                            settingsScreen.settingsAutoExitTimeStart()
                            mainStack.pop(StackView.Immediate)
                            mainStack.push(settingsScreen, StackView.Immediate)
                            passwdCnt = 0;
                        }
                        else
                        {
                            headerLabel = qsTr ( "Incorrect PIN entered" ) + languageTranslator.translatedText;
                            idPinValue.pin = '';
                            passwdCnt++
                        }
                    }

                }

            }
        }





    }

    NumericKeypad {
        id: idPinValue ;        
        x:115
        y: 10
        width: 240
        height: 181 //160 + 38
    }

    Button {
        id: backButton
        x: 24
        y: 140
        width: 24
        height: 40
        text: qsTr("")
        visible: deltaK.showBackKeyPad
        style: ButtonStyle {
            background: Rectangle {
                color: "Transparent"
            }
        }

        Image {
            id: backImage
            x: 0
            y: 0
            width: 24
            height: 40
            source: "./assets/Images/Settings/Back_icon_new.png"
            visible: deltaK.showBackKeyPad
        }

        onClicked: {
            deltaK.pinNotSet = false
            if(parentPageSettAcc === "Setting3")    //back clicked from reset pin screen option
            {
                settingsScreen.settingsAutoExitTimeStart()
                parentPageSettAcc = ""
            }


            deltaK.readPinFromFile()            
            mainStack.pop(StackView.Immediate)

        }
    }

    Text {
        id: forgotPINLabel
        x: 373
        y: 8
        width: 100
        height: 22
        text: "<a href='http://www.kde.org'>" + qsTr("Forgot PIN?")+ languageTranslator.translatedText + "</a>"
        textFormat: Text.RichText
        style: Text.Normal
        FontLoader {
            id: robotoRegular1
            source: "./assets/Fonts/roboto.regular.ttf"
        }
        font.pixelSize: 18
        visible: deltaK.showForgotPin
        onLinkActivated:{
            console.log("forgotPIN Label Clicked")

            idYesNo.visible = true
        }
    }

    Dialog {
        id: idYesNo ;
        visible: false
        title: qsTr ( "Reset PIN Confirmation" ) //+ languageTranslator.translatedText

        contentItem: Rectangle {
            color: "#f6f7f7"
            implicitWidth: 300
            implicitHeight: 100
            FontLoader {
                id: idRobotoFont
                source: "./assets/Fonts/roboto.regular.ttf"
            }
            Label {
                x: 50
                y: 20
                text: qsTr ( "Are you sure to reset PIN?" ) + languageTranslator.translatedText
                color: "#344550"
                font.family: idRobotoFont.name
                font.pixelSize: 18
                clip: true ;
            }

            Button {
                property color btncolor: "#AAAAAA" ;
                id: btnYes
                x: 70
                y: 60
                text: qsTr( "YES" ) //+ languageTranslator.translatedText
                width: 40
                height: 30

                style: ButtonStyle {
                    background: Rectangle {
                        //color: btnYes.btncolor
                        color: "#1FA2E6"
                        radius: 5
                    }

                    label: Text {
                        width: btnYes.width ;
                        height: btnYes.height ;
                        color: "#ffffff" ; //"#344550"
                        text: control.text
                        font.family: idRobotoFont.name
                        font.pixelSize: 16
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }
                }
                onClicked: {
                  idYesNo.close( ) ;
                    idPinValue.pin = ''
                    console.log("Yes... Reset PIN Clicked")
                    settingsLockedTimer.stop()
                    deltaK.resetPIN()
                    mainStack.push(resetPINScreen, StackView.Immediate)

                    headerLabel = qsTr ( "Please enter your PIN" ) + languageTranslator.translatedText;

                    idPinValue.enabled=true
                    passwdCnt = 0


                }
            }

            Button {
                property color btncolor: "#AAAAAA" ;
                id: btnNo
                x: 170
                y: 60
                text: qsTr( "NO" ) // + languageTranslator.translatedText
                width: 40
                height: 30

                style: ButtonStyle {
                    background: Rectangle {
                      //color: btnYes.btncolor
                    color: "#1FA2E6"
                    radius: 5
                    }

                    label: Text {
                        width: btnNo.width ;
                        height: btnNo.height ;
                        color: "#ffffff" ; //"#344550"
                        text: control.text
                        font.family: idRobotoFont.name
                        font.pixelSize: 16
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }
                }
                onClicked: {
                    console.log("No... Reset PIN Clicked")
                    idYesNo.close( ) ;
                }
            }
        }
    }



    Timer {
            id: timer
        }

        function delay(delayTime, cb) {
            timer.interval = delayTime;
            timer.repeat = false;
            timer.triggered.connect(cb);
            timer.start();
        }
}
