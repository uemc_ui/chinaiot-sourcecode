#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtGui/QScreen>
#include <QtQml>
#include <QThread>
#include <QQmlEngine>
#include <QQuickView>
#include <QQuickItem>
#include <QQmlContext>
#include <QTranslator>

#include "deltakcontroller.h"
#include "languagetranslator.h"

//#include <QDir>

//QFile appLogFile;
//QByteArray txt;

//void debugMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
//{
//    Q_UNUSED(context);
//    txt = QString("").toLocal8Bit();

//    switch ( type )
//    {
//        case QtDebugMsg:
//            txt += QString("%1:DEBUG    :%2").arg(QDateTime::currentDateTime().toTimeSpec(Qt::OffsetFromUTC).toString(Qt::ISODate),msg);
//        break;
//        case QtWarningMsg:
//            txt += QString("%1:WARNING  :%2").arg(QDateTime::currentDateTime().toTimeSpec(Qt::OffsetFromUTC).toString(Qt::ISODate),msg);
//        break;
//        case QtCriticalMsg:
//            txt += QString("%1:CRITICAL :%2").arg(QDateTime::currentDateTime().toTimeSpec(Qt::OffsetFromUTC).toString(Qt::ISODate),msg);
//        break;
//        case QtInfoMsg:
//            txt += QString("%1:INFO     :%2").arg(QDateTime::currentDateTime().toTimeSpec(Qt::OffsetFromUTC).toString(Qt::ISODate),msg);
//        break;
//        case QtFatalMsg:
//        //txt += QString("{Fatal:} \t %1").arg(msg);
//            txt += QString("%1:FATAL    :%2").arg(QDateTime::currentDateTime().toTimeSpec(Qt::OffsetFromUTC).toString(Qt::ISODate),msg);
//        //abort(); // deliberately core dump
//        break;
//        default:
//            txt += QString("%1:DEFAULT  :%2").arg(QDateTime::currentDateTime().toTimeSpec(Qt::OffsetFromUTC).toString(Qt::ISODate),msg);
//        break;
//    }
//    //QFile appLogFile("/home/pi/Desktop/EdgeFirmware/miscfiles/chinaiot.log");
//    appLogFile.setFileName("/home/pi/Desktop/EdgeFirmware/miscfiles/chinaiot.log");
//    appLogFile.open(QIODevice::WriteOnly | QIODevice::Append);

//    QTextStream textStream(&appLogFile);
//    textStream << txt << endl;
//    appLogFile.close();

//}


int main(int argc, char *argv[])
{
//    qInstallMessageHandler(debugMessageOutput);

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    DeltaKController *deltaKController = new DeltaKController();

    // CODE TO CHANGE
    // Set Device Name here - Hard coded for now
    //deltaKController->setDeviceName("Acme Labs ULT");
    //deltaKController->setReferenceTemperature(QString::number(-106.5));
    //deltaKController->setTemperature(QString::number(-106.5));

//    deltaKController->setReferenceTemperature("--.-");
//    deltaKController->setTemperature("--.-");

    // END - CODE TO CHANGE
    LanguageTranslator languageTranslator;
    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("deltaK" , deltaKController);
    engine.rootContext()->setContextProperty("languageTranslator",&languageTranslator);
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    QObject *root = engine.rootObjects()[0];
    QObject::connect(deltaKController, SIGNAL(OTPChanged()),
                         root, SLOT(valueChanged()));
    QObject::connect(deltaKController, SIGNAL(startRedFlash()),
                         root, SLOT(startRedAnimation()));
    QObject::connect(deltaKController, SIGNAL(stopRedFlash()),
                         root, SLOT(stopRedAnimation()));
    QObject::connect(deltaKController, SIGNAL(startYellowFlash()),
                         root, SLOT(startYellowAnimation()));
    QObject::connect(deltaKController, SIGNAL(stopYellowFlash()),
                         root, SLOT(stopYellowAnimation()));
    QObject::connect(deltaKController, SIGNAL(wifiConnSuccess()),
                         root, SLOT(wifiSuccessScreen()));
    QObject::connect(deltaKController, SIGNAL(wifiConnFail()),
                         root, SLOT(wifiFailScreen()));
    QObject::connect(deltaKController, SIGNAL(configProgress()),
                         root, SLOT(showConfigProgressScreen()));
    QObject::connect(deltaKController, SIGNAL(qrScanDone()),
                         root, SLOT(qrDoneClick()));
    QObject::connect(deltaKController, SIGNAL(apModeAutoExit()),
                         root, SLOT(apDoneButtonExitTimer()));
    QObject::connect(deltaKController, SIGNAL(apModeAutoExitTimerStop()),
                                 root, SLOT(apDoneButtonTimerStop()));


    return app.exec();
}
