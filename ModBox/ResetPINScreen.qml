import QtQuick 2.0
import QtQuick.Controls 2.0
import "./Backgrounds"

SetupContentBackground {
//    headerLabel: "Success"
    headerLabel: qsTr("Reset PIN Status") + languageTranslator.translatedText
    property var parentPage: parentPage

    Label {
        id: textMessage
        x: 44
        y: 138
        width: 392
        height: 44        
        wrapMode: Text.WordWrap
        color: "#33444f"
        text: qsTr("Your PIN has been reset. Please contact customer care for obtaining the same") + languageTranslator.translatedText
        FontLoader { id: robotoRegular1; source: "./assets/Fonts/roboto.regular.ttf" }
        font.family: robotoRegular1.name
        font.pixelSize: 18
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    doneButton.onClicked: {
        //mainStack.push(homeScreen)

        if(parentPage === "Setting"){
            mainStack.pop(StackView.Immediate)
        }else{
            //deltaK.completedDeviceConfig()
            mainStack.pop(StackView.Immediate)
        }
    }

}
