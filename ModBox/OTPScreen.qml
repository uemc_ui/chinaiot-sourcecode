import QtQuick 2.0
import QtQuick.Controls 2.0
import "./Backgrounds"

import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "./assets"

Rectangle {
    width: 480
    height: 320
    color: "#f6f7f7"

    property var parentPage: parentPage

    FontLoader { id: robotoRegular1; source: "../assets/Fonts/roboto.regular.ttf" }

    signal otpdonesig()


    function showWifiSuccessScreen(){
        console.log("Inside showWifiSuccessScreen function")
        mainStack.pop(StackView.Immediate)
        mainStack.push(wifiSuccessScreen, StackView.Immediate)
    }

    function showWifiFailScreen(){

        console.log("Inside showWifiFailScreen function")
        mainStack.pop(StackView.Immediate)
        mainStack.push(wifiFailScreen, StackView.Immediate)

    }

    Image {
        id: busyImage
        x: 80
        y: 10
        width: 68
        height: 68
        source: "./assets/Images/spinner.png"
        anchors.centerIn: parent
        visible: deltaK.isSetBusy

        NumberAnimation on rotation {
            from: 0; to: 360; running: busyImage.visible === true;
            loops: Animation.Infinite; duration: 700;
        }
    }

    Image {
        id: wifiSuccessImage
        x: 20
        y: 71
        width: 32
        height: 23
        //source: "../assets/Images/Home_Screen/Status_Icon.png"
        source: "./assets/Images/OK-Green.png"
        visible: deltaK.wifiIconSucces
    }

    Label {
        id:wifiSuccessTextMessage
        x: 75
        y: 73
        width: 400
        height: 22
        //text: qsTr("Wi-Fi Connection Successful") + languageTranslator.translatedText
        text: deltaK.wifiStatusText
        visible: deltaK.wifiUpdateSucces
        color: "#33444f"
        //FontLoader { id: robotoRegular1; source: "../assets/Fonts/roboto.regular.ttf" }
        font.family: robotoRegular1.name
        font.pixelSize: 18
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
    }

    Label {
        id:devConfigProgressTextMessage
        x: 100
        y: 85
        width: 400
        height: 22
        text: qsTr("Device configuration in progress") + languageTranslator.translatedText
        visible: deltaK.showConfigProg
        color: "#33444f"
        font.family: robotoRegular1.name
        font.pixelSize: 18
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
    }


    Image {
        id: assetSuccessImage
        x: 20
        y: 110
        width: 32
        height: 23
        //        source: "../assets/Images/Home_Screen/Status_Icon.png"
        source: "./assets/Images/OK-Green.png"
        visible: deltaK.assetIconSucces
    }

    Label {
        id:assetDetailsSuccessMessage
        x: 75
        y: 111
        width: 400
        height: 22
        //text: qsTr("Asset Details Pushed to Cloud Successfully") + languageTranslator.translatedText
        text: deltaK.assetStatusText
        visible: deltaK.assetUpdateSucces
        color: "#33444f"
        //FontLoader { id: robotoRegular1; source: "../assets/Fonts/roboto.regular.ttf" }
        font.family: robotoRegular1.name
        font.pixelSize: 18
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
    }

    Timer {
        id: apModeScreenExitTimer
        repeat: false
        running: false
        interval: 120000    //2 min timer will fire to auto redirect
        onTriggered: {
            console.log('Exit Timer Fired');

            doneButtonOTP.enabled = false

            if(parentPage === "Setting")
            {
               deltaK.startDLForDevReg()
                settingsScreen.settingsAutoExitTimeStart()
               mainStack.pop(StackView.Immediate)
                mainStack.push(settingsScreen, StackView.Immediate)
            }
            else
            {

            }

            doneButtonOTP.enabled = true
        }
    }

    function startAPModeScreenExitTimer(){

        console.log('Auto Exit Timer Started')

        apModeScreenExitTimer.start()

    }

    function stopAPModeScreenExitTimer(){

        apModeScreenExitTimer.stop()
        console.log('Auto Exit Timer Stopped')

    }

    Button {
        id: doneButtonOTP
        x: 155
        y: 260
        width: 170
        height: 50
        visible: deltaK.enableOTPButton
        text: qsTr("")
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        style: ButtonStyle {
            background: Rectangle {
                color: "#2ea2ec"
                radius: 5
            }
        }

         Text {
            id: doneButtonOTPText
            x: 63
            y: 10
            width: 78
            height: 30
            color: "#ffffff"
            // color: "#2ea2ec"
            text: qsTr("Done") + languageTranslator.translatedText
            FontLoader {
                id: robotoRegular
                source: "./assets/Fonts/roboto.regular.ttf"
            }
            verticalAlignment: Text.AlignVCenter
            font.family: robotoRegular.name
            font.pixelSize: 18
        }
         onClicked: {
             console.log('Done OTP Clicked');

             //mouseArea.enabled = false
             //doneButtonOTP.visible = false
             doneButtonOTP.enabled = false
             //otpScreen.enabled = false

             if(parentPage === "Setting")
             {
                deltaK.startDLForDevReg()
                 settingsScreen.settingsAutoExitTimeStart()
                mainStack.pop(StackView.Immediate)
                 mainStack.push(settingsScreen, StackView.Immediate)
             }
             else
             {

//                 deltaK.startDLForDevReg()
//                 mainStack.pop(StackView.Immediate)
//                 mainStack.push(deeplazerScreen, StackView.Immediate)

             }


             doneButtonOTP.enabled = true
             //doneButtonOTP.visible = true
             //mouseArea.enabled = true
             //otpScreen.enabled = true

         }
    }

    Label {
        id: assetDetailsFailedMessage
        x: 75
        y: 172
        width: 400
        height: 22
        color: "#33444f"
        //text: qsTr("Please check your internet connection Go to Settings > Device Configuration and try again later!") + languageTranslator.translatedText
        text: deltaK.failMessage
        wrapMode: Text.WordWrap
        font.pixelSize: 18
        visible: deltaK.assetUpdateFail
        font.family: robotoRegular1.name
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
    }

    Image {
        id: assetFailIcon
        x: 20
        y: 110
        width: 32
        height: 23
        source:"./assets/Images/Remove-Red.png"
        visible: deltaK.assetIconFail
    }

    Image {
        id: wifiFailIcon
        x: 20
        y: 71
        width: 32
        height: 23
        source:"./assets/Images/Remove-Red.png"
        visible: deltaK.wifiIconFail
    }

}


