import QtQuick 2.4
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls.Material 2.0
import "./Backgrounds"
import "./assets"

MainContentWhiteBg {
    width: 480
    height: 320
    logo: ""
    property alias backButton: backButton
    property alias apModeButton: apModeButton
    property alias greenImage: greenImage.source
    property alias alarmImage: alarmImage
    property alias warningImage: warningImage
    main: qsTr("Settings") + languageTranslator.translatedText
    degrees: deltaK.temperature + "°C"
    header: deltaK.deviceName

    function settingsAutoExitTimeStart()
    {
        settingsAutoExitTimer.start();
    }


    Timer {
        id: settingsTimer
        running: true
        repeat: false
        interval: 5000
        //signal otpdonesig()
        triggeredOnStart: true
        onTriggered: {
//            console.log('Setting Timer fired')
            settingsTimer.stop()
            deltaK.setSettingsMouseClickStatus(false);

            if(deltaK.language==="Chinese")
            {
                languageSelection.setCheckedChineseRB()
            }
            else
            {
                languageSelection.setCheckedEnglishRB()
            }
        }
        }

    Timer {
        id: settingsAutoExitTimer
        repeat: false
        running: false
        interval: 300000    //300 sectimer will fire to auto redirect
        onTriggered: {
            console.log('settingsAutoExitTimer Fired');

            mainStack.pop(StackView.Immediate)

        }
    }




    Image {
        id: warningImage
        x: 210
        y: 10
        width: 60
        height: 60
        //visible: deltaK.batteryWarning && !deltaK.tempAlarm || (deltaK.wifiWarning && deltaK.ethernetWarning) && !deltaK.tempAlarm || deltaK.powerWarning && !deltaK.tempAlarm || deltaK.userLinkWarning ||deltaK.unAckAlarmCleared && !deltaK.tempAlarm
        visible: deltaK.batteryWarning || (deltaK.wifiWarning && deltaK.ethernetWarning) || deltaK.powerWarning || deltaK.noProbeDetected || deltaK.userLinkWarning ||deltaK.unAckAlarmCleared && !deltaK.tempAlarm
        source: "../assets/Images/Status_Warning_Yellow_BG.png"
    }

    Image {
        id: silencedAlarmImage
        x: 210
        y: 10
        width: 60
        height: 60
        visible: deltaK.tempAlarm && deltaK.isSnooze
        source: "../assets/Images/Status_Bell_Snooze_Alarm_Red_BG.png"
    }

    Image {
        id: greenImage
        x: 210
        y: 10
        width: 58
        height: 58
        //visible: !deltaK.batteryWarning && (!deltaK.wifiWarning || !deltaK.ethernetWarning) && !deltaK.powerWarning && !deltaK.tempAlarm && !deltaK.userLinkWarning && !deltaK.unAckAlarmCleared
        visible: !deltaK.batteryWarning && (!deltaK.wifiWarning || !deltaK.ethernetWarning) && !deltaK.powerWarning && !deltaK.tempAlarm && !deltaK.noProbeDetected && !deltaK.userLinkWarning &&!deltaK.unAckAlarmCleared
        //source: "../assets/Images/Home_Screen/Status_Icon_BG_New.png"
        source: "../assets/Images/HealthIcon_Heart.png"
    }

    Image {
        id: alarmImage
        x: 210
        y: 10
        width: 60
        height: 60
        visible: deltaK.tempAlarm && !deltaK.isSnooze
        source: "../assets/Images/Status_Bell_Alarm.png"
    }




    Button {
        id: apModeButton
        x: 135
        y: 80
        text: qsTr("Device Configuration") + languageTranslator.translatedText
        width: 230
        height: 50

        style: ButtonStyle {
            background: Rectangle {
                color: "#eceeee"
                radius: 5
            }

            label: Text {
                x: 48
                y: 10
                width: 160
                height: 30
                FontLoader {
                    id: robotoRegular1
                    source: "./assets/Fonts/roboto.regular.ttf"
                }

                color: "#344550"
                text: control.text
                font.family: robotoRegular1.name
                font.pixelSize: 18
            }
        }

        Image {
            id: apModeImage
            x: 10
            source: "./assets/Images/Settings/AP mode_Wifi.png"
            y: 14
            width: 28
            height: 22
        }

        onClicked: {

            settingsAutoExitTimer.stop()

            //fwGradeButton.enabled = false
            backButton.enabled = false
            languageButton.enabled = false
            qrButton.enabled = false
            apModeButton.enabled = false
            resetPinButton.enabled = false

            deltaK.startApMode()
            //apModeScreen.parentPage = "Setting"
            otpScreen.parentPage = "Setting"
            linkingSuccessScreen.parentPage = "Setting"
            mainStack.pop(StackView.Immediate)
            mainStack.push(apModeScreen, StackView.Immediate)

            //fwGradeButton.enabled = true
            backButton.enabled = true
            languageButton.enabled = true
            qrButton.enabled = true
            apModeButton.enabled = true
            resetPinButton.enabled = true
        }
    }

    Button {
        id: languageButton
        x: 135
        y: 140
        text: qsTr("Language") + languageTranslator.translatedText
        width: 230
        height: 50

        style: ButtonStyle {
            background: Rectangle {
                color: "#eceeee"
                radius: 5
            }

            label: Text {
                x: 48
                y: 10
                width: 160
                height: 30
                FontLoader {
                    id: robotoRegular2
                    source: "./assets/Fonts/roboto.regular.ttf"
                }

                color: "#344550"
                text: control.text
                font.family: robotoRegular2.name
                font.pixelSize: 18
            }
        }

        Image {
            id: languageImage
            x: 10
            source: "assets/Images/languageImage.png"
            y: 10
            width: 30
            height: 30
        }

        onClicked: {
            settingsAutoExitTimer.stop()

            languageSelection.resetRadioButtons()
            if(deltaK.chineseLang === true)
            {
                languageSelection.setCheckedChineseRB()
            }
            else if(deltaK.chineseLang === false)
            {
                languageSelection.setCheckedEnglishRB()
            }

            languageButton.enabled = false
            languageSelection.parentPage1 = "Setting1"
            mainStack.push(languageSelection, StackView.Immediate)
            languageButton.enabled = true
        }
    }


    Image {
        id: busyImage
        x: 80
        y: 10
        width: 68
        height: 68
        source: "./assets/Images/spinner.png"
        anchors.centerIn: parent
        visible: deltaK.isSetBusy

        NumberAnimation on rotation {
            from: 0; to: 360; running: busyImage.visible === true;
            loops: Animation.Infinite; duration: 700;
        }
    }


    Button {
        id: qrButton
        x: 135
        y: 200
        text: qsTr("User Linking") + languageTranslator.translatedText
        width: 230
        height: 50

        style: ButtonStyle {
            background: Rectangle {
                color: "#eceeee"
                radius: 5
            }

            label: Text {
                x: 48
                y: 10
                width: 160
                height: 30
                FontLoader {
                    id: robotoRegular3
                    source: "./assets/Fonts/roboto.regular.ttf"
                }

                color: "#344550"
                text: control.text
                font.family: robotoRegular3.name
                font.pixelSize: 18
            }
        }

        Image {
            id: qrImage
            x: 10
            source: "assets/Images/QRCodeImage.png"
            y: 10
            width: 30
            height: 30
        }

        onClicked: {
            settingsAutoExitTimer.stop()
            //fwGradeButton.enabled = false
            backButton.enabled = false
            languageButton.enabled = false
            apModeButton.enabled = false
            qrButton.enabled = false
            resetPinButton.enabled = false

            deltaK.readQRImage()

            while(busyImage.visible===true)
            {

            }

//            mainStack.push(qrCode, StackView.Immediate)
            if(deltaK.readQRStatus === true)    //qr read is success
            {
                qrCode.startQRAutoExitTimer()
                qrCode.parentPage2 = "Setting2"
                mainStack.push(qrCode, StackView.Immediate)
                console.log('QR Code Screen is displayed')
                deltaK.readQRStatus = false
            }
            //else if(deltaK.readQRStatus === false)  //qr read failed
            else
            {
                readQRFailed.parentPage2 = "Setting2"
                mainStack.push(readQRFailed, StackView.Immediate)
                console.log('QR Code Failed !!!')
            }

            //fwGradeButton.enabled = true
            backButton.enabled = true
            languageButton.enabled = true
            apModeButton.enabled = true
            qrButton.enabled = true
            resetPinButton.enabled = true

        }
    }

    Button {
        id: resetPinButton
        x: 135
        y: 260
        text: qsTr("Change PIN") + languageTranslator.translatedText
        width: 230
        height: 50

        style: ButtonStyle {
            background: Rectangle {
                color: "#eceeee"
                radius: 5
            }

            label: Text {
                x: 48
                y: 10
                width: 160
                height: 30
                FontLoader {
                    id: robotoRegular4
                    source: "./assets/Fonts/roboto.regular.ttf"
                }

                color: "#344550"
                text: control.text
                font.family: robotoRegular4.name
                font.pixelSize: 18
            }
        }

        Image {
            id: resetPinImage
            x: 10
            source: "assets/Images/Settings/password_key.png"
            y: 10
            width: 30
            height: 30
        }

        onClicked: {
            settingsAutoExitTimer.stop()
            backButton.enabled = false
            languageButton.enabled = false
            apModeButton.enabled = false
            qrButton.enabled = false
            resetPinButton.enabled = false

            settingsAccess.parentPageSettAcc = "Setting3"

            settingsAccess.resetPin()
            mainStack.push(settingsAccess, StackView.Immediate)


            backButton.enabled = true
            languageButton.enabled = true
            apModeButton.enabled = true
            qrButton.enabled = true
            resetPinButton.enabled = true

        }
    }
/*
    Button {
        id: fwGradeButton
        x: 125
        y: 260
        text: qsTr("Firmware upgrade") + languageTranslator.translatedText
        width: 220
        height: 50

        style: ButtonStyle {
            background: Rectangle {
                color: "#eceeee"
                radius: 5
            }

            label: Text {
                x: 48
                y: 10
                width: 160
                height: 30
                FontLoader {
                    id: robotoRegular4
                    source: "./assets/Fonts/roboto.regular.ttf"
                }

                color: "#344550"
                text: control.text
                font.family: robotoRegular4.name
                font.pixelSize: 18
            }
        }

        Image {
            id: fwGradeImage
            x: 10
            source: "assets/Images/FWUpgrade.png"
            y: 10
            width: 30
            height: 30
        }

        onClicked: {
            //console.log("Settings-Pressed FW Upgrade")
            mainStack.push(firmwareUpgrade, StackView.Immediate)
        }
    }
*/
    Button {
        id: backButton
        x: 24
        y: 140
        width: 24
        height: 40
        text: qsTr("")

        style: ButtonStyle {
            background: Rectangle {
                color: "Transparent"
            }
        }

        Image {
            id: backImage
            x: 0
            y: 0
            width: 24
            height: 40
            source: "./assets/Images/Settings/Back_icon_new.png"
        }

        onClicked: {
            settingsAutoExitTimer.stop()
            mainStack.pop(StackView.Immediate)
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent

        onPressed:  {
            //console.log('onPressed Settings '+deltaK.enableOTPButtonClick)
            settingsTimer.start()
            mouse.accepted = deltaK.enableOTPButtonClick
        }
    }

}
