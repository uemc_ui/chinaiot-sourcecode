<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_IN" sourcelanguage="en">
<context>
    <name>AlaramScreen</name>
    <message>
        <source>Button</source>
        <translation type="obsolete">Button</translation>
    </message>
    <message>
        <source>Snooze</source>
        <translation>Snooze</translation>
    </message>
    <message>
        <source></source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ApModeScreen</name>
    <message>
        <source>AP Mode</source>
        <translation type="obsolete">AP Mode</translation>
    </message>
    <message>
        <source>Hotspot Name:</source>
        <translation>Hotspot Name:</translation>
    </message>
    <message>
        <source>Button</source>
        <translation type="obsolete">Button</translation>
    </message>
    <message>
        <source>Done</source>
        <translatorcomment>Done</translatorcomment>
        <translation type="obsolete">Done</translation>
    </message>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Please Use a Wi-Fi enabled device (smart phone, tablet etc.,) to find above hot spot name in Wi-Fi Settings</source>
        <translation type="obsolete">Please Use a Wi-Fi enabled device (smart phone, tablet etc.,) to find above hot spot name in Wi-Fi Settings</translation>
    </message>
    <message>
        <source>Device Configuration</source>
        <translation>Device Configuration</translation>
    </message>
    <message>
        <source>
Once the device pairing is complete, open a browser on your device and type in the address http://192.168.50.1:8500</source>
        <translation type="obsolete">Once the device pairing is complete, open a browser on your device and type in the address http://192.168.50.1:8500</translation>
    </message>
    <message>
        <source>
Please clear mobile browser history before pairing your mobile to this hotspot</source>
        <translation type="obsolete">Please clear mobile browser history before pairing your mobile to this hotspot</translation>
    </message>
    <message>
        <source>
Once the device pairing is complete, open a browser on your device and type in the address http://192.168.50.1:8500 </source>
        <translation type="obsolete">Once the device pairing is complete, open a browser on your device and type in the address http://192.168.50.1:8500 </translation>
    </message>
    <message>
        <source>Please Use a Wi-Fi enabled device (smart phone, tablet etc.,) to find above hot spot name in Wi-Fi Settings
Once the device pairing is complete, open a browser on your device and type in the address http://192.168.50.1:8500 
Please clear mobile browser history before pairing your mobile to this hotspot</source>
        <translation>Please Use a Wi-Fi enabled device (smart phone, tablet etc.,) to find above hot spot name in Wi-Fi Settings
Once the device pairing is complete, open a browser on your device and type in the address http://192.168.50.1:8500 
Please clear mobile browser history before pairing your mobile to this hotspot</translation>
    </message>
</context>
<context>
    <name>ApMode_WifiConnected</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>DeeplazerScreen</name>
    <message>
        <source>Deep Laser is successfully connected</source>
        <translation type="obsolete">Deep Laser is successfully connected</translation>
    </message>
    <message>
        <source>Cloud Connection</source>
        <translation type="obsolete">Cloud Connection</translation>
    </message>
    <message>
        <source>Cloud Connection established successfully</source>
        <translation type="obsolete">Cloud Connection established successfully</translation>
    </message>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="obsolete">Done</translation>
    </message>
</context>
<context>
    <name>DeltaKController</name>
    <message>
        <source>High Temperature Alarm Active</source>
        <translation type="obsolete">High Temperature Alarm Active</translation>
    </message>
    <message>
        <source>Low Temperature Alarm Active</source>
        <translation type="obsolete">Low Temperature Alarm Active</translation>
    </message>
    <message>
        <source>Alarm was cleared with out ACK from Cloud</source>
        <translation type="obsolete">Alarm was cleared with out ACK from Cloud</translation>
    </message>
    <message>
        <source>Text that describes primary critical cause of alarm. May be tricker.</source>
        <translation type="obsolete">Text that describes primary critical cause of alarm. May be tricker.</translation>
    </message>
    <message>
        <source>High Temperature Alarm &amp; Battery Power Warning Active</source>
        <translation type="obsolete">High Temperature Alarm &amp; Battery Power Warning Active</translation>
    </message>
    <message>
        <source>Low Temperature Alarm &amp; Battery Power Warning Active</source>
        <translation type="obsolete">Low Temperature Alarm &amp; Battery Power Warning Active</translation>
    </message>
    <message>
        <source>Battery Power Warning Active</source>
        <translation type="obsolete">Battery Power Warning Active</translation>
    </message>
    <message>
        <source>Battery Power Alarm Active</source>
        <translation type="obsolete">Battery Power Alarm Active</translation>
    </message>
    <message>
        <source>High Temperature Alarm &amp; Battery Power Alarm Active</source>
        <translation type="obsolete">High Temperature Alarm &amp; Battery Power Alarm Active</translation>
    </message>
    <message>
        <source>Low Temperature Alarm &amp; Battery Power Alarm Active</source>
        <translation type="obsolete">Low Temperature Alarm &amp; Battery Power Alarm Active</translation>
    </message>
    <message>
        <source>User Linking Failed</source>
        <translation>User Linking Failed</translation>
    </message>
    <message>
        <source>Device user linking successful</source>
        <translatorcomment>Device user linking successful</translatorcomment>
        <translation>Device user linking successful</translation>
    </message>
    <message>
        <source>Cloud Connection established successfully</source>
        <translatorcomment>Cloud Connection established successfully</translatorcomment>
        <translation>Cloud Connection established successfully</translation>
    </message>
    <message>
        <source>Cloud Connection Failed</source>
        <translation>Cloud Connection Failed</translation>
    </message>
    <message>
        <source>High Temp Alarm was cleared with out ACK from Cloud</source>
        <translation type="obsolete">High Temp Alarm was cleared with out ACK from Cloud</translation>
    </message>
    <message>
        <source>Low Temp Alarm was cleared with out ACK from Cloud</source>
        <translation type="obsolete">Low Temp Alarm was cleared with out ACK from Cloud</translation>
    </message>
    <message>
        <source>High Temp Alarm cleared with out ACK from Cloud</source>
        <translation type="obsolete">High Temp Alarm cleared with out ACK from Cloud</translation>
    </message>
    <message>
        <source>Low Temp Alarm cleared with out ACK from Cloud</source>
        <translation type="obsolete">Low Temp Alarm cleared with out ACK from Cloud</translation>
    </message>
    <message>
        <source>Alarm cleared with out ACK from Cloud</source>
        <translation>Alarm cleared with out ACK from Cloud</translation>
    </message>
    <message>
        <source>Alarm cleared with out ACK from Cloud &amp; Low Batt Warn Active</source>
        <translation type="obsolete">Alarm cleared with out ACK from Cloud &amp; Low Batt Warn Active</translation>
    </message>
    <message>
        <source>High Temp Alarm cleared with out ACK from Cloud &amp; Low Batt Warn Active</source>
        <translation type="obsolete">High Temp Alarm cleared with out ACK from Cloud &amp; Low Batt Warn Active</translation>
    </message>
    <message>
        <source>Low Temp Alarm cleared with out ACK from Cloud &amp; Low Batt Warn Active</source>
        <translation type="obsolete">Low Temp Alarm cleared with out ACK from Cloud &amp; Low Batt Warn Active</translation>
    </message>
    <message>
        <source>Low Batt Warn Active</source>
        <translation type="obsolete">Low Batt Warn Active</translation>
    </message>
    <message>
        <source>User Linking Failed !!!</source>
        <translation>User Linking Failed !!!</translation>
    </message>
    <message>
        <source>Power Outage Event cleared with out ACK from Cloud</source>
        <translation type="obsolete">Power Outage Event cleared with out ACK from Cloud</translation>
    </message>
    <message>
        <source>Power Outage Event cleared with out ACK from Cloud &amp; Low Batt Warn Active</source>
        <translation type="obsolete">Power Outage Event cleared with out ACK from Cloud &amp; Low Batt Warn Active</translation>
    </message>
    <message>
        <source>Wi-Fi connection successful</source>
        <translation>Wi-Fi connection successful</translation>
    </message>
    <message>
        <source>Wi-Fi connection failed</source>
        <translation>Wi-Fi connection failed</translation>
    </message>
    <message>
        <source>Please check your Wi-Fi network credentials! Go to Settings &gt; Device Configuration and try again later!</source>
        <translation>Please check your Wi-Fi network credentials! Go to Settings &gt; Device Configuration and try again later!</translation>
    </message>
    <message>
        <source>Asset details pushed to cloud successfully</source>
        <translation>Asset details pushed to cloud successfully</translation>
    </message>
    <message>
        <source>Failed to push Asset settings to cloud!</source>
        <translation>Failed to push Asset settings to cloud!</translation>
    </message>
    <message>
        <source>Please check your internet connection Go to Settings &gt; Device Configuration and try again later!</source>
        <translation>Please check your internet connection Go to Settings &gt; Device Configuration and try again later!</translation>
    </message>
    <message>
        <source>Firmware Update in Progress...
Do not turnoff the unit</source>
        <translation>Firmware Update in Progress...
Do not turnoff the unit</translation>
    </message>
    <message>
        <source>Firmware Updated Successfully</source>
        <translation>Firmware Updated Successfully</translation>
    </message>
    <message>
        <source>Firmware Update Failed...
Restoring Current Version...</source>
        <translation>Firmware Update Failed...
Restoring Current Version...</translation>
    </message>
    <message>
        <source>Unit will be rebooted now...</source>
        <translation>Unit will be rebooted now...</translation>
    </message>
    <message>
        <source>Device Configuration has expired!</source>
        <translation type="obsolete">Device Configuration has expired!</translation>
    </message>
    <message>
        <source>Completed Device Configuration</source>
        <translation type="obsolete">Completed Device Configuration</translation>
    </message>
    <message>
        <source>High Temp Alarm cleared with out ACK</source>
        <translation type="obsolete">High Temp Alarm cleared with out ACK</translation>
    </message>
    <message>
        <source> &amp; User Not Linked</source>
        <translation>&amp; User Not Linked</translation>
    </message>
    <message>
        <source>Device user linking incomplete</source>
        <translation>Device user linking incomplete</translation>
    </message>
    <message>
        <source>Warm Alarm Active</source>
        <translation>Warm Alarm Active</translation>
    </message>
    <message>
        <source>Cold Alarm Active</source>
        <translation>Cold Alarm Active</translation>
    </message>
    <message>
        <source>Warm Alarm cleared with out ACK from Cloud</source>
        <translation>Warm Alarm cleared with out ACK from Cloud</translation>
    </message>
    <message>
        <source>Cold Alarm cleared with out ACK from Cloud</source>
        <translation>Cold Alarm cleared with out ACK from Cloud</translation>
    </message>
    <message>
        <source>Warm Alarm cleared with out ACK</source>
        <translation>Warm Alarm cleared with out ACK</translation>
    </message>
    <message>
        <source>Warm Alarm cleared with out ACK from Cloud &amp; Low Batt Warn Active</source>
        <translation type="obsolete">Warm Alarm cleared with out ACK from Cloud &amp; Low Batt Warn Active</translation>
    </message>
    <message>
        <source>Cold Alarm cleared with out ACK from Cloud &amp; Low Batt Warn Active</source>
        <translation type="obsolete">Cold Alarm cleared with out ACK from Cloud &amp; Low Batt Warn Active</translation>
    </message>
    <message>
        <source>Warm Alarm &amp; Battery Power Alarm Active</source>
        <translation type="obsolete">Warm Alarm &amp; Battery Power Alarm Active</translation>
    </message>
    <message>
        <source>Cold Alarm &amp; Battery Power Alarm Active</source>
        <translation type="obsolete">Cold Alarm &amp; Battery Power Alarm Active</translation>
    </message>
    <message>
        <source>         Device Configuration has expired!</source>
        <translation>         Device Configuration has expired!</translation>
    </message>
    <message>
        <source>         Completed Device Configuration</source>
        <translation>         Completed Device Configuration</translation>
    </message>
    <message>
        <source>DeviceLink connected to new asset, rebooting now</source>
        <translation>DeviceLink connected to new asset, rebooting now</translation>
    </message>
</context>
<context>
    <name>DeviceConfigScreen</name>
    <message>
        <source>Device configuration</source>
        <translation type="obsolete">Device configuration</translation>
    </message>
    <message>
        <source>Device configuration completed</source>
        <translation type="obsolete">Device configuration completed</translation>
    </message>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Request QR Code</source>
        <translation type="obsolete">Request QR Code</translation>
    </message>
    <message>
        <source>Please install Instrument Connect app on your Smart Phone or Tablet and click on Request QR Code button</source>
        <translation type="obsolete">Please install Instrument Connect app on your Smart Phone or Tablet and click on Request QR Code button</translation>
    </message>
</context>
<context>
    <name>FWUpgradeCompleteScreen</name>
    <message>
        <source>Firmware Upgrade</source>
        <translation>Firmware Upgrade</translation>
    </message>
    <message>
        <source>Firmware update completed.</source>
        <translation>Firmware update completed.</translation>
    </message>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>FWUpgradeDownloading</name>
    <message>
        <source>Firmware Upgrade</source>
        <translation>Firmware Upgrade</translation>
    </message>
    <message>
        <source>Downloading..</source>
        <translation>Downloading..</translation>
    </message>
    <message>
        <source>0%</source>
        <translation>0%</translation>
    </message>
    <message>
        <source>100%</source>
        <translation>100%</translation>
    </message>
</context>
<context>
    <name>FirmwareUpgrade</name>
    <message>
        <source>Firmware Upgrade</source>
        <translation>Firmware Upgrade</translation>
    </message>
    <message>
        <source>New Firmware update is available. Press Start to download and update.</source>
        <translation>New Firmware update is available. Press Start to download and update.</translation>
    </message>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Start</translation>
    </message>
</context>
<context>
    <name>HomeScreen</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <source>Setpoint</source>
        <translation>Setpoint</translation>
    </message>
</context>
<context>
    <name>LanguageSelection</name>
    <message>
        <source>Language Selection</source>
        <translation>Language Selection</translation>
    </message>
    <message>
        <source>Language Selection Test</source>
        <translation type="obsolete">Language Selection Test</translation>
    </message>
    <message>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <source>Chinese</source>
        <translation>Chinese</translation>
    </message>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Done</source>
        <translation>Done</translation>
    </message>
</context>
<context>
    <name>LinkingSuccessScreen</name>
    <message>
        <source>Device user linking successful</source>
        <translation type="obsolete">Device user linking successful</translation>
    </message>
    <message>
        <source>Linking Status</source>
        <translation>Linking Status</translation>
    </message>
</context>
<context>
    <name>MemoryStatus</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>OTPScreen</name>
    <message>
        <source>Enter this OTP as password in mobile device:</source>
        <translation type="obsolete">Enter this OTP as password in mobile device:</translation>
    </message>
    <message>
        <source>OTP</source>
        <translation type="obsolete">OTP</translation>
    </message>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Done</source>
        <translation>Done</translation>
    </message>
    <message>
        <source>Device configuration in progress</source>
        <translation>Device configuration in progress</translation>
    </message>
</context>
<context>
    <name>QRCode</name>
    <message>
        <source>QR Code</source>
        <translation>QR Code</translation>
    </message>
    <message>
        <source>Please Scan the below QR Code from Instrument Connect App</source>
        <translation type="obsolete">Please Scan the below QR Code from Instrument Connect App</translation>
    </message>
    <message>
        <source>1. Launch the Instrument Connect mobile app
Available from iTunes and Google Play 

2.Within the app, tap the + icon and select QR code option 

3.Scan the QR code to connect the unit
Connection times vary depending upon network speeds 

Android™ users in China users must download the application from the following

link: http://downloads.thermofisher.com/APK/InstrumentConnect.apk</source>
        <translation type="obsolete">1. Launch the Instrument Connect mobile app\nAvailable from iTunes and Google Play \n\n2.Within the app, tap the + icon and select QR code option \n\n3.Scan the QR code to connect the unit\nConnection times vary depending upon network speeds \n\nAndroid™ users in China users must download the application from the following

link: http://downloads.thermofisher.com/APK/InstrumentConnect.apk</translation>
    </message>
    <message>
        <source>1. Launch the Instrument Connect mobile app
available from iTunes and Google Play 

2.Within the app, tap the + icon and select QR code option 

3.Scan the QR code to connect the unit
Connection times vary depending upon network speeds 

Android™ users in China must download the application from the following

link: http://downloads.thermofisher.com/APK/InstrumentConnect.apk</source>
        <translation type="obsolete">1. Launch the Instrument Connect mobile app
available from iTunes and Google Play 

2.Within the app, tap the + icon and select QR code option 

3.Scan the QR code to connect the unit
Connection times vary depending upon network speeds 

Android™ users in China must download the application from the following

link: http://downloads.thermofisher.com/APK/InstrumentConnect.apk</translation>
    </message>
    <message>
        <source>1. Launch the Instrument Connect mobile app
available from iTunes and Google Play</source>
        <translation>1. Launch the Instrument Connect mobile app
available from iTunes and Google Play</translation>
    </message>
    <message>
        <source>

2.Within the app, tap the + icon and select QR code option</source>
        <translation>

2.Within the app, tap the + icon and select QR code option</translation>
    </message>
    <message>
        <source>

3.Scan the QR code to connect the unit</source>
        <translation>

3.Scan the QR code to connect the unit</translation>
    </message>
    <message>
        <source>
Connection times vary depending upon network speeds</source>
        <translation type="obsolete">
Connection times vary depending upon network speeds</translation>
    </message>
    <message>
        <source>

Android™ users in China must download the application from the following link: http://downloads.thermofisher.com/APK/InstrumentConnect.apk</source>
        <translation type="obsolete">

Android™ users in China must download the application from the following link: http://downloads.thermofisher.com/APK/InstrumentConnect.apk</translation>
    </message>
    <message>
        <source>

Android™ users in China must download the application from the following link: 
http://downloads.thermofisher.com/APK/InstrumentConnect.apk</source>
        <translation type="obsolete">

Android™ users in China must download the application from the following link: 
http://downloads.thermofisher.com/APK/InstrumentConnect.apk</translation>
    </message>
    <message>
        <source>
Connection times vary depending upon network speeds

</source>
        <translation type="obsolete">
Connection times vary depending upon network speeds

</translation>
    </message>
    <message>
        <source>Android users in China must download the application from the following link:</source>
        <translation type="obsolete">Android users in China must download the application from the following link:</translation>
    </message>
    <message>
        <source>
http://downloads.thermofisher.com/APK/InstrumentConnect.apk</source>
        <translation type="obsolete">
http://downloads.thermofisher.com/APK/InstrumentConnect.apk</translation>
    </message>
    <message>
        <source>
Connection times vary depending upon network speeds

Android users in China must download the application from the following link:</source>
        <translation>
Connection times vary depending upon network speeds

Android users in China must download the application from the following link:</translation>
    </message>
</context>
<context>
    <name>ReadQRFailed</name>
    <message>
        <source>Read QR Status</source>
        <translation>Read QR Status</translation>
    </message>
    <message>
        <source>Failed to obtain QR Code from Cloud</source>
        <translation type="obsolete">Failed to obtain QR Code from Cloud</translation>
    </message>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="obsolete">Done</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>Exit</translation>
    </message>
    <message>
        <source>Failed to obtain QR code!</source>
        <translation>Failed to obtain QR code!</translation>
    </message>
    <message>
        <source>Please check your internet connection and click Retry</source>
        <translation>Please check your internet connection and click Retry</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation>Retry</translation>
    </message>
</context>
<context>
    <name>ResetPINScreen</name>
    <message>
        <source>Reset PIN Status</source>
        <translation>Reset PIN Status</translation>
    </message>
    <message>
        <source>Your PIN has been reset. Please contact customer care @xxxxxxxxxx for obtaining the same</source>
        <translation type="obsolete">Your PIN has been reset. Please contact customer care @xxxxxxxxxx for obtaining the same</translation>
    </message>
    <message>
        <source>Your PIN has been reset. Please contact customer care for obtaining the same</source>
        <translation>Your PIN has been reset. Please contact customer care for obtaining the same</translation>
    </message>
</context>
<context>
    <name>SettingsAccess</name>
    <message>
        <source>Exceeded max attempts ! Try after 5 min</source>
        <translation>Exceeded max attempts ! Try after 5 min</translation>
    </message>
    <message>
        <source>PIN must contain at least 4 digits</source>
        <translation type="obsolete">PIN must contain at least 4 digits</translation>
    </message>
    <message>
        <source>Incorrect PIN entered</source>
        <translation>Incorrect PIN entered</translation>
    </message>
    <message>
        <source>Confirm your PIN</source>
        <translation>Confirm your PIN</translation>
    </message>
    <message>
        <source>Locked ! Try after 5 min</source>
        <translation>Locked ! Try after 5 min</translation>
    </message>
    <message>
        <source>Please enter your pin</source>
        <translation type="obsolete">Please enter your pin</translation>
    </message>
    <message>
        <source>Choose your 4-digit PIN</source>
        <translation>Choose your 4-digit PIN</translation>
    </message>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Forgot PIN?</source>
        <translation>Forgot PIN?</translation>
    </message>
    <message>
        <source>Reset PIN Confirmation</source>
        <translation>Reset PIN Confirmation</translation>
    </message>
    <message>
        <source>Are you sure to reset PIN?</source>
        <translation>Are you sure to reset PIN?</translation>
    </message>
    <message>
        <source>YES</source>
        <translation>YES</translation>
    </message>
    <message>
        <source>NO</source>
        <translation>NO</translation>
    </message>
    <message>
        <source>PINs don&apos;t match</source>
        <translation>PINs don&apos;t match</translation>
    </message>
    <message>
        <source>Please enter your PIN</source>
        <translation>Please enter your PIN</translation>
    </message>
    <message>
        <source>PIN must contain 4 digits</source>
        <translation>PIN must contain 4 digits</translation>
    </message>
</context>
<context>
    <name>SettingsScreen</name>
    <message>
        <source>AP Mode</source>
        <translation type="obsolete">AP Mode</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <source>QR Code</source>
        <translation type="obsolete">QR Code</translation>
    </message>
    <message>
        <source>Firmware upgrade</source>
        <translation type="obsolete">Firmware upgrade</translation>
    </message>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <source>Device Configuration</source>
        <translation>Device Configuration</translation>
    </message>
    <message>
        <source>User Linking</source>
        <translation>User Linking</translation>
    </message>
    <message>
        <source>Update PIN</source>
        <translation type="obsolete">Update PIN</translation>
    </message>
    <message>
        <source>Change PIN</source>
        <translation>Change PIN</translation>
    </message>
</context>
<context>
    <name>Settings_Alarms_Popup</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>SetupContentBackground</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Done</source>
        <translation>Done</translation>
    </message>
</context>
<context>
    <name>TempPage</name>
    <message>
        <source>°C</source>
        <translation>°C</translation>
    </message>
</context>
<context>
    <name>TimePage</name>
    <message>
        <source>Setpoint</source>
        <translation>Setpoint</translation>
    </message>
    <message>
        <source>min</source>
        <translation>min</translation>
    </message>
    <message>
        <source>Warm Alarm</source>
        <translation>Warm Alarm</translation>
    </message>
    <message>
        <source>Cold  Alarm</source>
        <translation>Cold  Alarm</translation>
    </message>
    <message>
        <source>Snooze Time Out</source>
        <translation>Snooze Time Out</translation>
    </message>
</context>
<context>
    <name>WifiFailScreen</name>
    <message>
        <source>Wi-Fi connection failed</source>
        <translation>Wi-Fi connection failed</translation>
    </message>
    <message>
        <source>Please check your Wi-Fi network credentials and try again</source>
        <translation>Please check your Wi-Fi network credentials and try again</translation>
    </message>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>Exit</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation>Retry</translation>
    </message>
</context>
<context>
    <name>WifiSuccessScreen</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Request QR Code</source>
        <translation>Request QR Code</translation>
    </message>
    <message>
        <source>Device configuration</source>
        <translation type="obsolete">Device configuration</translation>
    </message>
    <message>
        <source>Wi-Fi connection successful</source>
        <translation>Wi-Fi connection successful</translation>
    </message>
    <message>
        <source>Click Request QR Code to proceed to Device user linking </source>
        <translation type="obsolete">Click Request QR Code to proceed to Device user linking</translation>
    </message>
    <message>
        <source>Click &quot;Request QR Code&quot; to proceed to Device user linking </source>
        <translation>Click &quot;Request QR Code&quot; to proceed to Device user linking </translation>
    </message>
</context>
</TS>
