import QtQuick 2.0
import QtQuick.Controls 2.0
import "./Backgrounds"
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "./assets"


Rectangle {

    width: 480
    height: 320
    color: "#f6f7f7"

//    BusyIndicator {
//        anchors.centerIn: parent
//        running: deltaK.isSetBusy
//    }

    Image {
        id: busyImage
        x: 80
        y: 10
        width: 68
        height: 68
        source: "./assets/Images/spinner.png"
        anchors.centerIn: parent
        visible: deltaK.isSetBusy

        NumberAnimation on rotation {
            from: 0; to: 360; running: busyImage.visible === true;
            loops: Animation.Infinite; duration: 700;
        }
    }


    Image {
        id: wifiFailIcon
        x: 50
        y: 110
        width: 32
        height: 23
//        source: "../assets/Images/cancel_blue.png"
        source:"./assets/Images/Remove-Red.png"
    }

    FontLoader {
        id: robotoRegular
        source: "./assets/Fonts/roboto.regular.ttf"
    }

    Label {
        id: textMessage
        x: 90
        y: 100
        width: 320
        height: 44
        text: qsTr("Wi-Fi connection failed") + languageTranslator.translatedText
        wrapMode: Text.NoWrap
        color: "#33444f"        
        font.family: robotoRegular.name
        font.pixelSize: 18
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
    }

    Label {
        id: wifiFailedMessage
        x: 75
        y: 172
        width: 400
        height: 22
        color: "#33444f"
        text: qsTr("Please check your Wi-Fi network credentials and try again") + languageTranslator.translatedText
        wrapMode: Text.WordWrap
        font.pixelSize: 18        
        font.family: robotoRegular.name
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
    }


    Button {
        id: exitButton
        x: 155
        y: 260
        width: 170
        height: 50
        text: qsTr("")
        anchors.horizontalCenterOffset: -102
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        style: ButtonStyle {
            background: Rectangle {
                color: "#2ea2ec"
                radius: 5
            }
        }

         Text {
            id: exitButtonText
            x: 63
            y: 10
            width: 78
            height: 30
            color: "#ffffff"
            // color: "#2ea2ec"
            text: qsTr("Exit") + languageTranslator.translatedText
             verticalAlignment: Text.AlignVCenter
            font.family: robotoRegular.name
            font.pixelSize: 18
        }
         onClicked: {
             exitButton.enabled = false
             retryButton.enabled = false
             deltaK.completedDeviceConfig()
             mainStack.pop(StackView.Immediate)
             mainStack.push(homeScreen, StackView.Immediate)
             exitButton.enabled = true
             retryButton.enabled = true
         }
    }




    property int timerCnt: 0

    Timer {
        id: dlTimer
        running: true
        repeat: false
        interval: 5000
        triggeredOnStart: true
        onTriggered: {
            dlTimer.stop()
            delay(500, function() {
                //console.log('`` dlTimer..Cnt '+timerCnt)

                if(timerCnt>=1)
                {
                    deltaK.setSettingsMouseClickStatus(true);
                    mouseArea.enabled=false
                }

                timerCnt++
            })

        }
    }


    MouseArea {
        id: mouseArea
        y: 0
        anchors.rightMargin: 0
        anchors.bottomMargin: 2
        anchors.leftMargin: 0
        anchors.topMargin: -2
        anchors.fill: parent
        onClicked:   {

            dlTimer.start()
            mouse.accepted = deltaK.enableOTPButtonClick
        }
    }

    Timer {
        id: timer
    }

        Button {
            id: retryButton
            x: 151
            y: 260
            width: 170
            height: 50
            text: qsTr("")
            style: ButtonStyle {
                background: Rectangle {
                    color: "#2ea2ec"
                    radius: 5
                }
            }
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.horizontalCenterOffset: 107
            Text {
                id: retryButtonText
                x: 63
                y: 10
                width: 78
                height: 30
                color: "#ffffff"
                text: qsTr("Retry") + languageTranslator.translatedText
                font.pixelSize: 18
                font.family: robotoRegular.name
                verticalAlignment: Text.AlignVCenter
            }
            anchors.bottomMargin: 10

            onClicked:{
                exitButton.enabled = false
                retryButton.enabled = false
                deltaK.startApMode();
                mainStack.pop(StackView.Immediate)
                mainStack.push(apModeScreen, StackView.Immediate)
                retryButton.enabled = true
                exitButton.enabled = true
            }
        }

        function delay(delayTime, cb) {
            timer.interval = delayTime;
            timer.repeat = false;
            timer.triggered.connect(cb);
            timer.start();
        }



}
