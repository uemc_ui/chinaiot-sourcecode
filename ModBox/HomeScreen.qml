import QtQuick 2.4
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "./Backgrounds"
import "./assets"
import "./CustomComponents"

MainContentBackground {
    id: mainContentBackground
    width: 480
    height: 320
    property alias settingsButton: settingsButton
    property alias settingsImage: settingsImage
    property alias headerLable: headerLable
    property alias alarmScreen: alarmScreen
    property int temperature: temperature
    //greenImage: "./assets/Images/Home_Screen/Status_Icon_BG_New.png"
    greenImage: "./assets/Images/HealthIcon_Heart.png"
    header: deltaK.deviceName


    HeaderLabelMedium {
        id: headerLable
        x: 20
        y: 113
        width: 440
        height: 94
        color: "#ffffff"
        text: deltaK.temperature + "°C"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    Button {
        id: settingsButton
        x: 169
        y: 260
        width: 142
        height: 50
        text: qsTr("")
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        style: ButtonStyle {
            background: Rectangle {
                color: "#344550"
                radius: 5
            }
        }

        Image {
            id: settingsImage
            x: 10
            y: 10
            width: 36
            height: 30
            source: "assets/Images/Home_Screen/Settings_new.png"
        }


        Text {
            id: settingsText
            x: 56
            y: 10
            width: 78
            height: 30
            color: "#ffffff"
            text: qsTr("Settings") + languageTranslator.translatedText
            FontLoader {
                id: robotoRegular2
                source: "./assets/Fonts/roboto.regular.ttf"
            }
            verticalAlignment: Text.AlignVCenter
            font.family: robotoRegular2.name
            horizontalAlignment: Text.AlignLeft
            wrapMode: Text.WordWrap
            font.pixelSize: 18
        }

        onClicked: {
            settingsButton.enabled = false
            settingsButton.visible = false
            //deltaK.updateMetaData()

            settingsAccess.clearPin()
            mainStack.push(settingsAccess, StackView.Immediate)
            //mainStack.push(settingsScreen, StackView.Immediate)
            settingsButton.visible = true
            settingsButton.enabled = true


        }
    }

    Popup {
        id: popup
        //anchors.centerIn: parent
        x: 100
        y: 130
        width: 300
        height: 60
        visible: deltaK.isShowPopUp
        modal: true
        focus: true
            closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

            contentItem: Text {
                id: popUpItemText
                x: 56
                y: 10
                width: 78
                height: 30
                //color: "#ffffff"
//                text: qsTr("Firmware Update in Progress...\nDo not turnoff the unit") + languageTranslator.translatedText
//                text: qsTr("Update Completed\nUnit will be rebooted now...") + languageTranslator.translatedText
                  text: deltaK.popUpString
                FontLoader {
                    id: robotoRegular3
                    source: "./assets/Fonts/roboto.regular.ttf"
                }
                verticalAlignment: Text.AlignVCenter
                font.family: robotoRegular3.name
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
                font.pixelSize: 18
            }
//            BusyIndicator {
//                anchors.centerIn: parent
//                running: true
//            }

            Image {
                id: busyImage
                x: 80
                y: 10
                width: 68
                height: 68
                //source: "assets/Images/spinner.png"
                source: "assets/Images/spinner.png"
                anchors.centerIn: parent
                visible: true

                NumberAnimation on rotation {
                    from: 0; to: 360; running: busyImage.visible === true;
                    loops: Animation.Infinite; duration: 700;
                }
            }
        }

    function updateHeaderLable(value)
    {
        headerLable.text = value + "Â°C"
    }



    Text {
        id: degreesText
        x: 144
        y: 220
        width: 90
        height: 30
        color: "#2ea2ec"
        text: deltaK.referenceTemperature + " °C"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignRight
        wrapMode: Text.WordWrap
        FontLoader {
            id: robotoRegular
            source: "./assets/Fonts/roboto.regular.ttf"
        }
        font.family: robotoRegular.name
        font.pixelSize: 18

        //        Component.onCompleted: {
        //            text = deltaK.referenceTemperature + ".0°C"
        //        }
    }

    Text {
        id: setupText
        x: 236
        y: 220
        width: 78
        height: 30
        color: "#ffffff"
        text: qsTr("Setpoint") + languageTranslator.translatedText
        FontLoader {
            id: robotoRegular1
            source: "./assets/Fonts/roboto.regular.ttf"
        }
        font.family: robotoRegular1.name
        font.pixelSize: 18
        horizontalAlignment: Text.AlignLeft
        wrapMode: Text.WordWrap
        verticalAlignment: Text.AlignVCenter
    }

    /*Image {
        id: wifiImage
        x: 216
        y: 82
        width: 27
        height: 20
        visible: !deltaK.wifiWarning
        source: "./assets/Images/Home_Screen/Wifi_icon_new.png"

    }*/

    Image {
        id: ethernetImage
        x: 226
        y: 82
        width: 27
        height: 20
        visible: !deltaK.ethernetWarning
        source: "./assets/Images/Home_Screen/Ethernet.png"

    }

          //New logic to update wifi icon dynamically... need to get some clarifications so commenting now
    Image {
        id: wifiImage
        x: 213
        y: 67
        width: 50
        height: 50

        //source: "./assets/Images/wifi_icons/Full_Signal.png"
        //visible: !deltaK.wifiWarning //only wifi icon present condition
        visible: !deltaK.wifiWarning && deltaK.ethernetWarning

        Connections
        {
            target: deltaK
            onValueChanged:
            {
                //console.log("Received Wifi Percentage: "+deltaK.wifiPercentage)
                var percent = parseInt ( deltaK.wifiPercentage ) ;

                if ( percent > 20 && percent <= 40 )
                    wifiImage.source =  "./assets/Images/wifi_icons/Full_Signal.png";
                else if ( percent > 40 && percent <=50 )
                    wifiImage.source =  "./assets/Images/wifi_icons/Medium_Signal.png";
                else if ( percent > 50 && percent <=70 )
                    wifiImage.source =  "./assets/Images/wifi_icons/Low_Signal.png";
                else if ( percent > 70 )
                    wifiImage.source =  "./assets/Images/wifi_icons/Poor_Signal.png";
                else if ( percent <= 0 )
                    wifiImage.source =  "./assets/Images/wifi_icons/No_Wifi.png";


            }
        }
    }


//    Image {
//        id: powerImage
//        x: 252
//        y: 82
//        width: 16
//        height: 20
//        visible: !deltaK.powerWarning
//        source: "./assets/Images/Home_Screen/Power_icon_new.png"

//    }


    Image {
        id: wifiOffImage
        x: 226
        y: 82
        width: 20
        height: 20
        source: "../assets/Images/Alarm/No Wifi.png"
        //visible: deltaK.wifiWarning   //only wifi icon condition
        visible: deltaK.wifiWarning && deltaK.ethernetWarning
    }

    AlaramScreen {
        id: alarmScreen
        visible: deltaK.tempAlarm && !deltaK.isSnooze
    }

    WarningAlarmScreen{
        x: 0
        y: 74
        //visible: deltaK.batteryWarning || deltaK.wifiWarning || deltaK.powerWarning && !deltaK.tempAlarm
        //visible: false //need to finalize for this visibility
        visible: deltaK.yellowWarning
        messageText: deltaK.warningAlarmMessageText

    }

//    Rectangle {
//        id: powerSaveRect
//        x: 0
//        y: 0
//        width: 480
//        height: 320
//        color: "#000000"
//        visible: false
//    }
}
