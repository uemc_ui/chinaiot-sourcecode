#ifndef LANGUAGETRANSLATOR_H
#define LANGUAGETRANSLATOR_H

#include <QObject>

class LanguageTranslator : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString translatedText READ translatedText NOTIFY translatedTextChanged)
public:
    explicit LanguageTranslator(QObject *parent = 0);

    QString m_translatedText;
    QString translatedText() const;
    Q_INVOKABLE void translateToChinese();
    Q_INVOKABLE void translateToEnglish();

signals:
    void translatedTextChanged();

public slots:
};

#endif // LANGUAGETRANSLATOR_H
